function implantID = medelGetImplantID(this)
% telemetry options only work on left channel RIP2
implantID = -1;
%% read implant ID
marker  = 42;                                                        % tell RIB2.dll that this is no filename but already the stimulation sequence
cr      = 13;                                                        % carriage return
stimSeq = [marker 'Implanttype PULSAR' cr ...
    'Default nosync Pulsar Phase 70.0 Gap 0.0 P Ranges 0 0 0 0 0 0 0 0 0 0 0 0' cr ...
    cr ...
    'Acq 3000 Implantid' cr];

source  = libpointer('int8Ptr',[uint8(stimSeq) 0]);                  % get pointer to string with stimulation sequence
hStim   = calllib('RIB2', 'srLoadStimulationSequence', source, 0);   % send stimulation sequence to RIB2.dll and get handle
if hStim == 0
    showRIB2LoadErrors();
end

% add sequence to foreground queue left channel
retval = calllib('RIB2', 'srAddFgStimulation', hStim, 1, 0);

switch retval
    case 0
        % everything OK, no need to report
    case 3
        this.dispmessage('(srAddFgStimulation) error: insufficient memory.');
    case 4
        this.dispmessage('(srAddFgStimulation) error: sequence handle was invalid.');
    case 255
        this.dispmessage('(srAddFgStimulation) error: Need to check error queue...');
        showRIB2Errors();
    otherwise
        this.dispmessage(sprintf('(srAddFgStimulation) Unknown return value "%d"!',retval));
end

srACQ_IMPID = 4;
% make buffer to acquire implant ID from RIB2.dll
hAcqBuf = calllib('RIB2','srMakeAcquisitionBuffer',srACQ_IMPID, 1);  
if hAcqBuf == 0
    showRIB2Errors();
end

% queue aquisition buffer in RIB2.dll
retval = calllib('RIB2','srQueueAcquisitionBuffer',hAcqBuf);
switch retval
    case 0
        % everything OK, no need to report
    case 3
        this.dispmessage('(srQueueAcquisitionBuffer) error: insufficient memory.');
    case 4
        this.dispmessage('(srQueueAcquisitionBuffer) error: buffer handle was invalid.');
    case 255
        this.dispmessage('(srQueueAcquisitionBuffer) error: Need to check error queue...');
        showRIB2Errors();
    otherwise
        this.dispmessage(sprintf('(srQueueAcquisitionBuffer) Unknown return value "%d"!',box));
end

% execute left foreground queue, stop all other
calllib('RIB2', 'srSwitchStimulation', 2, 1, 1, 1);

% wait for maximal 10 ms for left queue to be played
retval = calllib('RIB2', 'srWaitUntilNoFg', 1, 0, 10);               
if (retval ~= 0)
    this.dispmessage('(srWaitUntilNoFg) Foreground sequences still queued and/or playing.');
end

% wait for maximal 100 ms for acquisition buffer to be filled
retval = calllib('RIB2', 'srWaitUntilAcquired', hAcqBuf, 100);
if (retval ~= 0)
    this.dispmessage('(srWaitUntilAcquired) error: Timeout while waiting for acquisition buffer to be filled.');
end

% decode implant ID from acquisition buffer
retval = calllib('RIB2', 'srGetImplantID', hAcqBuf);
if (retval == -1)
    this.dispmessage('(srGetImplantID) error while decoding.');
else
    this.dispmessage(sprintf('Implant ID: %d',retval));
    implantID= retval;
end

% turn channels of and power down implant
retval = calllib('RIB2', 'srStopStimulation');
if (retval ~= 0)
    this.dispmessage('(srStopStimulation) error: Timeout while stopping stimulation.');
end

% wait until output really stopped
retval = calllib('RIB2', 'srWaitUntilStopped', 0);
if (retval ~= 0)
    this.dispmessage('(srWaitUntilStopped) error: Timeout while stopping output.');
end

% release handle to sequence
retval = calllib('RIB2', 'srDiscardAcquisitionBuffer',hAcqBuf);
switch retval
    case 0
        % everything OK, no need to report
    case 4
        this.dispmessage('(srDiscardAcquisitionBuffer) error: Acquisition buffer does not exist.');
    case 5
        this.dispmessage('(srDiscardAcquisitionBuffer) error: Acquisition buffer in use!');
end % switch retval

% release handle to sequence
retval = calllib('RIB2', 'srDiscardStimulationSequence',hStim);      
switch retval
    case 0
        % everything OK, no need to report
    case 4
        this.dispmessage('(srDiscardStimulationSequence) error: Sequence does not exist.');
    case 5
        this.dispmessage('(srDiscardStimulationSequence) error: Sequence in use!');
end % switch retval