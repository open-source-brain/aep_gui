function [outSeq, s] = medelAM_Alt_Phase_BiphasicPulses(varargin)
s = parseparameters(varargin{:});
%s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'ImplantType', ''); %Implant type used
s = ef(s, 'ImplantID', -1); %Implant type used
s = ef(s, 'CI_Channel', 1); %CI_Channel to stimulate
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'TriggerDuration', 0.0005); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 0); %in current units (maximum 1200 for pulsar and 1737 for C40H)
s = ef(s, 'PhaseWidth', 30e-6); % in seconds
s = ef(s, 'InterPhaseGap', 1e-6); % in seconds
s = ef(s, 'AnodicFirst', true); % defines polarity of first phase
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierRate', 500); %pulses per seconds
s = ef(s, 'CarrierAltPhase', pi/2); % phase in rad
s = ef(s, 'CarrierNAltPhasePerModCycle', 1); % phase in rad
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', 24414.0625); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'SendTrigger', false);% if true, it creates a trigger line
s = ef(s, 'Description', '');

outSeq = {};

if s.OnlyReturnParameters;
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end;
s = rmfield(s,'OnlyReturnParameters');
s.Fs = 600000;%implant time clock
%wrap phases to +- pi
%% wrap phase to 2*pi
s.CarrierPhase = wrapTo2Pi(s.CarrierPhase);
s.CarrierAltPhase = wrapTo2Pi(s.CarrierAltPhase);

cCarrierRate = s.CarrierRate;
cModulationFrequency = s.ModulationFrequency;
[NAlt,DAlt] = rat(s.CarrierNAltPhasePerModCycle, 0.01);% this is to fit to a fraction
if s.RoundToCycle
    %% fit carrier rate to clock
    cCarrierRate = round2closestMultiple('IniValue', cCarrierRate, 'RefValue', s.Fs);
    %finf minimum windows that fit both carrier rate and recording device
    %clock
    [N1, ~] = rat(s.CycleRate/cCarrierRate, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % fit time window
    s.Duration = round2closestMultiple('IniValue', s.Duration, 'RefValue', cMinDuration);
    %fit modulation rate to full period time
    cModulationFrequency = (2 * DAlt) / NAlt / round2closestMultiple('IniValue', (2 * DAlt) / (cModulationFrequency * NAlt) , 'RefValue', s.Duration);    
    s.CarrierRate  = cCarrierRate;
    s.ModulationFrequency  = cModulationFrequency;
    %% round trigger to Fs
    s.TriggerDuration = ceil(s.TriggerDuration*s.Fs)/s.Fs;
end
cInterPulseInterval = 1 / cCarrierRate;
phaseAltPeriod = 2 / (cModulationFrequency * NAlt / DAlt);
phaseAltRate = 1 / phaseAltPeriod;
s.AltPhaseRate = phaseAltRate;
%ncycles = round(s.Duration*phaseAltRate);

%% fit carrier phase to device clock
s.CarrierPhase = s.CarrierRate * (2 * pi) * round2closestMultiple('IniValue', 1 / s.CarrierRate * s.CarrierPhase / (2 * pi), 'RefValue', s.Fs); 
if ~isequal(s.ModulationFrequency, 0)
    s.ModulationPhase = s.ModulationFrequency * (2 * pi) * round2closestMultiple('IniValue', 1 / s.ModulationFrequency * s.ModulationPhase / (2 * pi) , 'RefValue', s.Fs);
else
    s.ModulationPhase = 0;
end

%% creates string lines
cNumFormat = '%f';
cStepSize = 0;
cNSubPulseWord = '';
cDefalultGapword = '';
cStrNSubPulses = '';
cImplantIDWord = '';
cCompatibilityWord = '';
cDefaultSeqParWord = '';
cDefaultRangesWord = '';
cPerPulseRangesWord = '';
cPerPulsePhaseWord = '';
if s.AnodicFirst
    cPolarityWord = '+';
else
    cPolarityWord = '-';
end
switch s.ImplantType
    case {'C40H','C40P'}
        cDefaultDataStreamWord = 'LEGACY';
        cBiphasicWord = 'NEUROLEGACY';
        cCompatibilityWord = 'COMPATIBILITY';
        cMaxAmp = 1737;
        cDefaultPhaseWord = '';
        s.Amplitude = min(cMaxAmp, s.Amplitude);
        s.PhaseWidth = max(min(s.PhaseWidth, 850e-6), 53.3e-6);
        if (s.Amplitude <= 303)
            s.Range = 0;
            cStepSize = 303/127;%current unit step
        elseif (s.Amplitude <= 543)
            s.Range = 1;
            cStepSize = 543/127;%current unit step
        elseif (s.Amplitude <= 987)
            s.Range = 2;
            cStepSize = 987/127;%current unit step
        elseif (s.Amplitude <= 1737)
            s.Range = 3;
            cStepSize = 1737/127;%current unit step
        end
        switch s.ImplantType
            case 'C40H'
                %                 cDefaultRangesWord = strcat('RANGES','\t', num2str(s.Range*ones(1,8)));
                cPerPulseRangesWord = strcat('RANGE','\t', num2str(s.Range));
                cPerPulsePhaseWord = strcat('PHASE','\t', num2str(s.PhaseWidth * 1e6, cNumFormat));
            case 'C40P'
                %check possible electrodes
                if isempty(find([1,3,5,7,9,10,11,12] == s.CI_Channel, 1))
                    errordlg('Only electrodes 1, 3, 5, 7, 9, 10, 11, and 12 can be used');
                    return;
                end
                %                 cDefaultRangesWord = strcat('RANGES','\t', num2str(s.Range*ones(1,12)));
                cPerPulseRangesWord = strcat('RANGE','\t', num2str(s.Range));
                cPerPulsePhaseWord = strcat('PHASE','\t', num2str(s.PhaseWidth * 1e6, cNumFormat));
        end
        s.InterPhaseGap = 0;
    case {'PULSAR', 'SONATA', 'CONCERTO'}
        cBiphasicWord = 'BIPHASIC';
        cImplantIDWord = strcat('IMPLANTID', '\t', num2str(s.ImplantID));
        cDefaultSeqParWord = 'PARALLEL';
        cDefaultPhaseWord = strcat('PHASE','\t', num2str(s.PhaseWidth * 1e6, cNumFormat));
        cMaxAmp = 1200;
        s.PhaseWidth = max(min(s.PhaseWidth, 426.24e-6), 6.25e-6);
        s.Amplitude = min(cMaxAmp, s.Amplitude);
        if (s.Amplitude <= 150)
            s.Range = 0;
            cStepSize = 150/127;%current unit step
        elseif (s.Amplitude <= 300)
            s.Range = 1;
            cStepSize = 300/127;%current unit step
        elseif (s.Amplitude <= 600)
            s.Range = 2;
            cStepSize = 600/127;%current unit step
        elseif (s.Amplitude <= 1200)
            s.Range = 3;
            cStepSize = 1200/127;%current unit step
        end
        cDefaultDataStreamWord = 'PULSAR';
        cDefaultRangesWord = strcat('RANGES','\t', num2str(s.Range*ones(1,12)));
        cNSubPulseWord = strcat('NUMBER','\t','1');
        cIPG = max(0, min(s.InterPhaseGap, 30));
        if cIPG <= 9.97
            s.InterPhaseGap = 2.1e-6;
        elseif cIPG <= 19.97
            s.InterPhaseGap = 10e-6;
        else
            s.InterPhaseGap = 30e-6;
        end
        cDefalultGapword = strcat('GAP','\t', num2str(s.InterPhaseGap*1e6,cNumFormat));
        
    otherwise
        errordlg('Unknown implant');
        return;
end
s.Amplitude = round(s.Amplitude/cStepSize)*cStepSize;
%% round pulse width to clock
s.PhaseWidth = round2closestMultiple('IniValue', s.PhaseWidth , 'RefValue', s.Fs);
%% initialize sequence
progressbar('Generating signal');
nSequences = 2 + s.AlternateRepetitions;
for i = 1 : nSequences
    seq = {};
    if i == 1
        cDefaultForceWord = '';
        seq{end + 1} = strcat('IMPLANTTYPE','\t',s.ImplantType,'\n');
        seq{end + 1} = strcat('DEFAULTS','\t', ...
            cDefaultForceWord, '\t', ...
            cDefaultDataStreamWord ,'\t', ...
            cDefaultPhaseWord, '\t', ...
            cDefalultGapword, '\t', ...
            cDefaultSeqParWord, '\t', ...
            cDefaultRangesWord, '\t', ...
            cImplantIDWord, '\t', ...
            cCompatibilityWord,'\t', ...
            '\n');
        seq{end + 1} = strcat('COMPENSATE ON','\n');
    end
    cNpulsesPerEpoch = round(s.Duration * cCarrierRate);
    cCIAmplitude = round(s.Amplitude/cStepSize);
    %change polarity if requested
    cAltPolarityWord = cPolarityWord;
    if s.AlternateRepetitions && isequal(mod(i,2), 1)
        if s.AnodicFirst
            cAltPolarityWord = '-';
        else
            cAltPolarityWord = '+';
        end
    end
    cSwitchCount = 0;
    for j = 1 : cNpulsesPerEpoch
        progressbar(i/nSequences,j/cNpulsesPerEpoch);
        cTime = (j-1) * cInterPulseInterval;
        cAmplitude = round(cCIAmplitude /(1 + s.ModulationIndex)* (1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*cTime + ...
            s.ModulationPhase)));
        if (j == 1)
            cDistance = cInterPulseInterval * (s.CarrierPhase/(2*pi)) * 1e6;
            cDistance = round2closestMultiple('IniValue', cDistance, 'RefValue', 1 / s.Fs);
            % set the starting phase for starting sequence
            cDistanceWord = strcat('DISTANCE' ,'\t',num2str(cDistance, cNumFormat));
            if (i > 1)
                cDistance = cInterPulseInterval * (1 + (s.CarrierPhase - s.CarrierAltPhase) / (2*pi)) * 1e6;
                cDistance = round2closestMultiple('IniValue', cDistance, 'RefValue', 1 / s.Fs);
                cDistanceWord = strcat('DISTANCE' ,'\t', num2str(cDistance, cNumFormat));
            end
            if s.SendTrigger
                seq{end + 1} = strcat(cBiphasicWord, '\t', ...
                    'TRIGGER', '\t', ...
                    cDistanceWord , '\t', ...
                    cPerPulsePhaseWord, '\t', ...
                    cAltPolarityWord, '\t', ...
                    cNSubPulseWord, '\t', cStrNSubPulses, '\t', ...
                    'CHANNEL', '\t', num2str(s.CI_Channel), '\t', ...
                    'AMPLITUDE', '\t', num2str(cAmplitude), '\t', ...
                    cPerPulseRangesWord, '\t', ...
                    '\n' ...
                    );
            else
                 seq{end + 1} = strcat(cBiphasicWord, '\t', ...
                    cDistanceWord, '\t', ...
                    cPerPulsePhaseWord, '\t', ...
                    cAltPolarityWord, '\t', ...
                    cNSubPulseWord, '\t', cStrNSubPulses, '\t', ...
                    'CHANNEL', '\t', num2str(s.CI_Channel), '\t', ...
                    'AMPLITUDE', '\t', num2str(cAmplitude), '\t', ...
                    cPerPulseRangesWord, '\t', ...
                    '\n' ...
                    );
            end
        else
            if (round(cTime*1e6) >= round(phaseAltPeriod / 2 * (cSwitchCount + 1) * 1e6))%rounded to micro_sec scale
                cSwitch = true;
                cSwitchCount = cSwitchCount + 1;
            else
                cSwitch = false;
            end
            if cSwitch
                if mod(cSwitchCount,2) == 1 
                    cSwitchPhase = cInterPulseInterval * (1 + (s.CarrierAltPhase - s.CarrierPhase) / (2*pi)) * 1e6;
                else
                    cSwitchPhase = cInterPulseInterval * (1 + (s.CarrierPhase - s.CarrierAltPhase) / (2*pi)) * 1e6;
                end
                %round to device clock
                cSwitchPhase = round2closestMultiple('IniValue', cSwitchPhase, 'RefValue', 1 / s.Fs);
                seq{end + 1} = strcat(cBiphasicWord, '\t', ...
                    'DISTANCE' ,'\t', num2str(cSwitchPhase, cNumFormat), '\t', ...
                    cPerPulsePhaseWord, '\t', ...
                    cAltPolarityWord, '\t', ...
                    cNSubPulseWord, '\t', cStrNSubPulses, '\t', ...
                    'CHANNEL', '\t', num2str(s.CI_Channel), '\t', ...
                    'AMPLITUDE', '\t', num2str(cAmplitude), '\t', ...
                    cPerPulseRangesWord, '\t', ...
                    '\n' ...
                    );
            else
                cDistance = cInterPulseInterval * 1e6;
                % correct to fit with clock
                cDistance = round2closestMultiple('IniValue', cDistance, 'RefValue', 1 / s.Fs);
                seq{end + 1} = strcat(cBiphasicWord, '\t', ...
                    'DISTANCE' ,'\t', num2str(cDistance, cNumFormat), '\t', ...
                    cPerPulsePhaseWord, '\t', ...
                    cAltPolarityWord, '\t', ...
                    cNSubPulseWord, '\t', cStrNSubPulses, '\t', ...
                    'CHANNEL', '\t', num2str(s.CI_Channel), '\t', ...
                    'AMPLITUDE', '\t', num2str(cAmplitude), '\t', ...
                    cPerPulseRangesWord, '\t', ...
                    '\n' ...
                    );
            end
        end
    end
    outSeq{i} = seq; %start sequence
end
outSeq = outSeq';
