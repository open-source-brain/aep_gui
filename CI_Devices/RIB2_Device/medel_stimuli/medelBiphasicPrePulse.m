function [outSeq, s] = medelBiphasicPrePulse(varargin)
s = parseparameters(varargin{:});
%s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'ImplantType', ''); %Implant type used
s = ef(s, 'ImplantID', -1); %Implant type used
s = ef(s, 'CI_Channel', 1); %CI_Channel to stimulate
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'TriggerDuration', 0.0005); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 0); %in current units (maximum 1200 for pulsar and 1737 for C40H)
s = ef(s, 'PhaseWidth', 30e-6); % in seconds
s = ef(s, 'InterPhaseGap', 1e-6); % in seconds
s = ef(s, 'AnodicFirst', true); % defines polarity of first phase
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierRate', 500); %pulses per seconds
s = ef(s, 'PPAmplitude', 0); %in current units (cannot exceed amplitude of main pulses)
s = ef(s, 'PPAnodicFirst', true); % defines polarity of first phase
s = ef(s, 'PPdelay', 50); % set offset of prepulses (in s)
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', 24414.0625); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'SendTrigger', false);% if true, it creates a trigger line
s = ef(s, 'Description', '');

outSeq = {};

if s.OnlyReturnParameters;
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end;
s = rmfield(s,'OnlyReturnParameters');
s.Fs = 600000;%implant time clock
cCarrierRate = s.CarrierRate;
if s.RoundToCycle
    %% fit carrier rate to clock
    cCarrierRate = round2closestMultiple('IniValue', cCarrierRate, 'RefValue', s.Fs);
    %finf minimum windows that fit both carrier rate and recording device
    %clock
    [N1, ~] = rat(s.CycleRate/cCarrierRate, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % fit time window
    s.Duration = round2closestMultiple('IniValue', s.Duration, 'RefValue', cMinDuration);
    s.CarrierRate  = cCarrierRate;
    %% round trigger to Fs
    s.TriggerDuration = ceil(s.TriggerDuration*s.Fs)/s.Fs;
end


%split timings to accomidate prepulse
cInterPulseInterval = 1/cCarrierRate;

cInterPulseIntervalA = round2closestMultiple(...
                    'IniValue',cInterPulseInterval/100*(100-s.PPdelay),...
                    'RefValue', 1 / s.Fs...
                    );
cInterPulseIntervalB = round2closestMultiple(...
                    'IniValue',cInterPulseInterval/100*s.PPdelay,...
                    'RefValue', 1 / s.Fs...
                    );


%% wrap phase to 2*pi
s.CarrierPhase = wrapTo2Pi(s.CarrierPhase);
%% fit carrier phase to device clock
s.CarrierPhase = s.CarrierRate * (2 * pi) * round2closestMultiple('IniValue', 1 / s.CarrierRate * s.CarrierPhase / (2 * pi), 'RefValue', s.Fs);



%% creates string lines
cNumFormat = '%f';
cStepSize = 0;
cNSubPulseWord = '';
cDefaultForceWord = '';
cDefalultGapword = '';
cStrNSubPulses = '';
cImplantIDWord = '';
cCompatibilityWord = '';
cDefaultSeqParWord = '';
cDefaultRangesWord = '';
cPerPulseRangesWord = '';
cPerPulsePhaseWord = '';
if s.AnodicFirst
    cPolarityWord = '+';
else
    cPolarityWord = '-';
end

if s.PPAnodicFirst
    cPPPolarityWord = '+';
else
    cPPPolarityWord = '-';
end

switch s.ImplantType
    case {'C40H','C40P'}
        cDefaultDataStreamWord = 'LEGACY';
        cDefaultForceWord = '';
        cBiphasicWord = 'NEUROLEGACY';
        cCompatibilityWord = 'COMPATIBILITY';
        cMaxAmp = 1737;
        cDefaultPhaseWord = '';
        s.Amplitude = min(cMaxAmp, s.Amplitude);
        s.PhaseWidth = max(min(s.PhaseWidth, 850e-6), 53.3e-6);
        if (s.Amplitude <= 303)
            s.Range = 0;
            cStepSize = 303/127;%current unit step
        elseif (s.Amplitude <= 543)
            s.Range = 1;
            cStepSize = 543/127;%current unit step
        elseif (s.Amplitude <= 987)
            s.Range = 2;
            cStepSize = 987/127;%current unit step
        elseif (s.Amplitude <= 1737)
            s.Range = 3;
            cStepSize = 1737/127;%current unit step
        end
        switch s.ImplantType
            case 'C40H'
                %                 cDefaultRangesWord = strcat('RANGES','\t', num2str(s.Range*ones(1,8)));
                cPerPulseRangesWord = strcat('RANGE','\t', num2str(s.Range));
                cPerPulsePhaseWord = strcat('PHASE','\t', num2str(s.PhaseWidth * 1e6, cNumFormat));
            case 'C40P'
                %check possible electrodes
                if isempty(find([1,3,5,7,9,10,11,12] == s.CI_Channel, 1))
                    errordlg('Only electrodes 1, 3, 5, 7, 9, 10, 11, and 12 can be used');
                    return;
                end
                %                 cDefaultRangesWord = strcat('RANGES','\t', num2str(s.Range*ones(1,12)));
                cPerPulseRangesWord = strcat('RANGE','\t', num2str(s.Range));
                cPerPulsePhaseWord = strcat('PHASE','\t', num2str(s.PhaseWidth * 1e6, cNumFormat));
        end
        s.InterPhaseGap = 0;
    case {'PULSAR', 'SONATA', 'CONCERTO'}
        cDefaultForceWord = '';
        cBiphasicWord = 'BIPHASIC';
        cImplantIDWord = strcat('IMPLANTID', '\t', num2str(s.ImplantID));
        cDefaultSeqParWord = 'PARALLEL';
        cDefaultPhaseWord = strcat('PHASE','\t', num2str(s.PhaseWidth * 1e6, cNumFormat));
        cMaxAmp = 1200;
        s.PhaseWidth = max(min(s.PhaseWidth, 426.24e-6), 6.25e-6);
        s.Amplitude = min(cMaxAmp, s.Amplitude);
        if (s.Amplitude <= 150)
            s.Range = 0;
            cStepSize = 150/127;%current unit step
        elseif (s.Amplitude <= 300)
            s.Range = 1;
            cStepSize = 300/127;%current unit step
        elseif (s.Amplitude <= 600)
            s.Range = 2;
            cStepSize = 600/127;%current unit step
        elseif (s.Amplitude <= 1200)
            s.Range = 3;
            cStepSize = 1200/127;%current unit step
        end
        cDefaultDataStreamWord = 'PULSAR';
        cDefaultRangesWord = strcat('RANGES','\t', num2str(s.Range*ones(1,12)));
        cNSubPulseWord = strcat('NUMBER','\t','1');
        cIPG = max(0, min(s.InterPhaseGap, 30));
        if cIPG <= 9.97
            s.InterPhaseGap = 2.1e-6;
        elseif cIPG <= 19.97
            s.InterPhaseGap = 10e-6;
        else
            s.InterPhaseGap = 30e-6;
        end
        cDefalultGapword = strcat('GAP','\t', num2str(s.InterPhaseGap*1e6,cNumFormat));
        
    otherwise
        errordlg('Unknown implant');
        return;
end
s.Amplitude = round(s.Amplitude/cStepSize)*cStepSize;
%% round pulse width to clock
s.PhaseWidth = round2closestMultiple('IniValue', s.PhaseWidth , 'RefValue', s.Fs);
%% initialize sequence
progressbar('Generating signal');
nSequences = 2 + s.AlternateRepetitions;
for i = 1 : nSequences
    seq = {};
    if i == 1
        seq{end + 1} = strcat('IMPLANTTYPE','\t',s.ImplantType,'\n');
        seq{end + 1} = strcat('DEFAULTS','\t', ...
            cDefaultForceWord, '\t', ...
            cDefaultDataStreamWord ,'\t', ...
            cDefaultPhaseWord, '\t', ...
            cDefalultGapword, '\t', ...
            cDefaultSeqParWord, '\t', ...
            cDefaultRangesWord, '\t', ...
            cImplantIDWord, '\t', ...
            cCompatibilityWord,'\t', ...
            '\n');
        seq{end + 1} = strcat('COMPENSATE ON','\n');
    end
    cNpulsesPerEpoch = round(s.Duration * cCarrierRate);
    cCIAmplitude = round(s.Amplitude/cStepSize);
    cCIPPAmpplitude = round(s.PPAmplitude/cStepSize);
    %change polarity if requested
    cAltPolarityWord = cPolarityWord;
    if s.AlternateRepetitions && isequal(mod(i,2), 1)
        if s.AnodicFirst
            cAltPolarityWord = '-';
        else
            cAltPolarityWord = '+';
        end
    end
    
    cAltPPPolarityWord = cPPPolarityWord;
    if s.AlternateRepetitions && isequal(mod(i,2), 1)
        if s.PPAnodicFirst
            cAltPPPolarityWord = '-';
        else
            cAltPPPolarityWord = '+';
        end
    end
    
    for j = 1 : cNpulsesPerEpoch
        progressbar(i/nSequences,j/cNpulsesPerEpoch);
        cTime = (j-1) * cInterPulseIntervalA;
        cAmplitude = round(cCIAmplitude);
        if j == 1
            % the offset of the phase is only added to the first epoch
            % otherwise it will offset the sequence for each epcoch consecutively
            cIniDistance = cInterPulseIntervalA * ( (i ~= 1) + (i == 1) * s.CarrierPhase/(2*pi)) * 1e6;
            % correct to fit with clock
            cIniDistance = round2closestMultiple('IniValue', cIniDistance, 'RefValue', 1 / s.Fs);
            if s.SendTrigger
                seq{end + 1} = strcat(cBiphasicWord, '\t', ...
                    'TRIGGER', '\t', ...
                    'DISTANCE' , '\t', num2str(cIniDistance, cNumFormat), '\t', ...
                    cPerPulsePhaseWord, '\t', ...
                    cAltPPPolarityWord, '\t', ...
                    cNSubPulseWord, '\t', cStrNSubPulses, '\t', ...
                    'CHANNEL', '\t', num2str(s.CI_Channel), '\t', ...
                    'AMPLITUDE', '\t', num2str(cCIPPAmpplitude), '\t', ...
                    cPerPulseRangesWord, '\t', ...
                    '\n' ...
                    );
            else
                seq{end + 1} = strcat(cBiphasicWord, '\t', ...
                    'DISTANCE' ,'\t',num2str(cIniDistance, cNumFormat), '\t', ...
                    cPerPulsePhaseWord, '\t', ...
                    cAltPPPolarityWord, '\t', ...
                    cNSubPulseWord, '\t', cStrNSubPulses, '\t', ...
                    'CHANNEL', '\t', num2str(s.CI_Channel), '\t', ...
                    'AMPLITUDE', '\t', num2str(cCIPPAmpplitude), '\t', ...
                    cPerPulseRangesWord, '\t', ...
                    '\n' ...
                    );
            end
        else
            % Make main pulse
            seq{end + 1} = strcat(cBiphasicWord, '\t', ...
                'DISTANCE' ,'\t', num2str(cInterPulseIntervalA * 1e6, cNumFormat), '\t', ...
                cPerPulsePhaseWord, '\t', ...
                cAltPolarityWord, '\t', ...
                cNSubPulseWord, '\t', cStrNSubPulses, '\t', ...
                'CHANNEL', '\t', num2str(s.CI_Channel), '\t', ...
                'AMPLITUDE', '\t', num2str(cAmplitude), '\t', ...
                cPerPulseRangesWord, '\t', ...
                '\n' ...
                );
            
            
            % Make pre-pulse
            seq{end + 1} = strcat(cBiphasicWord, '\t', ...
                'DISTANCE' ,'\t', num2str(cInterPulseIntervalB * 1e6, cNumFormat), '\t', ...
                cPerPulsePhaseWord, '\t', ...
                cAltPPPolarityWord, '\t', ...
                cNSubPulseWord, '\t', cStrNSubPulses, '\t', ...
                'CHANNEL', '\t', num2str(s.CI_Channel), '\t', ...
                'AMPLITUDE', '\t', num2str(cCIPPAmpplitude), '\t', ...
                cPerPulseRangesWord, '\t', ...
                '\n' ...
                );
            
            
        end
    end
    outSeq{i} = seq; %start sequence
end
s.PPDelayMs = cInterPulseIntervalB * 1e6;
outSeq = outSeq';
