%%
%clear all;
if exist('rib2', 'var')
    clear('rib2')
end
rib2 = RIB2Device;
rib2.setImplantType('PULSAR')
%% impedances
rib2.measureImpedances;
%% stimulation
s1.ImplantType  = rib2.ImplantType; 
s1.PhaseWidth = 50e-6;
s1.CarrierRate = 1000;
s1.ModulationFrequency = 125;
s2.ModulationPhase = 0;
s1.ModulationIndex = 1; 
s1.Amplitude = 1000;
s1.NRepetitions = 200;
s1.AnodicFirst = false;
s1.AlternateRepetitions = true;
s1.CarrierNAltPhasePerModCycle = 1/2;
s1.CarrierPhase = -pi/2;
s1.CarrierAltPhase = pi/2;
s1.SendTrigger = true;

s2.ImplantType  = rib2.ImplantType; 
s2.PhaseWidth = 50e-6;
s2.CarrierRate = 1000;
s2.ModulationFrequency = 125;
s2.ModulationPhase = 0;
s2.ModulationIndex = 1; 
s2.Amplitude = 1000;
s2.NRepetitions = 200;
s2.AnodicFirst = true;
s2.AlternateRepetitions = true;
s2.CarrierNAltPhasePerModCycle = 1/2;
s2.CarrierPhase = pi/2;
s2.CarrierAltPhase = -pi/2;
s2.SendTrigger = false;

%% clear previous sequences
rib2.clearStimulationSequences;
%% create sequences
[seq1, ~] = rib2.amAltPhaseBibphasicPulses(s1);
[seq2, ~] = rib2.amAltPhaseBibphasicPulses(s2);
rib2.saveAndLoadSequences('Ear','left', 'Sequence', seq1, 'NRepetitions', s1.NRepetitions);
rib2.saveAndLoadSequences('Ear','right', 'Sequence', seq2, 'NRepetitions', s2.NRepetitions);
%5 assign sequences

%% start stimulation
rib2.startStimulation;