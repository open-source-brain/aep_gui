function [outSeq, s] = externalCICalibrationSignal(varargin)
s = parseparameters(varargin{:});
%s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'ImplantType', ''); %Implant type used
s = ef(s, 'CI_Channel', 1); %CI_Channel to stimulate
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'TriggerDuration', 0.0005); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 0); %in current units (maximum 1200 for pulsar and 1737 for C40H)
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierRate', 500); %pulses per seconds
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', 24414.0625); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'SendTrigger', false);% if true, it creates a trigger line
s = ef(s, 'Description', '');

outSeq = {};

if s.OnlyReturnParameters;
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end;
s = rmfield(s,'OnlyReturnParameters');
s.Fs = 600000;%implant time clock
cCarrierRate = s.CarrierRate;
if s.RoundToCycle
    %% fit carrier rate to clock
    cCarrierRate = round2closestMultiple('IniValue', cCarrierRate, 'RefValue', s.Fs);
    %finf minimum windows that fit both carrier rate and recording device
    %clock
    [N1, ~] = rat(s.CycleRate/cCarrierRate, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % fit time window
    s.Duration = round2closestMultiple('IniValue', s.Duration, 'RefValue', cMinDuration);
end
outSeq = {};
