function [average, s] = assrAverager(varargin)
    s = parseparameters(varargin{:});
    s = ef(s, 'DataReader', []);
    s = ef(s, 'Triggers', []);
    s = ef(s, 'EpochBlocks',[]);% if given will contatenate this number of ephocs
    s = ef(s, 'NFFT', 4098); %if given it will estimate the number of epochs to concatenate to obtain a FFT with the closest amount of points
    s = ef(s, 'Channels',[1]);
    s = ef(s, 'VirtualChannels', [1,2]);
    s = ef(s, 'NTriggers', inf);
    s = ef(s, 'NSamplesSNR', 256);
    s = ef(s, 'TypeAverage', 3);
    s = ef(s, 'FFTAnalysis', true);
    s = ef(s, 'LowPass', s.DataReader.Fs/3);
    s = ef(s, 'HighPass', 2);
    s = ef(s, 'AnalysisWindow', []);
    s = ef(s, 'MinBlockSize', 5);
    s = ef(s, 'DoTriggerBlanking', false);
    s = ef(s, 'DoEletricalPulseBlancking', false);
    s = ef(s, 'PlotInterpolatedPulses', 1);
    s = ef(s, 'ReDetectPeaks', false);
    s = ef(s, 'Percentil', 0.9);

    nsamplesEpoch = min(diff(s.Triggers));
    %nsamplesEpoch = round(s.DataReader.Fs/s.DataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Rate);
    if isempty(s.EpochBlocks)
        s.EpochBlocks = max(round(s.NFFT/nsamplesEpoch), 1);
    end
    disp(strcat('Number of epochs concatenated: ', num2str(s.EpochBlocks), ...
        '/NFFT: ', num2str(nsamplesEpoch*s.EpochBlocks)));
    
    s.Channels = min(s.Channels, s.DataReader.NChannels);
    for i = 1 : numel(s.Channels)
        %% fetch data
        cCh = s.Channels(i);
        y(:,i) = s.DataReader.fetchDataFromChannels(cCh);
    end
    cStimuli = s.DataReader.MeasurementSettings.StimuliModule.Stimulus;
    if strcmp(class(cStimuli), 'struct')
        cStimuli = {cStimuli};
    end
    cTriggerChannel = s.DataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Parameters;
%     %% we removed the highpass filter befor  blanking tdt data
%     if s.DoEletricalPulseBlancking && isa(s.DataReader, 'TdtDataReader')
        %tdt has 2.2 hz filters in the high-stage and medusa press
        %since filtfilt filt filter twice we only need one time
%         %filter to have the effect of both filters
%         yorg = y;
%         [b1,a1] = butter(1,4/s.DataReader.Fs/2,'high');
%         [h,~] = impz(b1,a1, size(y,1), s.DataReader.Fs);
%         y = ifft(fft(y)./fft(h));
%         y = filtfilt(b,a,y);%a and b are inversed so that it is an inverted filter
%     end
    if s.DoEletricalPulseBlancking
        for i = 1 : numel(cStimuli)
            if isempty(find(ismember({'medelAM_Alt_Phase_BiphasicPulses','medelAMBiphasicPulses'}, ...
                    cStimuli{i}.Parameters.SignalType), 1)); 
                continue; 
            end;
            if s. PlotInterpolatedPulses && i == 1
                figure;
            end
            if cStimuli{i}.Parameters.Channel == cTriggerChannel.Channel
                cDelayTriggerOffset = 0;
            else
                cDelayTriggerOffset = cTriggerChannel.CarrierPhase/(2*pi) / cTriggerChannel.CarrierRate - ...
                    cStimuli{i}.Parameters.CarrierPhase/(2*pi) / cStimuli{i}.Parameters.CarrierRate;
            end
            switch cStimuli{i}.Parameters.SignalType
                case 'medelAM_Alt_Phase_BiphasicPulses'
                    pulsePositions = getMedelAM_Alt_Phase_BiphasicPulsePositions('Parameters', cStimuli{i}.Parameters, ...
                        'NEpochs', s.DataReader.MeasurementSettings.StimuliModule.NEpochs, ...
                        'TriggerPositions', s.Triggers - s.DataReader.TriggerDelay, ...
                        'Fs', s.DataReader.Fs, ...
                        'ExpectedSamplesPerTrigger', round(s.DataReader.Fs * s.DataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Parameters.Duration), ...
                        'TriggerOffsetCorrection', cDelayTriggerOffset);
                case 'medelAMBiphasicPulses'
                    pulsePositions = getMedelAMBiphasicPulses('Parameters', cStimuli{i}.Parameters, ...
                        'NEpochs', s.DataReader.MeasurementSettings.StimuliModule.NEpochs, ...
                        'TriggerPositions', s.Triggers - s.DataReader.TriggerDelay, ...
                        'Fs', s.DataReader.Fs, ...
                        'ExpectedSamplesPerTrigger', round(s.DataReader.Fs * s.DataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Parameters.Duration), ...
                        'TriggerOffsetCorrection', cDelayTriggerOffset);
            end
            
            pulseWidthSamples = round(s.DataReader.Fs * (2 * cStimuli{i}.Parameters.PhaseWidth + ...
                cStimuli{i}.Parameters.InterPhaseGap));
            yint = interpolatePulseSamples('Data', y, ...
                'SamplePositions', pulsePositions + round(pulseWidthSamples / 2), ...
                'HalfWidth', 4 * pulseWidthSamples);
            
            if s. PlotInterpolatedPulses
                cTrigTime = (s.Triggers - (s.Triggers(1) + s.DataReader.TriggerDelay)) / s.DataReader.Fs;
                cPulseTime = (pulsePositions - (s.Triggers(1))) / s.DataReader.Fs;
                hold on;
                plot(((0:numel(y(:,1)) -  1) - s.Triggers(1))/s.DataReader.Fs , y(:,1), 'k');
                hold on;
                plot(((0:numel(yint(:,1)) - 1) - s.Triggers(1))/s.DataReader.Fs , yint(:,1),'r');
                cColor = getColor(i);
                plot(cPulseTime, 0, 'o', 'Color', cColor, 'Markerfacecolor', cColor);
                plot(cTrigTime , 0, '^', 'Color', 'b', 'Markerfacecolor', 'b');
                hold off;
            end
            y = yint;
            s.Triggers = pulsePositions;
        end
    end
%     y = ifft(fft(y).*fft(h));
    %% trigger blanking
    if s.DoTriggerBlanking
        y = interpolatePulseSamples('Data', y, ...
            'SamplePositions', s.Triggers, ...
            'HalfWidth', round(s.DataReader.Fs*0.0005));
    end
    
%     if s.DoEletricalPulseBlancking && isa(s.DataReader, 'TdtDataReader')
%         %% after blanking we remove the dc again
%         y = filtfilt(bh,ah,y);%a and b are inversed so that it is an inverted filter
%     end
%     
    %% remove DC
    [B, A] = butter(2, [s.HighPass, s.LowPass]/ (s.DataReader.Fs/2)); %order = 2*N
    yfil = filtfilt(B,A,y);
%     yfil = y;
    %% check that all triggers plus epoch length have the right size
    outTrigger = find(s.Triggers - s.DataReader.TriggerDelay + nsamplesEpoch > size(yfil,1), 1);
    if ~isempty(outTrigger)
        usedTriggers = s.Triggers(1:outTrigger - 1);
    else
        usedTriggers = s.Triggers;
    end
    %%
    usedTriggers = usedTriggers(1:min(s.NTriggers, numel(usedTriggers)));
    %% search and save epochs maximum
    [~,cFileName, cExtension] = fileparts(s.DataReader.FileLocation);
    if ~exist(strcat(cFileName,'.mat'), 'file') || s.ReDetectPeaks
        iniPos = zeros(numel(usedTriggers), 1);
        endPos = zeros(numel(usedTriggers), 1);
        maxVal = zeros(numel(usedTriggers), 1);
        for i = 1 : numel(s.Channels)
            for j = 1:numel(usedTriggers )
                iniPos(j) = usedTriggers (j) - s.DataReader.TriggerDelay;
                endPos(j) = usedTriggers (j) - s.DataReader.TriggerDelay + nsamplesEpoch - 1;
                cSweep = yfil(iniPos(j):endPos(j), i);
                maxVal(j,i) = max(cSweep);
            end
        end
        save(strcat(cFileName,'.mat'), 'maxVal');
    else
        load(strcat(cFileName,'.mat'),'maxVal')
    end
    %% keep epochs where maximum is within s.Percentil% quantile
%     cThresholds = min(quantile(maxVal,s.Percentil), max(maxVal));
    cThresholds = max(maxVal);
    for i = 1 : numel(s.Channels)
        cTriggers{i} = usedTriggers(maxVal(:,i) <= cThresholds(i));
    end
    disp(strcat('Tiggers Used:', num2str(numel(cTriggers{1})),'/', num2str(numel(s.Triggers))));
    %% compute frequencies of interest to perform statistical tests
    freqtest = calculateInterestFrequencies(s.DataReader);
    nVirtualChannels = size(s.VirtualChannels, 1);
    %% averaging the data
    average = JAverager.empty(numel(s.Channels) + nVirtualChannels,0);
    nsamplesPerSweep = nsamplesEpoch*s.EpochBlocks;
    if isempty(s.AnalysisWindow)
        cAnalysisWin = [0,nsamplesPerSweep/s.DataReader.Fs];
    else
        cAnalysisWin = s.AnalysisWindow;
    end
    for i = 1 : numel(s.Channels)
        nblocks = floor(numel(cTriggers{i})/s.EpochBlocks);
        average(i) = JAverager;
        average(i).Fs = s.DataReader.Fs;
        average(i).tPSNR = (0:nsamplesPerSweep/s.NSamplesSNR:(nsamplesPerSweep - 1))/s.DataReader.Fs;% we track s.NSamplesSNR points per block
        average(i).AnalysisWindow = cAnalysisWin;
        average(i).MinBlockSize = s.MinBlockSize;
        average(i).NSampTeoResNoise = round(nblocks/10);
        average(i).PlotSteps = false;
        average(i).Splits = 1;
        average(i).TypeAverage = s.TypeAverage;
        average(i).FFTAnalysis = s.FFTAnalysis;
        average(i).FFTFrequencies = freqtest;
        if isprop(s.DataReader, 'MeasurementSettings')
            cData = s.DataReader.MeasurementSettings;
        end
        [~, cData.FileName] = fileparts(s.DataReader.FileLocation);
        cData.EpochBlocks = s.EpochBlocks;
        cData.DoEletricalPulseBlancking = s.DoEletricalPulseBlancking;
        average(i).UserData = cData;
        average(i).UserData.Position = s.DataReader.MeasurementSettings.MeasurementModule.Channel{s.Channels(i)}.Position;
        
        for j = 1 : nblocks
            blockSamples = zeros(nsamplesPerSweep, 1);
            for k = 1:s.EpochBlocks
                iniPos = cTriggers{i}((j - 1) * s.EpochBlocks + k - 1 + 1) - s.DataReader.TriggerDelay;
                endPos = cTriggers{i}((j - 1) * s.EpochBlocks + k - 1 + 1) - s.DataReader.TriggerDelay + nsamplesEpoch - 1;
                blockSamples((k-1)*nsamplesEpoch + 1 : k * nsamplesEpoch) = iniPos : endPos;
            end
            cSweep = yfil(blockSamples, i);
            average(i).AddSweep(cSweep);
        end
%         average(i).FilterLP = 1000;
%         average(i).GetRePro = true;
%         average(i).UserData.freqFTest = freqFTest('FreqVect', average(i).Freq, ...
%             'FreqTest', freqtest, ...
%             'FFT', average(i).FFT);
    end
    for i = 1 : nVirtualChannels
        average(numel(s.Channels) + i) = JAverager;
        average(numel(s.Channels) + i).Fs = s.DataReader.Fs;
        average(i).tPSNR = (0:nsamplesPerSweep/s.NSamplesSNR:(nsamplesPerSweep - 1))/s.DataReader.Fs;% we track s.NSamplesSNR points per block
        average(i).AnalysisWindow = cAnalysisWin;
        average(i).MinBlockSize = s.MinBlockSize;
        average(i).NSampTeoResNoise = round(nblocks/10);
        average(numel(s.Channels) + i).PlotSteps = false;
        average(numel(s.Channels) + i).Splits = 1;
        average(numel(s.Channels) + i).TypeAverage = s.TypeAverage;
        average(numel(s.Channels) + i).FFTAnalysis = s.FFTAnalysis;
        average(numel(s.Channels) + i).FFTFrequencies = freqtest;
        if isprop(s.DataReader, 'MeasurementSettings')
            cData = s.DataReader.MeasurementSettings;
            cData.MeasurementModule.Channel{end + 1}.Position = ...
                strcat(s.DataReader.MeasurementSettings.MeasurementModule.Channel{s.VirtualChannels(i, 1)}.Position,'-', ...
                s.DataReader.MeasurementSettings.MeasurementModule.Channel{s.VirtualChannels(i, 2)}.Position);
            cData.MeasurementModule.Channel{end}.Number = numel(s.DataReader.MeasurementSettings.MeasurementModule.Channel);
            cData.EpochBlocks = s.EpochBlocks;
            average(numel(s.Channels) + i).UserData = cData;
        end
        refChannel = s.VirtualChannels(i,1);
        nblocks = floor(numel(cTriggers{i})/s.EpochBlocks);
        for j = 1 : nblocks
            blockSamples = zeros(nsamplesEpoch*s.EpochBlocks, 1);
            for k = 1:s.EpochBlocks
                iniPos = cTriggers{refChannel}((j - 1)*s.EpochBlocks + k - 1 + 1) - s.DataReader.TriggerDelay;
                endPos = cTriggers{refChannel}((j - 1)*s.EpochBlocks + k - 1 + 1) - s.DataReader.TriggerDelay + nsamplesEpoch - 1;
                blockSamples((k-1)*nsamplesEpoch + 1 : k*nsamplesEpoch) = iniPos : endPos;
            end
            cSweep1 = yfil(blockSamples, s.VirtualChannels(i, 1));
            cSweep2 = yfil(blockSamples, s.VirtualChannels(i, 2));
            average(numel(s.Channels) + i).AddSweep(cSweep1 - cSweep2);
        end
    end