function measurement = assrReader(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Channels', [1:16]);
s = ef(s, 'SaveFigures', true);
s = ef(s, 'CreateDataTable', true);
filetype = 'tdtdata';
%  filetype = 'hdf5';
ud = dir(strcat('*.',filetype));
dates = [ud.datenum];
[~, sdidx] = sort(dates,'descend');
d = ud(sdidx);
% if matlabpool('size') == 0 % checking to see if my pool is already open
%     matlabpool open 8
% end
measurement = {};
ds = [];
for i = 1:numel(d)
    cFilePath = GetFullPath(d(i).name);
    disp(cFilePath);
    [path, name, ext] = fileparts(cFilePath);
    [cDataReader, triggerPosClean] = getDataAndTriggers(cFilePath);
%     intTrigger = interpolateTriggerPulses('DataReader', cDataReader,'Triggers', triggerPosClean);
    if ~isempty(triggerPosClean)
        measurement{end + 1} = assrAverager('DataReader', cDataReader, ... 
            'Triggers', triggerPosClean, ...
            'NFFT', 1*cDataReader.Fs, ...
            'Channels', [s.Channels], ...
            'VirtualChannels', [], ...
            'NTriggers', inf, ...
            'ReDetectPeaks', true ...
            );
    end
end

%% plot results
plotMeasurements('Measurements', measurement, 'PlotPhase',false, 'PlotTime', true,'PlotSpectrum', true)
%% set aesthethics
h = ImageSetup;
h.I_KeepColor = 1;
h.I_TitleInAxis=1; 
% h.I_Xlim = [0, 140];
%h.I_Ylim = [0, 140];
h.I_Matrix = [min(size(measurement{1},2), 2), ceil(size(measurement{1},2) / 2)];
h.I_Space = [0.01, 0.01];
h.I_Width = 8*2 ;
h.I_High = 6*1; 
h.OptimizeSpace = 1;
h.prepareAllFigures; 
h.prepareAllFigures; 
%h.fitImages;
h.OptimizeSpace = 0;
h.prepareAllFigures; 
h.OptimizeSpace = 1;
h.prepareAllFigures; 
if s.SaveFigures
    h.saveImages('ObtainingNameFrom','name');
    % close all
end

%% create data tabke
if s.CreateDataTable
    assrTable('Measurements', measurement);
end