function assrTable(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Measurements', []);
out = [];
for i = 1:numel(s.Measurements)
    cMearuements = s.Measurements{i};
    for j = 1:numel(cMearuements)
        cPosition = cMearuements(j).UserData.MeasurementModule.Channel{j}.Position;
        if isequal(cPosition, 'none'); continue; end;
        cmeasureInfo = removeFields(cMearuements(j).UserData.MeasurementModule,{'Channel', 'TriggerChannel','Version'});
        simuliInfo = removeFields(cMearuements(j).UserData.StimuliModule, {'Stimulus'});
        simuliInfo.Position = cPosition;
        cStimulus = cMearuements(j).UserData.StimuliModule.Stimulus;
        if isstruct(cStimulus)
            cStimulus = {cStimulus};
        end
        for k = 1 : numel(cStimulus)
            cnames = fieldnames(cStimulus{k}.Parameters);
            for m = 1 : numel(cnames)
                simuliInfo.(strcat('Stimulus',num2str(k),'_', cnames{m})) = cStimulus{k}.Parameters.(cnames{m});
            end
        end
        infostruct = catstruct(cmeasureInfo, simuliInfo);
        hotellingTests = cMearuements(j).hotellingTSquareTest;
        for k = 1:numel(hotellingTests)
            %if hotellingTests(k).pValue >= 0.05; continue; end
            cdatacat = catstruct(infostruct, hotellingTests(k));
            out = [out, cdatacat];
        end
    end
end
tableGenerator('StructureParams', out)
