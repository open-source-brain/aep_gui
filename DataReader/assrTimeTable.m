function assrTimeTable(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Measurements', []);
out = [];
for i = 1:numel(s.Measurements)
    cMearuements = s.Measurements{i};
    for j = 1:numel(cMearuements)
        cPosition = cMearuements(j).UserData.MeasurementModule.Channel{j}.Position;
        if isequal(cPosition, 'none'); continue; end;
        cmeasureInfo = removeFields(cMearuements(j).UserData.MeasurementModule,{'Channel', 'TriggerChannel','Version'});
        simuliInfo = removeFields(cMearuements(j).UserData.StimuliModule, {'Stimulus'});
        simuliInfo.Position = cPosition;
        cStimulus = cMearuements(j).UserData.StimuliModule.Stimulus;
        for k = 1 : numel(cStimulus)
            cnames = fieldnames(cStimulus{k}.Parameters);
            for m = 1 : numel(cnames)
                simuliInfo.(strcat('Stimulus',num2str(k),'_', cnames{m})) = cStimulus{k}.Parameters.(cnames{m});
            end
        end
        infostruct = catstruct(cmeasureInfo, simuliInfo);
        [tAmpMax, posMax] = max(cMearuements(j).Average);
        [tAmpMin, posMin] = min(cMearuements(j).Average);
        cData.AmpMax = tAmpMax;
        cData.AmpMin = tAmpMin;
        cData.Amp = tAmpMax - tAmpMin;
        cData.tMax = cMearuements(j).Time(posMax);
        cData.tmin = cMearuements(j).Time(posMin);
        cData.RN = cMearuements(j).RN;
        cData.SNR = cMearuements(j).SNR;
        cData.TimeData =  cMearuements(j).Average;
        cData.TimeVector =  cMearuements(j).Time;
        cdatacat = catstruct(infostruct, cData);
        out = [out, cdatacat];
        
    end
end
tableGenerator('StructureParams', out)