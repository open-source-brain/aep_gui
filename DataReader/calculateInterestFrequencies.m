function [freqtest] = calculateInterestFrequencies(cDataReader)
freqtest = [];
if isprop(cDataReader, 'MeasurementSettings')
    stimuliData = cDataReader.MeasurementSettings.StimuliModule;
%     if isstruct(stimuliData)
%         stimuliData = {stimuliData};
%     end
    if isfield(stimuliData, 'Stimulus')
        cTriggerChannel = cDataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Parameters.Channel;
        for j = 1:numel(stimuliData.Stimulus)
            if isequal(cTriggerChannel,stimuliData.Stimulus{j}.Parameters.Channel); continue; end;
            switch stimuliData.Stimulus{j}.Parameters.SignalType
                case 'sinusoidal'
%                     freqtest(end + 1) = stimuliData.Stimulus{j}.CarrierFrequency;
                    nHarmonics = 3;
                    for nH = 1 : nHarmonics
                        freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.ModulationFrequency*nH;
                        freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.CarrierFrequency + stimuliData.Stimulus{j}.Parameters.ModulationFrequency*nH;
                        freqtest(end + 1) = max(stimuliData.Stimulus{j}.Parameters.CarrierFrequency - stimuliData.Stimulus{j}.Parameters.ModulationFrequency*nH, 0);
                    end
                    %% carrier-intermodulations
%                     [~, D1] = rat(stimuliData.Stimulus{j}.Parameters.ModulationFrequency/stimuliData.Stimulus{j}.Parameters.CarrierFrequency, 1e-12);
%                     freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.CarrierFrequency/D1;
%                     freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.ModulationFrequency + stimuliData.Stimulus{j}.Parameters.CarrierFrequency/D1;
%                     freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.ModulationFrequency - stimuliData.Stimulus{j}.Parameters.CarrierFrequency/D1;
                    
                case 'clicks'
                    freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.Rate;
                case 'sin_alt_phase'
%                     freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.CarrierFrequency;
                    %% modulation frequencies 
                    nHarmonics = 1;
                    for nH = 1 : nHarmonics
                        freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.ModulationFrequency*nH;
                    end
                    %% carrier-inter modulations
%                     freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.CarrierFrequency + stimuliData.Stimulus{j}.Parameters.ModulationFrequency;
%                     freqtest(end + 1) = max(stimuliData.Stimulus{j}.Parameters.CarrierFrequency - stimuliData.Stimulus{j}.Parameters.ModulationFrequency, 0);
%                     [~,D1] = rat(stimuliData.Stimulus{j}.Parameters.ModulationFrequency/stimuliData.Stimulus{j}.Parameters.CarrierFrequency, 1e-12);
%                     freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.CarrierFrequency/D1;
%                     freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.ModulationFrequency + stimuliData.Stimulus{j}.Parameters.CarrierFrequency/D1;
%                     freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.ModulationFrequency - stimuliData.Stimulus{j}.Parameters.CarrierFrequency/D1;
                    %% alt rate and harmonics
                    nHarmonics = 5;
                    for nH = 1 : nHarmonics
%                         if ~isequal(round2(stimuliData.Stimulus{j}.Parameters.AltPhaseRate*nH,0.01), round2(stimuliData.Stimulus{j}.Parameters.ModulationFrequency,0.01))
                            freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.AltPhaseRate*nH;
%                         end
%                         freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.ModulationFrequency + stimuliData.Stimulus{j}.Parameters.AltPhaseRate*nH;
%                         freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.ModulationFrequency - stimuliData.Stimulus{j}.Parameters.AltPhaseRate*nH;
                    end
                case  'transposeToneAltPhase'
                    %                     freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.CarrierFrequency;
                    %% modulation frequencies
                    nHarmonics = 1;
                    for nH = 1 : nHarmonics
                        freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.ModulationFrequency*nH;
                    end
                    %% alt rate and harmonics
                    nHarmonics = 5;
                    for nH = 1 : nHarmonics
                        freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.AltPhaseRate*nH;
                    end
                    freqtest(end + 1) = stimuliData.Stimulus{j}.Parameters.TransposeFrequency;
            end
        end
    end
    freqtest = unique(freqtest);
    freqtest = freqtest(freqtest > 0);
    %% compute other possible frequency of interst
%     newFreqs = [];
%     for j = 1:numel(freqtest)
%         for k = 2:numel(freqtest)
%             freqDiff = abs(freqtest(j) - freqtest(k));
%             newFreqs = [newFreqs, freqDiff, ...
%                 freqtest(j) + freqDiff, freqtest(j) - freqDiff, ...
%                 freqtest(k) + freqDiff, freqtest(k) - freqDiff];
%         end
%     end
%     newFreqs = unique(newFreqs);
%     newFreqs = newFreqs(newFreqs>0);
%     freqtest = union(freqtest,newFreqs);
end