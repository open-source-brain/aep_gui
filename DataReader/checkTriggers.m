function [triggerPosClean,badtriggerPos,triggerCoding, cTriggerRate] = checkTriggers(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'DataReader', []);

cFs = s.DataReader.MeasurementSettings.RecordingModule.Fs;
cTriggerPos = s.DataReader.fetchTriggers;
cTriggerType = s.DataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Parameters.SignalType;
triggerPosClean = [];
badtriggerPos = [];
cTriggerRate = [];
switch cTriggerType
    case 'bic_clicks'
        cNEphocs = s.DataReader.MeasurementSettings.StimuliModule.NEpochs;
        cTriggerCoding = reshape(s.DataReader.MeasurementSettings.StimuliModule.Stimulus(1).Parameters.TriggerCoding, [], 1);
        disp(strcat('bic-clicks-expected triggers:', num2str(numel(cTriggerCoding)*cNEphocs), ...
            '-','Found:', num2str(numel(cTriggerPos))));
        triggerCoding = reshape(bsxfun(@times, cTriggerCoding, ones(numel(cTriggerCoding), cNEphocs)),[],1);
        triggerPosClean = cTriggerPos;
    otherwise
        switch cTriggerType
            case {'medelAM_Alt_Phase_BiphasicPulses', 'medelAMBiphasicPulses'}
                cTriggerRate = 1/s.DataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Parameters.Duration;
            otherwise
                cTriggerRate = s.DataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Parameters.Rate;
        end
        samplesperTrigger = round(1/cTriggerRate*cFs*0.9); %90% tolerance
        cposIn = 1;
        cposOut = 2;
        while cposOut <= numel(cTriggerPos)
            if cTriggerPos(cposOut) - cTriggerPos(cposIn) < samplesperTrigger
                badtriggerPos = [badtriggerPos, cTriggerPos(cposOut)];
                cposOut = cposOut + 1;
            elseif cposOut == numel(cTriggerPos)
                triggerPosClean = [triggerPosClean,cTriggerPos(cposIn),cTriggerPos(cposOut)];
                cposOut = cposOut + 1;
            else
                triggerPosClean = [triggerPosClean,cTriggerPos(cposIn)];
                cposIn = cposOut;
                cposOut = cposOut + 1;
            end
        end
        triggerCoding = ones(numel(triggerPosClean), 1);
end
% diff(triggerPosClean)
% numel(triggerPosClean)