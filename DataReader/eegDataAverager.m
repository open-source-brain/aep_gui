function [average] = eegDataAverager(varargin)
    s = parseparameters(varargin{:});
    s = ef(s, 'EEGData', []);
    s = ef(s, 'DataReader', []);
    s = ef(s, 'Triggers', []);
    s = ef(s, 'EpochBlocks',[]);% if given will contatenate this number of ephocs
    s = ef(s, 'NFFT', 4098); %if given it will estimate the number of epochs to concatenate to obtain a FFT with the closest amount of points
    s = ef(s, 'Channels',[1]);
    s = ef(s, 'VirtualChannels', [1,2]);
    s = ef(s, 'NTriggers', inf);
    s = ef(s, 'NSamplesSNR', 512);
    s = ef(s, 'TypeAverage', 3);
    s = ef(s, 'FFTAnalysis', true);
    s = ef(s, 'AnalysisWindow', []);
    s = ef(s, 'MinBlockSize', 5);
    s = ef(s, 'DoBlanking', false);
    s = ef(s, 'NSamplesPerEpoch', []);
    
    nsamplesEpoch = s.NSamplesPerEpoch;
    if isempty(s.NSamplesPerEpoch)
        nsamplesEpoch = min(diff(s.Triggers));
    end
    %nsamplesEpoch = round(s.DataReader.Fs/s.DataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Rate);
    if isempty(s.EpochBlocks)
        s.EpochBlocks = max(round(s.NFFT/nsamplesEpoch), 1);
    end
    disp(strcat('Number of epochs concatenated: ', num2str(s.EpochBlocks), ...
        '/NFFT: ', num2str(nsamplesEpoch*s.EpochBlocks)));
    
    s.Channels = min(s.Channels, s.DataReader.NChannels);
    
    
    %% check that all triggers plus epoch length have the right size
    outTrigger = find(s.Triggers - s.DataReader.TriggerDelay + nsamplesEpoch > size(s.EEGData,1), 1);
    if ~isempty(outTrigger)
        usedTriggers = s.Triggers(1:outTrigger - 1);
    else
        usedTriggers = s.Triggers;
    end
    %%
    usedTriggers = usedTriggers(1:min(s.NTriggers, numel(usedTriggers)));
    %% search and save epochs maximum
    [~,cFileName, cExtension] = fileparts(s.DataReader.FileLocation);
    if ~exist(strcat(cFileName,'.mat'), 'file')
        iniPos = zeros(numel(usedTriggers), 1);
        endPos = zeros(numel(usedTriggers), 1);
        maxVal = zeros(numel(usedTriggers), 1);
        for i = 1 : numel(s.Channels)
            for j = 1:numel(usedTriggers )
                iniPos(j) = usedTriggers (j) - s.DataReader.TriggerDelay;
                endPos(j) = usedTriggers (j) - s.DataReader.TriggerDelay + nsamplesEpoch - 1;
                cSweep = s.EEGData(iniPos(j):endPos(j), i);
                maxVal(j,i) = max(cSweep);
            end
        end
        save(strcat(cFileName,'.mat'), 'maxVal');
    else
        load(strcat(cFileName,'.mat'),'maxVal')
    end
    %% keep epochs where maximum is within 90% quantile
    cThresholds = quantile(maxVal,.9);
    for i = 1 : numel(s.Channels)
        cCh = s.Channels(i);
%         cTriggers{i} = usedTriggers(maxVal(:,cCh) <= cThresholds(cCh));
        cTriggers{i} = usedTriggers;
    end
    
    %% compute frequencies of interest to perform statistical tests
    freqtest = calculateInterestFrequencies(s.DataReader);
    nVirtualChannels = size(s.VirtualChannels, 1);
    %% averaging the data
    average = JAverager.empty(numel(s.Channels) + nVirtualChannels,0);
    nsamplesPerSweep = nsamplesEpoch*s.EpochBlocks;
    if isempty(s.AnalysisWindow)
        cAnalysisWin = [0,nsamplesPerSweep/s.DataReader.Fs];
    else
        cAnalysisWin = s.AnalysisWindow;
    end
    for i = 1 : numel(s.Channels)
        nblocks = floor(numel(cTriggers{i})/s.EpochBlocks);
        average(i) = JAverager;
        average(i).Fs = s.DataReader.Fs;
        average(i).tPSNR = (0:nsamplesPerSweep/s.NSamplesSNR:(nsamplesPerSweep - 1))/s.DataReader.Fs;% we track s.NSamplesSNR points per block
        average(i).AnalysisWindow = cAnalysisWin;
        average(i).MinBlockSize = s.MinBlockSize;
        average(i).NSampTeoResNoise = round(nblocks/10);
        average(i).PlotSteps = false;
        average(i).Splits = 1;
        average(i).TypeAverage = s.TypeAverage;
        average(i).FFTAnalysis = s.FFTAnalysis;
        average(i).FFTFrequencies = freqtest;
        if isprop(s.DataReader, 'MeasurementSettings')
            cData = s.DataReader.MeasurementSettings;
        end
        [~, cData.FileName] = fileparts(s.DataReader.FileLocation);
        average(i).UserData = cData;
        average(i).UserData.Position = s.DataReader.MeasurementSettings.MeasurementModule.Channel{s.Channels(i)}.Position;
        
        
        for j = 1 : nblocks
            blockSamples = zeros(nsamplesPerSweep, 1);
            for k = 1:s.EpochBlocks
                iniPos = cTriggers{i}((j - 1)*s.EpochBlocks + k - 1 + 1) - s.DataReader.TriggerDelay;
                endPos = cTriggers{i}((j - 1)*s.EpochBlocks + k - 1 + 1) - s.DataReader.TriggerDelay + nsamplesEpoch - 1;
                blockSamples((k-1)*nsamplesEpoch + 1 : k*nsamplesEpoch) = iniPos : endPos;
            end
            cSweep = s.EEGData(blockSamples, i);
            average(i).AddSweep(cSweep);
        end
    end
    for i = 1 : nVirtualChannels
        average(numel(s.Channels) + i) = JAverager;
        average(numel(s.Channels) + i).Fs = s.DataReader.Fs;
        average(i).tPSNR = (0:nsamplesPerSweep/s.NSamplesSNR:(nsamplesPerSweep - 1))/s.DataReader.Fs;% we track s.NSamplesSNR points per block
        average(i).AnalysisWindow = cAnalysisWin;
        average(i).MinBlockSize = s.MinBlockSize;
        average(i).NSampTeoResNoise = round(nblocks/10);
        average(numel(s.Channels) + i).PlotSteps = false;
        average(numel(s.Channels) + i).Splits = 1;
        average(numel(s.Channels) + i).TypeAverage = s.TypeAverage;
        average(numel(s.Channels) + i).FFTAnalysis = s.FFTAnalysis;
        average(numel(s.Channels) + i).FFTFrequencies = freqtest;
        if isprop(s.DataReader, 'MeasurementSettings')
            cData = s.DataReader.MeasurementSettings;
            cData.MeasurementModule.Channel{end + 1}.Position = ...
                strcat(s.DataReader.MeasurementSettings.MeasurementModule.Channel{s.VirtualChannels(i, 1)}.Position,'-', ...
                s.DataReader.MeasurementSettings.MeasurementModule.Channel{s.VirtualChannels(i, 2)}.Position);
            cData.MeasurementModule.Channel{end}.Number = numel(s.DataReader.MeasurementSettings.MeasurementModule.Channel);
            average(numel(s.Channels) + i).UserData = cData;
        end
        refChannel = s.VirtualChannels(i,1);
        nblocks = floor(numel(cTriggers{i})/s.EpochBlocks);
        for j = 1 : nblocks
            blockSamples = zeros(nsamplesEpoch*s.EpochBlocks, 1);
            for k = 1:s.EpochBlocks
                iniPos = cTriggers{refChannel}((j - 1)*s.EpochBlocks + k - 1 + 1) - s.DataReader.TriggerDelay;
                endPos = cTriggers{refChannel}((j - 1)*s.EpochBlocks + k - 1 + 1) - s.DataReader.TriggerDelay + nsamplesEpoch - 1;
                blockSamples((k-1)*nsamplesEpoch + 1 : k*nsamplesEpoch) = iniPos : endPos;
            end
            cSweep1 = s.EEGData(blockSamples, s.VirtualChannels(i, 1));
            cSweep2 = s.EEGData(blockSamples, s.VirtualChannels(i, 2));
            average(numel(s.Channels) + i).AddSweep(cSweep1 - cSweep2);
        end
    end