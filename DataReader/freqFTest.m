 function out = freqFTest(varargin)
        s = parseparameters(varargin{:});
        s = ef(s, 'FreqVect', []);
        s = ef(s, 'FreqTest', 0);
        s = ef(s, 'NoiseDeltaFreq', 3);
        s = ef(s, 'FFT');
        out = cell(numel(s.FreqTest), 1);
        for i = 1:numel(s.FreqTest)
            [~, fbin] = min(abs(s.FreqVect - s.FreqTest(i)));
            fbinl = find(s.FreqVect >= s.FreqTest(i) - s.NoiseDeltaFreq, 1);
            fbinh = 2*fbin - fbinl;
            noiseSqPower = sum(abs(s.FFT([fbinl:fbin - 1,fbin + 1: fbinh])).^2);
            binSqPower = abs(s.FFT(fbin)).^2;
            N = numel(fbinl : fbinh - 1);
            out{i}.BinSqPower = binSqPower;
            out{i}.BinPhase = angle(s.FFT(fbin));
            out{i}.NoiseSqPower = noiseSqPower;
            out{i}.FreqTest = s.FreqTest(i);
            out{i}.FreqNoiseLow = s.FreqVect(fbinl);
            out{i}.FreqNoiseHigh = s.FreqVect(fbinh);
            out{i}.F = N*binSqPower/noiseSqPower;
            out{i}.N = N;
            out{i}.pValue = fcdf(noiseSqPower/(N*binSqPower), 2*N, 2);
        end