function [average] = frequencyAverage(varargin)
    s = parseparameters(varargin{:});
    s = ef(s, 'DataReader', []);
    s = ef(s, 'Triggers', []);
    s = ef(s, 'EpochBlocks',225);
    y = s.DataReader.fetchDataFromChannels(1);
    StimuliSettings = s.DataReader.MeasurementSettings.StimuliModule;
    nsamplesEpoch = s.DataReader.Fs*StimuliSettings.Stimulus_1.Duration;
    nsamples = nsamplesEpoch*StimuliSettings.NEpochs;
    yfil = y - mean(y);
    average = JAverager;
    average.Fs = s.DataReader.Fs;
    average.tPSNR = [1:1:8]*1e-3;
    average.AnalysisWindow = [1,11]*1e-3;
    average.PlotSteps = false;
    average.Splits = 1;
    average.TypeAverage = 2;
%     average.MinBlockSize = 1;
    average.UserData = s.DataReader.MeasurementSettings;
%     average.MinBlockSize = 64;
    cbsize = nsamplesEpoch;
    nblocks = floor(numel(s.Triggers)/s.EpochBlocks);
    iniPos(1) = s.Triggers(1) - s.DataReader.TriggerDelay;
    endPos(1) = iniPos(1) + cbsize*s.EpochBlocks - 1;
    cSweep = yfil(iniPos(1):endPos(1));
    average.AddSweep(cSweep);
    for j = 2:nblocks
        iniPos(j) = endPos(j-1) + 1;
        endPos(j) = iniPos(j) + cbsize*s.EpochBlocks - 1;
        cSweep = yfil(iniPos(j):endPos(j));y1
        average.AddSweep(cSweep);
    end
    freqtest = [];
    stimuliData = average.UserData.StimuliModule;
    freqtest(end + 1) = stimuliData.Stimulus_2.CarrierFrequency;
    freqtest(end + 1) = stimuliData.Stimulus_2.ModulationFrequency;
    freqtest(end + 1) = 2*stimuliData.Stimulus_2.ModulationFrequency;
    freqtest(end + 1) = stimuliData.Stimulus_2.CarrierFrequency + stimuliData.Stimulus_2.ModulationFrequency;
    freqtest(end + 1) = max(stimuliData.Stimulus_2.CarrierFrequency - stimuliData.Stimulus_2.ModulationFrequency, 0);
    
    freqtest(end + 1) = stimuliData.Stimulus_3.CarrierFrequency;
    freqtest(end + 1) = stimuliData.Stimulus_3.ModulationFrequency;
    freqtest(end + 1) = 2*stimuliData.Stimulus_3.ModulationFrequency;
    freqtest(end + 1) = stimuliData.Stimulus_3.CarrierFrequency + stimuliData.Stimulus_3.ModulationFrequency;
    freqtest(end + 1) = max(stimuliData.Stimulus_3.CarrierFrequency - stimuliData.Stimulus_3.ModulationFrequency, 0);
    
    deltaFreq = abs(stimuliData.Stimulus_3.CarrierFrequency - stimuliData.Stimulus_2.CarrierFrequency);
    freqtest(end + 1) = deltaFreq;
    freqtest(end + 1) = deltaFreq*2;
    freqtest(end + 1) = abs(stimuliData.Stimulus_2.CarrierFrequency - deltaFreq);
    average.UserData.freqFTest = freqFTest('FreqVect', average.Freq, ...
        'FreqTest', freqtest, ...
        'FFT', average.FFT);