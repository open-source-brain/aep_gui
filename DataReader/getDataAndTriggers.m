function [cDataReader, triggerPosClean, triggerCoding] = getDataAndTriggers(filename)
[path, name, ext] = fileparts(filename);
    switch ext
        case '.tdtdata'
            
            % [triggerpos,triggerData] = findTriggerPos('triggerInput.data');
            % figure;
            % plot([triggerData]);
            %vline(triggerpos);
            % disp(['DetectedTriggers: ', num2str(numel(triggerPos))]);
            %%
%             triggerDetected = getDatafromFile(strcat(name,'.triggerdata'));
%             triggerPos = find(triggerDetected > 0);
            %% read eeg data
            cDataReader = TdtDataReader(strcat(name,'.tdtdata'));
            cDataReader.FileLocation = filename;
            cDataReader.Size = 10000;
            triggerPos = cDataReader.fetchTriggers;
            %% fix fake triggers
            [triggerPosClean, badpos, triggerCoding, triggerRate] = checkTriggers('DataReader',cDataReader);
        case '.hdf5'
            cDataReader = GtecDataReader(strcat(name,'.hdf5'));
            triggerPos = cDataReader.fetchTriggers;
            triggerPosClean = triggerPos;
            triggerCoding = ones(numel(triggerPos),1);
    end
%     disp(strcat('Number of Triggers: ', num2str(numel(triggerPos))));
    disp(strcat('Good Triggers: ', num2str(numel(triggerPosClean))));
    disp(strcat('Mean number of samples per trigger: ', num2str(mean(diff(triggerPosClean))), ...
        '/Min: ', num2str(min(diff(triggerPosClean))), ...
        '/Max: ', num2str(max(diff(triggerPosClean)))));
    nsamplesEpoch = round(cDataReader.Fs/triggerRate);
    disp(strcat('Expected number of samples per trigger: ', num2str(nsamplesEpoch)));
    