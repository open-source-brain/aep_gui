function dataout = gtecHdf5Reader(filename)
dataout = [];
att = hdf5info(filename);
if ~isfield(att, 'GroupHierarchy'); return; end
if ~isfield(att.GroupHierarchy, 'Groups'); return; end
groups = att.GroupHierarchy.Groups;
for i = 1:numel(groups)
    cDataSets = groups(i).Datasets;
    for j = 1 : numel(cDataSets)
        fieldName = regexprep(cDataSets(j).Name,'\/','\.');
        switch cDataSets(j).Datatype.Class
            case 'H5T_STRING'
                textData = h5read(filename,cDataSets(j).Name);
                %% xml data is converted to structure
                if ~isempty(regexp(textData{1},'xml version','match'))
                    fileID = fopen(strcat(tempdir,'temp.xml'), 'w');
                    fwrite(fileID, textData{:}, 'char');
                    fclose(fileID);
                    cData = xml2struct(strcat(tempdir,'temp.xml'));
%                     cMainFname = fieldnames(cData);
%                     cData = cData.(cMainFname{:});
                    %% convert xml fields to numeric
                    cData = convertxmlvalue2matvalue(cData);
                else
                    cData = textData;
                end
            otherwise
                cData = h5read(filename,cDataSets(j).Name);
        end
        eval(strcat('dataout',fieldName,'= cData;'));
    end
end
