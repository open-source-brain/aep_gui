function y = interpolatePulseSamples(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Data', []);
s = ef(s, 'SamplePositions', []);% central point (in sample) to interpolate
s = ef(s, 'HalfWidth', 100); %how many samples to interpolate from center (sample points)
s = ef(s, 'PlotInterpolation', false);
s = ef(s, 'UseMeanSamples2Interpolate', true);
s = ef(s, 'NSamples2MeanInterpolation', 5);
y = s.Data;
NSamples = size(y,1);
for  i = 1:numel(s.SamplePositions)
    inip = s.SamplePositions(i) - round(s.HalfWidth);
    endp = s.SamplePositions(i) + round(s.HalfWidth);
    if endp > size(y,1); break; end;
    if s.UseMeanSamples2Interpolate
        iniMeanVec = max(inip - s.NSamples2MeanInterpolation + 1, 1): inip;
        endMeanVec = endp : min(endp + s.NSamples2MeanInterpolation - 1, NSamples);
        intvect = [iniMeanVec,endMeanVec]';
        for j = 1 : size(y,2)
            [p,intS,mu] = polyfit(intvect,[y(iniMeanVec,j);y(endMeanVec,j)], 1);
            y(inip:endp,j) = polyval(p,inip:endp,intS,mu);
        end
        %y(inip:endp,:) = interp1([inip endp],[mean(y(iniMeanVec,:),1); ...
        %    mean(y(endMeanVec,:), 1)],inip:endp);
    else
        y(inip:endp,:) = interp1([inip endp],[y(inip,:); y(endp,:)],inip:endp);
    end
end 
if s.PlotInterpolation
    for i = 1 : size(y,2)
        figure;
        plot((1:size(y,1))', [s.Data(:,i), y(:,i)]);
        hold on;
        plot(s.SamplePositions - s.HalfWidth, zeros(numel(s.SamplePositions), 1), '^');
        plot(s.SamplePositions + s.HalfWidth, zeros(numel(s.SamplePositions), 1), 'v');
        hold off
    end
end