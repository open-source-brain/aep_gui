function intTrigger = interpolateTriggerPulses(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'DataReader', []);
s = ef(s, 'Triggers', []);
cStimuli = s.DataReader.MeasurementSettings.StimuliModule.Stimulus;
for j = 1 : numel(cStimuli)
    if isequal(cStimuli{j}.Parameters.SignalType, 'none'); continue; end;
    [N1, ~] = rat(cStimuli{j}.Parameters.CycleRate/cStimuli{j}.Parameters.Fs, 1e-12);
    cMinDuration = N1/cStimuli{j}.Parameters.CycleRate;
    switch cStimuli{j}.Parameters.SignalType
        case {'sin_alt_phase', 'transposeToneAltPhase', 'medelAM_Alt_Phase_BiphasicPulses'}
            nsamplesSwitch = round(s.DataReader.Fs/(cStimuli{j}.Parameters.AltPhaseRate*2));
            intTrigger = [];
            for  k = 1:numel(s.Triggers) - 1
                inip = s.Triggers(k);
                %endp = s.Triggers(k) + round((s.Triggers(k + 1) - s.Triggers(k) - 1)/nsamplesSwitch)*nsamplesSwitch - nsamplesSwitch;
                endp = s.Triggers(k + 1) - nsamplesSwitch;
                if mod((endp-inip + 1), nsamplesSwitch - 1) == 0
                    intTrigger = [intTrigger, inip : nsamplesSwitch - 1 : endp + 1];
                else
                    intTrigger = [intTrigger, inip : nsamplesSwitch - 1 : endp];
                end
            end
            break;
        case 'medelAMBiphasicPulses'
            intTrigger = s.Triggers;
%             nsamplesPerPulse = round(s.DataReader.Fs/(cStimuli{j}.Parameters.CarrierRate));
%             intTrigger = [];
%             for  k = 1:numel(s.Triggers) - 1
%                 inip = s.Triggers(k);
%                 %endp = s.Triggers(k) + round((s.Triggers(k + 1) - s.Triggers(k) - 1)/nsamplesSwitch)*nsamplesSwitch - nsamplesSwitch;
%                 endp = s.Triggers(k + 1) - nsamplesPerPulse;
%                 if mod((endp-inip + 1), nsamplesPerPulse - 1) == 0
%                     intTrigger = [intTrigger, inip : nsamplesPerPulse - 1 : endp + 1];
%                 else
%                     intTrigger = [intTrigger, inip : nsamplesPerPulse - 1 : endp];
%                 end
%             end
%             break;
    end
end