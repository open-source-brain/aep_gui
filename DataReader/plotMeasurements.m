function plotMeasurements(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Measurements', {});
s = ef(s, 'Test', 'Hotelling');
s = ef(s, 'PlotSpectrum', true);
s = ef(s, 'PlotPhase', true);
s = ef(s, 'PlotTime', true);
s = ef(s, 'PlotSinusoidal', true);
s = ef(s, 'XFreqLim', [0 200]);%in hz
s = ef(s, 'YFreqLim', [0 1000]);%in nV
s = ef(s, 'SkipNoneElectrodes', false);%in nV

nPlotRows = s.PlotSpectrum + s.PlotPhase + s.PlotTime;
for i = 1:numel(s.Measurements)
    cMearuements = s.Measurements{i};
    textlegend = {};
    postText = '';
    if isfield(cMearuements(1).UserData, 'DoEletricalPulseBlancking')
        if cMearuements(1).UserData.DoEletricalPulseBlancking
            postText = '-ElectBlanking';
        end
    end
    if ~s.PlotSpectrum
        figure('name',strcat(cMearuements(1).UserData.FileName, '-NCEp-', num2str(cMearuements(1).UserData.EpochBlocks),postText, '-time'));
    else
        figure('name',strcat(cMearuements(1).UserData.FileName, '-NCEp-', num2str(cMearuements(1).UserData.EpochBlocks),postText));
    end
    nPlotCols = 0;
    for j = 1:numel(cMearuements)
        if ~(s.SkipNoneElectrodes && isequal(cMearuements(j).UserData.Position, 'none'))
            nPlotCols = nPlotCols + 1;
        end
    end
    for j = 1:numel(cMearuements)
        if s.SkipNoneElectrodes && isequal(cMearuements(j).UserData.Position, 'none')
            continue;
        end
        cScale = 1e9;%nano
        subplot(nPlotRows, nPlotCols, j)
        stimuliData = cMearuements(j).UserData.StimuliModule;
        cTriggerChannel = cMearuements(j).UserData.MeasurementModule.TriggerChannel.Parameters.Channel;
        textlegend{j} = {strcat('SB:', cMearuements(j).UserData.MeasurementModule.Subject, '/Ch:', cMearuements(j).UserData.Position,'\n')};
        %textlegend{j} = [textlegend{j}; strcat('CD:', cMearuements(j).UserData.MeasurementModule.Condition)];
        cMFreq = [];
        cCFreq = [];
        cCPhase = [];
        cAltRate = [];
        cStimuli = stimuliData.Stimulus;
        for k = 1: numel(cStimuli)
            if isstruct(cStimuli)
                cStimuli = {cStimuli};
            end
            vlinePos = [];
%             if isequal(cTriggerChannel, cStimuli{k}.Parameters.Channel); continue; end;
            switch cStimuli{k}.Parameters.SignalType
                case 'sinusoidal'
                    textlegend{j} = [textlegend{j}; strcat('C', cStimuli{k}.Parameters.Channel, ':', num2str(cStimuli{k}.Parameters.CarrierFrequency))];
                    textlegend{j} = [textlegend{j}; strcat('/M',num2str(k), ':', num2str(cStimuli{k}.Parameters.ModulationFrequency))];
                    textlegend{j} = [textlegend{j}; strcat('/Amp',num2str(k), ':', num2str(cStimuli{k}.Parameters.Amplitude))];
                case 'medelAM_Alt_Phase_BiphasicPulses'
                    cCh = k;
                    cDescription = cStimuli{k}.Parameters.Description;
                    textlegend{j} = [textlegend{j}; cDescription(regexp(cDescription, '(?<![A-Z])[A-Z]{1,3}(?![A-Z])'))];
                    
                    cCFreq = round2(cStimuli{k}.Parameters.CarrierRate, 0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Fc', ':', num2str(cCFreq))];
                    
                    cPW = round2(cStimuli{k}.Parameters.PhaseWidth*1e6, 0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/PW', ':', num2str(cPW))];
                    
                    cAlterRept = round2(cStimuli{k}.Parameters.AlternateRepetitions,1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/AltRepetitions', ':', num2str(cAlterRept))];
                    
                    cMFreq = round2(cStimuli{k}.Parameters.ModulationFrequency,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('\n','/Fm', ':', num2str(cMFreq))];
                    
                    cCPhase = round2(cStimuli{k}.Parameters.CarrierPhase * 180 / pi,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Pc', ':', num2str(cCPhase))];
                    
                    cAltRate = round2(cStimuli{k}.Parameters.AltPhaseRate,0.1);
                    
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Fj', ':', num2str(cAltRate), '\n')];
                    
                    vlinePos = 0: 1/(cStimuli{k}.Parameters.AltPhaseRate*2):cMearuements(j).Time(end);
                    
                case 'medelAMBiphasicPulses'
                    cCh = k;
                    cDescription = cStimuli{k}.Parameters.Description;
                    textlegend{j} = [textlegend{j}; cDescription(regexp(cDescription, '(?<![A-Z])[A-Z]{1,3}(?![A-Z])'))];
                    %if ~isequal(cCFreq, cStimuli{k}.Parameters.CarrierFrequency)
                    cCFreq = round2(cStimuli{k}.Parameters.CarrierRate, 0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Fc', ':', num2str(cCFreq))];
                    %end
                    %if ~isequal(cMFreq, cStimuli{k}.Parameters.ModulationFrequency)
                    cMFreq = round2(cStimuli{k}.Parameters.ModulationFrequency,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Fm', ':', num2str(cMFreq))];
                    %end
                    %if ~isequal(cCPhase, cStimuli{k}.Parameters.CarrierPhase * 180 / pi)
                    cCPhase = round2(cStimuli{k}.Parameters.CarrierPhase * 180 / pi,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Pc', ':', num2str(cCPhase))];
                    %end
                case 'sin_alt_phase'
                    cCh = k;
                    cDescription = cStimuli{k}.Parameters.Description;
                    textlegend{j} = [textlegend{j}; cDescription(regexp(cDescription, '(?<![A-Z])[A-Z]{1,3}(?![A-Z])'))];
                    %if ~isequal(cCFreq, cStimuli{k}.Parameters.CarrierFrequency)
                    cCFreq = round2(cStimuli{k}.Parameters.CarrierFrequency, 0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Fc', ':', num2str(cCFreq))];
                    %end
                    %if ~isequal(cMFreq, cStimuli{k}.Parameters.ModulationFrequency)
                    cMFreq = round2(cStimuli{k}.Parameters.ModulationFrequency,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Fm', ':', num2str(cMFreq))];
                    %end
                    %if ~isequal(cCPhase, cStimuli{k}.Parameters.CarrierPhase * 180 / pi)
                    cCPhase = round2(cStimuli{k}.Parameters.CarrierPhase * 180 / pi,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Pc', ':', num2str(cCPhase))];
                    %end
                    %if ~isequal(cAltRate, cStimuli{k}.Parameters.AltPhaseRate)
                    cAltRate = round2(cStimuli{k}.Parameters.AltPhaseRate,0.1);
                    %textlegend{j} = [textlegend{j}; strcat('/CPAlt',num2str(k), ':', num2str(cStimuli{k}.Parameters.CarrierAltPhase * 180 / pi))];
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Fj', ':', num2str(cAltRate))];
                    %end
                    %textlegend{j} = [textlegend{j}; strcat('/Amp',num2str(k), ':', num2str(cStimuli{k}.Parameters.Amplitude))];
                    vlinePos = 0: 1/(cStimuli{k}.Parameters.AltPhaseRate*2):cMearuements(j).Time(end);
                case 'transposeToneAltPhase'
                    cCh = k;
                    cDescription = cStimuli{k}.Parameters.Description;
                    textlegend{j} = [textlegend{j}; cDescription(regexp(cDescription, '(?<![A-Z])[A-Z]{1,3}(?![A-Z])'))];
                    cCFreq = round2(cStimuli{k}.Parameters.CarrierFrequency, 0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('\n', '/Fc', ':', num2str(cCFreq))];
                    cMFreq = round2(cStimuli{k}.Parameters.ModulationFrequency,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Fm', ':', num2str(cMFreq))];
                    cTPhase = round2(cStimuli{k}.Parameters.TransposePhase * 180 / pi,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Pt', ':', num2str(cTPhase))];
                    cAltTPhase = round2(cStimuli{k}.Parameters.TransposeAltPhase * 180 / pi,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/PtAlt', ':', num2str(cAltTPhase))];
                    cAltRate = round2(cStimuli{k}.Parameters.AltPhaseRate,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/Fj', ':', num2str(cAltRate))];
                    cTransposeFrequency = round2(cStimuli{k}.Parameters.TransposeFrequency,0.1);
                    textlegend{j}{end} = [textlegend{j}{end}, strcat('/TF', ':', num2str(cTransposeFrequency))];
                    vlinePos = 0: 1/(cStimuli{k}.Parameters.AltPhaseRate*2):cMearuements(j).Time(end);
            end
        end
        if s.PlotSpectrum || s.PlotPhase
            switch s.Test
                case 'Hotelling'
                    ftests = cMearuements(j).hotellingTSquareTest;
                otherwise
                    ftests = cMearuements(j).fTestFrequency;
            end
            %         ftests = cMearuements(j).hotellingTSquareTest;
            %         ftests = cMearuements(j).hotellingTSquareCircTest;
            if s.PlotSpectrum
                plot(cMearuements(j).Freq, abs(cMearuements(j).FFT)*2/cMearuements(j).SamplesPerSweep*cScale);
                xlim(s.XFreqLim);
                ylim(s.YFreqLim);
                xlabel('Frequency [Hz]');
                ylabel('nV');
                title(sprintf(textlegend{j}{:}));
                %if isfield(cMearuements(j).UserData, 'StimuliModule')
                %stimuliData = cMearuements(j).UserData.StimuliModule;
                
                %%plot significant responses
                hold on;
                for k = 1 : numel(ftests)
                    if ftests(k).pValue < 0.05
                        cColor = getColor(k);
                        cMarkerFaceColor = cColor;
                    else
                        cColor = [0.95, 0.95, 0.95];
                        cMarkerFaceColor = 'none';
                    end
                    cMarker = getMarker(k);
                    hp = plot(ftests(k).FFTFrequencyTested,ftests(k).BinSqPower^0.5*cScale + 50, 'Marker', 'v', ...
                        'MarkerFaceColor',cMarkerFaceColor);
                    set(hp, 'Color', cColor);
                end
                hold off;
                xlim([0,500]);
            end
            if s.PlotPhase
                %%plot significant phases
                subplot(nPlotRows, nPlotCols, s.PlotSpectrum*nPlotCols + j);
                %                    title(textlegend{j});
                %plot magnitude
                for k = 1 : numel(ftests)
                    cColor = getColor(k);
                    cMarker = getMarker(k);
                    if ftests(k).pValue < 0.05
                        r1 = ftests(k).BinSqPower^0.5*cScale;
                        x1 = r1*cos(ftests(k).BinPhase);
                        y1 = r1*sin(ftests(k).BinPhase);
                        hpol = polar([ftests(k).BinPhase,ftests(k).BinPhase], [0, r1], '-');
                        %                             hpol = plot([0,x1],[0,y1], '-');
                        set(hpol, 'Color', cColor);
                        set(hpol, 'Marker', cMarker);
                        %plot noise
                        hold on
                        [xcord,ycord] = ellipse('ra', ftests(k).CIL(1)*cScale, ...
                            'rb', ftests(k).CIL(2)*cScale, ...
                            'ang', atan(ftests(k).CID(2)/ftests(k).CID(1)), ...
                            'x0', x1, 'y0', y1);
                        [theta, rho] = cart2pol(xcord,ycord);
                        hold on;
                        hpol = polar(theta, rho);
                        %                             hpol = plot(xcord,ycord);
                        set(hpol, 'Color', cColor);
                    end
                end
                
                %plot individual phases
                for k = 1 : numel(ftests)
                    cColor = getColor(k);
                    cMarker = getMarker(k);
                    if ftests(k).pValue < 0.05
                        %plot individual phases
                        RR = abs(cMearuements(j).TrackFreqBin{1}(:, k)) * cScale;
                        Theta = angle(cMearuements(j).TrackFreqBin{1}(:, k));
                        hpol = polar(Theta, RR, 'o');
                        set(hpol,'Color',[0.95,0.95,0.95]);
                        if isfield(ftests(k), 'FreqNoiseBins')
                            if ~isempty(ftests(k).FreqNoiseBins)
                                RR = abs(ftests(k).FreqNoiseBins) * cScale;
                                hpol = polar(angle(ftests(k).FreqNoiseBins), RR);
                                set(hpol, 'Color', cColor);
                                set(hpol, 'Marker', cMarker);
                            end
                        end
                    end
                end
                hold off;
            end
            %end
        end
        if s.PlotTime
            subplot(nPlotRows, nPlotCols, (nPlotRows-1)*nPlotCols + j)
            %%filter response to plot time domain
            %             cMearuements(j).FilterHP = 0.2;
            %             cMearuements(j).FilterLP = 80;
            %             cMearuements(j).GetRePro = true;
            polarity = 1;
            if isequal(cMearuements(j).UserData.MeasurementModule.RefElectrode,'cz')
                polarity = -1;
            end
            plot(cMearuements(j).Time, cMearuements(j).Average*cScale*polarity);
            xlim([0, cMearuements(j).Time(end)]);
            ylim([-1000000, 1000000]);
            cMearuements(j).GetRePro = false;
            xlabel('Time [s]');
            ylabel('nV');
            if ~isempty(vlinePos)
                vline(vlinePos,':r');
            end
            hline([-1,1]*cMearuements(j).RN*cScale,'-k');
            
            if s.PlotSinusoidal
                switch s.Test
                    case 'Hotelling'
                        ftests = cMearuements(j).hotellingTSquareTest;
                    otherwise
                        ftests = cMearuements(j).fTestFrequency;
                end
                count = 1;
                for k = 1 : numel(ftests)
                    if (ftests(k).FFTFrequencyTested == 2*cStimuli{2}.Parameters.AltPhaseRate) || ...
                            (ftests(k).FFTFrequencyTested == 4*cStimuli{2}.Parameters.AltPhaseRate)
                        if ftests(k).pValue < 0.05
                            cColorSig ='r';
                        else
                            cColorSig = [1, 1, 1]*0.4;
                        end
                        hold on
                        plot(cMearuements(j).Time, ftests(k).BinSqPower^0.5*cScale*cos(2*pi*ftests(k).FFTFrequencyTested*cMearuements(j).Time + ftests(k).BinPhase + pi), ...
                            'Color', cColorSig,...
                            'LineStyle', getLine(count+2));
                        hold off;
                        count = count + 1;
                    end
                end
                
            end
        end
        if ~s.PlotSpectrum
            title(textlegend{j});
        end
    end
end