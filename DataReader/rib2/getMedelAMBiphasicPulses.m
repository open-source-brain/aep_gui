function  pulsePositions = getMedelAMBiphasicPulses(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Parameters', []);
s = ef(s, 'NEpochs', []);
s = ef(s, 'TriggerPositions', []);
s = ef(s, 'Fs',  24414.0625);
s = ef(s, 'Trigger2FirstPulseDelay',  8.333e-6);
s = ef(s, 'ExpectedSamplesPerTrigger',  []);
s = ef(s, 'TriggerOffsetCorrection', 0); %in sec, used to compensate phase differences between trigger channel and other channles
s = ef(s, 'CompensateDrift', 0);
nSequences = 2;
cInterPulseInterval = 1 / s.Parameters.CarrierRate;
outSeq = {};
cNpulsesPerEpoch = round(s.Parameters.Duration * s.Parameters.CarrierRate);
if ~isempty(s.ExpectedSamplesPerTrigger)
    %cDrift = mean(diff(s.TriggerPositions)) - s.ExpectedSamplesPerTrigger;
    %cCompensationStep = round(s.ExpectedSamplesPerTrigger/cDrift)/s.Fs;
    cTriggerDistance = mean(diff(s.TriggerPositions));
    if cTriggerDistance > s.ExpectedSamplesPerTrigger
        cCompSlope = cTriggerDistance / s.ExpectedSamplesPerTrigger;
        cOffset = s.ExpectedSamplesPerTrigger;
        cTriggComp = cTriggerDistance;
    else
        cCompSlope = s.ExpectedSamplesPerTrigger / cTriggerDistance;
        cOffset = cTriggerDistance;
        cTriggComp = s.ExpectedSamplesPerTrigger;
    end
end
cPulsePositions = [];
for j = 1 : cNpulsesPerEpoch
    if j == 1
        % the offset of the phase is only added to the first epoch
        % otherwise it will offset the sequence for each epcoch consecutively
        % cPulsePositions(end + 1) = cInterPulseInterval * (i == 1) * s.Parameters.CarrierPhase/(2*pi);
        cPulsePositions(end + 1) = 0;
    else
        cPulsePositions(end + 1) = (j - 1) * cInterPulseInterval;
        %             cPulsePositions(end + 1) = (j - 1) * cInterPulseInterval  + cInterPulseInterval * s.Parameters.CarrierPhase/(2*pi);
    end
    %% compensate drift
    if s.CompensateDrift
        cPulsePositions(end) = cCompSlope*(cPulsePositions(end) - cOffset) + cTriggComp;
    end
end
outSeq{1} = ceil((s.Trigger2FirstPulseDelay + cPulsePositions - s.TriggerOffsetCorrection) * s.Fs); %1 start sequence, 2 steady sequence
pulsePositions = s.TriggerPositions(1) + outSeq{1};
for i = 2 : s.Parameters.NRepetitions * s.NEpochs
    if i > numel(s.TriggerPositions); break; end;
    pulsePositions = [pulsePositions, outSeq{1} + s.TriggerPositions(i)];
end