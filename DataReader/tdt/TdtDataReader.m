classdef TdtDataReader < hgsetget
    properties
        CurrentPosition;
        Fs;
        TriggerDelay = 41; %see documentation
        ScaleFactor = 1; %see documentation
    end
    properties(Dependent)
        DataType; %type of data contained in file
        FileLocation; %binary file containg data to read
        NChannels;%number of channels contained in file
        Size;%size of data to read per Channel
        IsBufferComplete;  %used to indicate whether full buffer was available
        MeasurementSettings; % shows measurement settings used during measurement
        
    end
    properties (GetAccess = private)
        cFileName = '';
        cCurrentPos = 0;
        cNChannels = 4;
        cFileIdEeg = -1;
        cFileIdTrigger = -1;
        cCurrentPosition = 0;
        cSize = 1024;
        cIsBufferComplete = false; %used to indicate whether full buffer was available
        cDataType = 'float';
        cVarSizeBytes = 4; %size in bytes of cDataType
        cTempPath = ''; %temporal folder to unzip data
        cData = [];
    end
    methods
        function this = TdtDataReader(fname)
            [~, name, ~] = fileparts(fname);
            this.cTempPath = strcat(tempdir,name,filesep);
            %% clean old files
            tempxml = dir(strcat(this.cTempPath,'*.xml'));
            for i = 1:numel(tempxml)
                delete(strcat(this.cTempPath, tempxml(i).name));
            end
%             if exist(this.cTempPath, 'dir') == 0
                unzip(fname, this.cTempPath);
%             end
            xmlFile = dir(strcat(this.cTempPath,'*.xml'));
            eegFile = dir(strcat(this.cTempPath,'*.eegdata'));
            triggerFile = dir(strcat(this.cTempPath,'*.triggerdata'));
            this.checkversion(strcat(this.cTempPath,xmlFile.name));
            this.cData = convertxmlvalue2matvalue(xml2struct(strcat(this.cTempPath,xmlFile.name)));
            this.ScaleFactor = this.cData.Measurement.RecordingModule.ScaleFactor;
            this.cFileIdEeg = fopen(strcat(this.cTempPath,eegFile.name), 'r');
            this.cFileIdTrigger = fopen(strcat(this.cTempPath,triggerFile.name), 'r');
            this.cNChannels = this.cData.Measurement.RecordingModule.NChannels;
        end
        function delete(this)
            this.closeFile;
        end
        function value = get.CurrentPosition(this)
            value = this.cCurrentPosition;
        end
        function value = get.Size(this)
            value = this.cSize;
        end
        function this = set.Size(this, value)
            this.cSize = value;
        end
        function value = get.NChannels(this)
            if isfield(this.cData.Measurement.RecordingModule, 'NChannels')
                value = this.cData.Measurement.RecordingModule.NChannels;
            else
                value = 4; %% this need to be deleted in the future.
            end
        end
        function value = get.FileLocation(this)
            value = this.cFileName;
        end
        function this = set.FileLocation(this, value)
            this.cFileName = value;
        end
        function value = get.IsBufferComplete (this)
            value = this.cIsBufferComplete ;
        end
        function this = set.IsBufferComplete (this, value)
            this.cIsBufferComplete  = value;
        end
        function value = get.DataType(this)
            value = this.cDataType;
        end
        function value = get.Fs(this)
            if isfield(this.cData.Measurement.RecordingModule, 'Fs')
                value = this.cData.Measurement.RecordingModule.Fs;
            else
                value =  24414.0625;%this need to be delete in the future;
            end
        end
        function value = get.MeasurementSettings(this)
            value = this.cData.Measurement;
        end
        function this = set.DataType(this, value)
            switch value
                case 'uint8'
                    this.cVarSizeBytes = 1;
                case 'uint16'
                    this.cVarSizeBytes = 2;
                case 'uint32'
                    this.cVarSizeBytes = 4;
                case 'uint64'
                    this.cVarSizeBytes = 8;
                case 'uchar'
                    this.cVarSizeBytes = 1;
                case 'unsigned char'
                    this.cVarSizeBytes = 1;
                case 'ushort'
                    this.cVarSizeBytes = 2;
                case 'int'
                    this.cVarSizeBytes = 4;
                case 'int8'
                    this.cVarSizeBytes = 1;
                case 'int16'
                    this.cVarSizeBytes = 2;
                case 'int32'
                    this.cVarSizeBytes = 4;
                case 'int64'
                    this.cVarSizeBytes = 8;
                case 'integer*1'
                    this.cVarSizeBytes = 1;
                case 'integer*2'
                    this.cVarSizeBytes = 2;
                case 'integer*4'
                    this.cVarSizeBytes = 4;
                case 'integer*8'
                    this.cVarSizeBytes = 8;
                case 'schar'
                    this.cVarSizeBytes = 1;
                case 'signed char';
                    this.cVarSizeBytes = 1;
                case 'short'
                    this.cVarSizeBytes = 2;
                case 'single'
                    this.cVarSizeBytes = 4;
                case 'double'
                    this.cVarSizeBytes = 8;
                case 'float'
                    this.cVarSizeBytes = 4;
                case 'float32'
                    this.cVarSizeBytes = 4;
                case 'float64'
                    this.cVarSizeBytes = 8;
                case 'real*4'
                    this.cVarSizeBytes = 4;
                case 'real*8'
                    this.cVarSizeBytes = 8;
                case 'char*1'
                    this.cVarSizeBytes = 1;
                otherwise
                    disp('unknown data type')
                    return;
            end
            this.cDataType = value;
        end
    end
    methods(Access = public)
        function [value, count] = fetchData(this)
            %fetch data
            value = [];
            if ~this.isEegFileOpen; return;end
            [value, count] = fread(this.cFileIdEeg,[this.cNChannels,this.cSize],this.cDataType);
            this.cIsBufferComplete = mod(count,this.cNChannels) == 0;
            this.cCurrentPosition = this.cCurrentPosition - mod(count,this.cNChannels) + count;
            %set position to to read remaining data on the next time
            fseek(this.cFileIdEeg,this.cCurrentPosition*this.cVarSizeBytes,-1);
            value = value(:,1:end - mod(count,this.cNChannels)>0)'*this.ScaleFactor;
            count = count - mod(count,this.cNChannels);
        end
        function [value] = fetchDataFromChannels(this,channels)
            if ~this.isEegFileOpen; return;end
            for i = 1:numel(channels)
                %we set the possition indicator offset to the bof
                fseek(this.cFileIdEeg, this.cVarSizeBytes*(channels(i) - 1), -1);
                value(:,i) = fread(this.cFileIdEeg, inf, ...
                    this.cDataType, this.cVarSizeBytes*(this.cNChannels - 1))*this.ScaleFactor;
            end
        end
        function value = fetchTriggers(this)
            %fetch data
            value = [];
            if ~this.isTriggerFileOpen; return;end
            frewind(this.cFileIdTrigger);
            value = find(fread(this.cFileIdTrigger,'float32') > 0);
        end
        
        function closeFile(this)
            status = fclose(this.cFileIdEeg);
            if status
                this.cFileIdEeg = -1;
            end
            status = fclose(this.cFileIdTrigger);
            if status
                this.cFileIdTrigger = -1;
            end
        end
        function value = isEegFileOpen(this)
            value = this.cFileIdEeg >= 3;
        end
        function value = isTriggerFileOpen(this)
            value = this.cFileIdTrigger >= 3;
        end
    end
    methods(Access = private)
        function checkversion(this, xmlfile)
            %check stimuliModule
            this.stimuliModuleVersionUpdate(xmlfile);
            this.measurementModuleVersionUpdate(xmlfile);
            this.recordingModuleVersionUpdate(xmlfile);
        end
        
        function value = getModuleVersion(~, moduleName, xmlfile)
            value = [];
            fid = fopen(xmlfile, 'r');
            exprIn = strcat('(<',moduleName,'>)');
            exprOut = strcat('(</',moduleName,'>)');
            inModuleSection = false; %indicates if we are iterating within the module section
            if ~isequal(fid, -1)
                tline = fgetl(fid);
                while ischar(tline)
                    [~, tokIn] = regexp(tline, exprIn, 'match', 'tokens');
                    if ~isempty(tokIn) %when find module looks for version
                        inModuleSection = true;
                    end
                    [~, tokOut] = regexp(tline, exprOut, 'match', 'tokens');
                    if ~isempty(tokOut)
                        inModuleSection = false;
                    end
                    
                    if inModuleSection
                        expr2 = '<Version>(.*?)</Version>';
                        [~,tok] = regexp(tline, expr2, 'match', 'tokens');
                        if ~isempty(tok)
                            value = str2double(tok{:});
                            break;
                        end
                    end
                    tline = fgetl(fid);
                end
                fclose(fid);
            end
        end
        
        function stimuliModuleVersionUpdate(this, xmlfile)
            cModule = 'StimuliModule';
            version = this.getModuleVersion(cModule, xmlfile);
            switch version
                case 1
                    fid = fopen(xmlfile, 'r');
                    if ~isequal(fid, -1)
                        [path,name] = fileparts(xmlfile);
                        fidnew = fopen(strcat(path,filesep,name,'-aux.xml'), 'w+');
                        tline = fgetl(fid);
                        inModuleSection = false; %indicates if we are iterating within the module section
                        exprIn = strcat('(<',cModule,'>)');
                        exprOut = strcat('(</',cModule,'>)');
                        while ischar(tline)
                            [~, tokIn] = regexp(tline, exprIn, 'match', 'tokens');
                            if ~isempty(tokIn) %when find module update version
                                inModuleSection = true;
                                fprintf(fidnew,'%s\n', tline);
                                tline = fgetl(fid);
                                nline = '<Version>2</Version>';
                                fprintf(fidnew,'%s\n', nline);
                                tline = fgetl(fid);
                                continue;
                            end
                            [~, tokOut] = regexp(tline, exprOut, 'match', 'tokens');
                            if ~isempty(tokOut)
                                inModuleSection = false;
                            end
                            
                            if inModuleSection
                                nline = regexprep(tline, 'Stimulus_[\d]','Stimulus');
                            else
                                nline = tline;
                            end
                            fprintf(fidnew,'%s\n', nline);
                            tline = fgetl(fid);
                        end
                        fclose(fidnew);
                        fclose(fid);
                        delete(xmlfile);
                        movefile(strcat(path,filesep,name,'-aux.xml'),xmlfile);
                    end
                    %evalaluate new version to keep updating
                    this.stimuliModuleVersionUpdate(xmlfile);
                case 2
                    fid = fopen(xmlfile, 'r');
                    if ~isequal(fid, -1)
                        [path,name] = fileparts(xmlfile);
                        fidnew = fopen(strcat(path,filesep,name,'-aux.xml'), 'w+');
                        tline = fgetl(fid);
                        inModuleSection = false; %indicates if we are iterating within the module section
                        exprIn = strcat('(<',cModule,'>)');
                        exprOut = strcat('(</',cModule,'>)');
                        while ischar(tline)
                            [~, tokIn] = regexp(tline, exprIn, 'match', 'tokens');
                            if ~isempty(tokIn) %when find module update version
                                inModuleSection = true;
                                fprintf(fidnew,'%s\n', tline);
                                tline = fgetl(fid);
                                nline = '<Version>3</Version>';
                                fprintf(fidnew,'%s\n', nline);
                                tline = fgetl(fid);
                                continue;
                            end
                            [~, tokOut] = regexp(tline, exprOut, 'match', 'tokens');
                            if ~isempty(tokOut)
                                inModuleSection = false;
                            end
                            
                            if inModuleSection
                                if isequal(strtrim(tline),'<Stimulus>')
                                    fprintf(fidnew,'%s\n', tline);
                                    fprintf(fidnew,'%s\n', '<Parameters>');
                                elseif isequal(strtrim(tline),'</Stimulus>')
                                    fprintf(fidnew, '%s\n', '</Parameters>');
                                    fprintf(fidnew,'%s\n', '<Calibration>');
                                    fprintf(fidnew,'%s\n', '<Name>nocalibration</Name>');
                                    fprintf(fidnew,'%s\n', '<CalibrationStimulus>');
                                    fprintf(fidnew,'%s\n', '<SignalType>none</SignalType>');
                                    fprintf(fidnew,'%s\n', '</CalibrationStimulus>');
                                    fprintf(fidnew,'%s\n', '<CalibrationFactor>1</CalibrationFactor>');
                                    fprintf(fidnew,'%s\n', '<CalibrationLevel>0</CalibrationLevel>');
                                    fprintf(fidnew,'%s\n', '<CalibrationLabel>Amplitude</CalibrationLabel>');
                                    fprintf(fidnew,'%s\n', '<Notes/>');
                                    fprintf(fidnew,'%s\n', '</Calibration>');
                                    fprintf(fidnew,'%s\n', tline);
                                else
                                    nline = tline;
                                    fprintf(fidnew,'%s\n', nline);
                                end
                            else
                                nline = tline;
                                fprintf(fidnew,'%s\n', nline);
                            end
                            tline = fgetl(fid);
                        end
                        fclose(fidnew);
                        fclose(fid);
                        delete(xmlfile);
                        movefile(strcat(path,filesep,name,'-aux.xml'),xmlfile);
                    end
                    %evalaluate new version to keep updating
                    this.stimuliModuleVersionUpdate(xmlfile);
            end
        end
        function measurementModuleVersionUpdate(this, xmlfile)
            cModule = 'MeasurementModule';
            version = this.getModuleVersion(cModule, xmlfile);
            if isempty(version)
                cModule = 'MeasurementModule';
                version = this.getModuleVersion(cModule, xmlfile);
            end
            switch version
                case 1
                    fid = fopen(xmlfile, 'r');
                    if ~isequal(fid, -1)
                        [path,name] = fileparts(xmlfile);
                        fidnew = fopen(strcat(path,filesep,name,'-aux.xml'), 'w+');
                        tline = fgetl(fid);
                        fprintf(fidnew,'%s\n', tline);
                        tline = fgetl(fid);
                        fprintf(fidnew,'%s\n', tline);
                        tline = fgetl(fid);%% skip firsts lines in porpouse
                        inModuleSection = false; %indicates if we are iterating within the module section
                        exprIn = strcat('(<',cModule,'>)');
                        exprOut = strcat('(</',cModule,'>)');
                        while ischar(tline)
                            [~, tokIn] = regexp(tline, exprIn, 'match', 'tokens');
                            if ~isempty(tokIn) %when find module update version
                                inModuleSection = true;
                                tline = '<MeasurementModule>';
                                fprintf(fidnew,'%s\n', tline);
                                tline = fgetl(fid);
                                nline = '<Version>2</Version>';
                                fprintf(fidnew,'%s\n', nline);
                                tline = fgetl(fid);
                                continue;
                            end
                            [~, tokOut] = regexp(tline, exprOut, 'match', 'tokens');
                            if ~isempty(tokOut)
                                inModuleSection = false;
                                if ~feof(fid)
                                    tline = '</MeasurementModule>';
                                end
                            end
                            
                            if inModuleSection
                                expr = '<Channel_(\d)>(.*?)</Channel_\d>';
                                [~,tok] = regexp(tline, expr, 'match', 'tokens');
                                if ~isempty(tok)
                                    fprintf(fidnew,'%s\n', '<Channel>');
                                    nline = strcat('<Position>',tok{:}{2},'</Position>');
                                    fprintf(fidnew,'%s\n', nline);
                                    nline = strcat('<Number>',tok{:}{1},'</Number>');
                                    fprintf(fidnew,'%s\n', nline);
                                    fprintf(fidnew,'%s\n', '</Channel>');
                                else
                                    fprintf(fidnew,'%s\n', tline);
                                end
                            else
                                fprintf(fidnew,'%s\n', tline);
                            end
                            tline = fgetl(fid);
                        end
                        fclose(fidnew);
                        fclose(fid);
                        delete(xmlfile);
                        movefile(strcat(path,filesep,name,'-aux.xml'),xmlfile);
                    end
                    %evalaluate new version to keep updating
                    this.stimuliModuleVersionUpdate(xmlfile);
                case 2
                    fid = fopen(xmlfile, 'r');
                    if ~isequal(fid, -1)
                        [path,name] = fileparts(xmlfile);
                        fidnew = fopen(strcat(path,filesep,name,'-aux.xml'), 'w+');
                        tline = fgetl(fid);
                        fprintf(fidnew,'%s\n', tline);
                        tline = fgetl(fid);
                        fprintf(fidnew,'%s\n', tline);
                        tline = fgetl(fid);%% skip firsts lines in porpouse
                        inModuleSection = false; %indicates if we are iterating within the module section
                        exprIn = strcat('(<',cModule,'>)');
                        exprOut = strcat('(</',cModule,'>)');
                        while ischar(tline)
                            [~, tokIn] = regexp(tline, exprIn, 'match', 'tokens');
                            if ~isempty(tokIn) %when find module update version
                                inModuleSection = true;
                                tline = '<MeasurementModule>';
                                fprintf(fidnew,'%s\n', tline);
                                tline = fgetl(fid);
                                nline = '<Version>3</Version>';
                                fprintf(fidnew,'%s\n', nline);
                                tline = fgetl(fid);
                                continue;
                            end
                            [~, tokOut] = regexp(tline, exprOut, 'match', 'tokens');
                            if ~isempty(tokOut)
                                inModuleSection = false;
                                if ~feof(fid)
                                    tline = '</MeasurementModule>';
                                end
                            end
                            
                            if inModuleSection
                                if isequal(strtrim(tline),'<TriggerChannel>')
                                    fprintf(fidnew,'%s\n', tline);
                                    fprintf(fidnew,'%s\n', '<Parameters>');
                                elseif isequal(strtrim(tline),'</TriggerChannel>')
                                    fprintf(fidnew, '%s\n', '</Parameters>');
                                    fprintf(fidnew,'%s\n', '<Calibration>');
                                    fprintf(fidnew,'%s\n', '<Name>nocalibration</Name>');
                                    fprintf(fidnew,'%s\n', '<CalibrationStimulus>');
                                    fprintf(fidnew,'%s\n', '<SignalType>none</SignalType>');
                                    fprintf(fidnew,'%s\n', '</CalibrationStimulus>');
                                    fprintf(fidnew,'%s\n', '<CalibrationFactor>1</CalibrationFactor>');
                                    fprintf(fidnew,'%s\n', '<CalibrationLevel>0</CalibrationLevel>');
                                    fprintf(fidnew,'%s\n', '<CalibrationLabel>Amplitude</CalibrationLabel>');
                                    fprintf(fidnew,'%s\n', '<Notes/>');
                                    fprintf(fidnew,'%s\n', '</Calibration>');
                                    fprintf(fidnew,'%s\n', tline);
                                else
                                    nline = tline;
                                    fprintf(fidnew,'%s\n', nline);
                                end
                            else
                                fprintf(fidnew,'%s\n', tline);
                            end
                            tline = fgetl(fid);
                        end
                        fclose(fidnew);
                        fclose(fid);
                        delete(xmlfile);
                        movefile(strcat(path,filesep,name,'-aux.xml'),xmlfile);
                    end
                    %evalaluate new version to keep updating
                    this.stimuliModuleVersionUpdate(xmlfile);
            end
        end
        function recordingModuleVersionUpdate(this, xmlfile)
            cModule = 'RecordingModule';
            version = this.getModuleVersion(cModule, xmlfile);
            if isempty(version)
                version = 1;
            end
            switch version
                case 1
                    fid = fopen(xmlfile, 'r');
                    if ~isequal(fid, -1)
                        [path,name] = fileparts(xmlfile);
                        fidnew = fopen(strcat(path,filesep,name,'-aux.xml'), 'w+');
                        tline = fgetl(fid);
                        fprintf(fidnew,'%s\n', tline);
                        tline = fgetl(fid);
                        fprintf(fidnew,'%s\n', tline);
                        tline = fgetl(fid);%% skip firsts lines in porpouse
                        inModuleSection = false; %indicates if we are iterating within the module section
                        exprIn = strcat('(<',cModule,'>)');
                        exprOut = strcat('(</',cModule,'>)');
                        while ischar(tline)
                            [~, tokIn] = regexp(tline, exprIn, 'match', 'tokens');
                            if ~isempty(tokIn) %when find module update version
                                inModuleSection = true;
                                fprintf(fidnew,'%s\n', tline);
                                tline = fgetl(fid);
                                nline = '<Version>2</Version>';
                                fprintf(fidnew,'%s\n', nline);
                                nline = '<ScaleFactor>0.05</ScaleFactor>';
                                fprintf(fidnew,'%s\n', nline);
                                tline = fgetl(fid);
                                continue;
                            end
                            [~, tokOut] = regexp(tline, exprOut, 'match', 'tokens');
                            if ~isempty(tokOut)
                                inModuleSection = false;
                            end
                            fprintf(fidnew,'%s\n', tline);
                            tline = fgetl(fid);
                        end
                        fclose(fidnew);
                        fclose(fid);
                        delete(xmlfile);
                        movefile(strcat(path,filesep,name,'-aux.xml'),xmlfile);
                    end
                    %evalaluate new version to keep updating
                    this.recordingModuleVersionUpdate(xmlfile);
            end
        end
    end
end
