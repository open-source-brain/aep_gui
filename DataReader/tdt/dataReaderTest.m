%% creates binary file
fname = 'testdata.tda';
fid = fopen(fname ,'w');
x=1.0:133.0;
fwrite(fid,x,'float');
fclose(fid);
%% creat data reader class
cDataReader = DataReader;
cDataReader.FileName = fname;
cDataReader.Size = 4;
cDataReader.openFile;
cDataReader.NChannels = 10;
%% get data from file
epochSize = 100;
count = 1;
recordedData = [];
while count>0
    [data, count] = cDataReader.FetchData;
    recordedData = [recordedData; data];
end
disp(recordedData);
%% close file
cDataReader.closeFile;
%% delete test file
delete(fname)