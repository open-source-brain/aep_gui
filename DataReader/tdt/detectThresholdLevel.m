function [trigThreshold] = detectThresholdLevel(triggerfilename)
triggerInput = getDatafromFile(triggerfilename);
%% we find a detection threshold
k = 2;%number of clausters
[IDX,C,~,~] = kmeans(triggerInput,k,'start',[0,max(triggerInput)]');
%% compute threshold
statsk = [];
for i = 1:k
   statsk(i).std = std((triggerInput(IDX==i)));
   statsk(i).mean = C(i);
   statsk(i).idx = i;
end
[~, idxmax] = max([statsk.mean]);
[~, idxmin] = min([statsk.mean]);
if ~(statsk(idxmax).mean - 3*statsk(idxmax).std > ... 
        statsk(idxmin).mean + 3*statsk(idxmin).std)
    disp('Could not detect a reliable threshold');
end
trigThreshold = statsk(idxmax).mean - 1*statsk(idxmax).std;