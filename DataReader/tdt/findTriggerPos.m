function [trigThreshold, triggerPos,triggerInput] = findTriggerPos(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'TriggerFileName', '');
s = ef(s, 'Data', []);
s = ef(s, 'Positive', true);%to detect only positive triggers
s = ef(s, 'Thr', 2); % number of sd for thresholds detection
s = ef(s, 'Plot', false);
if isempty(s.TriggerFileName)
    triggerInput = s.Data;
else
    triggerInput = getDatafromFile(s.TriggerFileName);
end
if s.Positive
    triggerInput(triggerInput <= 0) = 0;
else
    triggerInput(triggerInput >= 0) = 0;
end
% triggerInput  = triggerInput /max(triggerInput);
difftrigin = diff([0;triggerInput]);
%we will detect positive trantients only
difftrigin(difftrigin<0) = 0; 
maxval = max(difftrigin);
%% we find a detection threshold
k = 2;%number of clausters
[IDX,C,~,~] = kmeans(difftrigin,k,'start',[0,maxval]');
%% compute threshold
statsk = [];
for i = 1:k
   statsk(i).std = std((difftrigin(IDX==i)));
   statsk(i).mean = C(i);
   statsk(i).idx = i;
end
[~, idxmax] = max([statsk.mean]);
[~, idxmin] = min([statsk.mean]);
if ~(statsk(idxmax).mean - s.Thr*statsk(idxmax).std > ... 
        statsk(idxmin).mean + s.Thr*statsk(idxmin).std)
    disp('Could not detect a reliable threshold');
end
trigThreshold = statsk(idxmax).mean - s.Thr*statsk(idxmax).std;
%% detect trigger position
triggerPos = [];
maxk = -inf;
for i = 1:numel(difftrigin)-1
    if difftrigin(i)> trigThreshold 
        if difftrigin(i) >= maxk
            maxk = difftrigin(i);
            posk = i;
        end
        if difftrigin(i+1) <= trigThreshold 
           triggerPos = [triggerPos, posk];
           maxk = -inf;
        end
    end
end
if s.Plot
    figure;
    plot([triggerInput,difftrigin]);
    hold on
    hline(trigThreshold);
    plot(triggerPos,0,'ro','markersize',6,'MarkerFaceColor','r');
    hold off
end
