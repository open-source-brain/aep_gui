function [average] = timeAverageTest(varargin)
    s = parseparameters(varargin{:});
    s = ef(s, 'EpochBlocks',1);
    s = ef(s, 'Fs',24414.0625);
    s = ef(s, 'CycleRate', 48000);
    s = ef(s, 'CarrierFrequency', 500);
    s = ef(s, 'Duration', 1.4);
    s = ef(s, 'NEpochs', 225);
    s = ef(s, 'NRepetitions', 230);
    
    [y, ~, settings] = sinusoidal(s);
    disp(settings);
    yfil = y - mean(y);
    
    %% averaging the data
    average = JAverager;
    average.Fs = s.Fs;
    average.tPSNR = [1:1:8]*1e-3;
    average.AnalysisWindow = [1,11]*1e-3;
    average.PlotSteps = false;
    average.Splits = 1;
    average.TypeAverage = 2;
    nblocks = s.NEpochs;
    nSamplesEpochs = settings.Duration*settings.Fs;
    maxVal = zeros(s.NEpochs, 1);
    iniPos = zeros(s.NEpochs, 1);
    endPos = zeros(s.NEpochs, 1);
    dalay = 100;
    for j = 1:nblocks
        noise = 500*generateNoise(settings)*0;
        noise = noise(1:nSamplesEpochs);
        jitter = round(rand(1,1))*0;
%         jitter = floor(j/2);
        iniPos(j) = dalay + (j-1)*nSamplesEpochs + 1 + jitter;
        endPos(j) = iniPos(j) + nSamplesEpochs - 1;
        cSweep = yfil(iniPos(j) : endPos(j)) + noise;
        maxVal(j) = max(cSweep);
        average.AddSweep(cSweep);
    end
    freqtest = [];
    freqtest(end + 1) = settings.CarrierFrequency;
    freqtest(end + 1) = settings.ModulationFrequency;
    freqtest(end + 1) = 2*settings.ModulationFrequency;
    freqtest(end + 1) = settings.CarrierFrequency + settings.ModulationFrequency;
    freqtest(end + 1) = max(settings.CarrierFrequency - settings.ModulationFrequency, 0);
    
    ftests = freqFTest('FreqVect', average.Freq, ...
        'FreqTest', freqtest, ...
        'FFT', average.FFT);
    
    cScale = 1e9*2/average.SamplesPerSweep;%nano 
    figure;
    plot(average.Freq,abs(average.FFT)*cScale);
    hold on;
    
    for j = 1 : numel(ftests)
        if ftests{j}.pValue < 0.05
            cColor = 'r';
        else
            cColor = 'b';
        end
        plot(ftests{j}.FreqTest,ftests{j}.BinSqPower^0.5*cScale,['*', cColor]);
    end
    hold off;