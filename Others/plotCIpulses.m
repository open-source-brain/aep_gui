fs = 100000;
rate = 60;
pw = 1/rate/4 ;
phi = [-pi/2, pi/2];
Altphi = [pi/2, -pi/2];
nepochs = 2;
fm = 5;
edur = 4/fm;
ncyclesAlt = 2;
time = (0 : edur*fs - 1 )/fs;
npulsesperepoch = round(rate*edur);
modulationIndex = 1;
figure;
for i = 1 : 2
    cpulse = zeros(size(time));
    pulseAmp = (1 - modulationIndex*cos(2*pi*fm*(0 : npulsesperepoch*nepochs - 1)/rate))/(1 + modulationIndex);
    for j = 1 : npulsesperepoch*nepochs
        tdelay = -1/rate * phi(i)/(2*pi)*(-1)^((j - 1)/rate > ncyclesAlt/fm);
        cpulse = cpulse + pulseAmp(j)* (unitaryStep((j - 1)/rate - pw + tdelay - time ) - unitaryStep((j - 1)/rate + tdelay - time) - ...
            (unitaryStep((j - 1)/rate  + tdelay - 2*pw - time) - unitaryStep((j - 1)/rate  + tdelay - pw - time)));
    end
    hold on
    plot(time,cpulse, getColor(i))
    hold off
end
xlabel('Time [s]')
ylabel('Amplitude')
legend({'Left Ear', 'Rigth Ear'});

h = ImageSetup;
h.I_KeepColor = 1;
h.I_TitleInAxis=1; 
h.I_Grid = 'Off';
% h.I_Xlim = [0, 60];
h.I_Ylim = [-1.5, 1.5];
h.I_Matrix = [1,1]; 
h.I_Space = [0.01,0.01];
h.I_Width = 14 ;
h.I_High = 7; 
h.I_FontSize = 16;
h.OptimizeSpace = 1;
h.prepareAllFigures; 
h.OptimizeSpace = 0;
h.prepareAllFigures; 
h.OptimizeSpace = 1;
h.prepareAllFigures; 
vline(ncyclesAlt/fm);
