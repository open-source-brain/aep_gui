classdef ExternalRecordingDevice < handle
    properties
        DeviceName = '';%the name of the connected device
        NChannels = 1;
    end
    properties(Dependent)
        Fs;%get the sampling frequency of the device
    end
    properties (GetAccess = protected)
        cFs = 24414.0625;
    end
    methods
        %%contructor
        function this = ExternalRecordingDevice()
%             this.cDeviceName = deviceName;
        end
%         function value = get.DeviceName(this)
%             value = this.cDeviceName;
%         end
        function value = get.Fs(this)
            value = this.cFs;
        end
        function this = set.Fs(this,value)
            this.cFs = value;
        end
        function dispmessage(this,message)
            txMessage = strcat(this.DeviceName,'-',message);
            disp(txMessage);
            notify(this,'evNotifyStatus',MessageEventDataClass(txMessage));
        end
    end
end
