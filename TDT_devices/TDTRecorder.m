classdef TDTRecorder < TDTDevice
    properties
        RecordingTime = 0;
        ScaleFactor = 1/20; % compensates gain added by any head-stage such as RA16LI
        PermittedNRecCh;% possible recording channels
    end
    properties(Dependent)
        DataFileName;
        TriggersFileName;
        HPlot; %handle to plot; used to send and plot averaged data
        HPlotFFT; %handle to fft plot; used to send and plot averaged data
        PlotAverage; %if true, we will try to plot the average buffer
        PlotFFT; %if true, we will try to plot the fft of the average buffer
        AverageWindow; %duration in sec to show average 
        NChannels;
        PlotChannelNum = 1; %visualized channel
        InvertPlot; %if true it revert the plot on the y axes
    end
    properties (GetAccess = protected)
        cDataFileId = [];
        cTriggersFileId = [];
        cBufferSize = [];%buffer size per chanel
        cNChannels = 4;
        cPermittedNRecCh = [4, 8, 16];
        cTriggersFileName = '';
        cDataFileName = '';
        cPlotAverage = true;
        cPlotFFT = true;
        cPlotTimer = [];%Timer used to plot average buffer
        cTimerPlotPeriod = 2;%defines the period used by the timer in seconds
        cHPlot = [];
        cHPlotFFT = [];
        cBufferCounter = 0;
        cAverageWindow = 0;%duration in sec to show average 
        cTimeVector = [];
        cFrequencyVector = [];
        cSamplesAverage; %number of samples to read from averager
        cPlotChannelNum = 1;% visualized channel
        cInvertPlot = false;
    end
    properties (Constant = true, GetAccess = private)
        cTriggerBufferSize = 200000;% this value must have same value than .rcx file
    end
    methods
        function this = TDTRecorder(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'DeviceName', 'RX5');
            s = ef(s, 'DataFileName', strcat(tempdir,'eeg.eegdata'));
            s = ef(s, 'TriggerFileName', strcat(tempdir,'triggers.triggerdata'));%in seconds
            this = this@TDTDevice(s.DeviceName);
            this.cDataFileName = s.DataFileName;
            this.cTriggersFileName = s.TriggerFileName;
            this.cPlotTimer = timer;
        end
        function delete(this)
            delete(this.cPlotTimer);
        end
        function value = get.PlotAverage(this)
            value = this.cPlotAverage;
        end
        function set.PlotAverage(this, value)
            this.cPlotAverage = value;
        end
        function set.PlotFFT(this, value)
            this.cPlotFFT = value;
        end
        function value = get.HPlot(this)
            value = this.cHPlot;
        end
        function set.HPlot(this, value)
            this.cHPlot = value;
        end
        function set.NChannels(this, value)
            [~, pos] = min(abs(value - this.cPermittedNRecCh));
            this.cNChannels = this.cPermittedNRecCh(pos);
        end
        function value = get.InvertPlot(this)
            value = this.cInvertPlot;
        end
        function set.InvertPlot(this, value)
            this.cInvertPlot = value;
        end
        function set.HPlotFFT(this, value)
            this.cHPlotFFT = value;
        end
        function set.PlotChannelNum(this, value)
            this.cPlotChannelNum = min(value, this.cNChannels);
            if ~this.Connected; return; end;
            if this.setTagValue('SelectedPlotChannel',this.cPlotChannelNum)
                this.dispmessage(strcat('Visualising channel:', num2str(this.cPlotChannelNum)));
            else
                this.dispmessage('Could not change visualised channel');
            end
        end
        function value = get.AverageWindow(this)
            value = this.cAverageWindow;
        end
        function set.AverageWindow(this, value)
            this.cAverageWindow = value;
            this.cTimeVector = (0:floor(value*this.Fs)-3)*1/this.Fs;
            this.cSamplesAverage = floor(value*this.Fs)-2;
            % set frequency vector
            Nfft = numel(this.cTimeVector);
            if ~rem(Nfft, 2) %if even
                NUniqFreq = ceil(1 + Nfft/2);
            else %if odd
                NUniqFreq = ceil((Nfft+1)/2);
            end
            this.cFrequencyVector = (0 : NUniqFreq - 1) * this.Fs / numel(this.cTimeVector);
        end
        function setBufferSize(this)
            %we set the buffers on RPvds twice the trigger buffer perform
            %double buffering data reading
            if ~this.Connected; return; end;
            if this.setTagValue('ChBufferSize',this.cNChannels*this.cTriggerBufferSize)
                this.cBufferSize = this.cTriggerBufferSize/2;% set to half to perform double buffering reading
            else
                this.dispmessage('Could not set buffers');
            end
        end
        function value = getBufferSize(this)
            value = [];
            if ~this.Connected; return; end;
            value = this.cTriggerBufferSize/2;
        end
             
        function running(this, ~, ~)
           this.savedata;
        end
        function savedata(this)
             % Wait until first half of Buffer fills
            cIndex = this.Device.GetTagVal('OutChBufferIndex');
            halfEegBufSize = this.cBufferSize*this.cNChannels;
            while(cIndex < halfEegBufSize)
                cIndex = this.Device.GetTagVal('OutChBufferIndex');
            end
            % writes first half of the data to file
            fwrite(this.cDataFileId, this.Device.ReadTagV('EEGDataBuffer', ...
                0, halfEegBufSize),'float32');
            fwrite(this.cTriggersFileId, this.Device.ReadTagV('TriggerDataBuffer', ...
                0, this.cBufferSize),'float32');
            % Check to see if the data transfer rate is fast enough
            cIndex = this.Device.GetTagVal('OutChBufferIndex');
            if cIndex < halfEegBufSize 
                this.dispmessage('Transfer rate is too slow!!!');
            end
            
            %reads second segment
            cIndex = this.Device.GetTagVal('OutChBufferIndex');
            while (cIndex > halfEegBufSize)
                cIndex = this.Device.GetTagVal('OutChBufferIndex');
            end
            % writes second half of the data to file
            fwrite(this.cDataFileId, this.Device.ReadTagV('EEGDataBuffer', ...
                halfEegBufSize, halfEegBufSize),'float32');
            fwrite(this.cTriggersFileId, this.Device.ReadTagV('TriggerDataBuffer', ...
                this.cBufferSize, this.cBufferSize),'float32');
            % Check to see if the data transfer rate is fast enough
            cIndex = this.Device.GetTagVal('OutChBufferIndex');
            if cIndex > halfEegBufSize 
                this.dispmessage('Transfer rate is too slow!!!');
            end
%             this.dispmessage('Acquiring EEG Data');
            this.cBufferCounter = this.cBufferCounter + 1;
        end
        function saveRemainedData(this)
             % Wait until first half of Buffer fills
            cIndex = this.Device.GetTagVal('OutChBufferIndex');
            halfEegBufSize = this.cBufferSize*this.cNChannels;
            if (cIndex < halfEegBufSize)
                % writes first half of the data to file
                fwrite(this.cDataFileId, this.Device.ReadTagV('EEGDataBuffer', ...
                    0, cIndex ),'float32');
                cIndexTriger = this.Device.GetTagVal('TriggerBufferIndex');
                fwrite(this.cTriggersFileId, this.Device.ReadTagV('TriggerDataBuffer', ...
                    0, cIndexTriger),'float32');
            else
                %reads second segment
                cIndex = this.Device.GetTagVal('OutChBufferIndex');
                % writes second half of the data to file
                fwrite(this.cDataFileId, this.Device.ReadTagV('EEGDataBuffer', ...
                    halfEegBufSize, halfEegBufSize - cIndex),'float32');
                cIndexTriger = this.Device.GetTagVal('TriggerBufferIndex');
                fwrite(this.cTriggersFileId, this.Device.ReadTagV('TriggerDataBuffer', ...
                    this.cBufferSize, this.cBufferSize - cIndexTriger),'float32');
            end
        end
        function stopRecording(this)
            if this.IsRunning
                stop(this.cPlotTimer);
                this.stop;
                this.saveRemainedData;
                %%clean buffers
                this.cBufferCounter = 0;
                if this.RunTimer
                    fclose(this.cDataFileId);
                    fclose(this.cTriggersFileId);
                end
            end
        end
        function setTimerPeriod(this)
            if ~this.Connected || this.IsRunning; return; end;
            %time period is fixed to scheduele at a 1/4 period of buffer
            %filling (at fixedSpacing mode)
            this.TimerPeriod = this.cTriggerBufferSize/this.Fs/4;
        end
        function startRecording(this)
            if this.RunTimer
                this.cDataFileId = fopen(this.cDataFileName,'w');
                this.cTriggersFileId = fopen(this.cTriggersFileName,'w');
            end
            this.setBufferSize;
            this.setTimerPeriod;
            %% set channel to plot just to send visual message
            this.PlotChannelNum = this.cPlotChannelNum;

            %% set buffersize for showing average
            this.setTagValue('AverageWindow',this.cSamplesAverage);
            %% set x axes to plot
            if this.cPlotAverage
                if isempty(this.cHPlot)
                    figure;
                    this.cHPlot = plot(zeros(numel(this.cTimeVector),1));
                end
                set(this.cHPlot,'XData',this.cTimeVector*1000);
                set(this.cHPlot,'YData',zeros(numel(this.cTimeVector),1));
                xlabel('Time [ms]')
            end
            %% set fft axes
            if this.cPlotFFT
                if isempty(this.cHPlotFFT)
                    figure;
                    this.cHPlotFFT = plot(zeros(numel(this.cFrequencyVector),1));
                end
                set(this.cHPlotFFT,'XData',this.cFrequencyVector);
                set(this.cHPlotFFT,'YData',zeros(numel(this.cFrequencyVector),1));
                xlabel('Frequency [Hz]')
            end
            this.cBufferCounter = 0;
            if this.cPlotAverage && ~isempty(this.cHPlot)
                %% set timer and callback function
                set(this.cPlotTimer, 'TimerFcn', @this.plotaverage, ...
                    'Period', this.cTimerPlotPeriod, ...
                    'StartDelay', this.cTimerPlotPeriod ,...
                    'ExecutionMode', 'fixedSpacing', ...
                    'TasksToExecute', inf ...
                    );
                start(this.cPlotTimer);
            end
            this.run;
        end
        
        function plotaverage(this, ~, ~)
            if ~isempty(this.cHPlot)
                %time plot
                cScale = 1e6; %uV
                counter = max(this.Device.GetTagVal('AverageCounter'),1);
                cData = this.Device.ReadTagV('AverageBuffer', 0, this.cSamplesAverage)*this.ScaleFactor/counter * cScale * (-1)^(this.cInvertPlot);
                set(this.cHPlot,'YData', cData);
                drawnow;
                this.dispmessage(strcat('Number of triggers:', num2str(counter), ...
                    '/', ...
                    'Nch:', num2str(this.cNChannels)));
                % frequency plot
                if ~isempty(this.cHPlotFFT) && this.cPlotFFT
                    cFFTData = abs(fft(cData))*2/numel(cData); 
                    cFFTData(1) = 0;
                    set(this.cHPlotFFT,'YData', cFFTData(1 : numel(this.cFrequencyVector)));
                    drawnow;
                end
            end
        end
        
        function value = exportParameters(this)
            value.Version = 1.0;
            value.Fs = this.Fs;
            value.DataFileName = this.DataFileName;
            value.TriggersFileName = this.TriggersFileName;
            value.NChannels = this.NChannels;
            value.ScaleFactor = this.ScaleFactor;
        end
        function value = get.NChannels(this)
            value = this.cNChannels;
        end
        function value = get.PermittedNRecCh(this)
            value = this.cPermittedNRecCh;
        end
        function set.DataFileName(this, value)
            this.cDataFileName = value;
        end
        function value = get.DataFileName(this)
            value = this.cDataFileName;
        end
        function set.TriggersFileName(this, value)
            this.cTriggersFileName = value;
        end
        function value = get.TriggersFileName(this)
            value = this.cTriggersFileName;
        end
    end
end