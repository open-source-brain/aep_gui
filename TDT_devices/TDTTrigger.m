classdef TDTTrigger < TDTDevice
    properties
        RecordingTime = 0;%estimated recording time in seconds to prealocate databuffer
    end
    properties(Dependent)
        TriggerPulseWidth;
        TriggerInputFileName;
        TriggerThreshold;
    end
    properties (GetAccess = private)
        cDataFileId = [];
        cTriggerInputFileName = '';
        cBufferCounter = 0;
        cTriggerPulseWidth;%width in secconds of a trigger Pulse
    end
    properties (Constant = true, GetAccess = private)
        cTriggerBufferSize = 50000;% this value must have same value than .rcx file
    end
    methods
        function this = TDTTrigger(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'DeviceName', 'RP2');
            s = ef(s, 'TriggerInputFileName', strcat(tempdir,'triggers.triggerdata'));
            s = ef(s, 'TriggerPulseWidth', 0.001);%in seconds
            this = this@TDTDevice(s.DeviceName);
            this.cTriggerInputFileName = s.TriggerInputFileName;
            this.cTriggerPulseWidth = s.TriggerPulseWidth;
        end
        function startListening(this)
            if this.RunTimer
                this.cDataFileId = fopen(this.cTriggerInputFileName,'w');
                this.TimerPeriod = this.cTriggerBufferSize/this.Fs/4;
            end
            this.cBufferCounter = 0;
            this.setTagValue('TriggerPulseWidth', this.cTriggerPulseWidth*1000);
            this.setTagValue('TriggerOpenTime', 1/this.Fs*1000);%this will open the trigger for one sample
            this.run;
        end
        function stopListening(this)
            this.stop;
            if this.RunTimer
                fclose(this.cDataFileId);
            end
            this.cBufferCounter = 0;
        end
        function running(this, ~, ~)
            % Wait until first half of Buffer fills
            halfBufSize= this.cTriggerBufferSize/2;
            cIndex = this.Device.GetTagVal('TriggerIndex');
            while(cIndex < halfBufSize)
                cIndex = this.Device.GetTagVal('TriggerIndex');
            end
            % writes first half of the data
            fwrite(this.cDataFileId, this.Device.ReadTagV('TriggerDataOut', ...
               0, halfBufSize),'float32');
            cIndex = this.Device.GetTagVal('TriggerIndex');
            if cIndex < halfBufSize
                this.dispmessage('Transfer rate is too slow!!!');
            end
            %reads second segment
            cIndex = this.Device.GetTagVal('TriggerIndex');
            while (cIndex > halfBufSize)
                cIndex = this.Device.GetTagVal('TriggerIndex');
            end
            % writes second half of the data
            fwrite(this.cDataFileId, this.Device.ReadTagV('TriggerDataOut', ...
                halfBufSize, halfBufSize), 'float32');
            % Check to see if the data transfer rate is fast enough
            cIndex = this.Device.GetTagVal('TriggerIndex');
            if cIndex > halfBufSize
                this.dispmessage('Transfer rate is too slow!!!');
            end
            this.cBufferCounter = this.cBufferCounter + 1;
        end
        function set.TriggerPulseWidth(this, value)
            this.cTriggerPulseWidth = value;
        end
        function value = get.TriggerPulseWidth(this)
            value = this.cTriggerPulseWidth;
        end
        function set.TriggerInputFileName(this, value)
            this.cTriggerInputFileName = value;
        end
        function value = get.TriggerInputFileName(this)
            value = this.cTriggerInputFileName;
        end
        function set.TriggerThreshold(this, value)
            status = this.setTagValue('TriggerThreshold', value);
            if ~status
                this.dispmessage('Threshold level could not be set');
            end
        end
        function value = get.TriggerThreshold(this)
            value = this.getTagValue('TriggerThreshold');
        end
    end
end