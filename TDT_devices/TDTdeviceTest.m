function TDTdeviceTest
%% connect zbus
% zbus = ZBUSDevice;
% zbus.connectDevice('zBUS');
%zbus.hardwareReset('deviceType','RX5');
%% 
%% connect rp2
% rp2 = TDTDevice('RP2');
% rp2.connectDevice;
% rp2.FileRCX = 'C:\Documents and Settings\Jaime Undurraga\Desktop\RPvdsEx_Files\abr_trigger.rcx';
% rp2.loadRCXfile;
%% connect rx5
filename = 'test.data'
rx5 = TDTRecorder('RX5',filename);
rx5.connectDevice;
rx5.Fs = 0;
rx5.FileRCX = 'C:\Documents and Settings\Jaime Undurraga\Desktop\RPvdsEx_Files\abr_recording.rcx';
rx5.loadRCXfile;
rx5.Device.hFunRunning = @callbackRX5Running;
rx5.run;


function stopnow = callbackRX5Running(value)
    stopnow = false;


