classdef TTLSerial < handle
    events
        evNotifyStatus;
    end
    methods (Abstract)
        openPort(this, port);
        sendTrigger(this, value);
        dispmessage(this, message);
        selected_port(this);
    end
end