% this class provide TTL signals (at 0 -> -12 | 1 +12 Volts from pins 4 (DRT) and
% 7 (RTS) of serial a port. You can use a voltage divisor to provide +5 to -%
% signals instead
% example
% st = TTLSerial;
% st.openPort('/dev/ttyS0');
% st.sendTrigger('DTR', 0, 'DurationDTR', 1, 'RTS', 0, 'DurationRTS', 2)
% delete(st)
classdef TTLSerialNonStandard < TTLSerial
    properties (Access = private)
        hSerialPort = [];
    end
    methods
        function this = TTLSerialNonStandard(varargin)
        end
        function openPort(this, port)
            if isempty(this.hSerialPort)
                this.hSerialPort = serial(port, 'DataTerminalReady', 'off', ... 
                'RequestToSend', 'off', ...
                'BaudRate',9600, ...
                'DataBits', 8);
            end
            if isequal(this.hSerialPort.Status, 'open'); return; end;
            try
                fopen(this.hSerialPort);
                this.dispmessage('Serial port opened');
            catch err
                this.dispmessage(err.message);
            end
            
        end
        function port = selected_port(this)
            port = '';
            if isempty(this.hSerialPort); return; end;
            port = this.hSerialPort.propinfo.Port.DefaultValue;
        end
        function delete(this)
            if isempty(this.hSerialPort); return; end;
            fclose(this.hSerialPort);
            delete(this.hSerialPort)
        end
        function sendTrigger(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'DTR', 0);
            s = ef(s, 'RTS', 0);
            s = ef(s, 'DurationDTR', Inf);
            s = ef(s, 'DurationRTS', Inf);
            if isempty(this.hSerialPort) || isequal(this.hSerialPort.Status, 'closed')
                this.dispmessage('Serial port not assigned or closed');
                return;
            end;
            
            cDTR = 'off';
            if isequal(s.DTR, 1)
                cDTR = 'on';
            end
            cRTS = 'off';
            if isequal(s.RTS, 1)
                cRTS = 'on';
            end
            cDRTIniState = this.hSerialPort.DataTerminalReady;
            cRTSIniState = this.hSerialPort.RequestToSend;
            
            this.dispmessage(strcat('Serial port DRT initial state:', this.hSerialPort.DataTerminalReady));
            this.dispmessage(strcat('Serial port RTS initial state:', this.hSerialPort.RequestToSend));
            set(this.hSerialPort, 'DataTerminalReady', cDTR, 'RequestToSend', cRTS);
            this.dispmessage(strcat('Serial port DRT transition state:', this.hSerialPort.DataTerminalReady));
            this.dispmessage(strcat('Serial port RTS transition state:', this.hSerialPort.RequestToSend));
            if ~isequal(s.DurationDTR, Inf) && ~isequal(s.DurationRTS, Inf)
                [~, posMin]= min([s.DurationDTR, s.DurationRTS]);
                switch posMin
                    case 1
                        pause(s.DurationDTR);
                        set(this.hSerialPort, 'DataTerminalReady', cDRTIniState, 'RequestToSend', cRTS);
                        this.dispmessage(strcat('Serial port DRT transition state:', this.hSerialPort.DataTerminalReady));
                        this.dispmessage(strcat('Serial port RTS transition state:', this.hSerialPort.RequestToSend));
                        pause(s.DurationRTS - s.DurationDTR);
                        set(this.hSerialPort, 'RequestToSend', cDRTIniState);
                    case 2
                        pause(s.DurationRTS);
                        set(this.hSerialPort, 'DataTerminalReady', cDTR, 'RequestToSend', cRTSIniState);
                        this.dispmessage(strcat('Serial port DRT transition state:', this.hSerialPort.DataTerminalReady));
                        this.dispmessage(strcat('Serial port RTS transition state:', this.hSerialPort.RequestToSend));
                        pause(s.DurationDTR - s.DurationRTS);
                        set(this.hSerialPort, 'DataTerminalReady', cRTSIniState);
                end
            elseif ~isequal(s.DurationDTR, Inf)
                pause(s.DurationDTR);
                set(this.hSerialPort, 'DataTerminalReady', cDRTIniState);
            elseif ~isequal(s.DurationRTS, Inf)
                pause(s.DurationRTS);
                set(this.hSerialPort, 'DataTerminalReady', cRTSIniState);
            end
            this.dispmessage(strcat('Serial port DRT final state:', this.hSerialPort.DataTerminalReady));
            this.dispmessage(strcat('Serial port RTS final state:', this.hSerialPort.RequestToSend));
        end
        
        function dispmessage(this,message)
            txMessage = strcat(class(this), '-', message);
            disp(txMessage);
            notify(this,'evNotifyStatus', MessageEventDataClass(txMessage));
        end
        
    end
end
