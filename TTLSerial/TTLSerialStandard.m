% this class provide TTL signals via standard serial port
% example
% st = TTLSerial;
% st.openPort('/dev/ttyS0');
% st.sendTrigger('DTR', 0, 'DurationDTR', 1, 'RTS', 0, 'DurationRTS', 2)
% delete(st)
classdef TTLSerialStandard < TTLSerial
    properties (Access = private)
        hSerialPort = [];
    end
    methods
        function this = TTLSerialStandard(varargin)
        end
        function port = selected_port(this)
            port = '';
            if isempty(this.hSerialPort); return; end;
            port = this.hSerialPort.propinfo.Port.DefaultValue;
        end
        function openPort(this, port)
            if ~isequal(this.selected_port, port) && ...
                    ~isempty(this.hSerialPort)
                fclose(this.hSerialPort);
                this.hSerialPort = [];
            end
            if isempty(this.hSerialPort)
                this.hSerialPort = serialport(port, ...
                    'setDTR', 'on', ... 
                    'setRTS', 'off', ...
                    'BaudRate',9600, ...
                    'DataBits', 8);
            end
            if isequal(this.hSerialPort.Status, 'open'); return; end;
            try
                fopen(this.hSerialPort);
                this.dispmessage('Serial port opened');
            catch err
                this.dispmessage(err.message);
            end
            
        end
        function delete(this)
            if isempty(this.hSerialPort); return; end;
            fclose(this.hSerialPort);
            delete(this.hSerialPort)
        end
        function sendTrigger(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'TriggerValue', 0);
            if isempty(this.hSerialPort) || isequal(this.hSerialPort.Status, 'closed')
                this.dispmessage('Serial port not assigned or closed');
                return;
            end;
            
            fwrite(this.hSerialPort, s.TriggerValue);
            this.dispmessage(strcat('Serial port trigger sent:', num2str(s.TriggerValue)));
        end
        
        function dispmessage(this,message)
            txMessage = strcat(class(this), '-', message);
            disp(txMessage);
            notify(this,'evNotifyStatus', MessageEventDataClass(txMessage));
        end
        
    end
end
