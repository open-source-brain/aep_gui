% The arduinio appears as a serial devide under linux and windows. For
% linux serial ports can be found using the following command in the
% termimal:

% dmesg | grep tty

% the output will look something like this 
%[    0.000000] console [tty0] enabled
%[    0.886691] 00:02: ttyS0 at I/O 0x3f8 (irq = 4, base_baud = 115200) is a 16550A
%[  922.870473] cdc_acm 1-11:1.0: ttyACM0: USB ACM device

% Matlab provides the command instrhwinfo to list serial ports, however, in
% linux, matlab will only recognize serial ports which names start as
% ttSXXX so the arduino serial port (ttyACM0) will not be found by matlab.
% This can be solved  by creating a symbolic link so that you can trick 
% matlab to see the arduino port.
% The next command (in linux terminal) will create a symbolic link to the arduino port with
% a name that matlab can see:

% sudo ln -s /dev/ttyACM0 /dev/ttyS101

%matlab should now show the arduino serial port as '/dev/ttyS101'
clear all;
% list serial available ports
serialInfo = instrhwinfo('serial');
% serialInfo.AvailableSerialPorts should show the serial ports, in windows
% this will show something like 'COM4' whilst in linux it will show /dev/ttyS101

% now we open the serial port
%windows example
s = serial('COM5', 'DataTerminalReady', 'on', ... 
                'RequestToSend', 'off', ...
                'BaudRate',9600, ...
                'DataBits', 8);
%linux example
% s = serial('/dev/ttyS101', 'DataTerminalReady', 'on', ... 
%                 'RequestToSend', 'off', ...
%                 'BaudRate',9600, ...
%                 'DataBits', 8);
% we open the port
fopen(s);
% now we send  a trigger to every pin every 0.2 seconds            
for i = 0 : 7
    fwrite(s, 2^i);
    pause(0.2)
end

% finally, we close the port 
fclose(s);