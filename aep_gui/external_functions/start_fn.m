function start_fn(this,src,evnt)
% 
% addprop(this,'speaker');
% addprop(this,'speaker_test');

subj = get(findobj(this.HContainer, 'Tag', 'edSubject'), 'String');
ntrials=length(this.hStimuliModule.Stimulus(1).Parameters.FilesList);
iter = this.hStimuliModule.hSequenciator.Sequence{1, 1}.CurrentStep;
if iter ==1% first iteration?
    rng('shuffle')
    seed = rng;    
    mkdir(['data_' subj])
    mkdir(['results_' subj])
    % one for attend to female, zero attend to male
    randind = randperm(ntrials);
    speaker_tmp = [zeros(1,round(ntrials/2)) ones(1,  ntrials -round(ntrials/2))];
    this.hStimuliModule.Stimulus(1).Parameters.speaker = speaker_tmp(randind);
    this.hStimuliModule.Stimulus(1).Parameters.speaker_test = [zeros(1,round(ntrials/2)) ones(1,  ntrials -round(ntrials/2))];
    target_speaker = this.hStimuliModule.Stimulus(1).Parameters.speaker;
    sentence_speaker = this.hStimuliModule.Stimulus(1).Parameters.speaker_test;
    save(['data_' subj '/answers.mat'], 'target_speaker', 'sentence_speaker','seed')
end
load(['data_' subj '/answers.mat'], 'target_speaker', 'sentence_speaker')
this.hStimuliModule.Stimulus(1).Parameters.speaker = target_speaker;
this.hStimuliModule.Stimulus(1).Parameters.speaker_test = sentence_speaker;
switch target_speaker(iter)
    case 0 % male
         uiwait(msgbox('In this trial listen to the male talkers','Instructions','modal'));
    case 1 % female
         uiwait(msgbox('In this trial listen to the female talkers','Instructions','modal'));
end
