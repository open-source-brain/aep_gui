function callbackReadElectrodeConfiguration(hObject, ~)
this = get(hObject, 'Userdata');
[filename, pathname] = uigetfile('*.xml','Select the electrode configuration file', this.ElectrodeConfigFilesPath);
if ~isequal(filename, 0)
    %% gui settings
    settings = convertxmlvalue2matvalue(xml2struct(strcat(pathname,filename)));
    cTypeFile = fieldnames(settings);
    if ~isequal(cTypeFile{:}, 'ElectrodeConfiguration')
        errordlg({'The selected file is not an Electrode Configuration file', ...
            strcat('Type:', cTypeFile{:})});
        return;
    end
    msettings = settings.ElectrodeConfiguration;
    tagList = fieldnames(msettings);
    %% first we set all values
    for i = 1:numel(tagList)
        val = msettings.(tagList{i}).FieldValue;
        FieldType = msettings.(tagList{i}).FieldType;
        chobj = findobj(this.HContainer ,'Tag',tagList{i});
        set(chobj, FieldType, val);
    end
    %% after setting all values we invoke callbacks
    for i = 1:numel(tagList)
        chobj = findobj(this.HContainer ,'Tag',tagList{i});
        hgfeval(get(chobj,'Callback'),chobj);
    end
    this.ElectrodeConfigFilesPath = pathname;
end
