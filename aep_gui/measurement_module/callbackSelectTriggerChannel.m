function callbackSelectTriggerChannel(hObject, ~)
this = get(hObject, 'Userdata');
cChannel = getCurrentPopUpElement(hObject);
cval = str2double(cChannel);
if isnan(cval)
    this.TriggerChannel = {};
    return; 
end
cStimuliSettings = this.hStimuliModule.Stimulus;

for i = 1:numel(cStimuliSettings)
    if cStimuliSettings(i).Parameters.Channel == cval
        this.TriggerChannel.Parameters = cStimuliSettings(i).Parameters;
        this.TriggerChannel.Calibration= cStimuliSettings(i).Calibration;
        return;
    end
end