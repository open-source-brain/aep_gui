classdef ExternalRecordingDeviceModule < RecordingModule
    properties
        HContainer; %handles of object containing visual objects
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        ExternalDevice;
    end
    methods
        function this = ExternalRecordingDeviceModule(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'HContainer', []);%handle to container object
            s = ef(s, 'Topmargin', 0.05);
            s = ef(s, 'UiHigh', 0.03);%desired high for objects
            s = ef(s, 'UiWidth', 0.1);%desired width for objects
            this.HContainer = s.HContainer;
            this.Topmargin = s.Topmargin;
            this.UiHigh = s.UiHigh;%desired high for objects
            this.UiWidth = s.UiWidth;%desired width for objects
            this.ExternalDevice = ExternalRecordingDevice;
            this.setRecordingModuleGui;
            %% read saved seetings
            this.ReadSettings;
        end
        function setRecordingModuleGui(this)
            setExternalRecordingDeviceTab(this);
        end
        function value = getClassSettings(this)
            value = [];
        end
        function settings = GetModuleSettings(this)
            settings.(class(this)).GuiSettings = getExternalRecordingModuleSettings(this);
%             settings.(class(this)).ClassSettings = getClassSettings(this);
        end
        
        function SaveSettings(this)
            config_path = fullfile(homepath,'.aep_gui');
            if ~exist(config_path, 'dir')
                mkdir(config_path)
            end
            modulePath = fullfile(config_path, strcat(mfilename('class'), '.json'));
            convertStructure2JsonFile(this.GetModuleSettings, modulePath);
        end
        
        function ReadSettings(this)
            cFile = fullfile(homepath, '.aep_gui', strcat(mfilename('class'), '.json'));
            if exist(cFile, 'file')
                try
                    settings = convertJsonFile2Structure(cFile);
                    %% Class settings
                    if isfield(settings.(class(this)),'ClassSettings')
                        csettings = settings.(class(this)).ClassSettings;
                        vtagList = fieldnames(csettings);
                        for i = 1:numel(vtagList)
                            val = csettings.(vtagList{i});
                            eval(strcat('this.',vtagList{i},'= val;'));
                        end
                    end
                    %% GUI settings
                    msettings = settings.(class(this)).GuiSettings;
                    tagList = fieldnames(msettings);
                    %% first we set all values
                    for i = 1:numel(tagList)
                        val = msettings.(tagList{i}).FieldValue;
                        if ~iscell(val)
                            val = {val};
                        end
                        FieldType = msettings.(tagList{i}).FieldType;
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        for j = 1 : numel(FieldType)
                            set(chobj, FieldType{j}, val{j});
                        end
                    end
                    %% after setting all values we invoke callbacks
                    for i = 1:numel(tagList)
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        hgfeval(get(chobj,'Callback'),chobj);
                    end
                catch err
                    this.dispmessage(strcat('Could not read settings: ',err.message));
                end
            end
        end
        function value = getParameters(this)
            value.Version = 1.0;
            value.DeviceName = this.ExternalDevice.DeviceName;
            value.NChannels = this.ExternalDevice.NChannels;
            value.Fs =  this.ExternalDevice.Fs;
        end
        
        function status = setupRecordingDevice(this, varargin)
            s = parseparameters(varargin{:});
            status = true;
        end
        
        function startRecording(this)
        end
        function stopRecording(this)
        end
        function dispmessage(this, message)
            txMessage = strcat(class(this),'-',message);
            disp(txMessage);
            notify(this,'evNotifyStatus',MessageEventDataClass(txMessage));
        end
        function receiveMessage(this, ~, evnt)
            this.dispmessage(evnt.message);
        end
        function value = getNChannels(this)
            value = this.ExternalDevice.NChannels;
        end
        function value = getFs(this)
            value = this.ExternalDevice.Fs;
        end
    end
end
