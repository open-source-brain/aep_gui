classdef TDTRecordingDeviceModule < RecordingModule
    properties
        HContainer; %handles of object containing visual objects
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        RP2; %handle to RP2 device 
        RX5; %handle to RX5 device 
        ZBUS; %handle to ZBUS device 
        FilePathRP2 = which('RPvdsEx_Files/abr_trigger_pulse_shape.rcx');
        FilePathRX5 = which('RPvdsEx_Files/abr_recording_multichannel-4.rcx');
        ScaleFactor = 1/20;%head stage factor to compensate gain added by the RA16LI 
    end
    properties (Access = private)
        cPathTriggerCalibration = '';
        cThresholdLevel = 1.4;
    end
    methods
        function this = TDTRecordingDeviceModule(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'HContainer', []);%handle to container object
            s = ef(s, 'Topmargin', 0.05);
            s = ef(s, 'UiHigh', 0.03);%desired high for objects
            s = ef(s, 'UiWidth', 0.1);%desired width for objects
            this.HContainer = s.HContainer;
            this.Topmargin = s.Topmargin;
            this.UiHigh = s.UiHigh;%desired high for objects
            this.UiWidth = s.UiWidth;%desired width for objects
            %% creates TDT devices
            %% connect rp2
            this.RP2 = TDTTrigger('RP2',strcat(tempdir,'trigger.triggerdata'),0.001);
            %% connect rx5
            this.RX5 = TDTRecorder('RX5',strcat(tempdir,'trigger.triggerdata'),strcat(tempdir,'egg.data'));
            %% connect zbus
            this.ZBUS = ZBUSDevice;
            %% add listener to receive device messages
            addlistener(this.RP2, 'evNotifyStatus',@(src,evnt)receiveMessage(this,src,evnt));
            addlistener(this.RX5, 'evNotifyStatus',@(src,evnt)receiveMessage(this,src,evnt));
            addlistener(this.ZBUS, 'evNotifyStatus',@(src,evnt)receiveMessage(this,src,evnt));
            this.setRecordingModuleGui;
            %% read saved seetings
            this.ReadSettings;
        end
        function setRecordingModuleGui(this)
            setRecordingDeviceTab(this);
        end
        
        function settings = GetModuleSettings(this)
            settings.(class(this)).GuiSettings = getRecordingModuleSettings(this);
            settings.(class(this)).ClassSettings = getClassSettings(this);
        end
        function value = getClassSettings(this)
            value.cThresholdLevel = this.cThresholdLevel;
        end
        function SaveSettings(this)
             config_path = fullfile(homepath,'.aep_gui');
            if ~exist(config_path, 'dir')
                mkdir(config_path)
            end
            modulePath = fullfile(config_path, strcat(mfilename('class'), '.json'));
            convertStructure2JsonFile(this.GetModuleSettings, modulePath);
        end
        function ReadSettings(this)
            cFile = fullfile(homepath, '.aep_gui', strcat(mfilename('class'), '.json'));
            if exist(cFile, 'file')
                try
                    settings = convertJsonFile2Structure(cFile);
                    %% Class settings
                    if isfield(settings.(class(this)),'ClassSettings')
                        csettings = settings.(class(this)).ClassSettings;
                        vtagList = fieldnames(csettings);
                        for i = 1:numel(vtagList)
                            val = csettings.(vtagList{i});
                            eval(strcat('this.',vtagList{i},'= val;'));
                        end
                    end
                    %% GUI settings
                    msettings = settings.(class(this)).GuiSettings;
                    tagList = fieldnames(msettings);
                    %% first we set all values
                    for i = 1:numel(tagList)
                        val = msettings.(tagList{i}).FieldValue;
                        if ~iscell(val)
                            val = {val};
                        end
                        FieldType = msettings.(tagList{i}).FieldType;
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        for j = 1 : numel(FieldType)
                            set(chobj, FieldType{j}, val{j});
                        end
                    end
                    %% after setting all values we invoke callbacks
                    for i = 1:numel(tagList)
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        hgfeval(get(chobj,'Callback'),chobj);
                    end
                catch err
                    this.dispmessage(strcat('Could not read settings: ',err.message));
                end
            end
        end
        function value = getParameters(this)
           value.Version = 2.0;
           value.FilePathRP2 = this.FilePathRP2;
           value.FilePathRX5 = this.FilePathRX5;
           value.TriggerThreshold = this.RP2.TriggerThreshold;
           value.NChannels = this.RX5.NChannels;
           value.Fs =  this.RX5.Fs;
           value.ScaleFactor = this.ScaleFactor;
        end
        
        function status = setupRecordingDevice(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'TriggerPulseWidth', 0.001);
            s = ef(s, 'AverageWindow', 0.01);
            s = ef(s, 'RecordTriggerInput', false);
            s = ef(s, 'TriggersFileName','triggers.triggerdata');
            s = ef(s, 'DataFileName', 'eeg.eegdata');
            s = ef(s, 'hFigure', []);
            s = ef(s, 'hFigureFFT', []);
            try
                status = true;
                %% sync devices
                this.ZBUS.connectDevice('zBUS');
                this.ZBUS.syncBusses('deviceMaster', 'RP2', ...
                    'deviceSlave', 'RX5');
                %% setup recording devices
                this.RP2.connectDevice;
                this.RP2.loadRCXfile;
                this.RP2.TriggerPulseWidth = s.TriggerPulseWidth;
                this.RP2.RunTimer = s.RecordTriggerInput;
                this.RP2.TriggerThreshold = this.cThresholdLevel;
                this.RX5.connectDevice;
                this.RX5.loadRCXfile;
                this.RX5.PlotAverage = true;
                this.RX5.PlotFFT = true;
                this.RX5.AverageWindow = s.AverageWindow;
                this.RX5.RunTimer = true;
                this.RX5.TriggersFileName = s.TriggersFileName;
                this.RX5.DataFileName = s.DataFileName;
                this.RX5.ScaleFactor = this.ScaleFactor;
                %% dummy plot to draw faster
                axes(s.hFigure);
                hplot = plot(NaN,'Parent', s.hFigure);
                xlabel('Time [ms]');
                ylabel('Amplitude [uV]');
                
                %% dummy plot to draw faster
                axes(s.hFigureFFT);
                hplotFFT = plot(NaN,'Parent', s.hFigureFFT);
                xlabel('Frequency [Hz]');
                ylabel('Magnitude [uV]');
                set(s.hFigureFFT, 'XLim', [0, 400]);
                set(s.hFigureFFT, 'YLim', [0, 5]);
                this.RX5.HPlot = hplot;
                this.RX5.HPlotFFT = hplotFFT;
                if ~(this.RX5.Connected || ...
                        this.RP2.Connected)
                    status = false;
                end
            catch err
                status = false;
                this.dispmessage(err.message);
            end
        end
        
        function status = setupTriggerThredholdRecording(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'TriggerPulseWidth', 0.001);
            status = true;
            cDate = datestr(now,'mm-dd-yy-HH-MM-SS-FFF');
            this.cPathTriggerCalibration = strcat(tempdir,'calibtration',cDate,'.triggerdata');
            %% setup recording devices
             this.RP2.connectDevice;
             this.RP2.loadRCXfile;
             this.RP2.TriggerPulseWidth = s.TriggerPulseWidth; 
             this.RP2.RunTimer = true;
             this.RP2.TriggerInputFileName = this.cPathTriggerCalibration;
             if ~(this.RP2.Connected)
                 status = false;
             end
        end
        function analyzeTriggers(this)
            this.dispmessage('Detecting threshold');
            this.cThresholdLevel = detectThresholdLevel(this.cPathTriggerCalibration);
            this.dispmessage(strcat('Detected Level:', num2str(this.cThresholdLevel)));
        end
        
        function startRecording(this)
            this.RP2.startListening;
            this.RX5.startRecording;
        end
        function stopRecording(this)
            try
                this.RP2.stopListening;
            catch err
                this.dispmessage(err.message);
            end
            try
                this.RX5.stopRecording;
            catch err
                this.dispmessage(err.message);
            end
        end
        function setPlotChannel(this, value)
            this.RX5.PlotChannelNum = value;
        end
        function dispmessage(this, message)
            txMessage = strcat(class(this),'-',message);
            disp(txMessage);
            notify(this,'evNotifyStatus',MessageEventDataClass(txMessage));
        end
        function receiveMessage(this, ~, evnt)
            this.dispmessage(evnt.message);
        end
        function value = getNChannels(this)
            value = this.RX5.NChannels;
        end
        function value = getFs(this) 
            value =  this.RX5.Fs;
        end
    end
end
