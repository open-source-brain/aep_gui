function callbackHeadStage(hObject,~)
this = get(hObject, 'Userdata');
[value, ~] = getCurrentPopUpElement(hObject);
switch value 
    case 'RA16LI'
        this.ScaleFactor = 1/20;
    case 'none'
        this.ScaleFactor = 1;
end