function callbackRP2FilePath(hObject, ~)
this = get(hObject, 'Userdata');
if exist(get(hObject, 'String'), 'file')
    this.FilePathRP2 = get(hObject, 'String');
    this.RP2.FileRCX = this.FilePathRP2;
else
    set(hObject, 'String', this.FilePathRP2);
end
