classdef CIStimuliModule < StimuliModule
    properties
        HContainer; %handles of object containing visual objects
        SoundObjects = {};%handles to visual objets
        Sound_devices = {};%system's sound devices
        CurrentSoundDevice = {}; %selected device
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        NumberEpochs = 1;%number of ephocs to play each defined stimulus
        StimuliAxes = [];
        hSoundModule = []; % handle to sound module
        hRecordingModule = []; % handle to recording module
        Stimulus = {};
        StimuliChbList = [];
        hSequenciator;% handle to sequenciator class
        hStopEvent;% handle to stop listener
        SequenceFilesPath = '';
        hCIDevice = [];
        CI_SelectedSubject = 'none';
        %CI_Implant = '';%implanted ci model
        Channels = {'Left_ear', 'Right_ear'};
        Calibration;
        HiddenFields = {'Module','Fs', ...
            'RoundToCycle','SignalType', 'NRepetitions', 'Channel', 'CycleRate',...
            'ImplantType', 'Description', 'ImplantID', 'Range', 'AltPhaseRate'};
    end
    properties (Dependent)
        CI_Map; %array cell to ci maps
    end
    properties (Access = private)
        cCI_Map = [];
        cCalibration = [];
        hTTLSerial = [];
    end
    methods
        function this = CIStimuliModule(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'HContainer', []);%handle to container object
            s = ef(s, 'Topmargin', 0.05);
            s = ef(s, 'UiHigh', 0.03);%desired high for objects
            s = ef(s, 'UiWidth', 0.1);%desired width for objects
            s = ef(s, 'SoundModule', []); % sound modul han
            s = ef(s, 'RecordingModule', []); % sound modul han
            s = ef(s, 'Device', 'Medel'); %
            this.HContainer = s.HContainer;
            this.Topmargin = s.Topmargin;
            this.UiHigh = s.UiHigh;%desired high for objects
            this.UiWidth = s.UiWidth;%desired width for objects
            this.hSoundModule = s.SoundModule;
            this.hRecordingModule = s.RecordingModule;
            
            switch s.Device
                case 'Medel'
                    this.hCIDevice = RIB2Device;
                otherwise
                    errordlg(strcat('Unknown device:', s.Device));
                    return;
            end
            addlistener(this.hCIDevice, 'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            addlistener(this.hCIDevice, 'evCIDeviceStopped',@(src,evnt)callbackCIDeviceStopped(this,src,evnt)); %event to listen and update the module
            % default maps
            this.cCI_Map{1} = this.getDefaultMap;
            this.cCI_Map{2} = this.getDefaultMap('DummyName', 'new');
            % Default calibration
            this.cCalibration{1} = this.getDefaultCalibration;
            % set gui
            this.setStimuliModuleGui;
            %create sequenciator module
            this.hSequenciator = Sequenciator;
            addlistener(this.hSequenciator, 'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            % add listener to TTL port
            this.hTTLSerial = TTLSerial;
            addlistener(this.hTTLSerial, 'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            %we read gui and sounds settings
            this.ReadSettings;
        end
        function delete(this)
            delete(this.hSequenciator);
            delete(this.hTTLSerial);
        end
        function setStimuliModuleGui(this)
            setCIStimuliTab(this);
        end
        
        function value = getClassSettings(this)
            value.SequenceFilesPath = this.SequenceFilesPath;
            %             value.CI_Map = this.cCI_Map;
        end
        function settings = GetModuleSettings(this)
            settings.(class(this)).GuiSettings = getStimuliModuleSettings(this);
            for i = 1:numel(this.Stimulus)
                settings.(class(this)).Stimulus{i}.Parameters = this.Stimulus{i}.Parameters;
                settings.(class(this)).Stimulus{i}.CI_Map = this.Stimulus{i}.CI_Map;
            end
            settings.(class(this)).ClassSettings = getClassSettings(this);
        end
        function settings = getCIMaps(this)
            settings.CI_Maps.CI_Map = this.cCI_Map;
        end
        function SaveSettings(this)
            config_path = fullfile(homepath,'.aep_gui');
            if ~exist(config_path, 'dir')
                mkdir(config_path)
            end
            setting_path = fullfile(config_path, strcat(mfilename('class'), '.json'));
            convertStructure2JsonFile(this.GetModuleSettings, ...
                setting_path);
            
            cPath = fileparts(setting_path);
            convertStructure2JsonFile(this.getCIMaps, ...
                strcat(cPath,filesep,'CI_Maps.json'));
        end
        function ReadSettings(this)
            cFile = fullfile(homepath, '.aep_gui', strcat(mfilename('class'), '.json'));
            cPath = fileparts(cFile);
            cMapFile = strcat(cPath,filesep,'CI_Maps.json');
            if exist(cFile, 'file')
                try
                    settings = convertJsonFile2Structure(cFile);
                    %% class settings
                    if isfield(settings.(class(this)),'ClassSettings')
                        csettings = settings.(class(this)).ClassSettings;
                        vtagList = fieldnames(csettings);
                        for i = 1:numel(vtagList)
                            val = csettings.(vtagList{i});
                            eval(strcat('this.',vtagList{i},'= val;'));
                        end
                    end
                    %% read CI MAPS
                    if exist(cMapFile, 'file')
                        maps = convertJsonFile2Structure(cMapFile);
                        this.cCI_Map = maps.CI_Maps.CI_Map;
                    end
                    %% read Implants (also subjects)
                    this.updateSubjects;
                    
                    %% read stimuli settings
                    %update for new versions
                    for i = 1 : numel(settings.(class(this)).Stimulus)
                        if isfield(settings.(class(this)).Stimulus(i), 'Parameters')
                            cStimuli{i}.Parameters = settings.(class(this)).Stimulus(i).Parameters;
                            cStimuli{i}.CI_Map = settings.(class(this)).Stimulus(i).CI_Map;
                        end
                    end
                    for i = 1 : numel(cStimuli)
                        callbackCIStimulusReader(this, cStimuli{i});
                    end
                    notify(this,'evStimuliSettingsChanged');
                    
                    %% read gui settings
                    msettings = settings.(class(this)).GuiSettings;
                    tagList = fieldnames(msettings);
                    
                    %% after setting all values we invoke callbacks
                    for i = 1:numel(tagList)
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        hgfeval(get(chobj,'Callback'),chobj);
                    end
                    %% update gui
                    %                     this.stmPlotStimuli;
                    this.updateEstimatedTime;
                    this.updateSignalInfo;
                catch err
                    this.dispmessage(strcat('Could not read settings: ',err.message));
                end
            end
        end
        function updateSubjects(this)
            implants = this.getImplant;
            subjects{1} = 'none';
            subjects{2} = 'new';
            for i = 1: numel(implants)
                subjects{i + 2} = implants(i).Subject;
            end
            [~,idx] = unique(subjects,'stable');
            set(findobj(this.HContainer, 'Tag', 'puCISetSubject'), 'String', subjects(idx));
            cVal = get(findobj(this.HContainer, 'Tag', 'puCISetSubject'), 'Value');
            set(findobj(this.HContainer, 'Tag', 'puCISetSubject'), 'Value', min(cVal, numel(subjects(idx))));
        end
        function value = getImplant(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Subject', '');
            value = [];
            %% read Implants (also subjects)
            cFile = strcat(mfilename('fullpath'),'.xml');
            cPath = fileparts(cFile);
            cImplantFile = strcat(cPath,filesep,'CI_Implants.xml');
            if ~exist(cImplantFile, 'file'); return; end;
            caux = convertxmlvalue2matvalue(xml2struct(cImplantFile));
            cimplants = caux.Implants;
            if isempty(s.Subject)
                value = [cimplants.Implant{:}];
            else
                for i = 1 : numel(cimplants.Implant)
                    if isequal(cimplants.Implant{i}.Subject, s.Subject)
                        value = cimplants.Implant{i};
                        return;
                    end
                end
            end
        end
        function updateSignalInfo(this)
            hobj = findobj(this.HContainer, 'Tag', 'lbStimuliInfo');
            ctext = {};
            for i = 1 : numel(this.Stimulus)
                if isempty(this.Stimulus{i}); continue;end;
                ctext = [ctext; struct2text(this.Stimulus{i}.Parameters)];
                ctext(end + 1) = {'---MAP---'};
                ctext = [ctext; struct2text(this.Stimulus{i}.CI_Map)];
                ctext(end + 1) = {'----------------'};
            end
            set(hobj, 'String', ctext);
        end
        function value = getParameters(this)
            value.Version = 3;
            value.Implant = this.hCIDevice.Implant;
            for i = 1 : numel(this.Stimulus)
                value.Stimulus{i}.Parameters  = this.Stimulus{i}.Parameters;
                value.Stimulus{i}.CI_Map = this.Stimulus{i}.CI_Map;
            end
            value.NEpochs = this.NumberEpochs;
            value.Impedance = this.getImpedance('Subject', this.CI_SelectedSubject);
            value.Compliance = this.getCompliance('Subject', this.CI_SelectedSubject);
        end
        function playStimuli(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'NumberEpochs', this.NumberEpochs);
            %we use mulation instead of this.Stop since the last one
            %will remove the stop event used when running sequences
            this.hCIDevice.stopStimulation;
            if ~this.checkMap;  return; end;
            %% send starting trigger
            this.sendTTLSignal('Start')
            %% start stimulation
            this.hCIDevice.startStimulation;
        end
        
        function playStimuliFromFile(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'NRepetitions', 1);
            s = ef(s, 'Ear', ''); % cam be left, rigth, or both
            s = ef(s, 'FilePath', '');
            if numel(s.Ear) ~= numel(s.FilePath)
                errordlg('The number of sequences must be equal to the number of ears')
            end
            if numel(s.Ear) == 2 && strcmp(s.Ear{1}, s.Ear{2})
                errordlg('Ears must be different for each sequence')
            end
            if ~iscell(s.Ear)
                s.Ear = {s.Ear};
            end
            if ~iscell(s.FilePath)
                s.FilePath = {s.FilePath};
            end
            %we use stopStimulation instead of this.Stop since the last one
            %will remove the stop event used when running sequences
            this.hCIDevice.stopStimulation;
            %load file sequences
            for i = 1 : numel(s.Ear)
                this.hCIDevice.loadSequencesFromFile('Ear', s.Ear{i}, 'FilePath', s.FilePath{i});
            end
            %% start stimulation
            this.hCIDevice.startStimulation;
        end
        
        function fillBuffers(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'NumberEpochs', this.NumberEpochs);
            s = ef(s, 'ClearBuffers', true);
            if s.ClearBuffers
                this.hCIDevice.clearStimulationSequences;
            end
            cStimulus = this.Stimulus;
            for i = 1 : numel(cStimulus)
                if isequal(cStimulus{i}.Parameters.SignalType, 'none'); continue; end;
                switch cStimulus{i}.Parameters.Channel
                    case 1
                        this.hCIDevice.saveAndLoadSequences('Ear','left', 'Sequence', cStimulus{i}.Signal, 'NRepetitions', cStimulus{i}.Parameters.NRepetitions * s.NumberEpochs);
                    case 2
                        this.hCIDevice.saveAndLoadSequences('Ear','right', 'Sequence', cStimulus{i}.Signal, 'NRepetitions', cStimulus{i}.Parameters.NRepetitions * s.NumberEpochs);
                end
            end
        end
        
        function value = checkMap(this)
            value = true;
            ignoreList = {'Range', ...
                'SendTrigger', ...
                'CarrierPhase', ...
                'CarrierAltPhase', ...
                'ModulationPhase', ... 
                'CycleRate', ...
                'Description', ...
                'RoundToCycle', ...
                'CarrierEnvPhase', ...
                'CarrierEnvAltPhase', ...
                'CarrierEnvNAltPhasePerModCycle', ...
                'AltPhaseRate'};
            %list with parameters of the map that can be lower than the
            %value in the map
            flexibleList.Parameter = {'ModulationIndex', ...
                'ModulationFrequency', ...
                'GapPercent', ...
                'Amplitude', ...
                'PPAmplitude', ...
                'Amplitude_Ini', ...
                'Amplitude_End'};
            flexibleList.Direction = {'Higher', ...
                'Lower', ...
                'Lower', ...
                'Lower', ...
                'Lower', ...
                'Lower', ...
                'Lower'}; 
            for i = 1 : numel(this.Stimulus)
                if isequal(this.Stimulus{i}.Parameters.SignalType, 'none'); continue; end;
                cParameters = this.Stimulus{i}.Parameters;
                cMap = this.Stimulus{i}.CI_Map;
                cMapStimulus = cMap.Stimulus.Parameters;
                %% check if current stimulus and ci_map stimulus are the same
                if ~isequal(cParameters.SignalType, cMap.Stimulus.Parameters.SignalType)
                    value = false;
                    errordlg(strcat('No the map does not match the signal'));
                    return;
                end
                %first check that all parameters (except the map label) are the same
                cParNames = fieldnames(cParameters);
                for j = 1 : numel(cParNames)
                    if find(ismember(ignoreList, cParNames{j})); continue; end;
                    flexParIdx = find(ismember(flexibleList.Parameter,cParNames{j}));
                    if flexParIdx
                        if ~isfield(cMapStimulus, cParNames{j}); continue; end;
                        switch flexibleList.Direction{flexParIdx}
                            case 'Higher'
                                if isequal(cParNames{j}, cMap.ParameterLabel)
                                    if round2(cParameters.(cParNames{j}), 0.01) < round2(cMap.ComfortableLevel, 0.01)
                                        value = false;
                                        errordlg({strcat('Channel:', this.Channels{this.Stimulus{i}.Parameters.Channel}), ...
                                            strcat('The current parameter:',cParNames{j}, ...
                                            '(', num2str(cParameters.(cParNames{j})), ')', ...
                                            ' is lower the flexible mapped parameter', ...
                                            '(',num2str(cMap.ComfortableLevel), ')')});
                                        return;
                                    end
                                else
                                    if round2(cParameters.(cParNames{j}), 0.01) < round2(cMapStimulus.(cParNames{j}), 0.01)
                                        value = false;
                                        errordlg({strcat('Channel:', this.Channels{this.Stimulus{i}.Parameters.Channel}), ...
                                            strcat('The current parameter:',cParNames{j}, ...
                                            '(', num2str(cParameters.(cParNames{j})), ')', ...
                                            ' is lower the flexible mapped parameter', ...
                                            '(',num2str(cMapStimulus.(cParNames{j})), ')')});
                                        return;
                                    end
                                end
                            case 'Lower'
                                if isequal(cParNames{j}, cMap.ParameterLabel)
                                    if round2(cParameters.(cParNames{j}), 0.01) > round2(cMap.ComfortableLevel, 0.01)
                                        value = false;
                                        errordlg({strcat('Channel:', this.Channels{this.Stimulus{i}.Parameters.Channel}), ...
                                            strcat('The current parameter:',cParNames{j}, ...
                                            '(', num2str(cParameters.(cParNames{j})), ')', ...
                                            ' exeeds map maximum', ...
                                            '(',num2str(cMap.ComfortableLevel), ')')});
                                        return;
                                    end
                                else
                                    if round2(cParameters.(cParNames{j}), 0.01) > round2(cMapStimulus.(cParNames{j}), 0.01)
                                        value = false;
                                        errordlg({strcat('Channel:', this.Channels{this.Stimulus{i}.Parameters.Channel}), ...
                                            strcat('The current parameter:',cParNames{j}, ...
                                            '(', num2str(cParameters.(cParNames{j})), ')', ...
                                            ' is higher the flexible mapped parameter', ...
                                            '(',num2str(cMapStimulus.(cParNames{j})), ')')});
                                        return;
                                    end
                                end
                        end
                    else
                        if isequal(cParNames{j}, cMap.ParameterLabel)
                            if round2(cParameters.(cParNames{j}), 0.01) > round2(cMap.ComfortableLevel, 0.01)
                                value = false;
                                errordlg({strcat('Channel:', this.Channels{this.Stimulus{i}.Parameters.Channel}), ...
                                    strcat('The current parameter:',cParNames{j}, ...
                                    '(', num2str(cParameters.(cParNames{j})), ')', ...
                                    ' exeeds map maximum', ...
                                    '(',num2str(cMap.ComfortableLevel), ')')});
                                return;
                            end
                        else
                            if ~isequal2(cParameters.(cParNames{j}), cMap.Stimulus.Parameters.(cParNames{j}), 0.01)
                                value = false;
                                errordlg(strcat('The parameter:',cParNames{j},' does not match the map'));
                                return;
                            end
                        end
                    end
                end
            end
        end
        
        function stopStimuli(this)
            delete(this.hStopEvent);
            this.hCIDevice.Stop;
            %% send stop trigger
            this.sendTTLSignal('End');
        end
        function updateEstimatedTime(this)
            hobj = findobj(this.HContainer, 'Tag', 'txEtimatedTime');
            cmax = this.epochDuration;
            set (hobj, 'String', strcat('Time [min]: ', num2str(cmax*this.NumberEpochs/60)));
        end
        function value = epochDuration(this)
            value = 0;
            for i = 1 : numel(this.Stimulus)
                if isempty(this.Stimulus{i}.Parameters) || ...
                        isequal(this.Stimulus{i}.Parameters.SignalType, 'none'); continue; end;
                cdur = this.Stimulus{i}.Parameters.Duration*this.Stimulus{i}.Parameters.NRepetitions;
                value = max(cdur, value);
            end
        end
        
        function playSequence(this)
            %% check stop event being assigend and remove it
            this.stopStimuli;
            %% check sequences
            [status, message] = this.hSequenciator.checkSequences;
            if ~status
                errordlg(message, 'Sequence error');
                return;
            end
            %%% listen when sound device stops to stop measurement
            this.hStopEvent = addlistener(this.hCIDevice, 'evCIDeviceStopped', @(src,evnt)playNextSequence(this,src,evnt));
            %%initialize sequenciator
            this.hSequenciator.resetSequences;
            this.hSequenciator.saveSettings;
            this.playNextSequence([], []);
        end
        function playNextSequence(this, src, evt)
            %% send trigger indicating previous sequence finished
            this.sendTTLSignal('End');
            if this.loadNextSequence(src, evt);
                this.playStimuli;
            end
        end
        function status = loadNextSequence(this, src, ~)
            %% send trigger indicating previous sequence finished
            this.sendTTLSignal('End');
            status = true;
            %% get current sequence
            cSequences = this.hSequenciator.getCurrentChannelSequence;
            if ~isempty(src)
                %% move sequenciator to next sequence
                this.hSequenciator.iterate;
                cSequences = this.hSequenciator.getCurrentChannelSequence;
            end
            if this.hSequenciator.CycleCompleted
                delete(this.hStopEvent);
                status = false;
                return;
            end
            
            %% read sequences
            for i = 1 : numel(cSequences)
                callbackCIStimulusReader(this,cSequences{i});
            end
            this.fillBuffers;
            this.updateSignalInfo;
        end
        function status = loadAllSequenceStimuliSettings(this)
            status = true;
            %% get current sequence
            cStimuli = this.hSequenciator.getAllChannelStimuliSettings;
            %% read sequences
            for i = 1 : numel(cStimuli)
                callbackCIStimulusReader(this,cStimuli{i});
            end
            notify(this,'evStimuliSettingsChanged');
            %this.updateCalibrationItems;
            %this.stmPlotStimuli;
            this.updateSignalInfo;
        end
        function status = loadCurrentSequence(this)
            status = true;
            %% get current sequence
            cSequences = this.hSequenciator.getCurrentChannelSequence;
            %% read sequences
            for i = 1 : numel(cSequences)
                callbackCIStimulusReader(this,cSequences{i});
            end
            notify(this,'evStimuliSettingsChanged');
            this.updateSignalInfo;
        end
        
        function dispmessage(this, message)
            txMessage = strcat(class(this), '-', message);
            disp(txMessage);
            notify(this, 'evNotifyStatus', MessageEventDataClass(txMessage));
        end
        
        function receiveMessage(this, ~, evnt)
            this.dispmessage(evnt.message);
        end
        
        function value = get.CI_Map(this)
            if isempty(this.cCI_Map)
                value = {this.getDefaultMap};
            else
                value = this.cCI_Map;
            end
        end
        
        function set.CI_Map(this, value)
            this.cCI_Map = value;
        end
        function addCI_Map(this, newMap, calIdx)
            if isempty(calIdx)
                this.cCI_Map{end+1} = newMap;
            else
                this.cCI_Map{calIdx} = newMap;
            end
            this.applyMap(newMap.Channel, newMap.MapName);
            if strcmp(newMap.MapName, 'nomap'); return; end;
            callbackCIStimulusReader(this, this.Stimulus{newMap.Channel}.CI_Map.Stimulus);
        end
        function applyMap(this, channel, mapName)
            for i = 1 : numel(this.cCI_Map)
                if isequal(this.cCI_Map{i}.Subject, this.CI_SelectedSubject) && ...
                        isequal(this.cCI_Map{i}.MapName, mapName) && ...
                        isequal(this.cCI_Map{i}.Channel, channel)
                    this.Stimulus{channel}.CI_Map = this.cCI_Map{i};
                end
            end
            this.updateSignalInfo;
            this.updateSubjectMaps;
        end
        function updateSubjectMaps(this)
            cidx = 1;
            for i = 1 : numel(this.Stimulus)
                cText = {};
                if isempty(this.Stimulus{i}); continue; end;
                cText{1} = 'nomap';
                cText{2} = 'new';
                hobj = findobj(this.HContainer, 'Tag', strcat('puSetMap_',num2str(this.Stimulus{i}.Parameters.Channel)));
                cValue = getCurrentPopUpElement(hobj);
                for j = 1:numel(this.cCI_Map)
                    if isequal(this.CI_SelectedSubject, this.cCI_Map{j}.Subject) && ...
                            isequal(this.Stimulus{i}.Parameters.Channel, this.cCI_Map{j}.Channel)
                        cText{end + 1} = this.cCI_Map{j}.MapName;
                        if isequal(this.Stimulus{i}.CI_Map.MapName, this.cCI_Map{j}.MapName)
                            cidx = numel(cText);
                        end
                    end
                end
                set(hobj, 'String', cText);
                set(hobj, 'Value', max(min(numel(cText), cidx)));
            end
            this.updateSignalInfo;
            notify(this,'evStimuliSettingsChanged');
        end
        function value = getStimuliSettings(this)
            value = cell(numel(this.Stimulus), 1);
            for i = 1 : numel(this.Stimulus)
                value{i} = rmfield(this.Stimulus{i}, 'Signal');
            end
        end
        function value = get.Calibration(this)
            if isempty(this.cCalibration)
                value = {this.getDefaultCalibration};
            else
                value = this.cCalibration;
            end
        end
        function callbackCIDeviceStopped(this, ~, ~)
            notify(this,'evStimulatorDeviceStopped');
        end
        function saveImplant(this)
            outStruct.Implants.Implant = this.hCIDevice.getImplant;
            modulePath = mfilename('fullpath');
            cPath = fileparts(modulePath);
            appendStruct2xmlFile(outStruct, ...
                strcat(cPath,filesep,'CI_Implants.xml'));
        end
        function showImpedanceMeasurement(this, varargin)
            impedances = this.getImpedance(varargin{:});
            %find max numer of electrodes
            maxElec = -inf;
            for i = 1 : numel(impedances)
                if maxElec <= numel(impedances{i}.Electrode)
                    maxElec = numel(impedances{i}.Electrode);
                end
            end
            cData = [];
            for i = 1 : numel(impedances)
                cData(i).Subject = impedances{i}.Subject;
                cData(i).Ear = impedances{i}.Ear;
                cData(i).ImplantType = impedances{i}.ImplantType;
                cData(i).ImplantID = impedances{i}.ImplantID;
                cData(i).Date = impedances{i}.Date;
                %%initialize with maxElec  NaN (to ensure all implants give maxElec
                %electrodes.. this account for imlant with different
                %elecotrode number
                for j = 1 : maxElec 
                    cData(i).(strcat('El_',num2str(j))) = NaN;
                end
                %%now we fill with the right data
                for j = 1 : numel(impedances{i}.Electrode)
                    cData(i).(strcat('El_',num2str(j))) = round(impedances{i}.Impedance(j));
                end
            end
            tableGenerator('StructureParams', cData, ... 
                'SortBy',{'Subject', 'Ear', 'Date'});
        end
        function showComplianceMeasurement(this, varargin)
            compliance = this.getCompliance(varargin{:});
            %find max numer of electrodes
            maxVoltage = -inf;
            for i = 1 : numel(compliance )
                if maxVoltage <= numel(compliance{i}.Voltage)
                    maxVoltage = numel(compliance{i}.Voltage);
                end
            end
            cData = [];
            for i = 1 : numel(compliance )
                cData(i).Subject = compliance{i}.Subject;
                cData(i).Ear = compliance{i}.Ear;
                cData(i).Electrode = compliance{i}.Electrode;
                cData(i).ImplantType = compliance{i}.ImplantType;
                cData(i).ImplantID = compliance{i}.ImplantID;
                cData(i).Date = compliance{i}.Date;
                cData(i).ImpedanceFit = compliance{i}.ImpedanceFit;
                cData(i).Rsquared = compliance{i}.Rsquared;
                cData(i).ResidualTestPValue = compliance{i}.ResidualTest.PValue;
                
                %%initialize with maxVoltage  NaN (to ensure all implants give maxVoltage
                %electrodes.. this account for imlant with different
                %elecotrode number
                for j = 1 : maxVoltage 
                    cData(i).(strcat('V_',num2str(j))) = NaN;
                    cData(i).(strcat('C_',num2str(j))) = NaN;
                end
                %%now we fill with the right data
                for j = 1 : numel(compliance {i}.Voltage)
                    cData(i).(strcat('V_',num2str(j))) = compliance {i}.Voltage(j);
                    cData(i).(strcat('C_',num2str(j))) = compliance {i}.CurrentAmplitude(j);
                end
            end
            tableGenerator('StructureParams', cData, ... 
                'SortBy',{'Subject', 'Ear', 'Date'});
        end
        function resetChannel(this, channelNum)
            %reset stimuli
            this.Stimulus{channelNum}.Signal = [];
            this.Stimulus{channelNum}.Parameters = [];
            this.Stimulus{channelNum}.Parameters.Channel = channelNum;
            this.Stimulus{channelNum}.Parameters.SignalType = 'none';
            this.Stimulus{channelNum}.CI_Map = this.CI_Map{1};%% default map
            this.Stimulus{channelNum}.Calibration = this.Calibration{1};%% default calibration
            %reset module buttons
            cData.Channel = channelNum;
            cData.SignalType = 'none';
            cData.Module = this;
            cData.Calibration = this.Calibration{1};
            
            % update data handle of sequence buttons
            cHChb = findobj(this.HContainer,'Tag',strcat('pbSequence_', num2str(channelNum)));
            set(cHChb, 'UserData', cData);
            cHChb = findobj(this.HContainer,'Tag',strcat('pbStimulus_', num2str(channelNum)));
            set(cHChb,'UserData', cData);
            %update Map handles
            cHChb = findobj(this.HContainer,'Tag',strcat('puSetMap_', num2str(channelNum)));
            set(cHChb,'UserData', cData);
            this.hSequenciator.setAllChannelStimuliSettings(this.getStimuliSettings);
            this.hSequenciator.updateSequences('StimulusChannel', channelNum);
            %try to apply selected map is there is one
            this.applyMap(channelNum,  getCurrentPopUpElement(findobj(this.HContainer, 'Tag', strcat('puSetMap_',num2str(channelNum)))));
            notify(this,'evStimuliSettingsChanged');
            %     this.updateCalibrationItems;
            this.updateSignalInfo;
            this.updateEstimatedTime;
        end
        function sendTTLSignal(this, type)
            cSerialTTLconfig = this.getSerialTTLConfig;
            if ~cSerialTTLconfig.SendTrigger; return; end;
            try
                this.hTTLSerial.openPort(cSerialTTLconfig.SerialPort);
            catch err
                this.dispmessage(err.message)
                return;
            end
            switch type
                case 'Start'
                    this.hTTLSerial.sendTrigger('DTR', cSerialTTLconfig.StartDTR, ...
                        'DurationDTR', cSerialTTLconfig.StartDTRDuration, ...
                        'RTS', cSerialTTLconfig.StartRTS, ...
                        'DurationRTS', cSerialTTLconfig.StartRTSDuration)
                case 'End'
                    % we pause the stop event to make sure recording device
                    % has captured everything.
                    pause on 
                    pause(2);
                    this.hTTLSerial.sendTrigger('DTR', cSerialTTLconfig.EndDTR, ...
                        'DurationDTR', cSerialTTLconfig.EndDTRDuration, ...
                        'RTS', cSerialTTLconfig.EndRTS, ...
                        'DurationRTS', cSerialTTLconfig.EndRTSDuration)
            end
            
        end
        function value = getSerialTTLConfig(this)
            value.SendTrigger = get(findobj(this.HContainer, 'Tag', 'cbSendSerialTrigger'), 'Value');
            value.SerialPort = getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAvailableSerialPorts'));
            value.StartDTR = str2double(getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAtStartPin4')));
            value.StartDTRDuration = editString2Value(findobj(this.HContainer, 'Tag', 'edStartDurPin4'));
            value.StartRTS = str2double(getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAtStartPin7')));
            value.StartRTSDuration = editString2Value(findobj(this.HContainer, 'Tag', 'edStartDurPin7'));
            value.EndDTR = str2double(getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAtEndPin4')));
            value.EndDTRDuration = editString2Value(findobj(this.HContainer, 'Tag', 'edEndDurPin4'));
            value.EndRTS = str2double(getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAtEndPin7')));
            value.EndRTSDuration = editString2Value(findobj(this.HContainer, 'Tag', 'edEndDurPin7'));
        end
    end
    methods (Static)
        function value = getDefaultMap(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'DummyName', 'none');
            value.Subject = s.DummyName;
            value.Implant = [];
            value.Channel = [];
            value.Stimulus.Parameters.SignalType = 'none';
            value.MapName = 'dummy_map';
            value.ThresholdLevel = 0;
            value.ComfortableLevel = 0;
            value.ParameterLabel = 'Amplitude';
            value.Notes = '';
        end
        function value = getDefaultCalibration
            value.Name = 'nocalibration';
            value.CalibrationStimulus.SignalType = 'none';
            value.CalibrationRef = 0;
            value.CalibrationLevel = 0;
            value.CalibrationLabel = 'Amplitude';
            value.Notes = '';
        end
        function saveImpedanceMeasurement(measurement)
            outStruct.ImpedanceMeasurements.Measurement = measurement;
            modulePath = mfilename('fullpath');
            cPath = fileparts(modulePath);
            appendStruct2xmlFile(outStruct, ...
                strcat(cPath,filesep,'CI_Impedances.xml'));
        end
        function value = getImpedance(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Subject', '');
            value = [];
            %% read Implants (also subjects)
            cFile = strcat(mfilename('fullpath'),'.xml');
            cPath = fileparts(cFile);
            cImpedanceFile = strcat(cPath,filesep,'CI_Impedances.xml');
            if ~exist(cImpedanceFile, 'file'); return; end;
            cImpedances = convertxmlvalue2matvalue(xml2struct(cImpedanceFile));
            if isempty(s.Subject)
                value = cImpedances.ImpedanceMeasurements.Measurement{:};
            else
                if ~iscell(cImpedances.ImpedanceMeasurements.Measurement)
                    cImpedances.ImpedanceMeasurements.Measurement = {cImpedances.ImpedanceMeasurements.Measurement};
                end
                for i = 1 : numel(cImpedances.ImpedanceMeasurements.Measurement)
                    if isequal(cImpedances.ImpedanceMeasurements.Measurement{i}.Subject, s.Subject)
                        value{end + 1} = cImpedances.ImpedanceMeasurements.Measurement{i};
                    end
                end
            end
        end
        function saveComplianceMeasurement(measurement)
            outStruct.ComplianceMeasurements.Measurement = measurement;
            modulePath = mfilename('fullpath');
            cPath = fileparts(modulePath);
            appendStruct2xmlFile(outStruct, ...
                strcat(cPath,filesep,'CI_Compliance.xml'));
        end
        function value = getCompliance(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Subject', '');
            value = [];
            %% read Implants (also subjects)
            cFile = strcat(mfilename('fullpath'),'.xml');
            cPath = fileparts(cFile);
            cComplianceFile = strcat(cPath,filesep,'CI_Compliance.xml');
            if ~exist(cComplianceFile, 'file'); return; end;
            cCompliance = convertxmlvalue2matvalue(xml2struct(cComplianceFile));
            if isempty(s.Subject)
                value = cCompliance.ComplianceMeasurements.Measurement{:};
            else
                if ~iscell(cCompliance.ComplianceMeasurements.Measurement)
                    cCompliance.ComplianceMeasurements.Measurement = {cCompliance.ComplianceMeasurements.Measurement};
                end
                for i = 1 : numel(cCompliance.ComplianceMeasurements.Measurement)
                    if isequal(cCompliance.ComplianceMeasurements.Measurement{i}.Subject, s.Subject)
                        value{end + 1} = cCompliance.ComplianceMeasurements.Measurement{i};
                    end
                end
            end
        end
    end
end
