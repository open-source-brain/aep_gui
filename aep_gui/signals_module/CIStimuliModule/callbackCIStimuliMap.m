function callbackCIStimuliMap(hObject, ~)
cData = get(hObject, 'Userdata');
cCurrentMap = getCurrentPopUpElement(hObject);
this = cData.Module;
cStimuliSettings = this.Stimulus;
cMaps= [this.CI_Map{:}];
cidx = [];
for i = 1 : numel(cStimuliSettings)
    if cStimuliSettings{i}.Parameters.Channel == cData.Channel
        cidx = i;
        break
    end
end

switch cCurrentMap
    case 'new'
        %stop stimulation to remove events
        this.stopStimuli;
        createNewMap('Sender', cData, ... 
            'hCIDevice', this.hCIDevice, ...
            'HiddenFields', this.HiddenFields);
    otherwise
        this.applyMap(cData.Channel, cCurrentMap);
        if strcmp(cCurrentMap, 'nomap'); return; end;
        callbackCIStimulusReader(this, this.Stimulus{cData.Channel}.CI_Map.Stimulus);
%         cCal = this.Calibration{calIdx};
%         %% update stimulus calibration
%         this.Stimulus{cidx}.Calibration = cCal;
end
%% update sequences
%TODO ADD SEQUENCIES
% this.hSequenciator.updateCalibration('Stimulus', this.Stimulus{cidx});
