function callbackReadCISequenceFile(hObject, ~)
this = get(hObject, 'Userdata');
[filename, pathname] = uigetfile('*.xml','Select the sequence file', this.SequenceFilesPath);
if ~isequal(filename, 0)
    if ~this.hSequenciator.readSettings('Path', strcat(pathname,filename))
        return;
    end
    this.SequenceFilesPath = pathname;
end
this.loadAllSequenceStimuliSettings;
this.loadCurrentSequence;
