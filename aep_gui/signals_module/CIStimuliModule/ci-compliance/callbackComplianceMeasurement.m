function callbackComplianceMeasurement(hObject, ~)
this = get(hObject,'Userdata');
cCurrentValue = str2double(get(findobj(this.HContainer, 'Tag', 'edCurrentValue'), 'String')); 
cElectrode = str2double(get(findobj(this.HContainer, 'Tag', 'edElectrode'), 'String')); 
cNSteps = str2double(get(findobj(this.HContainer, 'Tag', 'edNumberSteps'), 'String'));
cAmplitudes = 0 : cCurrentValue/(cNSteps - 1): cCurrentValue;
cMeasuremnt = this.hCIDevice.measureCompliance('ElectStimulation', cElectrode, ...
    'ElectMeasurement', cElectrode, ...
    'CurrentAmplitudes', cAmplitudes , ...
    'Record', true, ...
    'HFigure', this.ComplianceAxes);

if isempty(cMeasuremnt); return; end;
this.Sender.saveComplianceMeasurement(cMeasuremnt);

