classdef measureNewCompliance < handle
    properties
        Map = [];
        HContainer = [];
        Sender = [];
        Topmargin = 0.05;
        UiHigh = 0.02;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        hCIDevice; %handle to CI devie
        ComplianceAxes = [];%axis to plot compliance measurement
    end
    methods
        function this = measureNewCompliance(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Sender', []);
            s = ef(s, 'hCIDevice', []);
            this.Sender = s.Sender;
            this.hCIDevice = s.hCIDevice;
            setComplianceWindow(this);
            uiwait(this.HContainer);
        end
    end
    
end

