function callbackAddMap(hObject, ~)
this = get(hObject, 'Userdata');
stimuliModule = this.Sender.Module;
cStimulus = this.Stimulus;
currentMaps = stimuliModule.CI_Map;
switch get(hObject,'Tag')
    case 'pbMapAccept_1'
        channel = 1;
    case 'pbMapAccept_2'
        channel = 2;
end
for i = 1 : numel(cStimulus)
    if isequal(cStimulus{i}.Parameters.SignalType, 'none'); continue; end;
    if cStimulus{i}.Parameters.Channel ~= channel; continue; end;
    cCI_Map.Subject = stimuliModule.CI_SelectedSubject;
    switch cStimulus{i}.Parameters.Channel 
        case 1%left implant
            cCI_Map.Implant = stimuliModule.hCIDevice.Implant.Left;
        case 2%right implant
            cCI_Map.Implant = stimuliModule.hCIDevice.Implant.Right;
    end
    cCI_Map.Channel = channel;
    cCI_Map.Stimulus.Parameters = cStimulus{i}.Parameters;
    cCI_Map.MapName = get(findobj(this.HContainer, 'Tag', strcat('edMapName_', num2str(channel))), 'String');
    cCI_Map.ThresholdLevel = str2double(get(findobj(this.HContainer, 'Tag', strcat('edThresholdLevel_', num2str(channel))), 'String'));
    cCI_Map.ComfortableLevel = str2double(get(findobj(this.HContainer, 'Tag', strcat('edComfortableLevel_', num2str(channel))), 'String'));
    cCI_Map.ParameterLabel = getCurrentPopUpElement(findobj(this.HContainer, 'Tag', strcat('puParameter2Map_', num2str(channel))));
    cCI_Map.Notes = get(findobj(this.HContainer, 'Tag', strcat('edMapNotes_',num2str(channel))), 'String');
    mapadded = false;
    for j = 1 : numel(currentMaps)
        if (isequal(cCI_Map.Subject, currentMaps{j}.Subject) && ...
                isequal(cCI_Map.MapName, currentMaps{j}.MapName) && ...
                isequal(cCI_Map.Channel, currentMaps{j}.Channel))
            choice = questdlg('A map with this name already exists. Do you wish to replace it?', ...
                'Map creation', ...
                'Yes', 'Cancel', 'Cancel');
            switch choice
                case {'Cancel', ''}
                    return;
            end
            stimuliModule.addCI_Map(cCI_Map, j);
            mapadded = true;
        end
    end
    if ~mapadded
        stimuliModule.addCI_Map(cCI_Map, []);
    end
end
stimuliModule.SaveSettings;