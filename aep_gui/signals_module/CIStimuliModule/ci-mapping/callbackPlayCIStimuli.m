function callbackPlayCIStimuli(hObject, ~)
this = get(hObject,'Userdata');
cStimulus = this.Stimulus;
this.hCIDevice.clearStimulationSequences;
for i = 1 : numel(cStimulus)
    if isequal(cStimulus{i}.Parameters.SignalType, 'none'); continue; end;
    [seq, sout] = eval(strcat(cStimulus{i}.Parameters.SignalType,'(cStimulus{i}.Parameters)'));
    if i == 1
        this.hCIDevice.saveAndLoadSequences('Ear','left', 'Sequence', seq, 'NRepetitions', sout.NRepetitions);
    else
        this.hCIDevice.saveAndLoadSequences('Ear','right', 'Sequence', seq, 'NRepetitions', sout.NRepetitions);
    end
end
this.hCIDevice.stopStimulation;
%% start stimulation
this.hCIDevice.startStimulation;