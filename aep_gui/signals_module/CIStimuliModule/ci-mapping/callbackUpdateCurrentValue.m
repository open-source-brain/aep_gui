function callbackUpdateCurrentValue(hObject, ~)
this = get(hObject, 'UserData');
cField = getCurrentPopUpElement(hObject);
switch get(hObject, 'Tag')
    case 'puParameter2Map_1'
        set(findobj(this.HContainer, 'Tag', 'edCurrentValue_1'), 'String', num2str(this.Stimulus{1}.Parameters.(cField)));
    case 'puParameter2Map_2'
        set(findobj(this.HContainer, 'Tag', 'edCurrentValue_2'), 'String', num2str(this.Stimulus{2}.Parameters.(cField)));
end
