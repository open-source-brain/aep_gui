classdef createNewMap < handle
    properties
        Map = [];
        HContainer = [];
        Sender = [];
        Topmargin = 0.05;
        UiHigh = 0.02;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        HideFields = {'Module','Fs', 'RoundToCycle', ...
            'SignalType', 'Channel', 'CycleRate', ...
            'ImplantType', 'TriggerDuration', 'SendTrigger', 'AltPhaseRate', ...
            'Description', 'Range', 'ImplantID'};
        hCIDevice; %handle to CI devie
        Stimulus; % stimuli to map
    end
    methods
        function this = createNewMap(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Sender', []);
            s = ef(s, 'hCIDevice', []);
            s = ef(s, 'HiddenFields', this.HideFields);
            this.Sender = s.Sender;
            this.hCIDevice = s.hCIDevice;
            this.Stimulus = s.Sender.Module.Stimulus;
            if setMapWindow(this)
                setGuiTheme('Figure', this.HContainer);
                uiwait(this.HContainer)
            end
        end
    end
    
end

