classdef ChannelSequence < handle
    properties
        hGui = [];%handles to visual objects
        IterationOrder = 1;
        IterationType = SequenceIterationType.Linear; % 1 Linear: 2 Logarithmic
        StartValue = 0;
        StepSize = 0;
        EndValue = 0;
        NumberSteps = 0;%% if set to zero sequence will be always played.
        CurrentStep = 1;
        CurrentSeqIndex = 1;%% index of sequence value to be used
        ParString;
        CurrentValue = 0;
        Channel = [];
        HContainer = [];
        HContainerPanel = [];%panel containing sequence gui
        StimulusVisibleSettings = []; %input settings show in gui
        StimulusHiddenFields = [];%input settings not showed in gui
        Calibration = [];
        GuiSettings = [];
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        Position; %position of container panel
        CycleCompleted = false; %true if cycle completed
        NCyclesCompleted = 0; %true if cycle completed
        SequenceTypeDef = SequenceType.Sequencial; % if true iteration will happen in a random order
        SequenceValues = 0; % used to save sequence's values array
        AutomaticSequence = false; %% used to know if a sequence is defined by the user or if this is automaticlly generated
        ID = [];% unique ID used to link sequences
        LinkIDs = -1; % list of of squences IDs
        SelectedLinkID = -1; %selected ID
        HideFields = {'Module','Fs', 'SignalType', 'Channel', 'ImplantType', 'ImplantID'}; %this specicy which filds of stimuli are not shown
            
    end
    properties (Access = private)
        cStepHistory = []; %used to save played values
    end
    events
        SequenceCompleted;
        SequenceChanged;
        SequenceDeleted;
    end
    methods
        function this = ChannelSequence(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Parameters', []);
            s = ef(s, 'Calibration', []);
            s = ef(s, 'GuiSettings', []);
            s = ef(s, 'ID', -1);
            s = ef(s, 'LinkIDs', -1);
            [cSettings, cHidden] = removeFields(s.Parameters, this.HideFields);
            
            this.StimulusHiddenFields = cHidden;
            this.StimulusVisibleSettings = cSettings;
            this.Calibration = s.Calibration;
            this.Channel = cHidden.Channel;
            this.ID = s.ID;
            this.LinkIDs = s.LinkIDs;
            cfname = fieldnames(cSettings);
            this.ParString = cfname{1};
            if ~isempty(s.GuiSettings)
                this.setGuiSettings(s.GuiSettings);
            end
            %initialize squence values
            this.updateSequenceValues;
        end
        function delete(this)
            if ishandle(this.HContainerPanel)
                delete(this.HContainerPanel);
            end
            notify(this,'SequenceDeleted');
        end
        function updateChannelSequence(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Parameters', []);
            s = ef(s, 'Calibration', []);
            s = ef(s, 'GuiSettings', []);
            s = ef(s, 'ID', -1);
            s = ef(s, 'LinkIDs', -1);
            [cSettings, cHidden] = removeFields(s.Parameters, this.HideFields);
            
            this.StimulusHiddenFields = cHidden;
            this.StimulusVisibleSettings = cSettings;
            this.Calibration = s.Calibration;
            this.Channel = cHidden.Channel;
            this.ID = s.ID;
            this.LinkIDs = s.LinkIDs;
            cfname = fieldnames(cSettings);
            this.ParString = cfname{1};
            if ~isempty(s.GuiSettings)
                this.setGuiSettings(s.GuiSettings);
            end
            %initialize squence values
            this.updateSequenceValues;
        end
        function value = getGuiSettings(this)
            value.IterationOrder = this.IterationOrder;
            value.StartValue = this.StartValue;
            value.StepSize = this.StepSize;
            value.EndValue = this.EndValue;
            value.NumberSteps = this.NumberSteps;
            value.CurrentStep = this.CurrentStep;
            value.Channel = this.Channel;
            value.CurrentValue = this.getCurrentValue;
            value.ParString = this.ParString;
            value.IterationType = int32(this.IterationType);
            value.SequenceTypeDef = int32(this.SequenceTypeDef); 
            value.AutomaticSequence = int32(this.AutomaticSequence);
            value.ID = this.ID;
            value.LinkIDs = this.LinkIDs;
            value.SelectedLinkID = this.SelectedLinkID;
        end
        function setGuiSettings(this, settings)
            cClassProperties = properties(class(this));
            for i = 1 : numel(cClassProperties)
                if isfield(settings, cClassProperties{i})
                    this.(cClassProperties{i}) = settings.(cClassProperties{i});
                end
            end
            this.updateGui;
        end
        function value = getChannelSettings(this)
            value.GuiSettings = this.getGuiSettings;
            value.HiddenFields = this.StimulusHiddenFields;
        end
        function callchannelGui(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'HContainer', []);
            s = ef(s, 'Position', [0, 0, 1, 1]);
            this.HContainer = s.HContainer;
            this.Position = s.Position;
            setSeqGui(this);
            this.updateGui;
        end
        
        function value = getNextSequence(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'SendNotification', true);
            s = ef(s, 'SeqIndex', []);
            %% now check if sequence is completed and reset
            this.CycleCompleted = (this.CurrentStep >= this.NumberSteps);
            if this.CycleCompleted
                this.NCyclesCompleted = this.NCyclesCompleted + 1;
                if s.SendNotification
                    notify(this, 'SequenceCompleted');
                end
                this.CurrentStep = 0;
                this.CurrentSeqIndex = 1;
                this.cStepHistory = [];
            end
            this.CurrentStep = this.CurrentStep + 1;
            %% update sequence position index
            this.CurrentSeqIndex = this.CurrentStep;
           
            %% if is random sequence, add current value to history 
            if this.SequenceTypeDef == SequenceType.Random
                this.CurrentSeqIndex = this.generateRandomStep;
                this.cStepHistory(end + 1) = this.CurrentSeqIndex;
            end
            value = this.getCurrentSequence;
        end
        function value = generateRandomStep(this)
            cStepsToDo = [];
            for i = 1 : this.NumberSteps
                if isempty(find(this.cStepHistory == i, 1))
                    cStepsToDo(end + 1) = i;
                end
            end
            rng('shuffle');
            randStepVal = cStepsToDo(round((numel(cStepsToDo) - 1)*rand(1,1)) + 1);
            value = randStepVal;
        end
        function value = getCurrentValue(this)
            cCurrentSeqIndex = max(1, min(this.CurrentSeqIndex, numel(this.SequenceValues)));
            if iscell(this.SequenceValues)
                value = this.SequenceValues{cCurrentSeqIndex};
            else
                value = this.SequenceValues(cCurrentSeqIndex);
            end
            if iscell(value)
                value = cell2mat(value);
            end
        end
        
        function value = convert2Linear(this, val)
            switch this.IterationType
                case SequenceIterationType.Linear %linear
                    value = double(val);
                case SequenceIterationType.Logarithmic %logarithmic
                    value = 10.^(val/20);
            end
        end
        
        function value = getCurrentSequence(this)
            value.Parameters = mergestruct(this.StimulusHiddenFields, this.StimulusVisibleSettings);
            value.Parameters.(this.ParString) = this.getCurrentValue;
            value.Calibration = this.Calibration;
        end
        function updateGui(this)
            %% if gui is open, update current value
            if ishandle(this.HContainerPanel) 
                cCurrentVal = this.getCurrentValue;
                value = cCurrentVal;
                if isnumeric(value)
                    set(findobj(this.HContainerPanel, 'Tag', 'txSeqCurrentVal'), 'String', num2str(value));
                else
                    set(findobj(this.HContainerPanel, 'Tag', 'txSeqCurrentVal'), 'String', value);
                end
                this.updateGuiComponents;
                drawnow;
            end
        end
        function resetSequence(this)
            this.CurrentStep = 1;%we reset to 1 and not to 0 to not skip the first seq
            this.CycleCompleted = false;
            this.NCyclesCompleted = 0;
            this.cStepHistory = [];
            this.CurrentSeqIndex = 1;            
            if this.SequenceTypeDef == SequenceType.Random
                this.CurrentSeqIndex = this.generateRandomStep;
                this.cStepHistory(end + 1) = this.CurrentSeqIndex;
            end
        end
        function updateSequenceValues(this)
            if this.AutomaticSequence
                if this.IterationType == SequenceIterationType.Linear
                    this.SequenceValues = this.StartValue + this.StepSize*(0 : this.NumberSteps - 1);
                else
                    this.SequenceValues = this.StartValue * this.StepSize .^(0 : this.NumberSteps - 1);
                end
                if isempty(this.SequenceValues)
                    this.SequenceValues = this.StartValue;
                end
            else
                this.SequenceValues = this.StartValue;
            end
        end
        function updateGuiComponents(this)
            chandles = this.hGui(ishandle(this.hGui));
            if this.AutomaticSequence
                set(findobj(chandles, 'Tag', 'edSeqStepSize'), 'Enable', 'On');
                set(findobj(chandles, 'Tag', 'edSeqEndValue'), 'Enable', 'On');
                set(findobj(chandles, 'Tag', 'edSeqNumberSteps'), 'Enable', 'On');
            else
                set(findobj(chandles, 'Tag', 'edSeqStepSize'), 'Enable', 'Off');
                set(findobj(chandles, 'Tag', 'edSeqEndValue'), 'Enable', 'Off');
                set(findobj(chandles, 'Tag', 'edSeqNumberSteps'), 'Enable', 'Off');
            end
        end
        function value = getLinkIDs(this)
            value = this.LinkIDs(this.LinkIDs ~= this.ID);
        end
        function updateLinkID(this)
            % update LinkID if link does not exits
            if  isempty(find(this.LinkIDs == this.SelectedLinkID, 1))
                this.SelectedLinkID = -1;% -1 for none
            end
        end
    end
end
