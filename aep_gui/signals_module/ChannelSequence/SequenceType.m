classdef SequenceType < uint32
    enumeration
        Sequencial (1);
        Random (2);
    end
end