function callbackAddSequence(hObject, ~)
cData = get(hObject, 'Userdata');
this = cData.Module;
if isequal(cData.SignalType, 'none')
    errordlg('You must define a stimulus', 'Sequenciator');
    return; 
end;
s.StimulusChannel = cData.Channel;
s.ID = max(this.hSequenciator.getSequenceIDs) + 1;
cAllChannelStimuliSettings = this.getStimuliSettings;
s.AllChannelStimuliSettings = cAllChannelStimuliSettings;
s.StimulatingDeviceConfiguration = this.hSoundModule.GetModuleSettings;
this.hSequenciator.addSequence(s);
this.hSequenciator.plotGui;