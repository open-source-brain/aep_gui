function callbackCalibration(hObject, ~)
cData = get(hObject, 'Userdata');
cCurrentCal = getCurrentPopUpElement(hObject);
this = cData.Module;
cStimuliSettings = this.Stimulus;
cCals = this.Calibration;
cidx = [];
for i = 1 : numel(cStimuliSettings)
    if cStimuliSettings(i).Parameters.Channel == cData.Channel
        cidx = i;
        break
    end
end

switch cCurrentCal
    case 'new'
        handCal = createNewCalibration('Sender', cData);
        cCal = handCal.Calibration;
        delete(handCal);
        if ~isempty(cCal)
            calIdx = find(ismember({cCals.Name}, cCal.Name), 1);
            %% update stimulus calibration
            this.Stimulus(cidx).Calibration = cCal;
            this.Stimulus(cidx).Parameters.refSPL = cCal.CalibrationRef;
            % update calibration list
            this.addCalibration(cCal, calIdx);
            this.updateCalibrationItems;
            % save stimuli module to keep calibration
            this.SaveSettings;
        else
            cItems = get(hObject, 'String');
            objectPos = 1;
            if ~isempty(cStimuliSettings)
                objectPos = find(ismember(cItems, cStimuliSettings(cidx).Calibration.Name), 1);
            end
            set(hObject, 'Value', objectPos);
            return;
        end
    otherwise
        calIdx = find(ismember({cCals.Name}, cCurrentCal), 1);
        cCal = this.Calibration(calIdx);
        %% update stimulus calibration
        this.Stimulus(cidx).Calibration = cCal;
        this.Stimulus(cidx).Parameters.refSPL = cCal.CalibrationRef;
end

%update selected stimulus
this.updateStimulus(this.Stimulus(cidx));

%% update sequences
this.update_all_sequence_stimuli();
this.updateCalibrationItems;
this.updateSignalInfo;