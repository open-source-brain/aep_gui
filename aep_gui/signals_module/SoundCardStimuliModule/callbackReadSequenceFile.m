function callbackReadSequenceFile(hObject, ~)
this = get(hObject, 'Userdata');
[filename, pathname] = uigetfile('*.json','Select the sequence file', this.SequenceFilesPath);
if ~isequal(filename, 0)
    this.hSequenciator.readSettings('Path', strcat(pathname,filename));
    this.SequenceFilesPath = pathname;
end
try
    %set stimuli to those found in sequence file
    this.Stimulus = this.hSequenciator.getAllChannelStimuliSettings;
     % make sure all Stimulus have  a Signal
    for i = 1: numel(this.Stimulus)
        if ~isfield(this.Stimulus(i), 'Signal')
            this.Stimulus(i).Signal = [];
        end
    end
    cStimulitingDeviceSettings = this.hSequenciator.getStimulatingDeviceConfiguration;
    if ~isempty(cStimulitingDeviceSettings)
        % try set current soundcard as in sequence file
        this.hSoundModule.readSettingsFromStructure(cStimulitingDeviceSettings);
    end
catch err
    errordlg(strcat('Sound device could not be configurated as in sequence file.', err.message));
end
% make sure sequenciator has the current soundcard
this.hSequenciator.setStimulatingDeviceConfiguration(this.hSoundModule.GetModuleSettings);
%
this.loadAllSequenceStimuliSettings;
this.loadCurrentSequence;
