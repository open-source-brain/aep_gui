function callbackSetPresentationTime(hObject, ~)
this = get(hObject, 'Userdata');
value = str2double(get(hObject, 'String'));
if ~isnan(value)
    value = max(0, value * 60);
    this.PresentationTime = value;
end
this.updateEstimatedTime;