function callbackStimulusReader(this,settings)
    cparams = settings.Parameters;
    cparams.Fs = this.hSoundModule.getParameters.Fs;
    cOutChannels = this.hSoundModule.getParameters.OutputChannels;
    if isempty(cparams.Channel) || isempty(cOutChannels == cparams.Channel)
        return;
    end
    cIdx = this.hSoundModule.smGetChannelOutPosition(cparams.Channel);
    if isequal(cparams.SignalType, 'none') || isequal(cparams.SignalType, 'slave-signal')
        return;
    else
        [cStimuli,~, cSettings] = eval(strcat(cparams.SignalType,'(cparams)'));
    end
    %% try finding current stimulus
    cCallingChannel = [];
    for i = 1 : numel(this.Stimulus)
        if ~isfield(this.Stimulus(i), 'Parameters') 
            continue; 
        end
        if this.Stimulus(i).Parameters.Channel == cparams.Channel
            cCallingChannel = i;
        end
    end
    if isempty(cCallingChannel)
        cCallingChannel = numel(this.Stimulus) + 1;
    end
    current_channel = cCallingChannel;
    if ~isempty(cStimuli)
        stimNChannels = size(cStimuli, 2);
        %% mono or multichannel output
        for i = 1 : stimNChannels
            %% here we update the data of the calling object
            cData = cSettings;
            cData.Module = this;
            cData.Channel = cOutChannels(cIdx);
            if i > 1 
                current_channel = current_channel + 1;
                cData.SignalType = 'slave-signal';
                cData.Channel = cOutChannels(find(cOutChannels == cOutChannels(cIdx + i - 1), 1));
            else
                cData.SignalType = cparams.SignalType;
            end
            if isempty(cStimuli)
                cCurrStimChan = [];
            else
                cCurrStimChan = cStimuli(:,i);
            end
            %% we now update plot checkbox data
            cHChb = findobj(this.HContainer,'Tag',strcat('chPlot_',num2str(cData.Channel)));
            set(cHChb, 'UserData',cData);
            %% we now update buttons data
            cHbt = findobj(this.HContainer,'Tag',strcat('pbStimulus_',num2str(cData.Channel)));
            set(cHbt, 'UserData',cData);
            %% we now update sequence data
            cHbt = findobj(this.HContainer,'Tag',strcat('pbSequence_',num2str(cData.Channel)));
            set(cHbt, 'UserData',cData);
            %% we now update calibration popup data
            cHbt = findobj(this.HContainer,'Tag',strcat('puCalibration_',num2str(cData.Channel)));
            set(cHbt, 'UserData',cData);
            %% pass buffers to handles
            
            this.Stimulus(current_channel).Signal = cCurrStimChan;
            this.Stimulus(current_channel).Parameters = cData;
            this.Stimulus(current_channel).Calibration = settings.Calibration;
        end
    end  
    % make sure all Stimulus have  a Signal
    for i = 1: numel(this.Stimulus)
        if ~isfield(this.Stimulus(i), 'Signal')
            this.Stimulus(i).Signal = [];
        end
    end
    notify(this,'evStimuliSettingsChanged');