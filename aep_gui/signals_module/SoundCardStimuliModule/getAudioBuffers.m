function buffers = getAudioBuffers(this)
    buffers = [];
    cSoundSettings = this.hSoundModule.smGetSoundDeviceConfiguration;
    cDefinedSignals = numel(this.Stimulus);
    cNChannels  = size(cSoundSettings.SelectedChannels,2);
    %used to set flat buffer when number signal is lower than number of
    %channels to use
    cDummy = cell(1,cNChannels-cDefinedSignals);
    if isempty(this.Stimulus); return; end
    cOutChannels = this.hSoundModule.getParameters.OutputChannels;
    cChannels = [];
    chMap = [];
    for i = 1 : numel(cOutChannels)
        chMap(i).Assigned = false;
        chMap(i).Channel = cOutChannels(i);
        chMap(i).StimulusIdx = [];
        for j = 1 : numel(this.Stimulus)
            if cOutChannels(i) == this.Stimulus(j).Parameters.Channel
                chMap(i).Assigned = true;
                chMap(i).StimulusIdx = j;
            end
        end
    end
    buffers = zeros(1, numel(cOutChannels));
    for i = 1 : numel(chMap)
        if chMap(i).Assigned
            aux = this.Stimulus(chMap(i).StimulusIdx).Signal;
            if isempty(aux)
                aux = nan;
            end
            % check size of other channels, otherwise pad with zeros
            if size(buffers, 1) < size(aux, 1)
                buffers = padarray(buffers, [size(aux, 1) - size(buffers, 1), 0], 0, 'post');
            elseif size(buffers, 1) > size(aux, 1)
                aux = padarray(aux, [size(buffers, 1) - size(aux, 1), 0], 0, 'post');
            end
            buffers(:, i) = aux;
        else
            buffers(:, i) = nan;
        end
    end
    buffers(isnan(buffers)) = 0;