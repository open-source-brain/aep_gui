classdef StimuliModule < handle
    properties(Abstract)
        Stimulus;
        hSequenciator;% handle to sequenciator class
        hStopEvent;% handle to stop listener
    end
    events
        evStimuliSettingsChanged;
        evNotifyStatus;
        evStimulatorDeviceStopped;
    end
    methods(Abstract)
        setStimuliModuleGui(this);
        value = getClassSettings(this);
        settings = GetModuleSettings(this);
        SaveSettings(this);
        ReadSettings(this);
        playStimuli(this);
        stopStimuli(this);
        playSequence(this)
        sendTTLSignal(this);
        dispmessage(this);
        getStimuliSettings(this);
        fillBuffers(this);
        receiveMessage(this);
    end
end
