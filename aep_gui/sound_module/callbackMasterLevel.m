function callbackMasterLevel(hObject, ~)
	value = reshape(str2num(get(hObject,'String')), 1, []); %#ok<ST2NM,ST3NM>
	if ~isempty(value)
		set(hObject,'Value', min(max(-inf, value), 2));
	end
	set(hObject,'String',num2str(get(hObject,'Value')));