function callbackSoundDeviceProperties(hObject, ~)
	this = get(hObject,'Userdata');
	didx = get(hObject,'Value');
    if numel(this.Sound_devices) < didx
        %if does not find the soundcard, it set this to default value
        set(hObject, 'Value' , 1);
        didx = 1;
    end
    this.CurrentSoundDevice  = this.Sound_devices(didx);
    set(findobj(this.HContainer,'Tag','lbCurrentSoundDevice'), ...
		'String',struct2text(this.CurrentSoundDevice));

    cNchannels = max(this.CurrentSoundDevice.NrInputChannels, ...
        this.CurrentSoundDevice.NrOutputChannels);
    cPos = get(findobj(this.HContainer,'Tag','puSoundChannesl2use'), ...
		'Value');
	set(findobj(this.HContainer,'Tag','puSoundChannesl2use'), ...
		'String',{1:cNchannels});
    set(findobj(this.HContainer,'Tag','puSoundChannesl2use'), ...
		'Value',min(cNchannels,cPos));
    this.updateInputOutputChannels;