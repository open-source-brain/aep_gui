function callbackUpdateChannelLevels(hObject, ~)
this = get(hObject,'Userdata');
value = reshape(str2num(get(hObject,'String')), 1, []); %#ok<ST2NM>
if ~isempty(value)
    value = min(value, 0);
    cChnnels2Use = get(findobj(this.HContainer,'Tag', ...
        'puSoundChannesl2use'),'Value');
    cNCh = size(value, 2);
    if  cNCh <= cChnnels2Use
        cval = [];
        if cNCh < cChnnels2Use 
            cval = zeros(1, cChnnels2Use - cNCh);
        end
        set(hObject,'Value', [value, cval]);
    else
        set(hObject,'Value', value(1:cChnnels2Use));
    end
end
set(hObject,'String',num2str(get(hObject,'Value')));