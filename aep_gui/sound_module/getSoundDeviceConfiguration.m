function value = getSoundDeviceConfiguration(this)
value = this.CurrentSoundDevice;
for i = 1:numel(this.SoundObjects)
    switch this.SoundObjects{i}
        case 'puSelectedDeviceFs'
            cfs = str2double(get(findobj(this.HContainer,'Tag','puSelectedDeviceFs'),'String'));
            value.Fs = cfs(get(findobj(this.HContainer,'Tag','puSelectedDeviceFs'),'Value'));
        case 'puMode'
            cmode = get(findobj(this.HContainer,'Tag','puMode'),'String');
            value.Mode = cmode(get(findobj(this.HContainer,'Tag','puMode'),'Value'));
        case 'puSoundChannesl2use'
            value.NChannels2Use = get(findobj(this.HContainer,'Tag','puSoundChannesl2use'),'Value');
        case 'edSelectOutputChannels'
            value.SelectedChannels(1,:) = get(findobj(this.HContainer,'Tag','edSelectOutputChannels'),'Value');
        case 'edSelectInputChannels'
            value.SelectedChannels(2,:)  = get(findobj(this.HContainer,'Tag','edSelectInputChannels'),'Value');
        case 'edMasterLevel'
            value.MasterLevel = get(findobj(this.HContainer,'Tag','edMasterLevel'),'Value');
        case 'edChannelLevels'
            value.ChannelLevels = get(findobj(this.HContainer,'Tag','edChannelLevels'),'Value');
    end
end