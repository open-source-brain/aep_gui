classdef bicDummyClass < handle
	properties
		Average;
        Time;
        RN;
        SNR;
        UserData;
        TotalSweeps;
    end
    methods
        function this = bicDummyClass(data)
            this.Average = data.Average;
            this.Time = data.Time ;
            this.RN = data.RN;
            this.SNR = data.SNR;
            this.UserData = data.UserData;
            this.TotalSweeps = data.TotalSweeps;
        end
    end
end