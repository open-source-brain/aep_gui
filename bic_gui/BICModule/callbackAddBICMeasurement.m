function callbackAddBICMeasurement(hObject, ~)
this = get(hObject, 'Userdata');
[value, ~] = getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'lbBICFiles'));
[path,filename,ext] = fileparts(value);
if ~isequal(ext, '.tdtdata'); return; end;
switch get(hObject, 'tag')
    case 'pbAddMeasurement1'
        this.PathMeasurementMonaural1 = strcat(pwd,filesep,value);
    case 'pbAddMeasurement2'
        this.PathMeasurementMonaural2 = strcat(pwd,filesep,value);
    case 'pbAddMeasurement3'
        this.PathMeasurementBinaural = strcat(pwd,filesep,value);
end
this.updategui;       
