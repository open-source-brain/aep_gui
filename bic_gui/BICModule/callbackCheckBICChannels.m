function callbackCheckBICChannels(hObject, ~)
this = get(hObject, 'Userdata');
cObjectVal = editString2Value(hObject);
if (~isempty(find(isnan(cObjectVal) ==1, 1)))
    setEditStringValue(hObject, get(hObject, 'Value'));
    return;
end
switch get(hObject, 'Tag')
    case 'etChannels'
        this.Channels = cObjectVal;
    case 'etVirtualChannels'
        this.VirtualChannels = cObjectVal;
end
setEditStringValue(hObject, cObjectVal);