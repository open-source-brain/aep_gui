function callbackSelectBICListItem(hObject, ~)
this = get(hObject, 'Userdata');
hFigParent = getParentFigure(hObject);
[value, ~] = getCurrentPopUpElement(hObject);
%% If double click
if strcmp(get(hFigParent, 'SelectionType'),'open')
    if  isdir(value)
        cd(value);
        % Load list box with new folder.
        set(hObject, 'Value', 1);
        set(hObject, 'String', this.getDirectoryFiles)
    else
        [~,~,ext] = fileparts(value);
        switch ext
            case '.tdtdata'
            otherwise
        end
    end
else
    [~,name,ext] = fileparts(value);
    switch ext
        case '.tdtdata'
        otherwise
            return;
    end
    if ~exist(strcat(name,'.xml'),'file')
        cDataReader = TdtDataReader(value);
        cMeasurements.Settings = cDataReader.MeasurementSettings;
        struct2xml(cMeasurements, strcat(name,'.xml'));
    end
    cSettings = convertxmlvalue2matvalue(xml2struct(strcat(name,'.xml')));
    set(findobj(this.HContainer, 'Tag', 'lbBICFileInfo'), 'String', struct2text(cSettings.Settings.StimuliModule));
end