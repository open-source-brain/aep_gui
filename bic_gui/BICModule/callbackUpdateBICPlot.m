function callbackUpdateBICPlot(hObject, ~)
this = get(hObject, 'Userdata');
this.TimeOffset = str2num(get(findobj('Tag', 'etBICTimeOffset'), 'String'));
this.plotMeasurements;