function varargout = bic_gui(varargin)
% BIC_GUI MATLAB code for bic_gui.fig
%      BIC_GUI, by itself, creates a new BIC_GUI or raises the existing
%      singleton*.
%
%      H = BIC_GUI returns the handle to a new BIC_GUI or the handle to
%      the existing singleton*.
%
%      BIC_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BIC_GUI.M with the given input arguments.
%
%      BIC_GUI('Property','Value',...) creates a new BIC_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bic_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bic_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bic_gui

% Last Modified by GUIDE v2.5 18-Nov-2013 10:23:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bic_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @bic_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bic_gui is made visible.
function bic_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bic_gui (see VARARGIN)

% Choose default command line output for bic_gui
handles.output = hObject;
handles.ObjectDimentions.topmargin = 0.05 ;
handles.ObjectDimentions.uiHigh = 0.03; %desired high in pixels
handles.ObjectDimentions.uiWidth = 0.1; %desired widh in pixels
handles.hTabGroup = uitabgroup;
handles.tabBIC= uitab('parent', handles.hTabGroup, ...
    'title','BIC Module');

%% set BIC module
handles.BICModule = BICModule('HContainer',handles.tabBIC, ...
    'Topmargin', handles.ObjectDimentions.topmargin, ...
    'UiHigh', handles.ObjectDimentions.uiHigh, ...
    'UiWidth',handles.ObjectDimentions.uiWidth);
%% set ui colors
brColor = [170,202,230]/256;
set(0,'defaultUicontrolBackgroundColor',brColor);
hbc = findobj(hObject, '-property','BackgroundColor','-and','-not','Type','uitab');
set(hbc,'BackgroundColor',brColor);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bic_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = bic_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
