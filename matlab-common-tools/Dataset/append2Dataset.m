function append2Dataset(varargin)
s = parseparameters(varargin{:});
s = ef(s , 'DataSetPath', '');
s = ef(s , 'Data', '');
s = ef(s , 'IDField', '');
cDataSet = [];

if exist(s.DataSetPath, 'file')
    auxdata = open(s.DataSetPath);
    cDataSet = auxdata.cDataSet;
end
%% make sure not adding repeated data
existData = false;
for i = 1 : size(cDataSet,1)
    if isequal(cDataSet.(s.IDField), s.Data.(s.IDField));
        existData = true;
        break;
    end
end
if ~existData
    cDataSet = [cDataSet; s.Data];
    save(s.DataSetPath, 'cDataSet');
end

