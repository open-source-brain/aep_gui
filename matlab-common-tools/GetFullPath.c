// GetFullPath.c
// GetFullPath - Get absolute path of a file or folder [WINDOWS]
// FullName = GetFullPath(Name)
// INPUT:
//   Name: String, file or folder name with relative or absolute path.
//         Unicode characters and UNC paths are supported.
//         Up to 8192 characters are allowed here, but some functions of the
//         operating system may support 260 characters only.
//
// OUTPUT:
//   FullName: String, file or folder name with absolute path. "\." and "\.."
//         are processed such that [FullName] is fully qualified.
//         If the input [Name] is empty, the current directory is replied.
//         The created path need not exist or be valid.
//
// NOTE: The called Mex function calls the Windows-API, therefore this is not
//   compatible to MacOS and Linux, but really fast.
//   The magic initial key '\\?\' is inserted on demand to support names
//   exceeding MAX_PATH characters as defined by the operating system.
//
// EXAMPLES:
//   cd(tempdir);
//   GetFullPath('File.Ext')         % ==>  'C:\Temp\File.Ext'
//   GetFullPath('..\File.Ext')      % ==>  'C:\File.Ext'
//   GetFullPath('..\..\File.Ext')   % ==>  'C:\File.Ext'
//   GetFullPath('.\File.Ext')       % ==>  'C:\Temp\File.Ext'
//   GetFullPath('*.txt')            % ==>  'C:\Temp\*.txt'
//   GetFullPath('..')               % ==>  'C:\'
//   GetFullPath('Folder\')          % ==>  'C:\Temp\Folder\'
//   GetFullPath('\\Server\Folder\Sub\..\File.ext')
//                                   % ==>  '\\Server\Folder\File.ext'
//
// COMPILE:
//   mex -O GetFullPath.c
//   Linux: mex -O CFLAGS="\$CFLAGS -std=C99" GetFullPath.c
//   Pre-compiled: http://www.n-simon.de/mex
//
// Tested: Matlab 6.5, 7.7, WinXP, 32bit
// Compiler: LCC 2.4/3.8, OpenWatcom 1.8, BCC 5.5, MSVC 2008
// Author: Jan Simon, Heidelberg, (C) 2009-2010 matlab.THISYEAR(a)nMINUSsimon.de
//
// See also: Rel2AbsPath, CD, FULLFILE, FILEPARTS.

/*
% $JRev: R0g V:006 Sum:5Hk7AzT1VvaO Date:21-Jul-2010 11:43:52 $
% $License: BSD $
% $UnitTest: TestGetFullPath $
% $File: Tools\Mex\Source\GetFullPath.c $
% History:
% 001: 19-Apr-2010 01:23, Successor of Rel2AbsPath.
%      No check of validity in opposite to Rel2AbsPath.
*/

#if defined(WIN32) || defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#else
#error Sorry: Implemented for Windows only now!
#endif

#include "mex.h"
#include <wchar.h>

// Assume 32 bit addressing for Matlab 6.5:
// See MEX option "compatibleArrayDims" for MEX in Matlab >= 7.7.
#ifndef MWSIZE_MAX
#define mwSize  int32_T           // Defined in tmwtypes.h
#define mwIndex int32_T
#define MWSIZE_MAX MAX_int32_T
#endif

// Error messages do not contain the function name in Matlab 6.5! This is not
// necessary in Matlab 7, but it does not bother:
#define ERR_HEAD "GetFullPath: "
#define ERR_ID   "JSimon:GetFullPath:"

// Static buffer and magic key to turn off path parsing:
wchar_t Buffer[MAX_PATH + 1],
        *MagicKey = L"\\\\?\\UNC";

// Do not accept ridiculous long file names:
#define MAXFILELEN_INPUT 8191L

// Main function ===============================================================
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  wchar_t *FileName, *FullPath;
  mwSize  NameLen, Offset, KeyLen, FullPathLen, dims[2] = {1L, 0L};
  bool    freeFullPath, freeFileName;
  
  // Check number of inputs and outputs:
  if (nrhs != 1 || nlhs > 1) {
     mexErrMsgIdAndTxt(ERR_ID   "BadNInput",
                       ERR_HEAD "1 input required, 1 output allowed.");
  }
  
  // Get file name from inputs as terminated unicode string: -------------------
  if (!mxIsChar(prhs[0])) {
     mexErrMsgIdAndTxt(ERR_ID   "BadInputType",
                       ERR_HEAD "Input [FileName] must be a string.");
  }
  
  NameLen = mxGetNumberOfElements(prhs[0]);
  if (NameLen == 0) {          // Empty input => CD: ---------------------------
     FileName     = L".";
     freeFileName = false;
     
  } else if (NameLen <= MAXFILELEN_INPUT) {
     // Copy string to Unicode string with a terminator:
     freeFileName = true;
     if ((FileName = (wchar_t *) mxMalloc((NameLen + 1) * sizeof(wchar_t)))
         == NULL) {
        mexErrMsgIdAndTxt(ERR_ID   "NoMemory",
                          ERR_HEAD "No memory for FileName.");
     }
     memcpy(FileName, mxGetData(prhs[0]), NameLen * sizeof(wchar_t));
     FileName[NameLen] = L'\0';
  
  } else {                     // Ridiculous input: ----------------------------
     mexErrMsgIdAndTxt(ERR_ID   "BadInputSize",
                       ERR_HEAD "FileName is too long.");
  }
  
  // Call Windows API to get the full path: ------------------------------------
  FullPathLen = GetFullPathNameW(
                      (LPCWSTR) FileName,  // address of file name
                      MAX_PATH,            // buffer length
                      (LPWSTR) Buffer,     // address of path buffer
                      NULL);               // address of filename in path
  
  if (FullPathLen == 0) {                  // GetFullPathName failed:
     mexErrMsgIdAndTxt(ERR_ID   "GetFullName_Failed_Static",
                       ERR_HEAD "GetFullPathName failed.");
                       
  } else if (FullPathLen <= MAX_PATH) {    // FullPath matches in static buffer:
     FullPath     = Buffer;
     freeFullPath = false;
    
  } else {     // Static buffer was too small - create a dynamic buffer instead:
     // Maximal 8 additional characters: "\\?\UNC" and terminator:
     freeFullPath = true;
     FullPath     = (wchar_t *) mxCalloc(FullPathLen + 8, sizeof(wchar_t));
     if (FullPath == NULL) {
        mexErrMsgIdAndTxt(ERR_ID   "NoMemory",
                          ERR_HEAD "No memory for dynamic buffer.");
     }
     
     // If FileName starts with "\\" it is a UNC path:
     KeyLen = 4;
     Offset = 4;
     if (FullPathLen >= 2) {
         if (memcmp(FileName, MagicKey, 2 * sizeof(wchar_t)) == 0) {
            KeyLen = 7;
            Offset = 6;
         }
     }

     // Call Windows API again to get the full path:
     FullPathLen = GetFullPathNameW(
                      (LPCWSTR) FileName,   // address of file name
                      FullPathLen,          // buffer length
                      FullPath + Offset,    // address of path buffer
                      NULL);                // address of filename in path
     
     if (FullPathLen == 0) {  // GetFullPathName failed:
        mexErrMsgIdAndTxt(ERR_ID   "GetFullName_Failed_Dynamic",
                          ERR_HEAD "GetFullPathName failed.");
     }
     
     // Either insert leading key "\\?\" or "\\?\UNC":
     FullPathLen += Offset;
     memcpy(FullPath, MagicKey, KeyLen * sizeof(wchar_t));
  }
  
  // Create output: ------------------------------------------------------------
  dims[1] = FullPathLen;
  plhs[0] = mxCreateCharArray(2, dims);
  memcpy(mxGetData(plhs[0]), FullPath, FullPathLen * sizeof(mxChar));
  
  // Cleanup: ------------------------------------------------------------------
  if (freeFileName) {
     mxFree(FileName);
  }
  if (freeFullPath) {
     mxFree(FullPath);
  }
  
  return;
}
