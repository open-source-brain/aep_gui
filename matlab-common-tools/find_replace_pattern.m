function find_replace_pattern(varargin)
% example
% org_file_name = 'C:\Users\jundurraga\Downloads\Loudness_balancing\75-150pps\SYMA-AltPS\AltPS_150pps_SYMA_150pps_RG1_adj.adj';
% patterns = {'defaultPeriod=', 'defaultPeriod='};
% new_values = {'10', '9'};
% [~, file_name, file_ext] = fileparts(s.org_file_name);
% file_path = GetFullPath(file_name);
% new_file_name = strcat(file_path, filesep, file_name, '_low', file_ext);
% find_replace_pattern('org_file_name', org_file_name, 'patterns', patterns, 'new_values', new_values, 'new_file_name', new_file_name)
s = parseparameters(varargin{:});
s = ef(s, 'org_file_name', '');
s = ef(s, 'patterns', {''});
s = ef(s, 'new_values', {''});
s = ef(s, 'new_file_name', '');
s = ef(s, 'replace_rescursively', true);

if numel(s.patterns) ~= numel(s.new_values)
    disp('the numner of patterns should match the number of values!!!');
    return
end
fid = fopen(s.org_file_name, 'r');
fidnew = fopen(s.new_file_name, 'w+');
tline = fgetl(fid);
pattern_count = 1;
c_p_id = 1;
while ~isequal(tline, -1)
    n_line = tline;
    matched = regexp(n_line, s.patterns{c_p_id}, 'split');
    if numel(matched) == 2
        n_line = strcat(s.patterns{c_p_id}, s.new_values{c_p_id});
        disp(strcat(tline,'~ Replaced by ~', n_line));
        pattern_count = pattern_count + 1;
        c_p_id = mod(pattern_count + 1, numel(s.patterns)) + 1;
    end
    fprintf(fidnew, strcat(strrep(n_line,filesep,'\\'), '\n'));
    tline = fgetl(fid);
end
fclose(fid);
fclose(fidnew);