function value = editString2Value(hObject)
str_value = get(hObject, 'String');
value = str2num(str_value);
if isempty(value)
    value = str2cell(str_value);
    value = value{1};
end

if isempty(value)
    value = regexp(strtrim(str_value),'\s+','split');
end
