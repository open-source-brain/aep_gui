function setEditStringValue(hObject,value)
if isnumeric(value)
    set(hObject, 'String', num2str(value));
    set(hObject, 'Value', value);
else
    if iscell(value)
        text = '';
        for i = 1 : numel(value)
            text = [text , ' ', value{i}];
        end
    else
        text = value;
    end
    set(hObject, 'String', text);
end
