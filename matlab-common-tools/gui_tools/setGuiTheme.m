function setGuiTheme(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Figure', []);
s = ef(s, 'TabGroup', []);
% brColor = [0.2,0.2,0.2];
% set(s.Figure, 'defaultUicontrolBackgroundColor',brColor);
% set(s.Figure, 'Color', brColor);
% hbc = findobj(s.Figure, '-property','BackgroundColor','-and','-not','Type','uitab');
% set(hbc,'BackgroundColor',brColor);
% if ~isempty(s.TabGroup)
%     set(s.TabGroup.Children, 'BackgroundColor',brColor);
% end
% fontColor = [1, 1, 1];
% set(hbc,'ForegroundColor',fontColor);
% set(s.Figure,'DefaultAxesXColor',fontColor);
% set(s.Figure,'DefaultAxesYColor',fontColor);
% set(findobj(s.Figure,'type', 'axes'), 'XColor', fontColor);
% set(findobj(s.Figure,'type', 'axes'), 'YColor', fontColor);
% set(findobj(s.Figure,'type', 'axes'), 'ZColor', fontColor);