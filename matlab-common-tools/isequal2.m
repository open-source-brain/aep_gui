function value = isequal2(value1, value2, tolerancePercentage)
if isequal(value1, value2)
    value = true;
    return;
end
vmax = max([value1, value2]);
vmin = min([value1, value2]);
value = abs((vmax - vmin)/vmax) < tolerancePercentage;