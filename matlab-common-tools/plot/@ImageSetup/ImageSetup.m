%example
% ImageSetup - Class to manipulate figures
% This class allows to manipulate open figures and save them in different
% formats. The differents parameters of the figures can be adjusted and
% then applied by using h.prepareAllFigures.
% Syntax:
%   Example
%   figure; plot(rand(1,100))
%   figure; plot(rand(1,100))
%   h = ImageSetup;
%   h.I_FontSize = 14;
%   h.I_FontName = 'Arial';
%   h.I_Width = 8;
%   h.I_High= 8;
%   h.I_TitleInAxis = 1;
%   h.I_Space = [0.01,0.01];
%   h.I_Ylim = [-1,1];
%   h.I_Grid = 'off';
%   h.I_KeepColor = 0;
%   h.prepareAllFigures
%
% Subfunctions: ImageSetup
%%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% You should have received a copy of the GNU General Public zLicense
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% Copyright 2012 Jaime Undurraga <jaime.undurraga@gmail.com>.
%%

classdef ImageSetup < handle
    properties (Dependent)
        I_Width;
        I_High;
        I_Unit;
        I_DPI;
        I_FontSize;
        I_FontName;
        I_Legend;
        I_LegendLocation;
        I_Xlabel;
        I_Ylabel;
        I_Title;
        I_Box;
        I_Grid;
        I_Handles;
        I_AutoYlim;
        I_AutoXlim;
        I_KeepColor;
        I_LineWidth;
        I_TitleInAxis;
        I_AlignAxesTexts;
        I_LegendBox;
        I_Matrix;
        I_Ylim;
        I_Space;
        I_Xlim;
        ResetLineWidth;
        OptimizeSpace;
    end
    
    properties (GetAccess = private)
        verAddedHandles;
        horAddedHandles;
        arrayAddedHandles;
        hOutFig;
        updateContainerFigure;
    end
    properties (Access = private)
        cI_Width = 17.81;% width of the figure;Hearing research 17.8 double column and 8.4single column
        cI_High = 16.01;%high of the figure
        cI_Unit = 'centimeters';%units to set with and high
        cI_DPI = 300;%this DPI will be use to save figures
        cI_FontSize = 8;%size of fonts in the figure
        cI_FontName = 'Arial';%type of fonts for the figure
        cI_Legend = 'on';%hide the legend when off
        cI_LegendLocation = '';%place of legend; see matlab legend documentation for details
        cI_Xlabel = 'on';%show xlabel when on
        cI_Ylabel = 'on';%show ylabel when on
        cI_Title = 'on';%shows title if on
        cI_Box = 'on';%show figure box when on
        cI_Grid = 'on';%hide the grid if on
        cI_Handles = [];
        cI_AutoYlim = false;%use I_YLim when false
        cI_AutoXlim = false;%use I_XLim when false
        cI_KeepColor = false;%if false, figures are converted to grayscale
        cI_LineWidth = 2;%apply this line with to all figures when ResetLineWidth is called
        cI_TitleInAxis = true;%puts the title inside or above the the axis
        cI_AlignAxesTexts = true;%this aling texts to right part if string saved in UserData is not'keep_pos'
        cI_LegendBox = true;%shows leyend box when true
        cI_Matrix = [4,4];%set the size of the container figure (rows,cols)
        cI_Space = [0.015 0.015];%control the space bwtween to figures (x,y)
        cI_Xlim = [-inf inf];
        cI_Ylim = [-inf inf];
        cResetLineWidth = false;%applies I_LineWidth to all figures
        cOptimizeSpace  = true;%when true, preparAllFigures will try to minimize empty white space on figures;
        cUpdateContainerFigure = true;
        %       hOutFig = -1;
    end
    methods
        function this = ImageSetup()
        end
        function  set.I_High(this,high)
            if isfloat(high)
                this.cI_High = high;
            else
                error('high must be float')
            end
        end
        function  set.I_Width(this,width)
            if isfloat(width)
                this.cI_Width = width;
            else
                error('width must be float')
            end
        end
        function  set.I_Unit(this,unit)
            if ischar(unit)
                this.cI_Unit = unit;
            else
                error('unit must be string')
            end
        end
        function  set.I_DPI(this,dpi)
            if isfloat(dpi)
                this.cI_DPI = dpi;
            else
                error('dpi must be float')
            end
        end
        function  set.I_FontSize(this,fontsize)
            if isfloat(fontsize)
                this.cI_FontSize = fontsize;
            else
                error('fontsize must be float')
            end
        end
        function  set.I_FontName(this,fontName)
            if ischar(fontName)
                this.cI_FontName = fontName;
            else
                error('fontName must be string')
            end
        end
        
        function  set.I_Legend(this,onoff)
            if ischar(onoff)
                this.cI_Legend = onoff;
            else
                error('onoff must be string')
            end
        end
        
        function  set.I_LegendLocation(this,location)
            if ischar(location)
                this.cI_LegendLocation = location;
            else
                error('location must be string')
            end
        end
        function  set.I_Xlabel(this,onoff)
            if ischar(onoff)
                this.cI_Xlabel = onoff;
            else
                error('onoff must be string')
            end
        end
        function  set.I_Ylabel(this,onoff)
            if ischar(onoff)
                this.cI_Ylabel = onoff;
            else
                error('onoff must be string')
            end
        end
        function  set.I_Title(this,onoff)
            if ischar(onoff)
                this.cI_Title = onoff;
            else
                error('onoff must be string')
            end
        end
        function  set.I_Box(this,onoff)
            if ischar(onoff)
                this.cI_Box = onoff;
            else
                error('onoff must be string')
            end
        end
        function  set.I_Grid(this,onoff)
            if ischar(onoff)
                this.cI_Grid = onoff;
            else
                error('onoff must be string')
            end
        end
        function  set.I_Space(this,space)
            if isvector(space)
                this.cI_Space = space;
                this.cUpdateContainerFigure = true;
            else
                error('width must be vector')
            end
        end
        function  set.I_AutoYlim(this,auto)
            if islogical(auto)
                this.cI_AutoYlim = auto;
            else
                error('width must be logical')
            end
        end
        function  set.I_Ylim(this,lim)
            if isvector(lim)
                this.cI_Ylim = lim;
                this.cUpdateContainerFigure = true;
            else
                error('width must be vector')
            end
        end
        function  set.I_AutoXlim(this,auto)
            if islogical(auto)
                this.cI_AutoXlim = auto;
            else
                error('width must be logical')
            end
        end
        function  set.I_Xlim(this,lim)
            if isvector(lim)
                this.cI_Xlim = lim;
                this.cUpdateContainerFigure = true;
            else
                error('width must be a vector')
            end
        end
        function set.I_KeepColor(this, value)
            this.cI_KeepColor = value;
        end
        function set.I_TitleInAxis(this, value)
            this.cI_TitleInAxis = value;
        end
        function set.OptimizeSpace(this, value)
            this.cOptimizeSpace = value;
        end
        function set.I_Matrix(this, value)
            this.cI_Matrix = value;
        end
        function set.I_Handles(this, value)
            this.cI_Handles = value;
        end
        
        
        function  value = get.I_Width(this)
            value = this.cI_Width;
        end
        function  value = get.I_High(this)
            value = this.cI_High;
        end
        function  value = get.I_Unit(this)
            value = this.cI_Unit;
        end
        
        function  value = get.I_DPI(this)
            value = this.cI_DPI;
        end
        
        function  value = get.I_FontSize(this)
            value = this.cI_FontSize;
        end
        
        function  value = get.I_FontName(this)
            value = this.cI_FontName;
        end
        
        function  value = get.I_Legend(this)
            value = this.cI_Legend;
        end
        
        function  value = get.I_LegendLocation(this)
            value = this.cI_LegendLocation;
        end
        
        function  value = get.I_Xlabel(this)
            value = this.cI_Xlabel;
        end
        
        function  value = get.I_Ylabel(this)
            value = this.cI_Ylabel;
        end
        
        function  value = get.I_Title(this)
            value = this.cI_Title;
        end
        
        function  value = get.I_Box(this)
            value = this.cI_Box;
        end
        
        function  value = get.I_Grid(this)
            value = this.cI_Grid;
        end
        
        function  value = get.I_Handles(this)
            value = this.cI_Handles;
        end
        
        function  value = get.I_AutoYlim(this)
            value = this.cI_AutoYlim;
        end
        
        function  value = get.I_AutoXlim(this)
            value = this.cI_AutoXlim;
        end
        
        function  value = get.I_KeepColor(this)
            value = this.cI_KeepColor;
        end
        
        function  value = get.I_LineWidth(this)
            value = this.cI_LineWidth;
        end
        
        function  value = get.I_TitleInAxis(this)
            value = this.cI_TitleInAxis;
        end
        
        function  value = get.I_AlignAxesTexts(this)
            value = this.cI_AlignAxesTexts;
        end
        
        function  value = get.I_LegendBox(this)
            value = this.cI_LegendBox;
        end
        
        function  value = get.I_Matrix(this)
            value = this.cI_Matrix;
        end
        
        function  value = get.I_Ylim(this)
            value = this.cI_Ylim;
        end
        
        function  value = get.I_Space(this)
            value = this.cI_Space;
        end
        
        function  value = get.I_Xlim(this)
            value = this.cI_Xlim;
        end
        
        function  value = get.ResetLineWidth(this)
            value = this.cResetLineWidth;
        end
        
        function  value = get.OptimizeSpace(this)
            value = this.cOptimizeSpace;
        end
        function set.I_AlignAxesTexts(this, value)
            this.cI_AlignAxesTexts = value;
        end
        
        
        function saveImages(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'FileName', '');
            s = ef(s, 'ObtainingNameFrom', 'name');
            %figHandles = findobj('Type','figure');
            figHandles = this.I_Handles;
            cFileName = s.FileName;
            for i = 1:numel(figHandles)
                %only detect figure that were set by this class
                if ~isempty(findobj(figHandles(i),'Tag', 'pb_IS_Save'))
                    if isempty(s.FileName)
                        if ~isempty(s.ObtainingNameFrom);
                            cFileName = get(figHandles(i), s.ObtainingNameFrom);
                        end
                    end
                    this.printNow('hgcf', figHandles(i), 'FileName', cFileName);
                end
            end
        end
        function hf = fitImages(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Direction', 'horizontal');
            s = ef(s, 'Name','');
            %this.clearHandles;
%             figHandles = sort(findobj('Type','figure'));
            figHandles = sort(this.I_Handles);
            for i = 1:numel(figHandles)
                switch s.Direction
                    case 'horizontal'
                        if ~isempty(findobj(figHandles(i),'Tag', 'pb_IS_Add2ArrHor'))
                            this.addHandle2Array(figHandles(i), 'horizontal');
                           % set(this.hOutFig, 'name', get(figHandles(i), 'name'));
                        end
                    case 'vertical'
                        if ~isempty(findobj(figHandles(i),'Tag', 'pb_IS_Add2ArrVer'))
                            this.addHandle2Array(figHandles(i), 'vertical');
                            %set(this.hOutFig, 'name', get(figHandles(i), 'name'));
                        end
                end
%                 if isempty(find(this.I_Matrix > 1,1))
%                     %we clean the handles is matrix is set to 1,1
%                     this.clearHandles;
%                     delete(figHandles(i));
%                 end
            end
%             if ~isempty(find(this.I_Matrix > 1,1))
%                 for i = 1:numel(figHandles)
%                     delete(figHandles(i));
%                 end
%             end
            hf = this.hOutFig;
            set(hf, 'name', s.Name);
        end
    end
end
