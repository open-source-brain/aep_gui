function this = addHandle2Array(this,newFigHandle,Direction)
this.checkFigHandles;
this.arrayAddedHandles = [this.arrayAddedHandles newFigHandle];
this.arrayAddedHandles = this.unique1(this.arrayAddedHandles);
if strcmp(Direction,'horizontal')
    this.add2ArraySubplotHor;
else
    this.add2ArraySubplotVer;
end
