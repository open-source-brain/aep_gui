function prepareFigures(this, varargin);
s = parseparameters(varargin{:});
s = ef(s,'FigHandle',[]);
hgcf = s.FigHandle;
%% first it is prapared the output image
% set(hgcf, 'PaperUnit', this.I_Unit);
% papersize = get(hgcf, 'PaperSize');
% left = (papersize(1)- this.I_Width)/4;
% bottom = (papersize(2)- this.I_High)/4;
% myfiguresize = [left, bottom, this.I_Width, this.I_High];
% set(hgcf, 'PaperPosition', myfiguresize);
% set(hgcf,'units',this.I_Unit,'position',myfiguresize);


allText   = findall(hgcf, 'type', 'text');
allAxes   = findall(hgcf, 'type', 'axes');
allFont   = [allText; allAxes];
set(allFont, 'FontUnits', 'points');
set(allFont, 'FontSize', this.I_FontSize);
set(allFont, 'FontName', this.I_FontName);
%%remove colors from texts
set(allText,'BackgroundColor','none')

%% limits for the axis
haxis = findobj(hgcf,'type','axes','-and','-not','Tag','legend','-and','-not','Tag','Colorbar'); %only axes
hColorbar = findobj(hgcf,'type','axes','-and','Tag','Colorbar','-and','-not','Tag','legend');

if this.OptimizeSpace
    hColorbar = sort(hColorbar);
end
allaxis = [hColorbar;haxis];
if this.I_AutoYlim
    y1=min(cell2mat(get(haxis,'YLim')));
    y2=max(cell2mat(get(haxis,'YLim')));
    this.I_Ylim= [y1(1) y2(2)];
    set(haxis,'YLim',this.I_Ylim);
else
    if ~isequal(this.I_Ylim,[-inf inf])
        set(haxis,'YLim',this.I_Ylim);
    end;
end

if this.I_AutoXlim
    x1=min(cell2mat(get(haxis,'XLim')));
    x2=max(cell2mat(get(haxis,'XLim')));
    this.I_Ylim= [x1(1) x2(2)];
    set(haxis,'XLim',this.I_Xlim);
else
    if ~isequal(this.I_Xlim,[-inf inf])
        set(haxis,'XLim',this.I_Xlim);
    end;
end;

%% preparation of axes 
for i=1:length(allaxis)
    hl = findobj(allaxis(i),'Type','line');
    lc = get(hl,{'Color'});
    hmarker = findobj(hl,'type','line','-and','-not','Marker','none');
    if ~this.I_KeepColor
        for j = 1:length(lc)
            ncolor = unique(lc{j});
            if length(ncolor)>1
                set(hl(j),'Color','k');
            end
        end
        for j = 1:numel(hmarker)
                %keep only grayscale colors
%                 if ~isequal(get(hmarker(j),'MarkerFaceColor'),'none') && numel(unique(get(hmarker(j),'MarkerFaceColor'))) > 1
                if ~isequal(get(hmarker(j),'MarkerFaceColor'),'none') && numel(unique(get(hmarker(j),'MarkerFaceColor'))) > 1
                    set(hmarker(j),'MarkerFaceColor',[0,0,0]);
                end
        end
        colormap('gray');
    end
    if this.ResetLineWidth
        set(hl,'lineWidth',this.I_LineWidth);
    end
    %set(hl,'lineStyle','-');
    %% Box of image
    set(allaxis(i),'box',this.I_Box);
    %% grid
    set(allaxis(i),'xgrid',this.I_Grid);
    set(allaxis(i),'ygrid',this.I_Grid);
    
    if this.I_TitleInAxis
        htitle = get(allaxis(i),'title');
        titpos = get(htitle,'position');
        axiLim = get(allaxis(i),'Ylim');
        titpos(2) = axiLim(2);
        set(htitle,'position',titpos);
        set(htitle,'VerticalAlignment','top');
    end
end
%% position of legends
hlegends = findobj(hgcf,'type','axes','Tag','legend'); %only legends
if ~isequal(this.I_LegendLocation,'')
    set(hlegends,'Location',this.I_LegendLocation);
end

%% legend setup
set(hlegends,...
    'visible',this.I_Legend); 
if this.I_LegendBox
    set(hlegends,'Box','on')
else
    set(hlegends,'Box','off')
end
if this.OptimizeSpace
    left = [];
    for l = 1:2
        lims = nan(1, 4);
        for k = 1:length(allaxis)
            h = allaxis(k);
            pos = get(h, 'Position');
            
            %    rectangle('Position', pos)
            
            inset = get(h, 'TightInset');
            
            pos(1:2) = pos(1:2) - inset(1:2);
            pos(3:4) = pos(3:4) + inset(3:4) + inset(1:2);
            
            %    rectangle('Position', pos)
            
            if ~(pos(1) >= lims(1))
                lims(1) = pos(1);
            end
            if ~(pos(2) >= lims(2))
                lims(2) = pos(2);
            end
            if ~(pos(1) + pos(3) <= lims(3))
                lims(3) = pos(1) + pos(3);
            end
            if ~(pos(2) + pos(4) <= lims(4))
                lims(4) = pos(2) + pos(4);
            end
        end
        if ~isempty(left)
            if l == 1
                lims(1) = left;
            else
                lims(1) = 0;
            end
        end
        %rectangle('Position', [lims(1:2) lims(3:4) - lims(1:2)])
        width = lims(3) - lims(1);
        height = lims(4) - lims(2);
        if l == 1
            ar = 4 / 3 * width / height;
        end
        for k = 1:length(allaxis)
            h = allaxis(k);
            pos = get(h, 'Position');
            pos(3) = pos(3) / width;
            pos(4) = pos(4) / height;
            pos(1) = (pos(1) - lims(1)) / width;
            pos(2) = (pos(2) - lims(2)) / height;
            set(h, 'Position', pos)
        end
    end
end

%% if there was a colorbar then we set them on in the new fig
if numel(hColorbar) == numel(haxis)
    %first we save the CData that is miss after create a new colorbar
    for i = 1:numel(haxis)
        set(0,'CurrentFigure',hgcf)
        set(hgcf,'CurrentAxes',haxis(i));
        barchild = get(hColorbar(i),'children');
        cdatabar{:,i} = get(barchild,'CData');
        cbarYTick{:,i} = get(hColorbar(i),'YTick');
        colorbar(hColorbar(i),'delete')
    end
    for i = 1:numel(haxis)
        set(0,'CurrentFigure',hgcf)
        set(hgcf,'CurrentAxes',haxis(i));
        cbar = colorbar;
        set(get(cbar,'children'),'CData',cdatabar{:,i});
        set(cbar,'YTick',cbarYTick{:,i});
    end
end

set(hgcf, 'Units', this.I_Unit);
fpos = get(hgcf, 'Position');
fpos(3:4) = [this.I_Width this.I_High];
set(hgcf, 'Position', fpos);
set(hgcf, 'Units', 'pixels');

set(hgcf, 'PaperUnits', this.I_Unit)
set(hgcf, 'PaperSize', [this.I_Width this.I_High])
set(hgcf, 'PaperUnits', 'normalized')
set(hgcf, 'PaperPosition', [0 0 1 1])
set(hgcf, 'PaperUnits', 'inches')
set(hgcf,'color','w');


%% all texts (not in legend)
% delete(findobj(hgcf,'Tag','toDelete'));%my created texts
htexts = findall(hgcf,'Type','text','-not','String','');
cont=1;
while cont<=length(htexts)
    if length(findobj(get(htexts(cont),'Parent'),'Tag','legend')) >=1
        htexts(cont)=[];
    else
        cont=cont+1;
    end
end
alltexts=get(htexts,'String');
set(htexts,'Visible','on');
%% align lines and texts in axes
for i=1:length(haxis)
    if this.I_AlignAxesTexts
        x=get(haxis(i),'Xlim');
        %% aling text objects
        halltexts = findobj(haxis(i),'Type','Text');
        htext = [];
        if numel(halltexts) > 0
            for j = 1:numel(halltexts)
                if ~isequal(get(halltexts(j),'UserData'),'keep_pos');
                    htext = [htext, halltexts(j)];
                end
            end
            
            pos = get(htext,'Position');
            if iscell(pos)
                pos = cell2mat(pos);
            end
            extent = get(htext,'Extent');
            if iscell(extent)
                extent = cell2mat(extent);
            end
            
            for j = 1:numel(htext)
                pos(j,1) = x(end)-extent(j,3);
                set(htext(j),'Position',pos(j,:));
            end
        end
        %% aling lines objects
        halllines = findobj(haxis(i),'Type','Line');
        hlines = [];
        htexts = [];
        if numel(halllines ) > 0
            for j = 1:numel(halllines)
                udata  =get(halllines(j),'UserData');
                if isfield(udata,'Align') && isfield(udata,'HText');
                    if udata.Align
                        hlines = [hlines, halllines(j)];
                    end
                end
            end
            if isequal(numel(htext),numel(hlines))
                for j = 1:numel(htext)
                    pos = get(htext(j),'Position');
                    extent = get(htext(j),'Extent');
                    pos(1) = x(end)-extent(3);
                    set(hlines(j),'XData',[pos(1)-diff(x)*0.05,pos(1)])
                    set(hlines(j),'YData',[pos(2),pos(2)]);
                end
            end
        end
    end
end


%% Subplot Buttons
uicontrol(hgcf,'Style', 'pushbutton', ... 
    'String', 'Save', ...
    'Position', [0 0 60 20], ...
    'Tag', 'pb_IS_Save', ...
    'Callback', @printNow1);
uicontrol(hgcf,'Style', 'pushbutton', ...
    'String', 'add2ArrHor', ...
    'Position', [60 0 100 20], ...
    'Tag', 'pb_IS_Add2ArrHor', ...
    'Callback',@addHandletoArrayHor);
uicontrol(hgcf,'Style', 'pushbutton', ...
    'String', 'add2ArrVer', ...
    'Position', [160 0 100 20], ...
    'Tag', 'pb_IS_Add2ArrVer', ...
    'Callback', @addHandletoArrayVer);
uicontrol(hgcf,'Style', 'pushbutton', ...
    'String', 'Copy2Clipboard', ...
    'Position', [260 0 60 20], ...
    'Tag', 'pb_IS_Copy2Clipboard', ...
    'Callback', @copy2clipboard);

%% buttons connections
    function printNow1(hObject,~)
        this.printNow('hgcf', hgcf);
    end
    function addVerHandle1(hObject,~)
        if hgcf == this.hOutFig
            this.clearHandles;
        end
        this.addVerHandle(hgcf);
    end
    function addHorHandle1(hObject,~)
        if hgcf == this.hOutFig
            this.clearHandles;
        end
        this.addHorHandle(hgcf);
    end
    function addHandletoArrayHor(hObject,~)
        if hgcf == this.hOutFig
            this.clearHandles;
        end
        this.addHandle2Array(hgcf,'horizontal');
    end
    function addHandletoArrayVer(hObject,~)
        if hgcf == this.hOutFig
            this.clearHandles;
        end
        this.addHandle2Array(hgcf,'vertical');
    end
    function addHandletoArray(hObject,~)
    end
    function copy2clipboard(hObject,~)
        if ~ispc
            return;
        end
        set(findobj(hgcf,'Type','uicontrol'),'Visible','off');
        %print(hgcf,['-r' num2str(this.I_DPI)], '-dmeta');
        hgexport(hgcf,'-clipboard');
        set(findobj(hgcf,'Type','uicontrol'),'Visible','on');
    end

end