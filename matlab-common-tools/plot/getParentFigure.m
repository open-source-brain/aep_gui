function hObject = getParentFigure(hObject)
% if the hObject is a figure or figure descendent, return the
% figure. Otherwise return [].
while ~isempty(hObject) && ~isequal('figure', get(hObject,'type'))
    hObject = get(hObject,'parent');
end
