function out = convertJsonFile2Structure(file_path)
text = fileread(file_path);
% text = regexprep(text,'\\', '\\\\');
% text = regexprep(text,'\/', '\/\/');
out = jsondecode(text);
