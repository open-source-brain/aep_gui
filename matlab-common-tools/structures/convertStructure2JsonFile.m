function convertStructure2JsonFile(s, file_path)
data = jsonencode(s);
data = regexprep(data,'\\', '\\\\');
fileID = fopen(file_path, 'w', 'n', 'utf-8');
fprintf(fileID, data);
fclose(fileID);