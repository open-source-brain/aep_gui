function [out, remfields] = removeFields(s, fieldnames)
    out = s;
    remfields = {};
    for i = 1:numel(fieldnames)
        if isfield(s, fieldnames{i})
            remfields.(fieldnames{i}) = s.(fieldnames{i});
            out = rmfield(out,fieldnames{i});
        end
    end