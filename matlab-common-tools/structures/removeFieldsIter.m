function [s] = removeFieldsIter(s, field_name_rm)
    if isstruct(s)
        for elem = 1 : numel(s)
            if isfield(s(elem), field_name_rm)
                sout = rmfield(s(elem), field_name_rm);
                if numel(s) == 1
                    s = sout;
                else
                    s(elem) = sout;
                end
            end
            field_names = fieldnames(s(elem));
            for i = 1:numel(field_names)
                ss = s(elem).(field_names{i});
                s(elem).(field_names{i}) = removeFieldsIter(ss, field_name_rm);
            end
        end
    end
    if iscell(s)
        for j = 1 : numel(s)
            s{j} = removeFieldsIter(s{j}, field_name_rm);
        end
    end
