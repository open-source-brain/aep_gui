function subgroups = getIdx2compare(varargin)
%%example 
%%out = slopesFromTable('ColumNames',data_name(1,:),'TableData',data_name(2:end,:),'SortBy',{'SB','SplitLabel','PeakLabel','StimAmp'}, 'CompareBy',{'SB','PeakLabel','SplitLabel'},'X','StimAmp','Y','Amp')
    s = parseparameters(varargin{:});
    s = ef(s,'CategoryIdx',[]);
    s = ef(s,'Dat',[]);
    s = ef(s,'Subrows',[]);
    s = ef(s,'Level',1);
    s = ef(s,'LevelText','');
    s = ef(s,'GroupIndexs',[]);
    
    if numel(s.CategoryIdx) < s.Level
        return
    end
    [nRows,~] = size(s.Dat);
    subgroups = [];
    if isempty(s.Subrows)%initialization
        s.Subrows = (1:nRows)';
    end
    cCategories = uniqueRowsCA(s.Dat(:,s.CategoryIdx(s.Level)));
    cont = 1;
    for j = 1:numel(cCategories)
        cidx = [];
        for m = 1:numel(s.Subrows)
            if isequal(cCategories{j},s.Dat{s.Subrows(m),s.CategoryIdx(s.Level)})
                cidx = [cidx,s.Subrows(m)];
            end
        end
        if ~isempty(cidx)
            if isequal(numel(s.CategoryIdx),s.Level)
                subgroups(cont).Category = [s.LevelText,cCategories(j)];
                subgroups(cont).Idx = cidx;
                subgroups(cont).GroupIndexs = [s.GroupIndexs, j];
                cont = cont + 1;
            else
                subgroups = [subgroups, getIdx2compare('CategoryIdx',s.CategoryIdx,...
                    'Dat',s.Dat,...
                    'Subrows',cidx,...
                    'Level',s.Level+1,...
                    'LevelText',[s.LevelText,cCategories(j)],...
                    'GroupIndexs',[s.GroupIndexs,j])];
            end
        end
    end