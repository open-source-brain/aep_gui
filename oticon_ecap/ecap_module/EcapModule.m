classdef EcapModule < hgsetget
    properties(Abstract)
        hStopEvent;% handle to stop listener
    end
    events
        evNotifyStatus;
    end
    methods(Abstract)
        setStimuliModuleGui(this);
        value = getClassSettings(this);
        settings = GetModuleSettings(this);
        SaveSettings(this);
        ReadSettings(this);
        startRecording(this);
        stopRecording(this);
        dispmessage(this);
        receiveMessage(this);
    end
end
