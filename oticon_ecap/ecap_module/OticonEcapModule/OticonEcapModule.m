classdef OticonEcapModule < handle
    properties
        HContainer; %handles of object containing visual objects
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        StimuliAxes = [];
        StimuliFFTAxes = [];
        EcapAxes = [];
        hSoundModule = []; % handle to sound module
        Stimulus = {};
        hStopEvent;% handle to stop listener
        RecordingPath = tempdir;
        TriggerChannel = [];
        AudioBufferSize = 0;
        ScaleFactor = 1;% to scale magnitude of figures
    end
    
    properties (Access = private)
        cCalibration = {};
        hCIDevice = [];
        hTimeFig = [];
        hFrequencyFig = [];
        hEcapFig = [];
        cFrequencyVector = [];
        cTimeVector = [];
        cTimeAverageVector= [];
        cFileName = '';
        cDateCurrentMeasurement = '';
        cAudioBuffer = [];
        cAverageBuffer = [];
        cAverageBufferM = [];
        cAverageBufferMP = [];
        cAverageBufferP = [];
        cAverageBufferB = [];
        cAudioBufferCounter = 0;
        cDetectedTriggers = [];
        cCountM = 0;
        cCountP = 0;
        cCountMP = 0;
        cCountB = 0;
        cWavFileName = '';
        cDataFileId = -1;
%         hTCPIPTimer = timer;
%         hTCPIPSocket = [];
        cBackgroundPlayColor = [];
    end
    
    events
        evNotifyStatus;
    end
    
    methods
        function this = OticonEcapModule(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'HContainer', []);%handle to container object
            s = ef(s, 'Topmargin', 0.05);
            s = ef(s, 'UiHigh', 0.03);%desired high for objects
            s = ef(s, 'UiWidth', 0.1);%desired width for objects
            s = ef(s, 'SoundModule', []); % sound modul han
            s = ef(s, 'RecordingModule', []); % sound modul han
            this.HContainer = s.HContainer;
            this.Topmargin = s.Topmargin;
            this.UiHigh = s.UiHigh;%desired high for objects
            this.UiWidth = s.UiWidth;%desired width for objects
            this.hSoundModule = s.SoundModule;
            addlistener(s.SoundModule,'evSoundSettingsChanged',@(src,evnt)callbackUpdateModule(this,src,evnt)); %event to listen and update the module
            addlistener(s.SoundModule.hSoundDevice,'evSoundDeviceStopped',@(src,evnt)callbackSoundDeviceStopped(this,src,evnt)); %event to listen and update the module
            %% add listener to message events
            addlistener(this.hSoundModule, 'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            addlistener(this,'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            this.setStimuliModuleGui;
            this.ReadSettings;
            
            %% setup TCP socket
            
            %% set TCP timer and callback function
%             this.hTCPIPSocket = tcpip('0.0.0.0', 4012);
%             set(this.hTCPIPTimer, 'TimerFcn', @this.TCPListen, ...
%                 'Period', 2, ...
%                 'StartDelay', 0.5 ,...
%                 'ExecutionMode', 'fixedRate', ...
%                 'BusyMode', 'drop', ...
%                 'TasksToExecute', inf ...
%                 );
%             start(this.hTCPIPTimer);
            %             c = parcluster();
            %             j = batch(c,@callbackTCPListen,1, {this});
            %             wait(j)
            %             diary(j)
            %             r = fetchOutputs(j); % Get results into a cell array
            %             r{1}                 % Display result
        end
        
        function setStimuliModuleGui(this)
            setOticonEcapTab(this);
        end
        
        function callbackUpdateModule(this, ~, ~)
            this.setStimuliModuleGui;
        end
        function delete(this)
            %% remove timer
%             stop(this.hTCPIPTimer);
%             delete(this.hTCPIPTimer);
            %% remove socket
%             fclose(this.hTCPIPSocket);
        end
        function settings = GetModuleSettings(this)
            settings.(class(this)).GuiSettings = getOticonEcapModuleSettings(this);
            settings.(class(this)).ClassSettings = getClassSettings(this);
        end
        
        function SaveSettings(this)
            modulePath = mfilename('fullpath');
            struct2xml(this.GetModuleSettings, ...
                strcat(modulePath,'.xml'));
        end
        
        function saveModulesSettings(this)
            this.hSoundModule.SaveSettings;
            this.SaveSettings;
        end
        
        function value = get.AudioBufferSize(this)
            value = round(this.hSoundModule.hSoundDevice.RecordingSettings.MinimumAmountToReturnSecs * this.hSoundModule.getParameters.Fs);
        end
        
        function ReadSettings(this)
            cFile = strcat(mfilename('fullpath'),'.xml');
            if exist(cFile, 'file')
                try
                    settings = convertxmlvalue2matvalue(xml2struct(cFile));
                    %% class settings
                    if isfield(settings.(class(this)),'ClassSettings')
                        csettings = settings.(class(this)).ClassSettings;
                        vtagList = fieldnames(csettings);
                        for i = 1:numel(vtagList)
                            val = csettings.(vtagList{i});
                            if isempty(val); continue; end;
                            eval(strcat('this.',vtagList{i},'= val;'));
                        end
                    end
                    %% read gui settings
                    msettings = settings.(class(this)).GuiSettings;
                    tagList = fieldnames(msettings);
                    %% first we set all values
                    for i = 1:numel(tagList)
                        val = msettings.(tagList{i}).FieldValue;
                        FieldType = msettings.(tagList{i}).FieldType;
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        set(chobj, FieldType, val);
                    end
                    %% after setting all values we invoke callbacks
                    for i = 1:numel(tagList)
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        hgfeval(get(chobj,'Callback'),chobj);
                    end
                catch err
                    this.dispmessage(strcat('Could not read settings: ',err.message));
                end
            end
        end
        function value = getClassSettings(this)
            value.RecordingPath = this.RecordingPath;
            value.TriggerChannel = this.TriggerChannel;
        end
        
        function updateRecordingTime(this, value)
            hobj = findobj(this.HContainer, 'Tag', 'txRecordingTime');
            set(hobj, 'String', strcat('Rec Time[s]:',num2str(value)));
        end
        
        function value = getParameters(this)
            value.Version = 1;
            value.Subject = get(findobj(this.HContainer, 'Tag', 'edSubject'), 'String');
            value.Date = this.cDateCurrentMeasurement;
            value.Condition = get(findobj(this.HContainer, 'Tag', 'edCondition'), 'String');
            soundSettings = this.hSoundModule.smGetSoundDeviceConfiguration;
            for i = 1:size(soundSettings.SelectedChannels,2)
                cChannel = soundSettings.SelectedChannels(2,i);
                cHandleM = findobj(this.HContainer, 'Tag', strcat('edMaskerTriggerWidth_',num2str(cChannel)));
                cHandleP = findobj(this.HContainer, 'Tag', strcat('edProbeTriggerWidth_',num2str(cChannel)));
                cHandleMP = findobj(this.HContainer, 'Tag', strcat('edMaskerProbeTriggerWidth_',num2str(cChannel)));
                cHandleB = findobj(this.HContainer, 'Tag', strcat('edBaselineTriggerWidth_',num2str(cChannel)));
                if ~(isempty(cHandleM) && isempty(cHandleP) && ...
                        isempty(cHandleMP) && isempty(cHandleB))
                    value.Channel{i}.Number = cChannel;
                    value.Channel{i}.MaskerTPW = get(cHandleM, 'String');
                    value.Channel{i}.ProbeTPW = get(cHandleP, 'String');
                    value.Channel{i}.MaskerProbeTPW = get(cHandleMP, 'String');
                    value.Channel{i}.BaselineTPW = get(cHandleB, 'String');
                end
            end
            value.TriggerChannel = this.TriggerChannel;
            value.OutputFolder = get(findobj(this.HContainer, 'Tag', 'edCurrentFolder'), 'String');
            value.Comments = get(findobj(this.HContainer, 'Tag', 'edComments'), 'String');
            value.ScaleFactor = str2double(get(findobj(this.HContainer, 'Tag', 'edScaleFactor'), 'String'));
        end
        
        function startRecording(this)
            %% clear message window
            this.cleanMessageWindows;
            %% prepare sound device
            cSoundDevice = this.hSoundModule.hSoundDevice;
            if  cSoundDevice.DeviceOpen
                %% delete old audio buffers
                cSoundDevice.deleteBuffers;
                cSoundDevice.closeDevice;
            end
            cSoundSetup = this.hSoundModule.smGetSoundDeviceConfiguration;
            cSoundSetup.Mode = 'capture';
            cSoundDevice.openDevice(cSoundSetup);
            % cSoundDevice.MasterLevel = cSoundSetup.MasterLevel;
            % cSoundDevice.ChannelLevels = cSoundSetup.ChannelLevels;
            %% assign sound device handles functions
            cSoundDevice.hFunCapturing = @this.callbackCapturingData;
            cSoundDevice.hFunPlaying = @callbackSoundDevicePlaying;
            cSoundDevice.hFunCapturingAndPlaying = @callbackSoundDeviceCapturingAndPlaying;
            
            %% Initialize time and frequency vector to speed up plots
            this.cTimeVector = (0 : this.AudioBufferSize - 1) * 1 / this.hSoundModule.getParameters.Fs;
            this.cFrequencyVector = (0 : this.AudioBufferSize - 1) * this.hSoundModule.getParameters.Fs / this.AudioBufferSize;
            
            cAveBufferTime = str2double(get(findobj(this.HContainer, 'Tag', 'edAverageEcapwindow'), 'String'));
            this.cTimeAverageVector = (0 : round(cAveBufferTime * this.hSoundModule.getParameters.Fs) - 1) * 1 / this.hSoundModule.getParameters.Fs;
            %% Initialize audio buffer counter and buffer
            this.cAudioBufferCounter = 0;
            this.cAudioBuffer = zeros(this.AudioBufferSize, cSoundSetup.NChannels2Use);
            this.cCountM = 0;
            this.cCountP = 0;
            this.cCountMP = 0;
            this.cCountB = 0;
            %% set average buffers
            this.cAverageBuffer = zeros(round(cAveBufferTime * this.hSoundModule.getParameters.Fs), cSoundSetup.NChannels2Use);
            this.cAverageBufferM = zeros(round(cAveBufferTime * this.hSoundModule.getParameters.Fs), cSoundSetup.NChannels2Use);
            this.cAverageBufferP = zeros(round(cAveBufferTime * this.hSoundModule.getParameters.Fs), cSoundSetup.NChannels2Use);
            this.cAverageBufferMP = zeros(round(cAveBufferTime * this.hSoundModule.getParameters.Fs), cSoundSetup.NChannels2Use);
            this.cAverageBufferB = zeros(round(cAveBufferTime * this.hSoundModule.getParameters.Fs), cSoundSetup.NChannels2Use);
            
            %% reset detected trigger buffer
            this.cDetectedTriggers = [];
            %% clean and prepar figures handles
            this.hTimeFig = plot(ones(2, cSoundSetup.NChannels2Use) * NaN,'Parent', this.StimuliAxes);
            axes(this.StimuliAxes);
            set(this.StimuliAxes, 'XLim', [0, this.cTimeVector(end)]);
            set(this.StimuliAxes, 'YLim', [-1, 1]);
            xlabel(this.StimuliAxes, 'Time [s]');
            ylabel(this.StimuliAxes, 'Amplitude [V]');
            
%             this.hFrequencyFig = plot(ones(2, cSoundSetup.NChannels2Use) * NaN,'Parent', this.StimuliFFTAxes);
%             axes(this.StimuliFFTAxes);
%             set(this.StimuliFFTAxes, 'XLim', [0, this.cFrequencyVector(end) / 2]);
%             set(this.StimuliFFTAxes, 'YLim', [0, 0.5]);
%             xlabel('Frequency [Hz]');
%             ylabel('Magnitude [V]');
            
            this.hEcapFig = plot(ones(2, cSoundSetup.NChannels2Use) * NaN,'Parent', this.EcapAxes);
            axes(this.EcapAxes);
            set(this.EcapAxes, 'XLim', [0, this.cTimeAverageVector(end)]);
            set(this.EcapAxes, 'YLim', [-1, 1]);
            xlabel(this.EcapAxes, 'Time [s]');
            ylabel(this.EcapAxes, 'Amplitude [V]');
            
            %% Initialize Recording File
            this.cDateCurrentMeasurement = datestr(now,'mm-dd-yy-HH-MM-SS-FFF');
            
            cParameters = this.getParameters;
            cName = strcat(cParameters.Subject, '-', ...
                cParameters.Condition, '-',...
                this.cDateCurrentMeasurement);
            
            this.cWavFileName = strcat(this.RecordingPath, cName,'.wav');
            
            [cFilePath, tempFileName] = fileparts(this.cWavFileName);
            %% save modules settings
            this.saveModulesSettings;
            %% create temporal file to record data
            this.cDataFileId = fopen(strcat(cFilePath, filesep, tempFileName, '.tmp'),'w');
            %% set color for playing
            this.cBackgroundPlayColor = get(findobj(this.HContainer, 'Tag', 'pbRecStimuli'), 'Backgroundcolor');
            set(findobj(this.HContainer, 'Tag', 'pbRecStimuli'), 'Backgroundcolor', 'r');
            %% start recording
            cSoundDevice.start;
        end
        
        function stopRecording(this)
            cSoundDevice = this.hSoundModule.hSoundDevice;
            delete(this.hStopEvent);
            cSoundDevice.Stop;
            set(findobj(this.HContainer, 'Tag', 'pbRecStimuli'), 'Backgroundcolor', this.cBackgroundPlayColor);
            try
                fclose(this.cDataFileId);
                this.saveMeasurement;
            catch
            end
        end
        
        function dispmessage(this,message)
            txMessage = strcat(class(this), '-', message);
            disp(txMessage);
            notify(this,'evNotifyStatus', MessageEventDataClass(txMessage));
        end
        
        function receiveMessage(this, ~, evnt)
            hobj = findobj(this.HContainer,'Tag','lbMeasurementInfo');
            cText = get(hobj,'String');
            cText{end + 1} = evnt.message;
            set(hobj,'String', cText);
            set(hobj,'Value', numel(cText));
            drawnow;
        end
        
        function callbackSoundDeviceStopped(this, ~, ~)
            notify(this,'evStimulatorDeviceStopped');
        end
        function stop = callbackCapturingData(this, audiodata, recordedTime)
            stop = false;
            if isempty(audiodata);
                return;
            end
            % update AudioBuffer Counter and buffer
            this.cAudioBufferCounter = this.cAudioBufferCounter + 1;
            audiodata = audiodata';
            
            %% test audio stream
%             audiodata = zeros(size(audiodata));
%             f = 1000;
%             fs = this.hSoundModule.getParameters.Fs;
%             cycles = 1.5/f*fs;
%             signal(1:cycles) = -0.1*sin(2*pi*f*(0:cycles-1)/fs-0*pi/2).*exp(-(0:cycles-1)/fs/0.0005);
%             signal = signal - mean(signal,2);
%             MPI = round(0.0005*fs);
%             iniPosM = round(1000 * rand(1,1)) + 1;
%             endPosM = iniPosM + round(this.TriggerChannel.MTPW * fs);
%             
%             for i = 1 :size(audiodata,2)
%                 if i == this.TriggerChannel.Number + 1
%                     audiodata(iniPosM : endPosM, i) = 1;
%                     iniPosMP = iniPosM + size(this.cAverageBuffer, 1);
%                     endPosMP = iniPosMP + round(this.TriggerChannel.MPTPW * fs);
%                     audiodata(iniPosMP:endPosMP - 1, i) = 1;
%                     iniPosP = iniPosMP + size(this.cAverageBuffer, 1);
%                     endPosP = iniPosP + round(this.TriggerChannel.PTPW * fs);
%                     audiodata(iniPosP : endPosP - 1, i) = 1;
%                     
%                     iniPosB = iniPosP + size(this.cAverageBuffer, 1);
%                     endPosB = iniPosB + round(this.TriggerChannel.BTPW * fs);
%                     audiodata(iniPosB : endPosB - 1, i) = 1;
%                 else
%                     audiodata(iniPosP + MPI : iniPosP + MPI + numel(signal) - 1, i) = signal;
%                 end
%             end
%             audiodata = audiodata + 0.01 * randn(size(audiodata));
            %%%%
            % save data to temfile
            fwrite(this.cDataFileId, audiodata,'double');
            
            set(this.hTimeFig, 'XData', this.cTimeVector, {'YData'}, num2cell(audiodata, 1)');
            drawnow;
%             % frequency plot
%             if ~isempty(this.hFrequencyFig)
%                 cFFTData = abs(fft(audiodata))*2/numel(audiodata);
%                 cFFTData(1) = 0; %removes dc
%                 set(this.hFrequencyFig, 'XData', this.cFrequencyVector, {'YData'}, num2cell(cFFTData, 1)');
%                 drawnow;
%             end
            if this.cAudioBufferCounter == 1
                cCatBuffer = audiodata;
            else
                cCatBuffer = [this.cAudioBuffer; audiodata];
            end
            iniPos = 1;
            if ~isempty(this.cDetectedTriggers)
                % start looking for triggers after last good known
                % trigger
                iniPos = this.cDetectedTriggers(end).NegativeRel + 1;
            end
            this.cDetectedTriggers = this.detectTriggers(cCatBuffer(: , this.TriggerChannel.Position), iniPos);
            %                 hold(this.StimuliAxes,'on')
            %                 hPos = plot(this.StimuliAxes, this.cTimeVector([this.cDetectedTriggers(:).PositiveRel]), ...
            %                     this.TriggerChannel.Threshold * ones(size([this.cDetectedTriggers(:).PositiveRel])), ...
            %                     'o','Markersize', 6, 'MarkerFaceColor', 'k');
            %                 hNeg = plot(this.StimuliAxes, this.cTimeVector([this.cDetectedTriggers(:).NegativeRel]), ...
            %                     this.TriggerChannel.Threshold * ones(size([this.cDetectedTriggers(:).NegativeRel])), ...
            %                     'o','Markersize', 6, 'MarkerFaceColor', 'k');
            %                 hold(this.StimuliAxes,'off')
            %                 drawnow;
            %                 delete(hPos);
            %                 delete(hNeg);
            this.averageEcap(cCatBuffer, this.cDetectedTriggers);
            this.cAudioBuffer = audiodata;
            
            %%
            this.updateRecordingTime(recordedTime);
        end
        
        function triggers = detectTriggers(this, triggerBuffer, startPoint)
            %% detect trigger positions
            upTriggerPos = [];
            downTriggerPos = [];
            maxk = -inf;
            mink = inf;
            difftrigin = diff([0; triggerBuffer]);
            for i = 1 : numel(difftrigin) - 1
                if difftrigin(i)> this.TriggerChannel.Threshold
                    if difftrigin(i) >= maxk
                        maxk = difftrigin(i);
                        posk = i;
                    end
                    if difftrigin(i+1) <= this.TriggerChannel.Threshold
                        upTriggerPos = [upTriggerPos, posk];
                        maxk = -inf;
                    end
                end
                if difftrigin(i) < -this.TriggerChannel.Threshold
                    if difftrigin(i) <= mink
                        mink = difftrigin(i);
                        posmink = i;
                    end
                    if difftrigin(i+1) >= -this.TriggerChannel.Threshold
                        downTriggerPos = [downTriggerPos, posmink];
                        mink = inf;
                    end
                end
            end
            triggers = [];
            for i = 1 : min(numel(upTriggerPos), numel(downTriggerPos))
                % we add trigger that cover the desiered average window
                % only
                if upTriggerPos(i) >= startPoint && ...
                        upTriggerPos(i) + size(this.cAverageBuffer, 1) <= ...
                        size(triggerBuffer, 1)
                    triggers(end + 1).Positive = upTriggerPos(i);
                    triggers(end).Negative = downTriggerPos(i);
                    triggers(end).Width = downTriggerPos(i) - upTriggerPos(i);
                    triggers(end).PositiveRel = upTriggerPos(i) - floor(upTriggerPos(i) / this.AudioBufferSize) * this.AudioBufferSize;
                    triggers(end).NegativeRel = downTriggerPos(i) - floor(downTriggerPos(i) / this.AudioBufferSize) * this.AudioBufferSize;
                end
            end
        end
        function averageEcap(this, audioBuffer, triggers)
            % clasify trigger
            for i = 1 : numel(triggers)
                [~, pos] = min(abs(triggers(i).Width/this.hSoundModule.getParameters.Fs - [this.TriggerChannel.MTPW, ...
                    this.TriggerChannel.PTPW, ...
                    this.TriggerChannel.MPTPW, ...
                    this.TriggerChannel.BTPW]));
                posIni = triggers(i).Positive;
                posEnd = triggers(i).Positive + size(this.cAverageBuffer, 1) - 1;
                cAveBuffer = audioBuffer(posIni : posEnd,:);
                switch pos
                    case 1 %masker
                        this.cCountM = this.cCountM + 1;
                        this.cAverageBufferM = ((this.cCountM - 1) * this.cAverageBufferM + cAveBuffer) / this.cCountM;
                        this.dispmessage(strcat('Masker triggers detected:', num2str(this.cCountM), ...
                            '/Detected Width:', num2str(triggers(i).Width / this.hSoundModule.getParameters.Fs), ...
                            '/Expected Width:', num2str(this.TriggerChannel.MTPW)));
                    case 2 %probe
                        this.cCountP = this.cCountP + 1;
                        this.cAverageBufferP = ((this.cCountP - 1) * this.cAverageBufferP + cAveBuffer) / this.cCountP;
                        this.dispmessage(strcat('Probe triggers detected:', num2str(this.cCountP), ...
                            '/Detected Width:', num2str(triggers(i).Width / this.hSoundModule.getParameters.Fs), ...
                            '/Expected Width:', num2str(this.TriggerChannel.PTPW)));
                    case 3 %masker probe
                        this.cCountMP = this.cCountMP + 1;
                        this.cAverageBufferMP = ((this.cCountMP - 1) * this.cAverageBufferMP + cAveBuffer) / this.cCountMP;
                        this.dispmessage(strcat('Masker-Probe triggers detected:', num2str(this.cCountMP), ...
                            '/Detected Width:', num2str(triggers(i).Width / this.hSoundModule.getParameters.Fs), ...
                            '/Expected Width:', num2str(this.TriggerChannel.MPTPW)));
                    case 4 %baseline
                        this.cCountB = this.cCountB + 1;
                        this.cAverageBufferB = ((this.cCountB - 1) * this.cAverageBufferB + cAveBuffer) / this.cCountB;
                        this.dispmessage(strcat('Baseline triggers detected:', num2str(this.cCountB), ...
                            '/Detected Width:', num2str(triggers(i).Width / this.hSoundModule.getParameters.Fs), ...
                            '/Expected Width:', num2str(this.TriggerChannel.BTPW)));
                end
                this.cAverageBuffer = this.cAverageBufferP - (this.cAverageBufferMP - this.cAverageBufferM) - this.cAverageBufferB;
                this.cAverageBuffer(:, this.TriggerChannel.Position) = audioBuffer(posIni : posEnd, this.TriggerChannel.Position);
            end
            set(this.hEcapFig, 'XData', this.cTimeAverageVector, {'YData'}, num2cell(this.ScaleFactor * this.cAverageBuffer, 1)');
            drawnow;
        end
        function cleanMessageWindows(this)
            hobj = findobj(this.HContainer,'Tag','lbMeasurementInfo');
            set(hobj,'String', '');
        end
        function saveMeasurement(this)
            this.dispmessage('saving data...');
            [cFilePath, tempFileName] = fileparts(this.cWavFileName);
            cTempFile = strcat(cFilePath, filesep, tempFileName, '.tmp');
            cData = [];
            ctempFileId = fopen(cTempFile);
%             count = 0;
            while ~feof(ctempFileId)
                cData = [cData ; fread(ctempFileId, [this.AudioBufferSize, this.hSoundModule.smGetSoundDeviceConfiguration.NChannels2Use], 'double')];
%                 count = count + 1;
%                 disp(count)
            end
            fclose(ctempFileId);
            try
                audiowrite(this.cWavFileName, cData, this.hSoundModule.getParameters.Fs);
            catch err
                wavwrite(cData, this.hSoundModule.getParameters.Fs, this.cWavFileName);
            end
            %% remove tem directory
            delete(cTempFile)
            %% save measurement info
            this.saveParameters(strcat(cFilePath, filesep, tempFileName));
            this.saveEcapFigure;
            this.dispmessage(strcat('Data saved into: ', this.cWavFileName));
        end
        
        function saveParameters(this,path)
            value.Measurement = this.getMeasurementParameters;
            struct2xml(value, ...
                strcat(path,'.xml'));
        end
        function value = getMeasurementParameters(this)
            value.SoundModule = this.hSoundModule.getParameters;
            value.MeasurementModule = this.getParameters;
        end
        function TCPListen(this, ~, ~)
            if isequal(this.hTCPIPSocket.Status, 'closed')
                echotcpip('off')
                echotcpip('on',4012)
                fopen(this.hTCPIPSocket);
                set(this.hTCPIPSocket,'Timeout',.01);
            else
                A = fread(this.hTCPIPSocket, 10);
                disp('listening')
            end
        end
        function saveEcapFigure(this)
            [path,name] = fileparts(this.cWavFileName);
            cPath = strcat(path,filesep,name,'');
            cHFig = figure('Visible', 'off');
            setGuiTheme('Figure', cHFig, 'BackgroundColor', [1,1,1], 'FontColor', [0, 0, 0]);
            copyobj(this.EcapAxes, cHFig);
            naxes = get(cHFig,'Currentaxes');
            set(naxes, 'position', [0.1,0.1,0.8,0.8]);
            saveas(cHFig,strcat(cPath));
            sProps.Format = 'eps';
            sProps.Preview = 'tiff';

            hgexport(cHFig, strcat(cPath, '.eps'), sProps);
            
            sProps.Format = 'tiff';
            sProps.Resolution = 600;
            hgexport(cHFig,strcat(cPath, '.tiff'), sProps);
            
            sProps.Format = 'pdf';
            sProps.Resolution = 600;
            hgexport(cHFig,strcat(cPath, '.pdf'), sProps);
        end
    end
end