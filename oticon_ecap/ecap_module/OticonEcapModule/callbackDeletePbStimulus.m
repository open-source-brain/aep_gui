function callbackDeletePbStimulus(hObject, ~)
    handles = guidata(hObject);
    try
        cSoundSetup = getSoundDeviceConfiguration(hObject);
        if ~isempty(handles.Stimulus.Signals)
        handles.Stimulus.Signals = handles.Stimulus.Signals(cSoundSetup.SelectedChannels(1,:));
        end
        guidata(handles.figure1, handles);
    catch
    end