function callbackExportFigureStimuliModule(hObject,~)
    this = get(hObject, 'Userdata');
    hf = figure;
    switch  get(hObject,'Tag')
        case 'itExpFig'
            copyobj(this.StimuliAxes, hf);
        case 'itExpFigFFT'
            copyobj(this.StimuliFFTAxes, hf);
    end
    naxes = get(hf,'Currentaxes');
    set(naxes, 'position', [0.1,0.1,0.8,0.8]);