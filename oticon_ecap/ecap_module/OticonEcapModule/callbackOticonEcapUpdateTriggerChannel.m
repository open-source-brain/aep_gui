function callbackOticonEcapUpdateTriggerChannel(hObject, ~)
if isequal(get(hObject, 'Value'), 0)
    return;
end
this = get(hObject,'Userdata');
soundSettings = this.Module.hSoundModule.smGetSoundDeviceConfiguration;
for i = 1:size(soundSettings.SelectedChannels,2)
    cChannel = soundSettings.SelectedChannels(2,i);
    if strcmp(strcat('chPlot_',num2str(cChannel)), get(hObject, 'Tag'))
        this.Module.TriggerChannel.Number = cChannel;
        this.Module.TriggerChannel.Position = this.Module.hSoundModule.smGetChannelInPosition(cChannel);
        continue;
    else
        set(findobj(this.Module.HContainer, ...
            'Tag', strcat('chPlot_',num2str(cChannel))), ...
            'Value', 0);
    end
end
