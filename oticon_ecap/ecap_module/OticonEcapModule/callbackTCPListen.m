function value = callbackTCPListen(this)
disp('listening')
value = 'listening'
if isequal(this.hTCPIPSocket.Status, 'closed')
    echotcpip('off')
    echotcpip('on',4012)
    fopen(this.hTCPIPSocket);
end
while isequal(this.hTCPIPSocket.Status, 'open')
    A = fread(this.hTCPIPSocket, 10);
    disp('listening')
end
end