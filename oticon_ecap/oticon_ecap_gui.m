function varargout = oticon_ecap_gui(varargin)
	% oticon_ecap_gui MATLAB code for oticon_ecap_gui.fig
	%      oticon_ecap_gui, by itself, creates a new oticon_ecap_gui or raises the existing
	%      singleton*.
	%
	%      H = oticon_ecap_gui returns the handle to a new oticon_ecap_gui or the handle to
	%      the existing singleton*.
	%
	%      oticon_ecap_gui('CALLBACK',hObject,eventData,handles,...) calls the local
	%      function named CALLBACK in oticon_ecap_gui.M with the given input arguments.
	%
	%      oticon_ecap_gui('Property','Value',...) creates a new oticon_ecap_gui or raises the
	%      existing singleton*.  Starting from the left, property value pairs are
	%      applied to the GUI before oticon_ecap_gui_OpeningFcn gets called.  An
	%      unrecognized property name or invalid value makes property application
	%      stop.  All inputs are passed to oticon_ecap_gui_OpeningFcn via varargin.
	%
	%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
	%      instance to run (singleton)".
	%
	% See also: GUIDE, GUIDATA, GUIHANDLES

	% Edit the above text to modify the response to help oticon_ecap_gui

	% Last Modified by GUIDE v2.5 29-Apr-2013 11:54:05

	% Begin initialization code - DO NOT EDIT
	gui_Singleton = 1;
	gui_State = struct('gui_Name',       mfilename, ...
		'gui_Singleton',  gui_Singleton, ...
		'gui_OpeningFcn', @oticon_ecap_gui_OpeningFcn, ...
		'gui_OutputFcn',  @oticon_ecap_gui_OutputFcn, ...
		'gui_LayoutFcn',  [] , ...
		'gui_Callback',   []);
	if nargin && ischar(varargin{1})
		gui_State.gui_Callback = str2func(varargin{1});
    end
    
    if nargout
		[varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
	else
		gui_mainfcn(gui_State, varargin{:});
	end
	% End initialization code - DO NOT EDIT


	% --- Executes just before oticon_ecap_gui is made visible.
function oticon_ecap_gui_OpeningFcn(hObject, ~, handles, varargin)
	% This function has no output args, see OutputFcn.
	% hObject    handle to figure
	% eventdata  reserved - to be defined in a future version of MATLAB
	% handles    structure with handles and user data (see GUIDATA)
	% varargin   command line arguments to oticon_ecap_gui (see VARARGIN)
    
    handles.ObjectDimentions.topmargin = 0.05 ;
	handles.ObjectDimentions.uiHigh = 0.03; %desired high in pixels
	handles.ObjectDimentions.uiWidth = 0.1; %desired widh in pixels
    
    % Choose default command line output for oticon_ecap_gui
	handles.output = hObject;
	% Update handles structure
	%% create tabs
	handles.hTabGroup = uitabgroup;
	handles.tabSoundDevice = uitab('parent', handles.hTabGroup, ...
		'title','Sound device settings');
	handles.tabStimuli = uitab('parent', handles.hTabGroup, ...
        'title','Stimulus settings');
    	
    %% set sound module
    handles.SoundModule = SoundModule('HContainer',handles.tabSoundDevice, ...
        'Topmargin', handles.ObjectDimentions.topmargin, ...
        'UiHigh', handles.ObjectDimentions.uiHigh, ...
        'UiWidth',handles.ObjectDimentions.uiWidth, ...
        'InitializeDeviceList',true);
    
    handles.ECAPModule = OticonEcapModule('HContainer',handles.tabStimuli, ...
                'Topmargin', handles.ObjectDimentions.topmargin, ...
                'UiHigh', handles.ObjectDimentions.uiHigh, ...
                'UiWidth',handles.ObjectDimentions.uiWidth, ...
                'SoundModule', handles.SoundModule);
    
    %% set ui colors
	setGuiTheme('Figure', handles.figure1, 'TabGroup', handles.hTabGroup);
    guidata(hObject,handles);
	% UIWAIT makes oticon_ecap_gui wait for user response (see UIRESUME)
	% uiwait(handles.figure1);
    set(hObject, 'DeleteFcn', @callbackCloseGuide)

	% --- Outputs from this function are returned to the command line.
function varargout = oticon_ecap_gui_OutputFcn(~, ~, handles) 
	% varargout  cell array for returning output args (see VARARGOUT);
	% hObject    handle to figure
	% eventdata  reserved - to be defined in a future version of MATLAB
	% handles    structure with handles and user data (see GUIDATA)

	% Get default command line output from handles structure
	varargout{1} = handles.output;
function callbackCloseGuide(hObject, ~)
    handles = guidata(hObject);
    delete(handles.ECAPModule);
    delete(handles.SoundModule);
    
    