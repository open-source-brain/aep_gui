function [value, time, s] = chirpGenerator(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'F1', 0);% initial frequency [Hz]
s = ef(s, 'F2', s.Fs/2);% end frequency [Hz]
s = ef(s, 'Amplitude', 0.9);%max wavfile amp
s = ef(s, 'Attenuation', 0);%attenuation
s = ef(s, 'Rate', 40);% repetition rate in hertz
s = ef(s, 'Duration', 1); %duration in secs
s = ef(s, 'dBnHL', 60);
s = ef(s, 'NRepetitions', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = ef(s, 'UseWindow',true);% apply a window to force zero begin-end
s = ef(s, 'Tol', 0.5);% spectral magnitude tolerance to force a zero start and end
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'Plot_results', 1);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory
s = ef(s, 'Bits', 16); %bit resolutiuon only used to fit residuals under LSB
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL; zero means no ref

value = [];
time = [];
if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end;
s = rmfield(s,'OnlyReturnParameters');
sweep_dur = cochlea_group_delay(s.F1, s.dBnHL) - cochlea_group_delay(s.F2, s.dBnHL);

s.Rate = min(1 / sweep_dur, s.Rate);
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Rate = 1 / (ceil(1 / (s.Rate * cMinDuration)) * cMinDuration);
end
s.Duration = ceil(s.Duration * s.Rate) / s.Rate;
time = (0:s.Fs*s.Duration-1)' * 1/s.Fs;
local_duration = sweep_dur + 0.004;
tDur = local_duration * 10;
nSamples= floor(tDur*s.Fs);
if ~rem(nSamples, 2) %if even
    NUniqFreq = ceil(1 + nSamples/2);
else %if odd
    NUniqFreq = ceil((nSamples+ 1)/2);
end
freq = (0:NUniqFreq - 1)' * s.Fs / nSamples;
pha = zeros(NUniqFreq,1);
Nf1 = find(freq>=s.F1,1,'first');
Nf2 = find(freq<=s.F2,1,'last');

%% group delay generation
Tg = cochlea_group_delay(freq(1:end), s.dBnHL);
Tg = -Tg + Tg(Nf1); % invert group delay and put it at zero
%% defining spectral magnitude
fc1 = s.F1*1.2;
s21 = 20*log10(exp(1)) * (s.F1 - fc1) ^ 2 / (2 * 3);
fc2 = s.F2*0.9;
s22 = 20*log10(exp(1)) * (s.F2 - fc2) ^ 2 / (2 * 3);
filter_amp = ones(NUniqFreq, 1);
for ni = 0 : NUniqFreq - 1
    f = freq(ni + 1);
    if (f<fc1)
        filter_amp(ni + 1) = exp(-(f - fc1) ^ 2 / (2 * s21));
    elseif (f>fc2)
        filter_amp(ni + 1) = exp(-(f - fc2) ^ 2 / (2 * s22));
    end
end
Amp = filter_amp;
% offset for fitting
% Tg_samples = round((Tg + (tDur - local_duration) / 2) * s.Fs);
Tg_samples = round((Tg) * s.Fs);
% Tg(1 : Nf1) = Tg(Nf1);
% Tg(Nf2:end) = Tg(Nf2);
if nSamples<Tg(Nf1)
    warning(['nSamples should be longer than:',num2str(Tg(Nf1))])
end

% Phase Generation (-int(Tg))
suma=0;
fstep=pi/NUniqFreq;
pha(1) = 0;
for ni = 1 : NUniqFreq - 1
    suma=suma + fstep*Tg_samples(ni);
    pha(ni + 1) = suma;
end
pha = unwrap(-pha);
% generate spectrum with negative phase to rotate time
spectrum = Amp.*exp(1i*pha) * 2;

%%reconstruct full fft
if ~rem(nSamples, 2) %if even
    fullSpectrum = [spectrum; conj(spectrum(end - 1 : -1 : 2))];
else %if odd
    fullSpectrum = [spectrum; conj(spectrum(end : -1 : 2))];
end
TfullPhase = angle(fullSpectrum);
TfullAmp = abs(fullSpectrum);
%correct phase
TfullPhase = TfullPhase - (0 : nSamples - 1)' * TfullPhase(NUniqFreq)/(NUniqFreq);
%% synthesized chirp
sweep1 = ifft(abs(TfullAmp) .* exp(1j*TfullPhase), 'symmetric');
% sweep1 = sweep1(end:-1:1);


w=ones(nSamples,1);
[w_s_end, w_e_ini] = find_window_point(sweep1);
delta_t = abs(local_duration - sweep_dur);
w_s_ini = round((Tg_samples(Nf1)/s.Fs - delta_t / 2) * s.Fs);
w_e_end = round((Tg_samples(Nf2)/s.Fs + delta_t / 2) * s.Fs);
w = generate_window(nSamples, w_s_ini, w_s_end, w_e_ini, w_e_end);
logC=1e-100;
LfftTarg=20*log10(abs(TfullAmp)+logC);

i=1;
criteria1=true;
criteria2=true;
t = ((0:(nSamples-1))/s.Fs)';
old_res = max(20*log10(abs(sweep1(w_e_end + 1: end))));
if s.UseWindow
    sweep1 = sweep1.* w; 
end
Tg_samples= -nSamples / (2 * pi) * unwrap(diff(TfullPhase));
while (criteria1 && criteria2) || i==1 
   Sfft1=fft(sweep1);
   LSfft=20*log10(abs(Sfft1)+logC);
   fullPhase = angle(Sfft1);
   OldSfft1=Sfft1;
   spectrum=TfullAmp .* exp(1i*fullPhase);
   sweep_t=real(ifft(spectrum, 'symmetric')); 
   Sfft1=fft(sweep_t,nSamples);
   residuals = max(20*log10(abs(sweep_t(w_e_end + 1: end))));
   if residuals > old_res
       criteria2 = false;
       sweep1 = sweep_t .* w;
%         w_e_ini = w_e_ini + 1;
%         w_s_end = w_s_end - 1;
%         w = generate_window(nSamples, w_s_ini, w_s_end, w_e_ini, w_e_end);
   else
       old_res = residuals; 
       criteria2 = ~isempty(find(residuals > -6.06 * s.Bits, 1));
       sweep1 = sweep_t .* w;
   end
   disp(max(residuals));
   Sstd=std(abs(OldSfft1)-abs(Sfft1));
   
   i=i+1;
   if i>1000
       criteria2 = false;
       sweep1 = sweep_t * w;
   elseif criteria2 == true
        sweep1 = sweep1.* w; 
   end
end

if s.Plot_results
    figure;
    subplot(3,2,1)
    plot(t(1:nSamples),sweep1)
    if s.UseWindow
        hold on 
        plot(t(1:nSamples),w)
        hold off
    end
    title(['Real Synthesized Chirp - ' ' model' ' - Spec Mag - ' num2str(s.Attenuation) '/oct'])
    xlabel('Time [s]')
    
    subplot(3,2,2);
    plot(freq,unwrap(TfullPhase(1:numel(freq))), ...
        freq, unwrap(fullPhase(1:numel(freq))),'linewidth',2);
    title(['Synthesized Phase - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    xlabel('Frequency [Hz]');
    ylabel('Rad')
    legend('Desired Pha','Obtained Pha')
    xlim([freq(Nf1) freq(Nf2)]);
    
    Tg1=-nSamples/(2*pi)*unwrap(diff(fullPhase));
    subplot(3,1,2);
    semilogx(freq,(Tg_samples(1:numel(freq)))./s.Fs*1e3,freq(1:end-1),(Tg1(1:numel(freq)-1))./s.Fs*1e3,'linewidth',2);
    title(['Group delay - ' ' - Spectral Magnitude']);
    xlim([freq(Nf1) freq(Nf2)]);
    xlabel('Frequency [Hz]');
    ylabel('Time [ms]');
    legend('Desired Tg','Obtained Tg');
    grid on
    
    subplot(3,1,3);
    semilogx(freq(1:round(nSamples/2)),LSfft(1:round(nSamples/2)),...
        freq(1:round(nSamples/2)),LfftTarg(1:round(nSamples/2)),...
        freq(1:round(nSamples/2)),LfftTarg(1:round(nSamples/2))+s.Tol,...
        freq(1:round(nSamples/2)),LfftTarg(1:round(nSamples/2))-s.Tol,...sweep_dur
        'linewidth',2);
    
    title(['FFT Synthesized Sweep - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    grid on;
    ylim([-6.02*s.Bits, max(max(LSfft(Nf1:Nf2))+s.Tol, -6.02*s.Bits+1)]);
    xlabel('Frequency [Hz]');
    xlim([0 s.Fs/2]);
    ylabel('Amplitude [dB]');
    legend(['Iteration number: ' num2str(i) ' std:' num2str(Sstd)]);
    
    drawnow;
end

% normalize 
cSignal = sweep1./max(abs(sweep1));
cSignal = circshift(cSignal, -w_s_ini + 1);
cSignal = cSignal(1:(w_e_end - w_s_ini) + 1);
% compute normalize rms for calibration
sRMS = rms(cSignal(:));
if s.refSPL
    scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
else
    scale_factor = s.Amplitude;
end
cSignal = scale_factor * cSignal;
s.RMS = rms(cSignal);

single_chirp_duration = max(1/s.Rate, sweep_dur);
final_size = round(single_chirp_duration * s.Fs);
cSignal = padarray(cSignal, final_size - numel(cSignal), 'pre');
s_t = (0 : final_size - 1) / s.Fs;

if s.IncludeTriggers
    value = zeros(s.Duration * s.Fs * s.NRepetitions, 2);
    s_v = zeros(s.Duration * s.Fs, 2);
    p_sam = min(round(s.PulseWidthTrigger * s.Fs), final_size);
    triggers = zeros(final_size, 1);
    triggers(final_size - p_sam + 1:end) = s.TriggerAmp;
    cChannelData = [triggers, cSignal];
else
    value = zeros(s.Duration * s.Fs * s.NRepetitions, 1);
    s_v = zeros(s.Duration * s.Fs, 1);
    cChannelData = cSignal;
end

for i = 1 : s.Duration * s.Rate
    ini_p = (i - 1) * final_size + 1;
    end_p = i * final_size;
    if s.IncludeTriggers
        s_v(ini_p: end_p, :) = cChannelData;
    else
        s_v(ini_p: end_p) = cChannelData;
    end
end

n_samples = round(s.Duration*s.Fs);
for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*n_samples + 1 : i*n_samples, :) = s_v;
    if s.IncludeTriggers
        value((i-1)*n_samples + 1 : i*n_samples, 2:end) = factor * s_v(:, 2);
    else
        value((i-1)*n_samples + 1 : i*n_samples, :) = factor * s_v;
    end
end


if s.SaveWaveFile, audiowrite('Sweep.wav', value,s.Fs); end

if s.Listen, sound(value,s.Fs,s.Bits); end

if s.Plot_results
    figure;
    w=hamming(round(max(local_duration*s.Fs/10,2)));
    spectrogram(cSignal,w,max(round(numel(w)/2),10), ...
        2^nextpow2(numel(w)), ...
        s.Fs,'yaxis');
    set(gca, 'xlim', [s_t(end) - local_duration, s_t(end)]*1000)
end

function Tg = cochlea_group_delay(f, dBnHL)
Tg = zeros(numel(f), 1);
f_lim = max(20, min(f));
for c_f = 1:numel(f)
    if f(c_f) > f_lim
        Tg(c_f) = 1.65 * exp(-0.0625*dBnHL) .* f(c_f).^-(-0.00755 * dBnHL + 0.788);
    else
        Tg(c_f) = 1.65 * exp(-0.0625*dBnHL) .* f_lim.^-(-0.00755 * dBnHL + 0.788);
    end
end

function w = generate_window(nSamples,w_s_ini, w_s_end, w_e_ini, w_e_end)
    w = ones(nSamples,1);
    w_s_ini = max(w_s_ini, 1);
    w_s_end = max(w_s_end, w_s_ini + 2);
    wsize = max(w_s_end - w_s_ini,1);
    w1 = sin(2 * pi * (0 : wsize-1) / (4 * (wsize - 1)))';
    w(1 : w_s_ini) = 0;
    w(w_s_ini : w_s_end - 1) = w1;
    w_e_end = min(w_e_end, nSamples);
    w_e_ini = min(w_e_ini, w_e_end - 2);
    wsize = w_e_end - w_e_ini;
    w2=sin(2 * pi * (0 : wsize-1) / (4 * (wsize - 1)) + pi / 2)';
    w(w_e_ini : w_e_end - 1) = w2;
    w(w_e_end : end) = 0;


function [p1, p2] = find_window_point(data)
    env =  envelope(data);
    hv1 = find(env > max(env)*0.1);
    hv2 = find(env > max(env)*0.9);
    p1 = hv1(1);
    p2 = hv2(end);
