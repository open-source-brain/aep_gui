function [value, time, s, ncycles] = clicks2(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'Phase', 0); % phase in rad
s = ef(s, 'Rate', 500); %frequency in Hz
s = ef(s, 'PulseWidth', 0.00050); %pulse width in seconds
s = ef(s, 'Alternating', false); %alternate pulses polarity
s = ef(s, 'OnlyReturnParameters', false); %dummy var
s = ef(s, 'RoundToCycle', true); %this round the click rate and file length to fit with fs
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

cRate = s.Rate;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;

    % fit rate to duration
    cRate = round(s.Duration*cRate)/s.Duration;
    s.Rate = cRate;
end
time = (0:round(s.Fs*s.Duration)-1)'*1/s.Fs;
cSignal = zeros(numel(time),1);
cTemplate = ones(round(s.Fs*s.PulseWidth), 1);
ncycles = round(s.Duration*cRate);
cPhase = s.Phase - floor(s.Phase/(2*pi))*2*pi;
for i = 1:ncycles
    cpulse_pos = round((cPhase / (2*pi) + (i - 1)) * s.Fs / cRate) + 1;
    cSignal(cpulse_pos: cpulse_pos + numel(cTemplate) - 1) = cTemplate; 
end
cSignal = s.Amplitude * cSignal;
nsamples = numel(cSignal);
value = zeros(nsamples*s.NRepetitions,1);
for i = 1:s.NRepetitions
    value((i-1)*nsamples + 1 : i*nsamples) = cSignal*(-1)^((i+1)*s.Alternating);
end
