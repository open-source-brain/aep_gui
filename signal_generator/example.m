% example
fs=44100;
t=[0:2*fs]*1/fs;
% h=y;
h = fir1(35001, [100 3000] / (fs/2))'; %
%h=exp(-t*20)';
x=constantEnvelopeSweep('Attenuation',0);
y=conv(x,h);
ymax=max(abs(y));
Ns=1;
yn=zeros(length(y),Ns);
SNR = -60;% below the convolved signal
for i=1:Ns
    yn(:,i)=y + normrnd(zeros(length(y),1),ymax*10^(SNR/20));
end;
h1=impulseResponse(x,yn);
y1=conv(x,h1(1:length(h)));
Ey=abs(y-y1);
Erms=10*log10((sum(Ey.^2)/length(Ey))^0.5);
figure;
subplot(4,1,1)
plot(x)
legend('x')
subplot(4,1,2);
plot(y);legend('y=x*h')
subplot(4,1,3);
plot(mean(yn'));
legend('yn=y+noise averaged');
subplot(4,1,4);
plot([h h1(1:length(h))]);
legend('Original IR','Estimated IR');
figure;
plot([y y1 Ey])
legend('y','y1',['abs(y-y1)''-Erms:' num2str(Erms)]);