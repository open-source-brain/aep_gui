function [value, time, s] = generateSCS(varargin)
%noise generator
%crated by super jaimito
%examples
%y=generateNoise;
%y=generateNoise('T',0.5,'Listen',true)
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3,'F1',1000,'FNoiseH',2000);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',0,'F1',1000,'FNoiseH',1020)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Duration', 4);%Duration in seconds
s = ef(s, 'Amplitude_1', 0.9);%Amplitude of the signal
s = ef(s, 'Amplitude_2', 0.9);%Amplitude of the signal
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'FLow', 300);% initial frequency [Hz]
s = ef(s, 'FHigh', 2000);% end frequency [Hz]
s = ef(s, 'NCarriers', 17);% number of carriers to be used
s = ef(s, 'AltRate', 1);
s = ef(s, 'AltDeltaChange', 1);
s = ef(s, 'DeltaFrequencyPercentage', 0); % percentage of frequencyt change 
s = ef(s, 'FMFrequency', 0); % frequency in Hz of the delta frequecy
s = ef(s, 'FMPhase', 0); % phase of the FM modulator in rad
s = ef(s, 'EdgeFrequencyWidthPercentage', 0.2); % keep edged unchanged within this amount of Hz
s = ef(s, 'ConstrainEdges', 0); % if true, spectrum will be limited 
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'PlotResults', false);
s = ef(s, 'ModulationFrequency', 41); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 1); %frequency in Hz
s = ef(s, 'ModulationDepthNoise', 1); % depth of level roving (1 is fully modulated, 0 is no modulation)
s = ef(s, 'ModulationCutoffLow', 0.2); % low cutoff frequency for envelope modulation
s = ef(s, 'ModulationCutoffHigh', 4); % low cutoff frequency for envelope modulation
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'PulseWidth', 0.0005); % duration in seconds
s = ef(s, 'IncludeTriggers', 1); % 
s = ef(s, 'TriggersEveryModulation', 1); % 
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); % duration in seconds
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL; zero means no ref
s = ef(s, 'ShowSpectrogram', false);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', 0);%if is true a wavfile is created in the current directory
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.AltRate = 1 / (round((1 / s.AltRate)/cMinDuration) * cMinDuration);
end

block_duration = 1 / s.AltRate;
% we ensure even number of events for epoch continuity
s.Duration = ceil(s.Duration * s.AltRate / 2) * 2 / s.AltRate;

cModulationFrequency = round(s.ModulationFrequency / s.AltRate) * s.AltRate;
s.ModulationFrequency = cModulationFrequency;

s.FMPhase = round(s.FMPhase / s.AltRate) * s.AltRate;
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
N = ceil(s.Duration*s.Fs);

stim_freqs = logspace(log10(s.FLow), log10(s.FHigh), s.NCarriers);
amp = 1;


nCycles = round(s.Duration * s.AltRate);
nBlockSamples = round(block_duration*s.Fs);


tblock = (0 : nBlockSamples - 1)' / s.Fs;
y_signal_1 = zeros(nBlockSamples * nCycles, 1); % reference
y_signal_2 = zeros(nBlockSamples * nCycles, 1); % target

for n_block = 0 : nCycles / 2 - 1
    for c_idx = 1:numel(stim_freqs)
        rng(c_idx);
        phase = rand * 2 * pi;
        c_ini = nBlockSamples * (2 * n_block) + 1;
        c_end = c_ini + nBlockSamples - 1;
        freq_dev = stim_freqs(c_idx) * s.DeltaFrequencyPercentage;
        alt_factor = (-1) ^ (s.AltDeltaChange*(c_idx + 1));
        instantaneous_frequency = stim_freqs(c_idx) + freq_dev * sin(...
            2 * pi * s.FMFrequency * tblock + s.FMPhase) * ...
            alt_factor;
        int_f = cumsum(instantaneous_frequency)/s.Fs;
        current_carrier = sin(2 * pi  * stim_freqs(c_idx) * tblock + ...
            phase);
        y_signal_1(c_ini : c_end, 1) = y_signal_1(c_ini : c_end, 1) ...
            + current_carrier;
        % update indexes for second signal
        c_ini = nBlockSamples * (2 * n_block + 1) + 1;
        c_end = c_ini + nBlockSamples - 1;
        if s.ConstrainEdges
            if ~((stim_freqs(c_idx) < (s.FHigh * (1 - s.EdgeFrequencyWidthPercentage) - ...
                    s.ModulationFrequency - freq_dev)) && ...
               (stim_freqs(c_idx) > (s.FLow * (1 + s.EdgeFrequencyWidthPercentage) + ...
               s.ModulationFrequency + freq_dev)))
                int_f = stim_freqs(c_idx) * tblock;
            end
        end
        y_signal_2(c_ini : c_end, 1) = y_signal_2(c_ini : c_end, 1) ...
                +  sin(2 * pi  * int_f + phase);
    end
end

% normalize 
y_signal_1 = y_signal_1 ./ max(abs(y_signal_1(:)));
y_signal_2 = y_signal_2 ./ max(abs(y_signal_2(:)));
% compute normalize rms for calibration
sRMS_1 = rms(y_signal_1(y_signal_1 ~= 0));
sRMS_2 = rms(y_signal_2(y_signal_2 ~= 0));

if s.refSPL
    scale_factor_1 = 10^(s.Amplitude_1/20) * s.refSPL/sRMS_1;
    scale_factor_2 = 10^(s.Amplitude_2/20) * s.refSPL/sRMS_2;
else
    scale_factor_1 = s.Amplitude_1;
    scale_factor_2 = s.Amplitude_2;
end
cSignal = scale_factor_1 * y_signal_1 + scale_factor_2 * y_signal_2;
s.RMS = rms(cSignal(:,1));

m_amp = (1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase)) / 2;
cSignal = m_amp .* cSignal;
nsamples = numel(cSignal);


%% Level roving
% The amplitude of the signal is modulated using a generated noise signal
% with low frequency components to mimick slow variations found in speech
% signals. If fully modulated (1), minimum is 0, if not modulated (0),
% the resulting modulation signal is constant at 1.

[Modulation_env, ~, ~] = generateModulatedNoise(...
    'Fs', s.Fs, ...
    'Amplitude', 1, ...
    'FNoiseL', s.ModulationCutoffLow, ...
    'FNoiseH', s.ModulationCutoffHigh, ...
    'Duration', s.Duration);

m = s.ModulationDepthNoise /(max(Modulation_env) - min(Modulation_env)); % scale factor
x0 = s.ModulationDepthNoise * ( 0.5 - max(Modulation_env)) /(max(Modulation_env) - min(Modulation_env)); % offset factor

Modulation_env = Modulation_env * m + x0;
Modulation_env = Modulation_env + 1 - max(Modulation_env);

% Apply Modulation envelope signal to carrier signal
cSignal = cSignal .* Modulation_env;

cSignal = [cSignal, cSignal];

%% Include triggers
if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions, 3);
    if s.TriggersEveryModulation
        trigger_rate = s.AltRate;
    else
        trigger_rate = 1 / s.Duration;
    end
    triggers = clicks2('Duration', s.Duration, 'Rate', trigger_rate, ...
        'PulseWidth', s.PulseWidthTrigger, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cSignal];
else
    value = zeros(nsamples*s.NRepetitions, 2);
    cChannelData = cSignal;
end

for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples,2:end) = factor*cChannelData(:,2:end);
    else
        value((i-1)*nsamples + 1 : i*nsamples, :) = factor*cChannelData;
    end
end

file_name = '';
if s.SaveWaveFile
    fnames = fieldnames(s);
    for i = 1 : numel(fnames)
        cItem = fnames{i};
        cVal = s.(cItem);
        cItemAbb = cItem(regexp(cItem,'(?<![A-Z])[A-Z]{1,3}(?![A-Z])'));
        cItemAbb = cItemAbb(1:min(2,numel(cItemAbb)));
        ctext = '';
        switch  class(cVal)
        	case 'double'
            	textvalue = sprintf('%.1e', cVal);
                textvalue = textvalue(1:min(6,numel(textvalue)));
                ctext = strcat(cItemAbb,'-',textvalue);
            case 'char'
            	cVal = cVal(1:min(2,numel(cVal)));
                ctext = strcat(cItemAbb,'-',cVal);
        end
        file_name = strcat(file_name, ctext, '_');    
    end    
    file_name = regexprep(file_name,'\.','_');
    file_name = num2str(s.ITD*1e6);
    audiowrite(strcat(file_name,'.wav'), value, s.Fs, 'BitsPerSample', 16);
end

if s.Listen, sound(real(sweep),s.Fs,16); end

if s.ShowSpectrogram 
    hspc=figure;
    samples = min(size(cSignal, 1), 10 * s.Fs);
    cwt(cSignal(1:samples,1), s.Fs);
%     w=hamming(1024 * 4);
%     spectrogram(y_sum_signal,w,length(w)/2,2048 * 2,s.Fs,'yaxis');
    hcb=colorbar;
    max_val= max(abs(cSignal(:,1)));
    min_val = min(abs(cSignal(:,1)));
    set(hcb,'Limits',[min_val, max_val]);
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end
