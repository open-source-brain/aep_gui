function [value, time, s] = generateModulatedNoiseInNotchNoise(varargin)
%noise generator
%crated by super jaimito
%examples
%generateSinusoidalNotchNoise('Amplitude', 0.03, 'CarrierFrequency', 4000, 'FMNoiseL', 3200, 'FMNoiseH', 4800, 'Listen',1, 'PlotResults',1, 'SNR',20, 'ModulationFrequency', 100, 'ModulationIndex', 1)

s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Duration', 1);%Duration of file(would be round to a power of 2)
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'FNoiseL', 1000);% initial frequency [Hz]
s = ef(s, 'FNoiseH', 2000);% end frequency [Hz]
s = ef(s, 'FMNoiseL', 1000);% initial frequency [Hz]
s = ef(s, 'FMNoiseH', 2000);% end frequency [Hz]
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'SNR', 10);%SNR in dB between not modulated sinusoidal and noise
s = ef(s, 'NRepetitions', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'PlotResults', false);
s = ef(s, 'ShowSpectrogram', false);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory
s = ef(s, 'OnlyReturnParameters', 0); % dummy var

value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');


cModulationFrequency = s.ModulationFrequency;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    %fit modulation rate
    cModulationFrequency   = round(s.Duration*cModulationFrequency)/s.Duration;
    s.ModulationFrequency  = cModulationFrequency;
end
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;

%% notch noise
N = ceil(s.Duration * s.Fs);
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
else %if odd
    NUniqFreq = ceil((N + 1)/2);
end

freq = ((0:NUniqFreq - 1) * s.Fs/N)';
p=s.Attenuation/(20*log10(0.5));
Amp=zeros(NUniqFreq,1);
Nf1=find(freq < s.FMNoiseL,1,'first');
Nf2=find(freq > s.FMNoiseH,1,'last');

% defining spectral magnitude
% start from 1 to ensure no DC
for n = 1:NUniqFreq - 1;
    f=freq(n+1);
    if (f < s.FMNoiseL) || (f> s.FMNoiseH) 
        Amp(n+1) = 1 / ((f+1)^(p));
    else
        Amp(n+1)=0;
    end;
end;
 
% Phase Generation
pha = 2*pi*rand(NUniqFreq,1);
spectrum=Amp.*exp(1i*pha);

%%reconstruct full fft
if ~rem(N, 2) %if even
    fullSpectrum = [spectrum; conj(spectrum(end - 1 : -1 : 2))];
else %if odd
    fullSpectrum = [spectrum; conj(spectrum(end : -1 : 2))];
end

% synthesized noise
cNoise=real(ifft(fullSpectrum));


%% modulated band noise
AmpBN=zeros(NUniqFreq,1);
NFNoiseL=find(freq>=s.FNoiseL,1,'first');
NFNoiseH=find(freq<=s.FNoiseH,1,'last');

% defining spectral magnitude
for n = 1:NUniqFreq - 1;
    f=freq(n+1);
    if (f>=s.FNoiseL) && (f<=s.FNoiseH) 
        AmpBN(n+1) = 1/((f+1)^(p));
    else
        AmpBN(n+1)=0;
    end;
end;
 
% Phase Generation
phaBN = 2*pi*rand(NUniqFreq,1);
spectrumBN=AmpBN.*exp(1i*phaBN);

%reconstruct full fft
if ~rem(N, 2) %if even
    fullSpectrumBN = [spectrumBN; conj(spectrumBN(end - 1 : -1 : 2))];
else %if odd
    fullSpectrumBN = [spectrumBN; conj(spectrumBN(end : -1 : 2))];
end

% synthesized noise
cBNoise=real(ifft(fullSpectrumBN));
cBNoise = cBNoise / max(abs(cBNoise));

% modulated noise
cAmplitude = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
     s.ModulationPhase))/ (1 + s.ModulationIndex);
cBNoise = cAmplitude .* cBNoise;
% %% filter and remove boundary artifacts
% cBNoiseFft = fft(cBNoise);
% AmpBN = zeros(NUniqFreq,1);
% % defining spectral magnitude
% for n = 1:NUniqFreq - 1;
%     f=freq(n+1);
%     if (f>=s.FNoiseL) && (f<=s.FNoiseH) 
%         AmpBN(n+1) = cBNoiseFft(n + 1);
%     else
%         AmpBN(n+1) = 0;
%     end
% end
% 
% %reconstruct full fft
% if ~rem(N, 2) %if even
%     fullSpectrumBN = [AmpBN; conj(AmpBN(end - 1 : -1 : 2))];
% else %if odd
%     fullSpectrumBN = [AmpBN; conj(AmpBN(end : -1 : 2))];
% end
% cBNoise=real(ifft(fullSpectrumBN));
%%


cNoiseFactor = 10 ^ (- s.SNR / 20) * nt_rms(cBNoise) / nt_rms(cNoise);

cNoise = cNoiseFactor * cNoise;
disp(20*log10(nt_rms(cBNoise) / nt_rms(cNoise)));
stimulus = cNoise + cBNoise;


if s.PlotResults
    Sfft1=abs(fft(stimulus)) * 2  / numel(stimulus);
    figure;
    subplot(3,1,1)
    plot(time,[stimulus, cAmplitude])
    title(['Real Synthesized Noise - ' ' model' ' - Spec Mag - ' num2str(s.Attenuation) '/oct'])
    xlabel('Time [ms]')
    
    subplot(3,1,2);
    plot(freq,unwrap(pha),'linewidth',2);
    title(['Synthesized Phase - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    xlabel('Frequency [Hz]');
    ylabel('Rad')
    legend('Obtained Pha')
    xlim([freq(Nf1) freq(Nf2)]);
    grid on
    subplot(3,1,3);
    plot(freq,Sfft1(1:NUniqFreq))
    
    title(['FFT Synthesized Sweep - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    grid on;
    xlabel('Frequency [Hz]');
    xlim([0 s.Fs/2]);
    ylabel('Amplitude [V]');
    legend('Obtained');
end   

for i = 1 : s.NRepetitions 
    value = [value; stimulus];
end;

if s.SaveWaveFile, audiowrite(value, s.Fs, 16,'Sweep.wav'); end
if s.Listen, sound(value, s.Fs, 16); end

if s.ShowSpectrogram 
    hspc=figure;
    w=hamming(1024);
    spectrogram(cNoise,w,length(w)/2,2048,s.Fs,'yaxis');
%     hcb=colorbar;
    %set(hcb,'CLim',[min(abs(cNoise)) max(abs(cNoise))]);
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end
