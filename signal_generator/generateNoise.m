function [value, time, s] = generateNoise(varargin)
%noise generator
%crated by super jaimito
%examples
%y=generateNoise;
%y=generateNoise('T',0.5,'Listen',true)
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3,'F1',1000,'F2',2000);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',0,'F1',1000,'F2',1020)

s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Duration', 1);%Duration of file(would be round to a power of 2)
s = ef(s, 'Amplitude', 0.9);%max wavfile amp
s = ef(s, 'FLow', 0);% initial frequency [Hz]
s = ef(s, 'FHigh', s.Fs/2);% end frequency [Hz]
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'NRepetitions', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'PlotResults', 0);
s = ef(s, 'ShowSpectrogram', true);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL; zero means no ref

value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end;
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
     %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
end

time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
nSamples= floor(s.Duration*s.Fs);
if ~rem(nSamples, 2) %if even
    NUniqFreq = ceil(1 + nSamples/2);
else %if odd
    NUniqFreq = ceil((nSamples+ 1)/2);
end
freq = (0:NUniqFreq - 1)' * s.Fs / nSamples;
p=s.Attenuation/(20*log10(0.5));

Amp=zeros(NUniqFreq,1);
Nf1=find(freq>=s.FLow,1,'first');
Nf2=find(freq<=s.FHigh,1,'last');

%% defining spectral magnitude
for ni = 0 : NUniqFreq - 1
    f = freq(ni + 1);
    if (f>=s.FLow) && (f<=s.FHigh) 
        Amp(ni + 1) = 1/((f+1)^p);
    elseif (f<s.FLow)
        Amp(ni + 1) = 0;
    elseif (f>s.FHigh)
        Amp(ni + 1) = 0;
    end;
end;

%% Phase Generation
pha = pi * (rand(NUniqFreq,1) - 0.5) / 0.5;
pha = unwrap(-pha);
spectrum = Amp.*exp(1i*pha) * 2 / nSamples;

%%reconstruct full fft
if ~rem(nSamples, 2) %if even
    fullSpectrum = [spectrum; conj(spectrum(end - 1 : -1 : 2))];
else %if odd
    fullSpectrum = [spectrum; conj(spectrum(end : -1 : 2))];
end
TfullPhase = angle(fullSpectrum);
TfullAmp = abs(fullSpectrum);
%correct phase
TfullPhase = TfullPhase - (0 : nSamples - 1)' * TfullPhase(NUniqFreq)/(NUniqFreq);
sweep1 = ifft(TfullAmp .* exp(1j * TfullPhase), 'symmetric');

% normalize
cSignal=sweep1./max(abs(sweep1));

% compute normalize rms for calibration
sRMS = rms(cSignal(:));
if s.refSPL
    scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
else
    scale_factor = s.Amplitude;
end
cSignal = scale_factor * cSignal;
s.RMS = rms(cSignal);


Sfft1=abs(fft(cSignal));

if s.PlotResults
    disp(['Approx. Time: ' num2str(s.Duration)]);
    figure;
    subplot(3,1,1)
    plot(time, cSignal)
    title(['Real Synthesized Noise - ' ' model' ' - Spec Mag - ' num2str(s.Attenuation) '/oct'])
    xlabel('Time [ms]')
    
    subplot(3,1,2);
    plot(freq,unwrap(pha),'linewidth',2);
    title(['Synthesized Phase - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    xlabel(['Frequency [Hz]']);
    ylabel('Rad')
    legend('Obtained Pha')
    xlim([freq(Nf1) freq(Nf2)]);
    
    grid on
    subplot(3,1,3);
    plot(freq,20*log10(Sfft1(1:NUniqFreq)))
    
    title(['FFT Synthesized noise - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    grid on;
    xlabel('Frequency [Hz]');
    xlim([0 s.Fs/2]);
    ylabel('Amplitude [dB]');

    hspc=figure;
    w=hamming(1024);
    spectrogram(sweep1,w,length(w)/2,2048,s.Fs,'yaxis');
    hcb=colorbar;
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end

N = numel(cSignal);
value = zeros(N*s.NRepetitions,1);
for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1) * N+ 1 : i*N) = factor*cSignal;
end

if s.SaveWaveFile, audiowrite('Sweep.wav', sweep,s.Fs); end
if s.Listen, sound(real(sweep),s.Fs,16); end


