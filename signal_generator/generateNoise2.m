function [sweep] = generateNoise2(varargin)
%noise generator
%crated by super jaimito
%examples
%y=generateNoise;
%y=generateNoise('T',0.5,'Listen',true)
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3,'F1',1000,'F2',2000);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',0,'F1',1000,'F2',1020)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Duration', 1);%Duration of file(would be round to a power of 2)
s = ef(s, 'F1', 0);% initial frequency [Hz]
s = ef(s, 'F2', s.Fs/2);% end frequency [Hz]
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'Periods', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = ef(s, 'PlotResults', false);
s = ef(s, 'ShowSpectrogram', false);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory

N = ceil(s.Duration*s.Fs);
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
else %if odd
    NUniqFreq = ceil((N + 1)/2);
end

tn=(0:N-1)'; 
Duration=((0:(N*s.Periods-1))/s.Fs)';
freq=((0:NUniqFreq - 1)*s.Fs/N)';

p=s.Attenuation/(20*log10(0.5));

pha=zeros(NUniqFreq,1);
Tg=zeros(NUniqFreq,1);
Amp=zeros(NUniqFreq,1);
NT0=0;
NT1=NUniqFreq;
Nf1=find(freq>=s.F1,1,'first');
Nf2=find(freq<=s.F2,1,'last');

%% defining spectral magnitude
for n = 0:NUniqFreq - 1;
    f=freq(n+1);
    if (f>=s.F1) && (f<=s.F2) 
        Amp(n+1) = 1/((f+1)^(p));
    else
        Amp(n+1)=0;
    end;
end;

% %% even symmetry magnitude
% for n=1:(N/2)-1;
%     Amp(N/2+n+1)=Amp(N/2-n+1);
% end;

 
%% Phase Generation
pha = 2*pi*rand(NUniqFreq,1);

% %% odd symmetry 
% for n=1:(N/2)-1;
%     pha(N/2+n+1)=-pha(N/2-n+1);
% end;
% pha=unwrap(pha);
pha1=pha;


spectrum=Amp.*exp(j*pha);

%%reconstruct full fft
if ~rem(N, 2) %if even
    fullSpectrum = [spectrum; conj(spectrum(end - 1 : -1 : 2))];
else %if odd
    fullSpectrum = [spectrum; conj(spectrum(end : -1 : 2))];
end

%% synthesized noise
sweep1=real(ifft(fullSpectrum, 'symmetric'));
Sfft1=abs(fft(sweep1));
%logC=1e-100;
%LSfft=20*log10(Sfft1/max(Sfft1)+logC);
%LfftTarg=20*log10(abs(Amp)/max(Amp)+logC);

if s.PlotResults
    disp(['Approx. Time: ' num2str(N/s.Fs)]);
    figure;
    subplot(3,1,1)
    plot(Duration(1:N),sweep1./max(abs(sweep1)))
    title(['Real Synthesized Noise - ' ' model' ' - Spec Mag - ' num2str(s.Attenuation) '/oct'])
    xlabel('Time [ms]')
    
    subplot(3,1,2);
    plot(freq,unwrap(pha),'linewidth',2);
    title(['Synthesized Phase - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    xlabel(['Frequency [Hz]']);
    ylabel('Rad')
    legend('Obtained Pha')
    xlim([freq(Nf1) freq(Nf2)]);
    
    grid on
    subplot(3,1,3);
    plot(freq,Sfft1(1:NUniqFreq))
%     semilogx(freq(1:N/2),LSfft(1:N/2),...
%         freq(1:N/2),LfftTarg(1:N/2),...
%         'linewidth',2);
    
    title(['FFT Synthesized Sweep - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    grid on;
    %ylim([0 max(LSfft(Nf1:Nf2))]);
    xlabel('Frequency [Hz]');
    xlim([0 s.Fs/2]);
    ylabel('Amplitude [dB]');
    legend('Obtained','Desiered');
end   
sweep1=sweep1./max(abs(sweep1));
sweep=[];
for i = 1 : s.Periods 
    sweep = [sweep sweep1];
end;

if s.SaveWaveFile, wavwrite(sweep,s.Fs,16,'Sweep.wav'); end
if s.Listen, sound(real(sweep),s.Fs,16); end

if s.ShowSpectrogram 
    hspc=figure;
    w=hamming(1024);
    spectrogram(sweep1,w,length(w)/2,2048,s.Fs,'yaxis');
    hcb=colorbar;
    set(hcb,'Limits',[min(abs(sweep1)) max(abs(sweep1))]);
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end
