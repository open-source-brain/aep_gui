function [value, time, s] = generateNoiseDelaySignal(varargin)
%noise generator
%crated by super jaimito
%examples
%y=generateNoise;
%y=generateNoise('T',0.5,'Listen',true)
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3,'F1',1000,'F2',2000);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',0,'F1',1000,'F2',1020)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Duration', 1);%Duration in seconds
s = ef(s, 'Amplitude', 1);%Amplitude of the signal
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'F1', 0);% initial frequency [Hz]
s = ef(s, 'F2', s.Fs/2);% end frequency [Hz]
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'Periods', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = ef(s, 'PlotResults', false);
s = ef(s, 'ITD', 0);
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'Description', '');
s = ef(s, 'ShowSpectrogram', false);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

N = ceil(s.Duration*s.Fs);
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
else %if odd
    NUniqFreq = ceil((N + 1)/2);
end
Duration=((0:(N*s.Periods-1))/s.Fs)';
freq=((0:NUniqFreq - 1)*s.Fs/N)';
p=s.Attenuation/(20*log10(0.5));
Amp=zeros(NUniqFreq,1);
Nf1=find(freq>=s.F1,1,'first');
Nf2=find(freq<=s.F2,1,'last');

%% defining spectral magnitude
for n = 0:NUniqFreq - 1
    f=freq(n+1);
    if (f>=s.F1) && (f<=s.F2) 
        Amp(n+1) = 1/((f+1)^(p));
    else
        Amp(n+1)=0;
    end;
end;

%% Phase Generation
pha = 2*pi*rand(NUniqFreq,1);
spectrum=Amp.*exp(1i*pha);
%%reconstruct full fft
if ~rem(N, 2) %if even
    fullSpectrum = [spectrum; conj(spectrum(end - 1 : -1 : 2))];
else %if odd
    fullSpectrum = [spectrum; conj(spectrum(end : -1 : 2))];
end

%% synthesized noise
cSignal=real(ifft(fullSpectrum, 'symmetric'));
% normalize 
cSignal = s.Amplitude * cSignal / max(abs(cSignal));
delay_samples = round(s.ITD * s.Fs);
dSignal= circshift(cSignal, delay_samples);
%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow.*cSignal;
    dSignal = cWindow.*dSignal;
end

time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
nsamples = numel(cSignal);
value = zeros(nsamples*s.NRepetitions,2);
progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = factor*[cSignal, dSignal];
end

if s.SaveWaveFile, audiowrite('signal.wav', value,s.Fs, 'BitsPerSample', 16); end
if s.Listen, sound(real(sweep),s.Fs,16); end

if s.PlotResults
    Sfft1=abs(fft(cSignal));
    disp(['Approx. Time: ' num2str(N/s.Fs)]);
    figure;
    subplot(3,1,1)
    plot(Duration(1:N),cSignal./max(abs(cSignal)))
    title(['Real Synthesized Noise - ' ' model' ' - Spec Mag - ' num2str(s.Attenuation) '/oct'])
    xlabel('Time [ms]')
    
    subplot(3,1,2);
    plot(freq,unwrap(pha),'linewidth',2);
    title(['Synthesized Phase - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    xlabel('Frequency [Hz]');
    ylabel('Rad')
    legend('Obtained Pha')
    xlim([freq(Nf1) freq(Nf2)]);
    
    grid on
    subplot(3,1,3);
    plot(freq,Sfft1(1:NUniqFreq))
   
    title(['FFT Synthesized Sweep - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    grid on;
    %ylim([0 max(LSfft(Nf1:Nf2))]);
    xlabel('Frequency [Hz]');
    xlim([0 s.Fs/2]);
    ylabel('Amplitude [dB]');
    legend('Obtained','Desiered');
end



if s.ShowSpectrogram 
    hspc=figure;
    w=hamming(1024);
    spectrogram(cSignal,w,length(w)/2,2048,s.Fs,'yaxis');
    hcb=colorbar;
    set(hcb,'Limits',[min(abs(cSignal)) max(abs(cSignal))]);
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end
