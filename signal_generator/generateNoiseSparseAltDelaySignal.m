function [value, time, s] = generateNoiseSparseAltDelaySignal(varargin)
%noise generator
%crated by super jaimito
%examples
%y=generateNoise;
%y=generateNoise('T',0.5,'Listen',true)
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3,'F1',1000,'FNoiseH',2000);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',0,'F1',1000,'FNoiseH',1020)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Duration', 4);%Duration in seconds
s = ef(s, 'Amplitude', 0.9);%Amplitude of the signal
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'FNoiseL', 300);% initial frequency [Hz]
s = ef(s, 'FNoiseH', 700);% end frequency [Hz]
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'PlotResults', false);
s = ef(s, 'ITD', 0);
s = ef(s, 'AltITD', 0);
s = ef(s, 'ITDModIniPhase', 0);
s = ef(s, 'ITDModEndPhase', pi/2);
s = ef(s, 'DioticControl', 0); %if true, the ipd is zero but monaural phase shifts are kept
s = ef(s, 'ITDModRate', 2.0); % phase in Hz
s = ef(s, 'ModulationFrequency', 41); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 1); %frequency in Hz
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'FitSpectrum', 0); % will fit spectrom to meet time constrains
s = ef(s, 'FilterType', 'gaussian'); % can be gaussian or rectangular
s = ef(s, 'IncludeTriggers', 0); % if true add trigger channel
s = ef(s, 'PulseWidth', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); % duration in seconds
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL; zero means no ref
s = ef(s, 'ShowSpectrogram', false);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', 0);%if is true a wavfile is created in the current directory
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.ITDModRate = 1 / (round((1 / s.ITDModRate)/cMinDuration) * cMinDuration);
end

block_duration = 1 / s.ITDModRate;
% we ensure even number of events for epoch continuity
s.Duration = ceil(s.Duration * s.ITDModRate / 2) * 2 / s.ITDModRate;

cModulationFrequency   = round(s.ModulationFrequency / s.ITDModRate) * s.ITDModRate;
s.ModulationFrequency = cModulationFrequency;

time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
N = ceil(s.Duration*s.Fs);
% 
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
else %if odd
    NUniqFreq = ceil((N + 1)/2);
end
freq = ((0:NUniqFreq - 1) * s.Fs/N)';
lSignal = [];
rSignal = [];
nCycles = round(s.Duration * s.ITDModRate);
for i = 1 : nCycles
    %% synthesized noise (we make it longer than original block to apply a delay safely
    %% without edge distorsions
    if mod(i,2) == 0
        delay = s.AltITD;
    else    
        delay = s.ITD;
    end
    lSignal_block = generateModulatedNoise('Duration', block_duration + abs(2 * delay), ...
        'Fs', s.Fs, ...
        'PlotResults', 0);
    delaySignal_block = delay_signal(lSignal_block, s.Fs,  delay);
    vini_pos = floor(size(lSignal_block, 1) / 2 - block_duration * s.Fs / 2) + 1;
    samp_vec = vini_pos : vini_pos + round(block_duration * s.Fs) - 1;
    delaySignal_block = delaySignal_block(samp_vec);
    rSignal_block = lSignal_block(samp_vec);
    
    nModPerBlcok = s.ModulationFrequency / s.ITDModRate;
    for j = 1 : nModPerBlcok
        ini_pos = floor((s.ITDModIniPhase/ (2 * pi) / s.ModulationFrequency + (j - 1) / s.ModulationFrequency) * s.Fs) + 1;
        end_pos = floor((s.ITDModEndPhase/ (2 * pi) / s.ModulationFrequency + (j - 1) / s.ModulationFrequency) * s.Fs) + 1;
        rSignal_block(ini_pos: end_pos) = delaySignal_block(ini_pos: end_pos);
    end
    
    lSignal = [lSignal; lSignal_block(samp_vec)];
    rSignal = [rSignal; rSignal_block];
end

m_amp = (1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));
lSignal = lSignal .* m_amp;
rSignal = rSignal .* m_amp;
if s.DioticControl
    lSignal = rSignal;
end

target_spectrum = create_spectrum('NSamples', numel(lSignal),...
    'Attenuation', s.Attenuation, ...
    'Fs', s.Fs, ...
    'FLow', s.FNoiseL, ...
    'FHigh', s.FNoiseH, ...
    'Type', s.FilterType);
% fit resulting signals to improve desired bandwidth
%% generates rise fall window
cWindow = ones(size(lSignal));
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    lSignal = cWindow.*lSignal;
    rSignal = cWindow.*rSignal;
end

if s.FitSpectrum
    lSignal = fit_spectrum(lSignal, m_amp .* cWindow, target_spectrum);
    rSignal = fit_spectrum(rSignal, m_amp .* cWindow, target_spectrum);
else
    lSignal = filt_filt_fft_filter(lSignal, target_spectrum) .* cWindow ;
    rSignal = filt_filt_fft_filter(rSignal, target_spectrum) .* cWindow;
end
%equalize energy
rms_ref = rms(lSignal);
rSignal = rms_ref / rms(rSignal) * rSignal;
nsamples = numel(lSignal);
cSignal = [lSignal, rSignal];
% normalize 
cSignal = cSignal ./ max(abs(cSignal));
% compute normalize rms for calibration
sRMS = rms(lSignal(:))/max(abs(lSignal(:)));
if s.refSPL
    scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
else
    scale_factor = s.Amplitude;
end
cSignal = scale_factor * cSignal;
s.RMS = rms(cSignal(:,1));

if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions,3);
    triggers = clicks('Duration', s.Duration, 'Rate', 1 / s.Duration, ...
        'PulseWidth', s.PulseWidth, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cSignal];
else
    value = zeros(nsamples*s.NRepetitions,2);
    cChannelData = cSignal;
end

for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples,2:end) = factor*cChannelData(:,2:end);
    else
        value((i-1)*nsamples + 1 : i*nsamples, :) = factor*cChannelData;
    end
end

file_name = '';
if s.SaveWaveFile
    fnames = fieldnames(s);
    for i = 1 : numel(fnames)
        cItem = fnames{i};
        cVal = s.(cItem);
        cItemAbb = cItem(regexp(cItem,'(?<![A-Z])[A-Z]{1,3}(?![A-Z])'));
        cItemAbb = cItemAbb(1:min(2,numel(cItemAbb)));
        ctext = '';
        switch  class(cVal)
        	case 'double'
            	textvalue = sprintf('%.1e', cVal);
                textvalue = textvalue(1:min(6,numel(textvalue)));
                ctext = strcat(cItemAbb,'-',textvalue);
            case 'char'
            	cVal = cVal(1:min(2,numel(cVal)));
                ctext = strcat(cItemAbb,'-',cVal);
        end
        file_name = strcat(file_name, ctext, '_');    
    end    
    file_name = regexprep(file_name,'\.','_');
    audiowrite(strcat(file_name,'.wav'), value, s.Fs, 'BitsPerSample', 16);
end

if s.Listen, sound(real(sweep),s.Fs,16); end

if s.PlotResults
    lSignal = [lSignal, rSignal];
    Sfft1=abs(fft(lSignal));
    disp(['Approx. Time: ' num2str(N/s.Fs)]);
    figure;
    subplot(3,1,1)
    n_signal = lSignal./ max(abs(lSignal));
    envs = abs(hilbert(n_signal));
    plot(time, [m_amp, n_signal, envs])
    title(['Real Synthesized Noise - ' ' model' ' - Spec Mag - ' num2str(s.Attenuation) '/oct'])
    xlabel('Time [ms]')
    
    subplot(3,1,2);
    plot(freq(1:NUniqFreq),unwrap(angle(Sfft1(1:NUniqFreq, :))),'linewidth',2);
    title(['Synthesized Phase - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    xlabel('Frequency [Hz]');
    ylabel('Rad')
    legend('Obtained Pha')
        
    grid on
    subplot(3,1,3);
    plot(freq(1:NUniqFreq),Sfft1(1:NUniqFreq, :))
   
    title(['FFT Synthesized Sweep - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    grid on;
    %ylim([0 max(LSfft(Nf1:NFNoiseH))]);
    xlabel('Frequency [Hz]');
    xlim([0 s.Fs/2]);
    ylabel('Amplitude [dB]');
    legend('Obtained','Desiered');
end

if s.ShowSpectrogram 
    hspc=figure;
    w=hamming(1024);
    spectrogram(lSignal,w,length(w)/2,2048,s.Fs,'yaxis');
    hcb=colorbar;
    set(hcb,'Limits',[min(abs(lSignal)) max(abs(lSignal))]);
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end
