function [value, time, s] = generateNotchNoise(varargin)
%noise generator
%crated by super jaimito
%examples
%y=generateNoise;
%y=generateNoise('T',0.5,'Listen',true)
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',-3,'FNoiseL',1000,'FNoiseH',2000);
%y=generateNoise('T',0.5,'Listen',true,'Attenuation',0,'FNoiseL',1000,'FNoiseH',1020)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'Duration', 1);%Duration of file(would be round to a power of 2)
s = ef(s, 'FNoiseL', 1000);% initial frequency [Hz]
s = ef(s, 'FNoiseH', 2000);% end frequency [Hz]
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'NRepetitions', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'PlotResults', false);
s = ef(s, 'ShowSpectrogram', false);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory
s = ef(s, 'OnlyReturnParameters', 0); % dummy var

value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

cModulationFrequency = s.ModulationFrequency;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    %fit modulation rate
    cModulationFrequency   = round(s.Duration*cModulationFrequency)/s.Duration;
    s.ModulationFrequency  = cModulationFrequency;
end
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
N = ceil(s.Duration * s.Fs);
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
else %if odd
    NUniqFreq = ceil((N + 1)/2);
end

freq = ((0:NUniqFreq - 1) * s.Fs/N)';

p=s.Attenuation/(20*log10(0.5));

%pha=zeros(NUniqFreq,1);
%Tg=zeros(NUniqFreq,1);
Amp=zeros(NUniqFreq,1);
%NT0=0;
%NT1=NUniqFreq;
Nf1=find(freq < s.FNoiseL,1,'first');
Nf2=find(freq > s.FNoiseH,1,'last');

%% defining spectral magnitude
for n = 0:NUniqFreq - 1;
    f=freq(n+1);
    if (f < s.FNoiseL) || (f> s.FNoiseH) 
        Amp(n+1) = 1/((f+1)^(p));
    else
        Amp(n+1)=0;
    end;
end;

% %% even symmetry magnitude
% for n=1:(N/2)-1;
%     Amp(N/2+n+1)=Amp(N/2-n+1);
% end;

 
%% Phase Generation
pha = 2*pi*rand(NUniqFreq,1);

% %% odd symmetry 
% for n=1:(N/2)-1;
%     pha(N/2+n+1)=-pha(N/2-n+1);
% end;
% pha=unwrap(pha);
%pha1=pha;


spectrum=Amp.*exp(1i*pha);

%%reconstruct full fft
if ~rem(N, 2) %if even
    fullSpectrum = [spectrum; conj(spectrum(end - 1 : -1 : 2))];
else %if odd
    fullSpectrum = [spectrum; conj(spectrum(end : -1 : 2))];
end

%% synthesized noise
sweep1=real(ifft(fullSpectrum));
sweep1 = sweep1 / max(abs(sweep1));

%% modulated noise
cAmplitude = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
     s.ModulationPhase))/ (1 + s.ModulationIndex);
sweep1 = cAmplitude .* sweep1;
%logC=1e-100;
%LSfft=20*log10(Sfft1/max(Sfft1)+logC);
%LfftTarg=20*log10(abs(Amp)/max(Amp)+logC);

if s.PlotResults
    Sfft1=abs(fft(sweep1));
    %disp(['Approx. Time: ' num2str(N/s.Fs)]);
    figure;
    subplot(3,1,1)
    plot(time,[sweep1, cAmplitude])
    title(['Real Synthesized Noise - ' ' model' ' - Spec Mag - ' num2str(s.Attenuation) '/oct'])
    xlabel('Time [ms]')
    
    subplot(3,1,2);
    plot(freq,unwrap(pha),'linewidth',2);
    title(['Synthesized Phase - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    xlabel('Frequency [Hz]');
    ylabel('Rad')
    legend('Obtained Pha')
    xlim([freq(Nf1) freq(Nf2)]);
    
    grid on
    subplot(3,1,3);
    plot(freq,Sfft1(1:NUniqFreq))
%     semilogx(freq(1:N/2),LSfft(1:N/2),...
%         freq(1:N/2),LfftTarg(1:N/2),...
%         'linewidth',2);
    
    title(['FFT Synthesized Sweep - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    grid on;
    %ylim([0 max(LSfft(Nf1:Nf2))]);
    xlabel('Frequency [Hz]');
    xlim([0 s.Fs/2]);
    ylabel('Amplitude [dB]');
    legend('Obtained','Desiered');
end   

for i = 1 : s.NRepetitions 
    value = [value; sweep1];
end;

if s.SaveWaveFile, audiowrite(value, s.Fs, 16,'Sweep.wav'); end
if s.Listen, sound(value, s.Fs, 16); end

if s.ShowSpectrogram 
    hspc=figure;
    w=hamming(1024);
    spectrogram(sweep1,w,length(w)/2,2048,s.Fs,'yaxis');
    hcb=colorbar;
    %set(hcb,'Limits',[min(abs(sweep1)) max(abs(sweep1))]);
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end
