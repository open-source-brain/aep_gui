function [value, time, s] = generate_ten_noise(varargin)
%noise generator
%crated by super jaimito
%examples
%y=generateNoise;
%y=generateNoise('T',0.5,'Listen',true)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Duration', 4);%Duration in seconds
s = ef(s, 'Amplitude', 0.9);%Amplitude of the signal
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'FNoiseL', 20);% initial frequency [Hz]
s = ef(s, 'FNoiseH', 12500);% end frequency [Hz]
s = ef(s, 'PHONE', 0); %PHONE according to ISO 226 spl level
s = ef(s, 'PlotResults', false);
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL; zero means no ref
s = ef(s, 'ShowSpectrogram', false);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', 0);%if is true a wavfile is created in the current directory
s = ef(s, 'KeepSeed', 0); % will keep seed to generate noise

value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
N = ceil(s.Duration * s.Fs);
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
else %if odd
    NUniqFreq = ceil((N + 1)/2);
end

freq = ((0:NUniqFreq - 1) * s.Fs/N)';

Amp=zeros(NUniqFreq,1);
NFNoiseL=find(freq>=s.FNoiseL,1,'first');
NFNoiseH=find(freq<=s.FNoiseH,1,'last');

[spl, ff] = iso226(s.PHONE);
int_spl = spline(ff, spl, freq);
lin_amp = 1 ./ (10 .^ (int_spl / 20) * 20e-6);
%% defining spectral magnitude
for n = 1:NUniqFreq - 1
    f=freq(n+1);
    if (f>=s.FNoiseL) && (f<=s.FNoiseH) 
        Amp(n+1) = lin_amp(n);
    else
        Amp(n+1)=0;
    end
end

%% Phase Generation
pha = 2 * pi * rand(NUniqFreq,1);
spectrum = Amp .* exp(1i*pha);

%%reconstruct full fft
if ~rem(N, 2) %if even
    Nini = NUniqFreq - 1;
    Nend = 2;
else %if odd
    Nini = NUniqFreq;
    Nend = 2;
end
fullSpectrum = [spectrum; conj(spectrum(Nini : -1 : Nend))];
cAmplitude = s.Amplitude;
%% synthesized noise
cNoise=real(ifft(fullSpectrum));
% cNoise = cNoise / max(abs(cNoise));
% Amp  = Amp / max(abs(Amp(NFNoiseL:NFNoiseH)));
% scale 
if s.PlotResults
    Sfft1 = abs(fft(cNoise));
    figure;
    subplot(3,1,1)
    cNoise = cNoise / max(abs(cNoise));
    cNoise = cAmplitude .* cNoise;
    plot(time, cNoise)
    title(['Real Synthesized Noise - ' ' model'])
    xlabel('Time [ms]')
    
    subplot(3,1,2);
    plot(freq,unwrap(pha),'linewidth',2);
    title(['Synthesized Phase - ' ' - Spectral Magnitude'])
    xlabel('Frequency [Hz]');
    ylabel('Rad')
    legend('Obtained pha')
    xlim([freq(NFNoiseL) freq(NFNoiseH)]);
    
    grid on
    subplot(3,1,3);
    semilogx(freq, 20*log10([Sfft1(1:NUniqFreq), Amp]))
    title(['FFT Synthesized Sweep - ' ' - Spectral Magnitude dB'])
    grid on;
    %ylim([0 max(LSfft(NFNoiseL:NFNoiseH))]);
    xlabel('Frequency [Hz]');
    xlim([s.FNoiseL, s.FNoiseH]);
    ylabel('Amplitude');
    legend('Obtained', 'desired');
end   
cNoise = cAmplitude .* cNoise;

value = repmat(cNoise,s.NRepetitions);

if s.SaveWaveFile, audiowrite(value, s.Fs, 16,'Sweep.wav'); end
if s.Listen, sound(value, s.Fs, 16); end

if s.ShowSpectrogram 
    hspc=figure;
    w=hamming(1024);
    spectrogram(cNoise,w,length(w)/2,2048,s.Fs,'yaxis');
  %  hcb=colorbar;
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end

function [spl, freq] = iso226(phon)
    %                /---------------------------------------\
    %%%%%%%%%%%%%%%%%          TABLES FROM ISO226             %%%%%%%%%%%%%%%%%
    %                \---------------------------------------/
    f = [20 25 31.5 40 50 63 80 100 125 160 200 250 315 400 500 630 800 ...
        1000 1250 1600 2000 2500 3150 4000 5000 6300 8000 10000 12500];

    af = [0.532 0.506 0.480 0.455 0.432 0.409 0.387 0.367 0.349 0.330 0.315 ...
        0.301 0.288 0.276 0.267 0.259 0.253 0.250 0.246 0.244 0.243 0.243 ...
        0.243 0.242 0.242 0.245 0.254 0.271 0.301];

    Lu = [-31.6 -27.2 -23.0 -19.1 -15.9 -13.0 -10.3 -8.1 -6.2 -4.5 -3.1 ...
        -2.0  -1.1  -0.4   0.0   0.3   0.5   0.0 -2.7 -4.1 -1.0  1.7 ...
        2.5   1.2  -2.1  -7.1 -11.2 -10.7  -3.1];

    Tf = [ 78.5  68.7  59.5  51.1  44.0  37.5  31.5  26.5  22.1  17.9  14.4 ...
        11.4   8.6   6.2   4.4   3.0   2.2   2.4   3.5   1.7  -1.3  -4.2 ...
        -6.0  -5.4  -1.5   6.0  12.6  13.9  12.3];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %
    % Generates an Equal Loudness Contour as described in ISO 226
    %
    % Usage:  [SPL FREQ] = ISO226(PHON);
    %
    %         PHON is the phon value in dB SPL that you want the equal
    %           loudness curve to represent. (1phon = 1dB @ 1kHz)
    %         SPL is the Sound Pressure Level amplitude returned for
    %           each of the 29 frequencies evaluated by ISO226.
    %         FREQ is the returned vector of frequencies that ISO226
    %           evaluates to generate the contour.
    %
    % Desc:   This function will return the equal loudness contour for
    %         your desired phon level.  The frequencies evaulated in this
    %         function only span from 20Hz - 12.5kHz, and only 29 selective
    %         frequencies are covered.  This is the limitation of the ISO
    %         standard.
    %
    %         In addition the valid phon range should be 0 - 90 dB SPL.
    %         Values outside this range do not have experimental values
    %         and their contours should be treated as inaccurate.
    %
    %         If more samples are required you should be able to easily
    %         interpolate these values using spline().
    %
    % Author: Jeff Tackett 03/01/05

    %Error Trapping
    if((phon < 0) || (phon > 90))
        disp('Phon value out of bounds!')
        spl = 0;
        freq = 0;
    else
        %Setup user-defined values for equation
        Ln = phon;

        %Deriving sound pressure level from loudness level (iso226 sect 4.1)
        Af=4.47E-3 * (10.^(0.025*Ln) - 1.15) + (0.4*10.^(((Tf+Lu)/10)-9 )).^af;
        Lp=((10./af).*log10(Af)) - Lu + 94;

        %Return user data
        spl = Lp;
        freq = f;
    end


