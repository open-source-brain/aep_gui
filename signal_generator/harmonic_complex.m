%if number of alt phase per modulation does not match the period the
%modulation rate will be adjusted so that the phase alternation results
%periodic within the period.
function [value, time, s] = harmonic_complex(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 4); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'Frequencies', 1162); % Frequency in Hz
s = ef(s, 'RefFrequency', 1162); % Frequency in Hz (use as ref to round burst duration
s = ef(s, 'Phases', 0); % phase in rad
s = ef(s, 'Burst_Duration', 0.01); % in sec
s = ef(s, 'InterStimInterval', 0.15); % in sec
s = ef(s, 'InterStimJitter', 0.1); % in sec
s = ef(s, 'AlternatePolarity', 0); % boolean
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'SaveWaveFile', 0); % if true will save file on pwd
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0.002);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.RefFrequency);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.RefFrequency;
    cMinDuration = m1/s.CycleRate;
    s.Burst_Duration = ceil(s.Burst_Duration/cMinDuration) * cMinDuration;
    s.Frequencies = round(s.Burst_Duration .* s.Frequencies) / s.Burst_Duration;
    % now we change the input duration to the closet minduration multiple
end
% here we make sure this total duration meets all requirements


tone_time= (0:round(s.Fs*s.Burst_Duration) - 1)' * 1/s.Fs;
cSignal = zeros(numel(tone_time), 1);

%make sure all phases are set
if numel(s.Phases) ~= numel(s.Frequencies)
    s.Phases = s.Phases(1) * ones(size(s.Frequencies));
end

for i = 1: numel(s.Frequencies)
    cCarrier1 = sin(2 * pi * s.Frequencies(i) * tone_time + s.Phases(i));
    cSignal = cSignal + cCarrier1;
end

%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Burst_Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow .* cSignal;
end
% normalize
cSignal = cSignal ./ max(abs(cSignal));
% compute normalize rms for calibration
sRMS = rms(cSignal(:,1));
if s.refSPL
    scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
else
    scale_factor = s.Amplitude;
end
cSignal = scale_factor .* cSignal;
s.RMS = rms(cSignal(:,1));
%make signal diotic
cSignal = [cSignal, cSignal];

trigger_rate = 1 / s.Burst_Duration;
triggers = clicks2('Duration', s.Burst_Duration, 'Rate', trigger_rate, ...
        'PulseWidth', s.PulseWidthTrigger, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);

if s.InterStimInterval
    n_stim = round(s.Duration / (s.Burst_Duration + s.InterStimInterval));
else
    n_stim = round(s.Duration / s.Burst_Duration);
end
n_stim = max(1, n_stim);

% now we make sure that number of stimuli are even when alternating
% polarity
if s.AlternatePolarity && mod(n_stim, 2)
    n_stim = n_stim + 1;
end
    
for i = 1 : n_stim
    factor = 1;
    if s.AlternatePolarity
        factor = (-1)^(i+1);
    end
    interval = zeros(round((s.InterStimInterval + s.InterStimJitter * ...
        2 *(rand() - 0.5)) * s.Fs), 1);
    if s.IncludeTriggers
        c_stim = [[triggers; interval], [factor * cSignal; [interval, interval]]];
        if i == 1
            cChannelData = c_stim;
        else
            cChannelData = [cChannelData; c_stim];
        end
    else
        c_stim = [factor * cSignal; [interval, interval]];
        if i == 1
            cChannelData = c_stim;
        else
            cChannelData = [cChannelData; c_stim];
        end
    end
end
nsamples = size(cChannelData, 1);
if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions, 3);
else
    value = zeros(nsamples*s.NRepetitions, 2);
end
for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples, 2:3) = factor * cChannelData(:, 2:3);
    else
        value((i-1)*nsamples + 1 : i*nsamples, :) = factor * cChannelData;
    end
end
time = (0:size(value,1) - 1)' * 1/s.Fs;
s.Duration = size(value,1) / s.Fs;
if s.SaveWaveFile, audiowrite(strcat('audio_', num2str(s.Frequencies), '.wav'), value,s.Fs); end
end
