%if number of alt phase per modulation does not match the period the
%modulation rate will be adjusted so that the phase alternation results
%periodic within the period.
function [value, time, s] = harmonic_complex_roving_alt(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 40); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude_1', 1); %between -1 and 1
s = ef(s, 'Amplitude_2', 1); %between -1 and 1
s = ef(s, 'Frequencies_1', [150, 300, 900, 1300]); % Frequency in Hz
s = ef(s, 'Frequencies_2', [150, 300, 2400, 1300]); % Frequency in Hz
s = ef(s, 'GainFrequencies_1', [0, 0, 0, 0]); % Frequency in dB
s = ef(s, 'GainFrequencies_2', [0, 0, 0, 0]); % Frequency in dB
s = ef(s, 'Phases_1', 0); % phase in rad
s = ef(s, 'Phases_2', 0); % phase in rad
s = ef(s, 'ModulationFrequencies_1', [40, 40, 43, 40]); %frequencies in Hz
s = ef(s, 'ModulationPhases_1', 0); %phases in rad
s = ef(s, 'ModulationIndexes_1', 1); %float between 0 and 1
s = ef(s, 'ModulationFrequencies_2', [40, 40, 43, 40]); %frequencies in Hz
s = ef(s, 'ModulationPhases_2', 0); %phases in rad
s = ef(s, 'ModulationIndexes_2', 1); %float between 0 and 1
s = ef(s, 'Modulationdepth', 1); % depth of level roving (1 is fully modulated, 0 is no modulation)
s = ef(s, 'ModulationCutoffLow', 0.2); % low cutoff frequency for envelope modulation
s = ef(s, 'ModulationCutoffHigh', 4); % low cutoff frequency for envelope modulation
s = ef(s, 'AltRate', 0.5); % in Hz
s = ef(s, 'ConstreinCarrierAltRatePeriods', 1); % integer
% s = ef(s, 'InterStimInterval', 0.15); % in sec
% s = ef(s, 'InterStimJitter', 0.1); % in sec
s = ef(s, 'AlternatePolarity', 0); % boolean
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'TriggersEveryModulation', 1); % if true add trigger channel
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'SaveWaveFile', 0); % if true will save file on pwd
s = ef(s, 'ShowSpectrogram', 0); % whether to plot spectrogram or not
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0.002);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    s.AltRate = 1 / (ceil(1 / (s.AltRate * cMinDuration)) * cMinDuration);
    % now we change the input duration to the closet minduration multiple
end
% here we make sure this total duration meets all requirements
altPeriod = 2 / s.AltRate;

s.Duration = round(s.Duration / altPeriod) * altPeriod;
time = (0:round(s.Fs*s.Duration)-1)'*1/s.Fs;

cModulationFrequencies_1 = reshape(s.ModulationFrequencies_1, 1, []);
cModulationFrequencies_1   = round(cModulationFrequencies_1/s.AltRate) * s.AltRate;
s.ModulationFrequencies_1 = cModulationFrequencies_1;

cModulationFrequencies_2 = reshape(s.ModulationFrequencies_2, 1, []);
cModulationFrequencies_2   = round(cModulationFrequencies_2/s.AltRate) * s.AltRate;
s.ModulationFrequencies_2 = cModulationFrequencies_2;

all_modulation_frequencies = reshape([s.ModulationFrequencies_1, ...
    s.ModulationFrequencies_2], 1, []);

s.Frequencies_1 = round(s.Frequencies_1 * s.ConstreinCarrierAltRatePeriods ...
    / s.AltRate) * s.AltRate / s.ConstreinCarrierAltRatePeriods;
s.Frequencies_2 = round(s.Frequencies_2 * s.ConstreinCarrierAltRatePeriods ...
    / s.AltRate) * s.AltRate / s.ConstreinCarrierAltRatePeriods;
s.Frequencies_1 = reshape(s.Frequencies_1, 1, []);
s.Frequencies_2 = reshape(s.Frequencies_2, 1, []);

all_carrier_frequencies = reshape([s.Frequencies_1, ...
    s.Frequencies_2], 1, []);

tone_time= (0:round(s.Fs / s.AltRate) - 1)' * 1/s.Fs;
cSignal_1 = zeros(numel(tone_time), 1);
cSignal_2 = zeros(numel(tone_time), 1);
%make sure all phases are set
if numel(s.Phases_1) ~= numel(s.Frequencies_1)
    s.Phases_1 = s.Phases_1(1) * ones(size(s.Frequencies_1));
end
if numel(s.Phases_2) ~= numel(s.Frequencies_2)
    s.Phases_2 = s.Phases_2(1) * ones(size(s.Frequencies_2));
end

cAmplitudes_1 = (1 - s.ModulationIndexes_1 .* ...
    cos(2 * pi * cModulationFrequencies_1 .* ...
    tone_time + s.ModulationPhases_1)) / 2;

cAmplitudes_2 = (1 - s.ModulationIndexes_2 .* ...
    cos(2 * pi * cModulationFrequencies_2 .* ...
    tone_time + s.ModulationPhases_2)) / 2;

if size(cAmplitudes_1, 2) == 1
    cAmplitudes_1 = repmat(cAmplitudes_1, 1, numel(s.Frequencies_1));
end
if size(cAmplitudes_2, 2) == 1
    cAmplitudes_2 = repmat(cAmplitudes_2, 1, numel(s.Frequencies_2));
end


for i = 1: numel(s.Frequencies_1)
    cCarrier1 = cAmplitudes_1(:, i) .* sin(2 * pi * s.Frequencies_1(i) * ...
        tone_time + s.Phases_1(i));
    
    % compute normalize rms for each carrier and apply gain to the carrier
    cCarrier1  = cCarrier1 / max(abs(cCarrier1));
    scale_factor_1 = 10^(s.GainFrequencies_1(i)/20);
    cCarrier1 = scale_factor_1 .* cCarrier1 ;
    cSignal_1 = cSignal_1 + cCarrier1;
end

for i = 1: numel(s.Frequencies_2)
    cCarrier2 = cAmplitudes_2(:, i) .* sin(2 * pi * s.Frequencies_2(i) * ...
        tone_time + s.Phases_2(i));
    % compute normalize rms for each carrier and apply gain to the carrier
    cCarrier2  = cCarrier2 / max(abs(cCarrier2));
    scale_factor_2 = 10^(s.GainFrequencies_2(i)/20);
    cCarrier2 = scale_factor_2 .* cCarrier2;
    cSignal_2 = cSignal_2 + cCarrier2;
end


% normalize
cSignal_1 = cSignal_1 ./ max(abs(cSignal_1));
cSignal_2 = cSignal_2 ./ max(abs(cSignal_2));
% compute normalize rms for calibration
sRMS_1 = rms(cSignal_1);
sRMS_2 = rms(cSignal_2);
if s.refSPL
    scale_factor_1 = 10^(s.Amplitude_1/20) * s.refSPL/sRMS_1;
    scale_factor_2 = 10^(s.Amplitude_2/20) * s.refSPL/sRMS_2;
else
    scale_factor_1 = s.Amplitude_1;
    scale_factor_2 = s.Amplitude_2;
end
cSignal_1 = scale_factor_1 .* cSignal_1;
cSignal_2 = scale_factor_2 .* cSignal_2;

ncycles = round(s.Duration*s.AltRate);
cSignal = zeros(size(time, 1), 1);
block_size = size(tone_time, 1);
pol_pow = reshape(cat(1, (0: ncycles / 2 - 1), (0: ncycles / 2 - 1)), ...
    1, []);
for i = 1: ncycles
    ini_pos = 1 + (i - 1) * block_size;
    end_pos = i * block_size;
    cSignal(ini_pos: end_pos, 1) = (-1) ^ (pol_pow(i) * ...
        s.AlternatePolarity) * ...
        (cSignal_1 * (mod(i,2) == 1) + cSignal_2 * (mod(i,2) == 0));
end
%make signal diotic
cSignal = [cSignal, cSignal];

%% generates rise fall window
nsamples = size(cSignal, 1);
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow .* cSignal;
end

s.RMS = rms(cSignal(:,1));

%% Level roving
% The amplitude of the signal is modulated using a generated noise signal
% with low frequency components to mimick slow variations found in speech
% signals. If fully modulated (1), minimum is 0, if not modulated (0),
% the resulting modulation signal is constant at 1.

[Modulation_env, ~, ~] = generateModulatedNoise(...
    'Fs', s.Fs, ...
    'Amplitude', 1, ...
    'FNoiseL', s.ModulationCutoffLow, ...
    'FNoiseH', s.ModulationCutoffHigh, ...
    'Duration', s.Duration);

m = s.Modulationdepth /(max(Modulation_env) - min(Modulation_env)); % scale factor
x0 = s.Modulationdepth * ( 0.5 - max(Modulation_env)) /(max(Modulation_env) - min(Modulation_env)); % offset factor

Modulation_env = Modulation_env * m + x0;
Modulation_env = Modulation_env + 1 - max(Modulation_env);

% Apply Modulation envelope signal to carrier signal
cSignal = cSignal .* Modulation_env;

%% Include triggers
if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions, 3);
    if s.TriggersEveryModulation
        trigger_rate = s.AltRate;
    else
        trigger_rate = 1 / s.Duration;
    end
    triggers = clicks2('Duration', s.Duration, 'Rate', trigger_rate, ...
        'PulseWidth', s.PulseWidthTrigger, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cSignal];
else
    value = zeros(nsamples*s.NRepetitions, 2);
    cChannelData = cSignal;
end

% progressbar('Generating signal');
for i = 1:s.NRepetitions
%     progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples, 2:end) = factor * cChannelData(:, 2:end);
    else
        value((i-1)*nsamples + 1 : i*nsamples, :) = factor * cChannelData;
    end
end
function value = unitaryStep(x)
    value = double(x>=0);
end
file_name = '';
if s.SaveWaveFile
    fnames = fieldnames(s);
    for i = 1 : numel(fnames)
        cItem = fnames{i};
        cVal = s.(cItem);
        cItemAbb = cItem(regexp(cItem,'(?<![A-Z])[A-Z]{1,3}(?![A-Z])'));
        cItemAbb = cItemAbb(1:min(2,numel(cItemAbb)));
        ctext = '';
        switch  class(cVal)
            case 'double'
                textvalue = sprintf('%.1e', cVal);
                textvalue = textvalue(1:min(6,numel(textvalue)));
                ctext = strcat(cItemAbb,'-',textvalue);
            case 'char'
                cVal = cVal(1:min(2,numel(cVal)));
                ctext = strcat(cItemAbb,'-',cVal);
        end
        file_name = strcat(file_name, ctext, '_');
    end
    file_name = regexprep(file_name,'\.','_');
    file_name = file_name(1:64);
    audiowrite(strcat(file_name,'.wav'), value, s.Fs, 'BitsPerSample', 16);
end

if s.ShowSpectrogram
    figure;
    w_size = 1024*8;
    w=hamming(w_size );
    spectrogram(value(:,2) ,w, w_size/2, 2 * w_size,s.Fs,'yaxis');
    colorbar;
    ymin = min(all_carrier_frequencies) / 1000 - ...
        max(all_modulation_frequencies) / 1000;
    ymax = max(all_carrier_frequencies) / 1000 + ...
        max(all_modulation_frequencies) / 1000;
    
    if ymin == ymax
        ymax = ymin * 1.2;
    end
    ylim([ymin, ymax]);
end

end