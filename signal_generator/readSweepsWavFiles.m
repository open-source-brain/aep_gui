function [x]=readSweepsWavFiles(varargin);
s = parseparameters(varargin{:});
s = ef(s, 'File','');
s = ef(s, 'NSamples',0);
s = ef(s, 'Offset',0);%in samples

wsize = wavread(s.File,'size');
info = mmfileinfo(s.File);
N = floor(wsize(1)/s.NSamples);
x=zeros(s.NSamples,wsize(2));
cont=0;
for i = 1:N-1
    cont = cont+1;
    posIni = (i-1)*s.NSamples+1;
    posEnd = i*s.NSamples;
    [yIn, FSIn, nbits]=wavread(char(s.File),[posIni,posEnd]);
    x = x + yIn;
end;
x = circshift(x,s.Offset);
x=x/cont;
% x=x-mean(x,1);