function outWindow = riseFallWindow(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs',44100);
s = ef(s, 'Duration', 0);
s = ef(s, 'RiseFallTime', 0);
outWindow = ones(round(s.Fs*s.Duration), 1);
%% generates rise fall window
if s.RiseFallTime > 0
    cNWin = round(s.Fs*s.RiseFallTime);
    riseFallSamples = 0 : (cNWin - 1);
    outWindow(1:cNWin) = sin(2*pi*riseFallSamples/(4*(cNWin - 1))).^2;
    outWindow(end-cNWin + 1:end) = cos(2*pi*riseFallSamples/(4*(cNWin - 1))).^2;
    outWindow(end) = 0; % Due to numeric errors, the last sample might not be zero, but close to it
    outWindow(1) = 0; % Due to numeric errors, the first sample might not be zero, but close to it
end