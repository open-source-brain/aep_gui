function varargout = signalGeneratorGui(varargin)
% SIGNALGENERATORGUI MATLAB code for signalGeneratorGui.fig
%      SIGNALGENERATORGUI, by itself, creates a new SIGNALGENERATORGUI or raises the existing
%      singleton*.
%
%      H = SIGNALGENERATORGUI returns the handle to a new SIGNALGENERATORGUI or the handle to
%      the existing singleton*.
%
%      SIGNALGENERATORGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIGNALGENERATORGUI.M with the given input arguments.
%
%      SIGNALGENERATORGUI('Property','Value',...) creates a new SIGNALGENERATORGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before signalGeneratorGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to signalGeneratorGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help signalGeneratorGui

% Last Modified by GUIDE v2.5 31-Jul-2013 14:30:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @signalGeneratorGui_OpeningFcn, ...
                   'gui_OutputFcn',  @signalGeneratorGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before signalGeneratorGui is made visible.
function signalGeneratorGui_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to signalGeneratorGui (see VARARGIN)
% Choose default command line output for signalGeneratorGui
s = parseparameters(varargin{:});
s = ef(s, 'NoGui', false);
s = ef(s, 'SignalType', '');

handles.Signal = [];
handles.Parameters = [];
handles.output = hObject;
handles.vararin = varargin;
handles.SignalType = s.SignalType;
% Update handles structure
guidata(hObject, handles);
if s.NoGui
    set(handles.figure1,'Visible','off');
    generateSignalnoGui(hObject);
    handles = guidata(hObject);
%     signalGeneratorGui_OutputFcn(hObject,[], handles) 
else
    createSignalMenu(hObject, varargin);
    createParametersList(hObject);
    % UIWAIT makes signalGeneratorGui wait for user response (see UIRESUME)
    uiwait(handles.figure1);
end



% --- Outputs from this function are returned to the command line.
function varargout = signalGeneratorGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.Signal;
varargout{2} = handles.Parameters;
varargout{3} = handles.SignalType;
delete(handles.figure1);

    

function handles = createSignalMenu(hObject,varargin)
    handles = guidata(hObject);
    %% here one shall put the name of a new function
    [pathstr,name,ext] = fileparts(mfilename('fullpath'));
    all_functions = dir(strcat(pathstr,filesep,'*.m'));
    signalFunctionList = {'none'};
    for func_name = 1 : numel(all_functions)
        if ~strcmp(strcat(name, ext), all_functions(func_name).name)
            [~,c_name,~] = fileparts(all_functions(func_name).name);
            signalFunctionList{end + 1} = c_name;
        end
    end
    %%
    cpuVal = 1;
    cStruct = getGuiInput(hObject);
    if ~isempty(cStruct)
        if ~(strcmp(cStruct.SignalType,'none') || strcmp(cStruct.SignalType,'slave-signal'))
            cSelcted = find(ismember(signalFunctionList, cStruct.SignalType));
            cpuVal = cSelcted;
        end
    end
            
    topmargin =  0.05;
	uiHigh = 0.05; %desired high in pixels
	uiWidth = 0.1; %desired widh in pixels
    set(hObject,'Units','normalized');
        uicontrol(hObject, ...
		'Tag','pupSelectSignal', ...
		'Style','popup', ...
		'String',signalFunctionList, ...
		'Units', 'normalized', ...
		'Position',[0, 1 - topmargin, 4*uiWidth, 1*uiHigh], ...
		'Callback',@createParametersList, ...
		'Value', cpuVal ...
		)

function createParametersList(hObject,~)
	handles = guidata(hObject);
    cSignals = get(findobj(hObject,'Tag','pupSelectSignal'), 'String');
    cSelected = cSignals(get(findobj(hObject,'Tag', 'pupSelectSignal'), 'Value'));
    topmargin =  0.05;
    uiHigh = 0.05; %desired high in pixels
    uiWidth = 0.1; %desired widh in pixels
    %% delete all objects except the main popup
    hParent = handles.figure1;
    cObj2Delete = findobj(hParent,'Type','uicontrol','-and','-not','Tag','pupSelectSignal');
    if ~isempty(cObj2Delete)
        delete(cObj2Delete);
    end
    cpos = 1;
    %% dummyParameters
    if isequal(cSelected{:}, 'none') || isequal(cSelected{:}, 'slave-signal')
        handles.CurrentSignalParams = {};
        handles.SignalType = cSelected{:};
        handles.Signal = [];
    else
        [~,~,cDummyParameters] = eval(strcat(cSelected{:},'(','''OnlyReturnParameters''',',1)'));
        %% if settings are passed we used them
        cStruct = getGuiInput(hObject);
        cParameters = substituteStructFields('OriginalStruct', cDummyParameters, ...
            'NewStruct', cStruct);
        
        %% now we set up the gui
        cFields = fieldnames(cParameters);
        uiHigh = 1/(numel(cFields) + 3) ;
        handles.CurrentSignalParams = {};
        handles.SignalType = cSelected{:};
        for i = 1:numel(cFields)
            cpos = cpos + 1;
            handles.CurrentSignalParams(end + 1) = cFields(i);
            uicontrol(hParent, ...
                'Tag',strcat('signalgen_text',num2str(i)), ...
                'Style','text', ...
                'String',cFields(i), ...
                'Units', 'normalized', ...
                'Position',[0, 1 - topmargin - cpos*uiHigh, 4*uiWidth, 1*uiHigh] ...
                )
            switch class(cParameters.(cFields{i}))
                case {'double', 'logical'}
                    uicontrol(hParent, ...
                        'Tag',cFields{i}, ...
                        'Style','edit', ...
                        'String',num2str(reshape(cParameters.(cFields{i}),1,[])), ...
                        'Units', 'normalized', ...
                        'Position',[0 + 4*uiWidth, 1 - topmargin - cpos*uiHigh, 4*uiWidth, 1*uiHigh], ...
                        'Value', cParameters.(cFields{i}), ...
                        'Callback', @callbackCheckfield ...
                        )
                case {'string','char'}
                    cVal = str2num(cParameters.(cFields{i}));
                    if isempty(cVal)
                        cFieldVal = cParameters.(cFields{i});
                    else
                        cFieldVal = cVal;
                    end
                    uicontrol(hParent, ...
                        'Tag',cFields{i}, ...
                        'Style','edit', ...
                        'String', cFieldVal, ...
                        'Units', 'normalized', ...
                        'Position',[0 + 4*uiWidth, 1 - topmargin - cpos*uiHigh, 4*uiWidth, 1*uiHigh], ...
                        'Value', [] ...
                        );
                case {'cell'}
%                     uicontrol(hParent, ...
%                         'Tag',cFields{i}, ...
%                         'Style','listbox', ...
%                         'String',cParameters.(cFields{i}), ...
%                         'Units', 'normalized', ...
%                         'Position',[0 + 4*uiWidth, 1 - topmargin - cpos*uiHigh, 4*uiWidth, 1*uiHigh], ...
%                         'Value', 1 ...
%                         );
            end
        end
    end
    %% ready button 
    uicontrol(hParent, ...
        'Style','pushbutton', ...
        'String','Accept', ...
        'Units', 'normalized', ...
        'Position',[2*4*uiWidth, 1 - topmargin - (0 + 1)*uiHigh, 2*uiWidth, 1*uiHigh], ...
        'Callback',@callbackAccept ...
        )
    setGuiTheme('Figure', handles.figure1);
    guidata(hObject, handles);

%% callback ready

function callbackAccept(hObject,~)
	handles = guidata(hObject);
    hParent = handles.figure1;
    for i = 1:numel(handles.CurrentSignalParams)
        cObject = get(findobj(hParent, 'Tag',handles.CurrentSignalParams{i}),'Style');
        if isempty(cObject); continue; end
        switch cObject
            case 'edit'
                cVal = str2num(get(findobj(hParent, 'Tag',handles.CurrentSignalParams{i}), 'String'));
                if ~isempty(cVal)
                    s.(handles.CurrentSignalParams{i}) = cVal;
                else
                    s.(handles.CurrentSignalParams{i}) = get(findobj(hParent, 'Tag',handles.CurrentSignalParams{i}), 'String');
                end
            case 'listbox'
                s.(handles.CurrentSignalParams{i}) = get(findobj(hParent, ...
                    'Tag',handles.CurrentSignalParams{i}), 'String');
        end
    end
    if isequal(handles.SignalType, 'none') || isequal(handles.SignalType, 'slave-signal')
        handles.Signal = [];
        handles.Parameters = [];
    else
        [value,~,cSettings] = eval(strcat(handles.SignalType,'(s)'));
        handles.Signal = value;
        handles.Parameters = cSettings;
    end
    guidata(hObject, handles);
    close(handles.figure1);
    
function generateSignalnoGui(hObject)
    handles = guidata(hObject);
    s = parseparameters(handles.vararin{:});
    s = ef(s, 'SignalType', 'none');
    %% dummyParameters
    [~,~,cDummyParameters] = eval(strcat(s.SignalType,'(','''OnlyReturnParameters''',',1)'));
    %% if settings are passed we used them
    cStruct = getGuiInput(hObject);
    cParameters = substituteStructFields('OriginalStruct', cDummyParameters, ...
        'NewStruct', cStruct);
    [value,~,cSettings] = eval(strcat(s.SignalType,'(cParameters)'));
    handles.Signal = value;
    handles.Parameters = cSettings;
    handles.SignalType = s.SignalType;
    guidata(hObject, handles);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end

function out = getGuiInput(hObject)
    handles = guidata(hObject);
    s = parseparameters(handles.vararin{:});
    if ~isempty(s)
        out = s;
    end
    if ~isfield(out,'SignalType')
        out.SignalType = 'none';
    end
    
function callbackCheckfield(hObject, ~)
    ctext = str2num(get(hObject, 'String'));
    if ~isempty(ctext)
        set(hObject, 'Value', ctext);
    end
    set(hObject, 'String', num2str(get(hObject, 'Value')));
        
