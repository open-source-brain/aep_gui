%if number of alt phase per modulation does not match the period the
%modulation rate will be adjusted so that the phase alternation results
%periodic within the period.
function [value, time, s] = sin_alt_burst(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'BurstDuration', 0.06); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude_1', 1); %between -1 and 1
s = ef(s, 'Amplitude_2', 1); %between -1 and 1
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'AltLevelRate', 7.0); % phase in rad
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'TriggersEveryModulation', 0); % if true add trigger channel
s = ef(s, 'PulseWidth', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.AltLevelRate = 1 / (ceil((1 / s.AltLevelRate)/cMinDuration) * cMinDuration);
    %fit carrier freq
end
% here we make sure this total duration meets all requirements
s.Duration = ceil(s.Duration * s.AltLevelRate) / s.AltLevelRate;

cCarrierFrequency  = round(s.CarrierFrequency/s.AltLevelRate) * s.AltLevelRate;
s.CarrierFrequency  = cCarrierFrequency;
cModulationFrequency   = round(s.ModulationFrequency/s.AltLevelRate) * s.AltLevelRate;
s.ModulationFrequency = cModulationFrequency;

s.BurstDuration = round(s.BurstDuration*cModulationFrequency) / cModulationFrequency;
s.BurstDuration = min(s.BurstDuration, 1 / s.AltLevelRate);

time = (0:s.Fs*s.Duration-1)' * 1/s.Fs;
ncycles = s.Duration * s.AltLevelRate;
cSignal = zeros(size(time));
cCarrier = sin(2*pi*cCarrierFrequency * time + s.CarrierPhase);
rep_period = 1 / s.AltLevelRate;

burst_n_samples = floor(s.BurstDuration * s.Fs);
cWindow = ones(burst_n_samples, 1);
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.BurstDuration, ...
        'RiseFallTime', s.RiseFallTime);
end
for i = 1:ncycles
%     progressbar(i/ncycles);
    ini_pos = round(rep_period * s.Fs) * (i - 1) + 1;
    end_pos = ini_pos + burst_n_samples - 1;
    cSignal(ini_pos: end_pos) = cCarrier(ini_pos: end_pos);
    cSignal(ini_pos: end_pos) = cWindow .* cSignal(ini_pos: end_pos);
    %% generates rise fall window
end
cSignal = cSignal .* (1 - s.ModulationIndex*cos(2 * pi * cModulationFrequency * time + ...
    s.ModulationPhase));
cSignal = [cSignal, cSignal];
%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
nsamples = size(cSignal, 1);


% normalize
cSignal = cSignal ./ max(abs(cSignal));
% compute normalize rms for calibration
sRMS = rms(cSignal(:,1));

if s.refSPL
    scale_factor_1 = 10^(s.Amplitude_1/20) * s.refSPL/sRMS;
    scale_factor_2 = 10^(s.Amplitude_2/20) * s.refSPL/sRMS;
else
    scale_factor_1 = s.Amplitude_1;
    scale_factor_2 = s.Amplitude_2;
end

cSignal = [scale_factor_1, scale_factor_2] .* cSignal;
s.RMS = rms(cSignal(:));

if s.IncludeTriggers
    if s.TriggersEveryModulation
        trigger_rate = s.AltLevelRate;
    else
        trigger_rate = 1 / s.Duration;
    end
    value = zeros(nsamples*s.NRepetitions,3);
    triggers = clicks2('Duration', s.Duration, 'Rate', trigger_rate, ...
        'PulseWidth', s.PulseWidth, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cSignal];
else
    value = zeros(nsamples*s.NRepetitions,2);
    cChannelData = cSignal;
end

for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples,:) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples,2:3) = factor*cChannelData(:,2:3);
    else
        value((i-1)*nsamples + 1 : i*nsamples, :) = factor*cChannelData;
    end
end