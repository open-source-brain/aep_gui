%if number of alt phase per modulation does not match the period the
%modulation rate will be adjusted so that the phase alternation results
%periodic within the period.
function [value, time, s] = sin_alt_phase(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 4); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'CarrierPhase', -pi/4); % phase in rad
s = ef(s, 'CarrierAltPhase', pi/4); % phase in rad
s = ef(s, 'CarrierPhaseModRate', 2.0); % phase in Hz
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'PulseWidth', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'SaveWaveFile', 1); % if true will save file on pwd
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end;
s = rmfield(s,'OnlyReturnParameters');

cCarrierFrequency = s.CarrierFrequency;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.CarrierPhaseModRate = 1 / (round((1 / s.CarrierPhaseModRate)/cMinDuration) * cMinDuration);
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
end
% here we make sure this total duration meets all requirements
phaseAltPeriod = 2 / s.CarrierPhaseModRate;
s.Duration = ceil(s.Duration / phaseAltPeriod) * phaseAltPeriod;
cModulationFrequency = s.ModulationFrequency;
cModulationFrequency   = round(cModulationFrequency/s.CarrierPhaseModRate) * s.CarrierPhaseModRate;
s.ModulationFrequency = cModulationFrequency;

%fit carrier freq
cCarrierFrequency  = round(s.Duration*cCarrierFrequency)/s.Duration;
s.CarrierFrequency  = cCarrierFrequency;

time = (0:s.Fs*s.Duration-1)' * 1/s.Fs;
cTempPhase = zeros(numel(time),1);
phaseAltRate = 1 / phaseAltPeriod;
ncycles = round(s.Duration*phaseAltRate);

cAltPhase = 0;
progressbar('Generating signal');
for i = 1:ncycles
    progressbar(i/ncycles);
    cSinglePhase = (s.CarrierPhase - s.CarrierAltPhase)*(unitaryStep(time - (1/phaseAltRate)*(cAltPhase)/(2*pi) - ...
        (i-1)/phaseAltRate) - unitaryStep(time - (1/phaseAltRate)*(cAltPhase) / (2*pi) - phaseAltPeriod/2 - (i-1)/phaseAltRate));
    cTempPhase = cTempPhase + cSinglePhase;
end
cTempPhase = cTempPhase + s.CarrierAltPhase;

cAmplitude = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));
cCarrier = sin(2*pi*cCarrierFrequency*(time) + cTempPhase);
cSignal = cAmplitude .* cCarrier;
nsamples = numel(cSignal);
%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow .* cSignal;
end

% normalize
scale_factor = s.Amplitude / max(abs(cSignal));
if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions,2);
    triggers = clicks('Duration', s.Duration, 'Rate', 1 / s.Duration, ...
        'PulseWidth', s.PulseWidth, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, scale_factor * cSignal];
else
    value = zeros(nsamples*s.NRepetitions,1);
    cChannelData = scale_factor * cSignal;
end

progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples,2) = factor*cChannelData(:,2);
    else
        value((i-1)*nsamples + 1 : i*nsamples) = factor*cChannelData;
    end
end
    function value = unitaryStep(x)
        value = double(x>=0);
    end
file_name = '';
if s.SaveWaveFile
    fnames = fieldnames(s);
    for i = 1 : numel(fnames)
        cItem = fnames{i};
        cVal = s.(cItem);
        cItemAbb = cItem(regexp(cItem,'(?<![A-Z])[A-Z]{1,3}(?![A-Z])'));
        cItemAbb = cItemAbb(1:min(2,numel(cItemAbb)));
        ctext = '';
        switch  class(cVal)
            case 'double'
                textvalue = sprintf('%.1e', cVal);
                textvalue = textvalue(1:min(6,numel(textvalue)));
                ctext = strcat(cItemAbb,'-',textvalue);
            case 'char'
                cVal = cVal(1:min(2,numel(cVal)));
                ctext = strcat(cItemAbb,'-',cVal);
        end
        file_name = strcat(file_name, ctext, '_');
    end
    file_name = regexprep(file_name,'\.','_');
    audiowrite(strcat(file_name,'.wav'), value, s.Fs, 'BitsPerSample', 16);
end

end
