function [value, time, s] = sinusoidal(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 1); % this parameter is necesary to calibrate stimuli SPL
             
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

cCarrierFrequency = s.CarrierFrequency;
cModulationFrequency = s.ModulationFrequency;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    %fit carrier freq
    cCarrierFrequency  = round(s.Duration*cCarrierFrequency)/s.Duration;
    %fit modulation rate
    cModulationFrequency   = round(s.Duration*cModulationFrequency)/s.Duration;
    s.ModulationFrequency  = cModulationFrequency;
end
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;

cAmplitude = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));
cCarrier = sin(2*pi*cCarrierFrequency*(time) + s.CarrierPhase);
cSignal = cAmplitude.*cCarrier;

nsamples = numel(cSignal);
%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow.*cSignal;
end

% normalize 
cSignal = cSignal/max(abs(cSignal));
% compute normalize rms for calibration
sRMS = rms(cSignal);
if s.refSPL
    scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
else
    scale_factor = s.Amplitude;
end
cSignal = scale_factor * cSignal;
s.RMS = rms(cSignal);

if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions,2);
    triggers = clicks('Duration', s.Duration, 'Rate', 1 / s.Duration, ...
        'PulseWidth', s.PulseWidthTrigger, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cSignal];
else
    value = zeros(nsamples*s.NRepetitions,1);
    cChannelData = cSignal;
end

for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples,:) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples,2) = factor*cChannelData(:,2);
    else
        value((i-1)*nsamples + 1 : i*nsamples) = factor*cChannelData;
    end
end
