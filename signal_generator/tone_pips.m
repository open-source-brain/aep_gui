%if number of alt phase per modulation does not match the period the
%modulation rate will be adjusted so that the phase alternation results
%periodic within the period.
function [value, time, s] = tone_pips(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 4); %duration in seconds
s = ef(s, 'NRepetitions', 1); %number of repetitions of the same sequence
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'freq_ini', 222); % lowest frequency in Hz
s = ef(s, 'freq_end', 2000); % highest frequency in Hz
s = ef(s, 'n_possible_pips', 20); % Total number of available frequency pips
s = ef(s, 'pip_duration', 0.050); % duration (in seconds) of every pip)
s = ef(s, 'n_pips_in_repeated_seq', 10); % number of pips in a repeated sequence
s = ef(s, 'n_repeated_seqs', 5); % number of repeated sequences (n_pips_in_repeated_seq will be repeated n_repeated_seqs times)
s = ef(s, 'n_pips_in_random_seq', 40); % number of pips in random sequences
s = ef(s, 'n_rand_reg_sequences', 1); % number of rand-reg sequences
s = ef(s, 'n_reg_rand_sequences', 0); % number of reg-rand sequences
s = ef(s, 'n_rand_rand_sequences', 0); % number of rand-rand sequences
s = ef(s, 'n_anchor_sequences', 0); %how many anchor (identical) sequences to repeat
s = ef(s, 'anchor_type_code', 0); %Defines where in the sequence  the anchor is; see below
s = ef(s, 'RandomSeed', -1); % seed, if different of -1 then the use of that seed will trigger same sequence
s = ef(s, 'ISI', 0.8); %time in secs between complete sequences
s = ef(s, 'ISIJitter', 1.2); %jitter time in secs between complete sequences
s = ef(s, 'ShowSpectrogram', 0); % whether to plot spectrogram or not
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.02); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'TriggerOffset', 0.1); %time between trigger and signal
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'SaveWaveFile', 0); % if true will save file on pwd
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0.005);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end
s = rmfield(s,'OnlyReturnParameters');

%define frequency alaphabet
frequencies = zeros(s.n_possible_pips,1);
for i = 1:s.n_possible_pips
    frequencies(i) = s.freq_ini*(s.freq_end/s.freq_ini)^((i - 1) / ...
        (s.n_possible_pips - 1));
end
cOutputChannelData = [];
[s1] = RandStream.create('mrg32k3a', 'NumStreams', 1, ...
    'Seed', 'shuffle');

if s.RandomSeed ~= -1
    s1 = RandStream('mrg32k3a','Seed', s.RandomSeed);
end
% generate sequence codes
codes = [
    0 * ones(1, s.n_rand_reg_sequences), ...
    1 * ones(1, s.n_reg_rand_sequences), ...
    2 * ones(1, s.n_rand_rand_sequences), ...
    3 * ones(1, s.n_anchor_sequences), ...
    ];

s.SequenceCodes = codes(randperm(s1, numel(codes)));
s.SequencesCodeMeaning = {'0-rand-reg', ...
    '1-reg-rand', ...
    '2-rand-rand', ...
    '3-anchor'};
s.SequencesCodeTriggerWidthds = [
    1 * s.PulseWidthTrigger, ...
    2 * s.PulseWidthTrigger, ...
    3 * s.PulseWidthTrigger, ...
    4 * s.PulseWidthTrigger];

switch s.anchor_type_code
    case 0
        s.AnchorType = 'reg_in_rand_reg_seq';
    case 1
        s.AnchorType = 'rand_in_rand_reg_seq';
    case 2
        s.AnchorType = 'reg_and_rand_in_rand_reg_seq';
    case 3
        s.AnchorType = 'reg_in_reg_rand_seq';
    case 4
        s.AnchorType = 'rand_in_reg_rand_seq';
    case 5
        s.AnchorType = 'reg_and_rand_in_reg_rand_seq';
    case 6
        s.AnchorType = 'rand_in_rand_seq';
    otherwise
        s.AnchorType = 'unknown';
end
s.TotalNumberSequences = numel(s.SequenceCodes);
total_pips_in_cyc_sequence = s.n_pips_in_repeated_seq * s.n_repeated_seqs;

% generate anchor
r_cyc_sequence_anch  = repmat(...
    datasample(s1, frequencies, s.n_pips_in_repeated_seq), ...
    [s.n_repeated_seqs, 1]);
random_pips_anch = datasample(s1, frequencies, s.n_pips_in_random_seq);
random_pips_anch_full = datasample(s1, frequencies, ...
    total_pips_in_cyc_sequence + s.n_pips_in_random_seq);

for c_seq = 1 : s.TotalNumberSequences
    random_pips = datasample(s1, frequencies, s.n_pips_in_random_seq);
    r_cyc_pips = datasample(s1, frequencies, s.n_pips_in_repeated_seq);
    r_cyc_sequence = repmat(r_cyc_pips, [s.n_repeated_seqs, 1]);
    
    switch s.SequenceCodes(c_seq)
        case 0 % rand - reg
            all_frequencies = [random_pips; r_cyc_sequence];
        case 1 % reg - rand
            all_frequencies = [r_cyc_sequence; random_pips];
        case 2 % rand - rand
            all_frequencies = datasample(s1, ...
                frequencies, ...
                total_pips_in_cyc_sequence + s.n_pips_in_random_seq);
        case 3 % anchor
            switch s.AnchorType
                case 'reg_in_rand_reg_seq'
                    all_frequencies = [random_pips; r_cyc_sequence_anch];
                case 'rand_in_rand_reg_seq'
                    all_frequencies = [random_pips_anch; r_cyc_sequence];
                case 'reg_and_rand_in_rand_reg_seq'
                    all_frequencies = [random_pips_anch; r_cyc_sequence_anch];
                case 'reg_in_reg_rand_seq'
                    all_frequencies = [r_cyc_sequence_anch; random_pips];
                case 'rand_in_reg_rand_seq'
                    all_frequencies = [r_cyc_sequence; random_pips_anch];
                case 'reg_and_rand_in_reg_rand_seq'
                    all_frequencies = [r_cyc_sequence_anch; random_pips_anch];
                case 'rand_in_rand_seq'
                    all_frequencies = random_pips_anch_full;
            end
    end
    
    n_samples_pip = round(s.Fs*s.pip_duration);
    pip_time = (0: n_samples_pip - 1)' * 1/s.Fs;
    nsamples_all_pips = n_samples_pip * numel(all_frequencies);
    cSignal = zeros(numel(nsamples_all_pips), 1);
    
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.pip_duration, ...
        'RiseFallTime', s.RiseFallTime);
    
    for i = 1: numel(all_frequencies)
        cSignal((i-1)* n_samples_pip + 1: i*n_samples_pip, 1) = ...
            sin(2*pi*all_frequencies(i)*pip_time) .* cWindow ;
    end
    
    % normalize
    cSignal = cSignal ./ max(abs(cSignal));
    % compute normalize rms for calibration
    sRMS = rms(cSignal(:,1));
    if s.refSPL
        scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
    else
        scale_factor = s.Amplitude;
    end
    cSignal = scale_factor .* cSignal;
    s.RMS = rms(cSignal(:,1));
    
    % generate interstimuli interval (ISI)
    cISItData = zeros(round((s.ISI + rand(s1, 1) * s.ISIJitter) * s.Fs), 1);
    % add trigger ofset
    cTrigerOffsetData = zeros(round(s.TriggerOffset * s.Fs), 1);
    cSignal = [cTrigerOffsetData; cSignal; cISItData];
    nsamples = size(cSignal, 1);
    if s.IncludeTriggers
        value = zeros(nsamples*s.NRepetitions, 3);
        trigger_rate = s.Fs / nsamples;
        trigger_width = s.PulseWidthTrigger;
        
        switch s.SequenceCodes(c_seq)
            case 0 % rand - reg
                trigger_width = s.SequencesCodeTriggerWidthds(1);
            case 1 % reg - rand
                trigger_width = s.SequencesCodeTriggerWidthds(2);
            case 2 % rand - rand
                trigger_width = s.SequencesCodeTriggerWidthds(3);
            case 3 % anchor
                trigger_width = s.SequencesCodeTriggerWidthds(4);
        end
        
        triggers = clicks2('Duration', nsamples / s.Fs, ...
            'Rate', trigger_rate, ...
            'PulseWidth', trigger_width, ...
            'Amplitude', s.TriggerAmp, ...
            'RoundToCycle', 0, 'Fs', s.Fs);
        cChannelData = [triggers, cSignal, cSignal];
    else
        value = zeros(nsamples*s.NRepetitions, 2);
        cChannelData = [cSignal, cSignal];
    end
    cOutputChannelData  = [cOutputChannelData ; cChannelData];
end
nsamples = size(cOutputChannelData , 1);
time = (0:  (nsamples - 1))' * 1/s.Fs;
s.Duration = time(end);

% progressbar('Generating signal');
for i = 1:s.NRepetitions
    %     progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cOutputChannelData ;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples, 2:end) = factor * cOutputChannelData (:, 2:end);
    else
        value((i-1)*nsamples + 1 : i*nsamples, :) = factor * cOutputChannelData ;
    end
end

file_name = '';
if s.SaveWaveFile
    fnames = fieldnames(s);
    for i = 1 : numel(fnames)
        cItem = fnames{i};
        cVal = s.(cItem);
        cItemAbb = cItem(regexp(cItem,'(?<![A-Z])[A-Z]{1,3}(?![A-Z])'));
        cItemAbb = cItemAbb(1:min(2,numel(cItemAbb)));
        ctext = '';
        switch  class(cVal)
            case 'double'
                textvalue = sprintf('%.1e', cVal);
                textvalue = textvalue(1:min(6,numel(textvalue)));
                ctext = strcat(cItemAbb,'-',textvalue);
            case 'char'
                cVal = cVal(1:min(2,numel(cVal)));
                ctext = strcat(cItemAbb,'-',cVal);
        end
        file_name = strcat(file_name, ctext, '_');
    end
    file_name = regexprep(file_name,'\.','_');
    audiowrite(strcat(file_name,'.wav'), value, s.Fs, 'BitsPerSample', 16);
end
if s.ShowSpectrogram
    figure;
    w=hamming(1024);
    spectrogram(value(:,2) ,w,length(w)/2,2048,s.Fs,'yaxis');
    colorbar;
    if s.freq_ini ~= s.freq_end
        ylim([s.freq_ini, s.freq_end]/1000);
    end
end

end
