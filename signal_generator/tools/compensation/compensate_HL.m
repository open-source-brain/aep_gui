function stimulus_compensated = compensate_HL(...
    stimulus_cal, ...
    PTA, ...
    audiogram_freqs, ...
    fs, ...
    FLow, ...
    FHigh, ...
    show_figures ...
)
% compensate_HL compensates the input stimulus for hearing loss
% using the 'sufficient audibility' approach (Humes,2007.)
%
% Inputs:
% - stimulus_cal: calibrated stimulus signal
% - PTA: Pure-tone hearing thresholds (dB SPL)
% - audiogram_freqs: corresponding pure-tone frequencies of the thresholds
% - fs: sampling frequency of soundcard
% - refSPL: new reference SPL after calibration
%
% Output:
% - stimulus_compensated: stimulus after being compensated for hearing loss

%% Constants
% Minimum audible field values for NH according to ISO 389-7
frequencies_MAF_NH = [20, 25, 31.5, 40, 50, 63, 80, 100, ...
    125, 160, 200, 250, 315, 400, 500, 630, 750, 800, ...
    1000, 1250, 1500, 1600, 2000, 2500, 3000, 3150, ...
    4000, 5000, 6000, 6300, 8000, 9000, 10000, ...
    11200, 12500, 14000, 16000, 18000];
MAF_NH             = [78.1, 68.7, 59.5, 51.1, 44.0, 37.5, 31.5, 26.5, ...
    22.1, 17.9, 14.4, 11.4, 8.6, 6.2, 4.4, 3.0, 2.4, ...
    2.2, 2.4, 3.5, 2.4, 1.7, -1.3, -4.2, -5.8, -6.0, ...
    -5.4, -1.5, 4.3, 6.0, 12.6, 13.9, 13.9, 13.0, 12.3, ...
    18.4, 40.2, 70.4];

% PTA frequencies
frequencies_mid    = [250, 315, 400, 500, 630, 800, 1000, 1250, 1600, ...
    2000, 2500, 3150, 4000];

% Interpolating PTA and MAF NH values for the mid frequencies
PTA_interpolated    = interp1(audiogram_freqs, PTA, frequencies_mid, ...
    'linear', 'extrap');
MAF_NH_interpolated = interp1(frequencies_MAF_NH, MAF_NH, ...
    frequencies_mid, 'linear', 'extrap');

%% Power Spectrum using Welch's method
[Pxx, F] = pwelch(stimulus_cal, [], [], [], fs);

% scale Pxx so that energy matches with rms^2
correction = rms(stimulus_cal) ^ 2 / sum(Pxx);
Pxx = correction * Pxx;

% 1/3-octave synthesis of power data
[m_power_spectrum, f_power_spectrum] = OneThirdSynthPow(Pxx, F);
m_power_spectrum_dB = 10*log10(m_power_spectrum / 400e-12);


% Interpolating the MAF_NH with the f_power_spectrum to get the same
% freq. vector
MAF_interp = interp1(frequencies_MAF_NH, MAF_NH, f_power_spectrum, ...
    "linear", "extrap");

% Identify cutoff frequencies based on audibility thresholds
diff_levels = m_power_spectrum_dB - MAF_interp;
idx_diff = find(diff_levels >= 0);
if isempty(idx_diff)
    stimulus_compensated = stimulus_cal;
    return;
end
fc_1 = f_power_spectrum(idx_diff(1));
fc_2 = f_power_spectrum(idx_diff(end));
idx_f_power_spectrum_mid = f_power_spectrum >= fc_1 & ...
    f_power_spectrum <= fc_2;
f_power_spectrum_mid = f_power_spectrum(idx_f_power_spectrum_mid);

%% Calculating the gains for compensation

% Calculating the target spectrum and interpolating to match f_power_spectrum
MAF_TS = MAF_NH_interpolated + PTA_interpolated;
MAF_TS_interp = interp1(frequencies_mid, MAF_TS, f_power_spectrum_mid, ...
    "linear","extrap");

% Interpolating the pxx to only have the mid frequencies.
pxx_interp_dB = interp1(f_power_spectrum, m_power_spectrum_dB, ...
    f_power_spectrum_mid,"linear","extrap");

% Calculating the gains for the compensation
difference = pxx_interp_dB - MAF_TS_interp;
gains_dB = zeros(length(difference), 1);

for i = 1:length(gains_dB)
    if difference(i) < 15
        if difference(i) > 0
            gains_dB(i) = 15 - difference(i);
        elseif difference(i) < 0
            gains_dB(i) = 15 + abs(difference(i));
        end
    end

    % Not to exceed maximum SPL
    if (pxx_interp_dB(i) + gains_dB(i)) > 104
        gains_dB(i) = 104 - pxx_interp_dB(i);
    end

    pxx_interp_dB(i) = pxx_interp_dB(i) + gains_dB(i);
end

%% Filter

N = 300;

idx_f_power_spectrum_filter = find(f_power_spectrum_mid >= FLow & ...
    f_power_spectrum_mid <= FHigh);
f_power_spectrum_filter = f_power_spectrum_mid(idx_f_power_spectrum_filter);
gains_filter = gains_dB(idx_f_power_spectrum_filter)';

% Relevant for plotting
MAF_TS_interp_filter = MAF_TS_interp(idx_f_power_spectrum_filter);
pxx_interp_dB_filter = pxx_interp_dB(idx_f_power_spectrum_filter);

% Rolloff frequencies
rolloff_freqs_1 = sort (fc_1 * 2.^ (-1:-1:-6));
rolloff_freqs_2 = sort (fc_2 * 2);

% Creating the filter
freq_filter = [0, rolloff_freqs_1, f_power_spectrum_filter, ...
    rolloff_freqs_2, fs/2];
freq_filter_norm = freq_filter ./ (fs/2);
magnitudes_dB = [0, 0.*[rolloff_freqs_1], gains_filter, ...
    0.* rolloff_freqs_2, 0];
magnitudes_linear = (10 .^ (magnitudes_dB / 10)) .^ (1/4);
d = fdesign.arbmag("N,F,A", ...
    N, ...
    freq_filter_norm, ...
    magnitudes_linear);
FIRFilter = design(d, 'freqsamp', 'SystemObject', true);
b = coeffs(FIRFilter);
stimulus_compensated = filtfilt(b.Numerator, 1, stimulus_cal);

% Display calibrated and compensated SPL levels
level_stimulus_cal = 20 * log10(rms(stimulus_cal)/20e-6);
disp(fprintf('Level stimulus_calibrated is %f dB.', level_stimulus_cal));
level_stimulus_compensated = 20 * log10(rms(stimulus_compensated)/20e-6);
disp(fprintf('Level stimulus_compensated is %f dB.', level_stimulus_compensated));

[Pxx_comp, F_comp] = pwelch(stimulus_compensated, [], [], [], fs);
[m_power_spectrum_comp, f_power_spectrum_comp] = OneThirdSynthPow(Pxx_comp, F_comp);
m_power_spectrum_comp_dB = 10*log10(correction * m_power_spectrum_comp/400e-12);



%% Plot
if show_figures
    [h,fh] = freqz(b.Numerator, 1, 2048, fs);
    figure; plot(fh, 20*log10(abs(h))) % plot filter

    figure;
    hold on;
    plot(frequencies_MAF_NH, MAF_NH, 'b-', 'LineWidth', 1, 'DisplayName', 'MAF NH'); % MAF NH
    plot(f_power_spectrum_filter, MAF_TS_interp_filter,'r-', 'LineWidth', 1, 'DisplayName', 'MAF TS');
    plot(f_power_spectrum, m_power_spectrum_dB, 'k-', 'LineWidth',1,'DisplayName', 'Unaided');
    plot(f_power_spectrum_filter, pxx_interp_dB_filter, 'mx-', 'LineWidth', 1, 'DisplayName', 'Target Stimulus Spectrum');
    plot(f_power_spectrum_filter, gains_filter ,'o-','LineWidth', 1 ,'DisplayName', 'Gain');
    plot(f_power_spectrum_comp, m_power_spectrum_comp_dB, 'x-', 'LineWidth', 1, 'DisplayName','Compensated Stimulus Spectrum')
    % plot(freq_filter, magnitudes_dB)

    xlabel('Frequency (Hz)');
    ylabel('Power (dB SPL)');
    legend('Location', 'best');
    grid on;
    axis([20 20000 -10 110]);
    set(gca, 'XScale', 'log');
end

end
