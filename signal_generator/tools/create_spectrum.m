function Amp = create_spectrum(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'NSamples', 0);
s = ef(s, 'Attenuation', 0);
s = ef(s, 'Fs', 44100);
s = ef(s, 'FLow', 0);
s = ef(s, 'FHigh', 22050);
s = ef(s, 'FLow', 0);
s = ef(s, 'Type', 'gaussian');
N = s.NSamples;
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
else %if odd
    NUniqFreq = ceil((N + 1)/2);
end
if s.FLow == 0 && s.FHigh == s.Fs/2
    Amp=ones(NUniqFreq,1);
    return;
end
freq=((0:NUniqFreq - 1) * s.Fs/N)';
Amp=zeros(NUniqFreq,1);
switch s.Type
    case 'gaussian' %% defining spectral magnitude
        fc = s.FLow + (s.FHigh - s.FLow) / 2.0;
        s2 = 20*log10(exp(1)) * (s.FLow - fc) ^ 2 / (2 * 3);
        for n = 0:NUniqFreq - 1
            f = freq(n+1);
            Amp(n+1) = exp(-(f - fc) ^ 2 / (2 * s2));
        end
    case 'rectangular'
        p = s.Attenuation / (20 * log10(0.5));
        %% defining spectral magnitude
        for n = 0:NUniqFreq - 1
            f=freq(n+1);
            if (f>=s.FLow) && (f<=s.FHigh)
                Amp(n+1) = 1/((f+1)^(p));
            else
                Amp(n+1)=0;
            end
        end;
end
end