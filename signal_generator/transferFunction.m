%% ref sin 1000hz 103dB
measuredRef = 103;
[refdb, fs] = audioread('Sinusoidal1000Hz(103SPL).wav',[15000,500480/2]);
time = (0 : numel(refdb) - 1)/fs;
freq = (0 : numel(refdb) - 1)*fs/numel(refdb);
figure;
subplot(2,1,1);
plot(time, refdb);
subplot(2,1,2);
yfftscaled = 2*abs(fft(refdb))/numel(refdb);
plot(freq, yfftscaled);
amp = max(yfftscaled);
refFactor = amp*10^(-measuredRef/20);
outrefhelper = helperHarmonicDistortionAmplifier(refdb');
[thddBc, harmpow, harmfreq] = thd(refdb,fs);
thdPercentage = 100*10^(r/20);
disp(strcat('THD[dBc]',num2str(thddBc),'/THD%:',num2str(thdPercentage),'%'));
helperPlotPeriodogram(outrefhelper,fs,'Power','Annotate')
%% sin 1000hz 1Amp 92dB
[input1, fs] = audioread('sin-tone-1000hz-amp1-92dBSPL.wav',[1,500480/2]);
time = (0 : numel(input1) - 1)/fs;
freq = (0 : numel(input1) - 1)*fs/numel(input1);
figure;
subplot(2,1,1);
plot(time, input1);
subplot(2,1,2);
yfftscaled1 = 2*abs(fft(input1))/numel(input1);
plot(freq, yfftscaled1);
amp = max(yfftscaled1);
refFactor2 = amp*10^(-92/20);
amppp = 2*amp;
LeqEqpp = 20*log10(amppp/2/refFactor);

%% clicks 19hz PW 100us 0.5Amp
[clicks19zh, fs] = audioread('Clicks19HzRecorded.wav',[10000,900480/2]);
time = (0 : numel(clicks19zh) - 1)/fs;
freq = (0 : numel(clicks19zh) - 1)*fs/numel(clicks19zh);
figure;
plot(time, clicks19zh);
nsamples =  round(1/18.75*fs);
yaveclicks = sweepAverage('recordedSweeps',clicks19zh,'sweepLength',nsamples, 'nSweeps', 200, 'Alternate', false);
figure;
plot(yaveclicks);
amppp = max(yaveclicks) - min(yaveclicks);
LeqEqpp = 20*log10(amppp/2/refFactor)
disp(strcat('Calibrated level:', num2str(LeqEqpp)));
%%
x = constantEnvelopeSweep('Amplitude', 0.8, 'SaveWaveFile', true, 'Periods', 1);
close all;
[y, fs] = audioread('Sweep-BandK5.wav');
freq = (0:numel(x) - 1)*fs/numel(x);
yave = sweepAverage('recordedSweeps',y, 'sweepLength',numel(x),'nSweeps', 11);
xfft = fft(x);
yfft = fft(yave);
tf = yfft./xfft;
figure;
semilogx(freq,20*log10(abs(tf)/max(abs(abs(tf)))), 'r');
hold on;
semilogx(freq,20*log10(abs(xfft)/max(abs(xfft))), 'b');
xlim([100, fs/2]);
ylim([-20, 20]);
xlabel('Frequency [kHz]');
grid on;