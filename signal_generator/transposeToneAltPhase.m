function [value, time, s] = transposeToneAltPhase(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'TransposeFrequency', 500); %frequency in Hz
s = ef(s, 'TransposePhase', 0); %frequency in Hz
s = ef(s, 'TransposeAltPhase', 0); %frequency in Hz
s = ef(s, 'TransposeNAltPhasePerModCycle', 1); % phase in rad
s = ef(s, 'TransposeLowPass', 2000); %frequency cutoffin Hz
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters; 
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

cCarrierFrequency = s.CarrierFrequency;
cModulationFrequency = s.ModulationFrequency;
cTransposeFrequency = s.TransposeFrequency;
cTransposeLowPass = s.TransposeLowPass;
[NAlt,DAlt] = rat(s.TransposeNAltPhasePerModCycle, 0.01);% this is to fit to infinit fractions
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    %fit carrier freq
    cCarrierFrequency  = round(s.Duration*cCarrierFrequency)/s.Duration;
    s.CarrierFrequency  = cCarrierFrequency;
    %fit modulation rate
    cModulationFrequency = round(s.Duration * cModulationFrequency * NAlt / (2 * DAlt)) / (s.Duration * NAlt / (2 * DAlt));
    s.ModulationFrequency  = cModulationFrequency;
    %fit TransposeFrequency
    cTransposeFrequency = round(s.Duration*cTransposeFrequency)/s.Duration;
    s.TransposeFrequency  = cTransposeFrequency;
    %fit lowpass filter
    cTransposeLowPass = round(s.Duration*cTransposeLowPass)/s.Duration;
    s.TransposeLowPass = cTransposeLowPass;
end
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;

cTempPhase = zeros(numel(time),1);
phaseAltPeriod = 2 / (cModulationFrequency * NAlt / DAlt);
phaseAltRate = 1/phaseAltPeriod;
s.AltPhaseRate = phaseAltRate;
ncycles = round(s.Duration*phaseAltRate);

%fit cphase to move together with envelope phase
% cAltPhase = 2 * s.ModulationPhase / s.CarrierNAltPhasePerModCycle - floor(2 * s.ModulationPhase / s.CarrierNAltPhasePerModCycle / (2 * pi)) * 2 * pi;
cAltPhase = 0;
progressbar('Generating signal');
for i = 1:ncycles
    progressbar(i/ncycles);
    cSinglePhase = (s.TransposePhase - s.TransposeAltPhase)*(unitaryStep(time - (1/phaseAltRate)*(cAltPhase)/(2*pi) - ...
        (i-1)/phaseAltRate) - unitaryStep(time - (1/phaseAltRate)*(cAltPhase) / (2*pi) - phaseAltPeriod/2 - (i-1)/phaseAltRate));
    cTempPhase = cTempPhase + cSinglePhase;
end
cTempPhase = cTempPhase + s.TransposeAltPhase;
%%transpose envelope
cAmplitudeTranspose = s.Amplitude*(sin(2*pi*cTransposeFrequency*time + ...
    cTempPhase));
%%rectification
cAmplitudeTranspose(cAmplitudeTranspose < 0) = 0;
%%remove frequency components above TransposeLowPass
cfft = fft(cAmplitudeTranspose);
Nfft = numel(cAmplitudeTranspose);
if ~rem(Nfft, 2) %if even
    NUniqFreq = ceil(1 + Nfft/2);
else %if odd
    NUniqFreq = ceil((Nfft+1)/2);
end
chFFT = cfft(1:NUniqFreq);
cfreq = (0:NUniqFreq - 1)'*s.Fs/Nfft;
bins2remove = find(cfreq >= s.TransposeLowPass);
chFFT(bins2remove) = 0.0;

%%reconstruct full fft
if ~rem(Nfft, 2) %if even
    fullFFT = [chFFT; conj(chFFT(end - 1 : -1 : 2))];
else %if odd
    fullFFT = [chFFT; conj(chFFT(end : -1 : 2))];
end
cFilteredAmplitudeTranspose = ifft(fullFFT);

%%amplitude modulation
cAmplitudeModulation = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));

cCarrier = sin(2*pi*cCarrierFrequency*(time));
cSignal = cFilteredAmplitudeTranspose.*cAmplitudeModulation.*cCarrier;
%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
cSignal = s.Amplitude*cSignal/(1 + s.ModulationIndex);
nsamples = numel(cSignal);
%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow.*cSignal;
end

value = zeros(nsamples*s.NRepetitions,1);
progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples) = factor*cSignal;
end
