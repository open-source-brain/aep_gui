recording = audioread('/home/jundurraga/Documents/Measurements/sound_buffer_test/tdt_running/clicksSin80Hz.wav');
aux = open('/home/jundurraga/Documents/Measurements/sound_buffer_test/tdt_running/clicksSin80Hz.mat');
buffer = aux.y2;
nsamples = size(buffer,1);
nepochs = 400;
nsignals = size(buffer,2);
completeBuffer = zeros(nsamples*nepochs,nsignals);

for i=1:nepochs
    completeBuffer((i-1)*nsamples + 1: i*nsamples, :) = buffer;
end
%% analysis channel
fs = 44100;
y1 = completeBuffer(:,1);
y1 = y1/max(abs(y1));
y2 = recording(:,1);
y2 = y2/max(abs(y2))/2;

t1 = (0:numel(y1) - 1)*1/fs;
t2 = (0:numel(y2) - 1)*1/fs;
%% compute delay
[r,lags] = xcorr(y1,y2);
figure; plot(lags,r);
[~, pmax] = max(r);
samplesdalay = lags(pmax);
y2 = circshift(y2,samplesdalay);

%% plot figures
figure;
plot(y1,'b')
hold on
plot(y2, 'r')
hold off
f1 = 0:fs/(numel(y1)-1):fs;
f2 = 0:fs/(numel(y2)-1):fs;
fy1 = (abs(fft(y1)));
fy2 = (abs(fft(y2)));
figure;
plot(f1,fy1,'-b')
hold on
plot(f2,fy2, 'r')
hold off